<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $key
 * @var $el_class
 * @var $align
 * @var $label
 * Shortcode class
 * @var $this WPBakeryShortCode_VC_Gitem_Post_Meta
 */
$key = $el_class = $align = $label = '';
$label_html = '';


if (strpos($key, "url_") !== FALSE):?>

        <?php
        $atts = vc_map_get_attributes( $this->getShortcode(), $atts );
        extract( $atts );

        $css_class = 'vc_gitem-post-meta-field-' . $key
                . ( strlen( $el_class ) ? ' ' . $el_class : '' )
                . ( strlen( $align ) ? ' vc_gitem-align-' . $align : '' );
        if ( strlen( $label ) ) {
                $label_html = '<span class="vc_gitem-post-meta-label">' . esc_html( $label ) . '</span>';
        }
        if ( strlen( $key ) ): ?>

                <div class="field_<?php echo $key?> <?php echo esc_attr( $css_class ) ?>">
                    <a href="http://{{post_meta_value:<?php echo esc_attr( $key ) ?> }}" ><?php print $label?></a>                             
                </div>

        <?php endif ?>

<?php else:?>

<?php 

$key = $el_class = $align = $label = '';
$label_html = '';

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = 'vc_gitem-post-meta-field-' . $key
	. ( strlen( $el_class ) ? ' ' . $el_class : '' )
	. ( strlen( $align ) ? ' vc_gitem-align-' . $align : '' );
if ( strlen( $label ) ) {
	$label_html = '<span class="vc_gitem-post-meta-label">' . esc_html( $label ) . '</span>';
}
?>
<?php if ( strlen( $key ) ): ?>
	<div class="<?php echo esc_attr( $css_class ) ?>"><?php echo $label_html ?> {{
		post_meta_value:<?php echo esc_attr( $key ) ?> }}
	</div>

<?php endif;?>
<?php endif;?>


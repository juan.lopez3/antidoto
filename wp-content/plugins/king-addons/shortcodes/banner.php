<?php
/**
 * Shortcode Title: Banner
 * Shortcode: banner
 * Usage: [banner animation="bounceInUp" style="1" image="image.png" title="Your title" subtitle="Your subtitle" button_text="Click me"  url="http://yourdomain.com" target="_blank"]
 */
add_shortcode('banner', 'king_banner_func');

function king_banner_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'animation'     => '',
		'style'         => 'style1',
		'image'         => '',
		'heading'   => '',
		//'small_heading' => '',
		'button_text'   => '',
		'btn_link'      => '',
		),
	$atts));

	if ( function_exists( 'vc_parse_multi_attribute' ) ) {
	    $parse_args = vc_parse_multi_attribute( $btn_link );
	    $href       = ( isset( $parse_args['url'] ) ) ? $parse_args['url'] : '#';
	    $title      = ( isset( $parse_args['title'] ) ) ? $parse_args['title'] : 'button';
	    $target     = ( isset( $parse_args['target'] ) ) ? trim( $parse_args['target'] ) : '_self';
  	}

  	$image_style = '';
  	if ( is_numeric( $image ) && !empty($image) ) {
	    $image_src  = wp_get_attachment_image_src( $image, 'full' );
	    if(isset($image_src[0])) {
	      	$image_style = ' style="background-image:url('.esc_url($image_src[0]).')"';
	    }
  	}

	$output	=	'';
	switch ($style) {
		case 'style1':
			$output	.='<div class="shop-banner full_height"'.$image_style.'>';
			$output	.=	'<div class="banner-content-wrapper">';
			$output	.=	'<div class="banner-content">';
			$output	.=	'<div class="overlay style1">';
			$output	.=	( $heading ) ? '<h2>'.esc_html($heading).'</h2>':'';
			$output	.=	( $content ) ? '<p>'.wp_kses_post($content).'</p>':'';
			$output	.=	'<p><a href="'.esc_url($href).'" target="'.esc_attr($target).'" class="button shaped bordered white">'.esc_html($button_text).'</a></p>';
			$output	.=	'</div>';
			$output	.=	'</div>';
			$output	.=	'</div>';
			$output	.=	'</div>';
			break;
		case 'style2':
			$output	.=	'<div class="shop-banner half_height"'.$image_style.'>';
			$output	.=	'<div class="banner-content-wrapper">';
			$output	.=	'<div class="banner-content">';
			$output	.=	'<div class="overlay style2">';
			$output	.=	( $heading ) ? '<h3>'.esc_html($heading).'</h3>':'';
			$output	.=	( $content) ? '<p>'.wp_kses_post($content).'</p>':'';
			$output	.=	'</div>';
			$output	.=	'</div>';
			$output	.=	'</div>';
			$output	.=	'</div>';
			break;

		case 'style3':
		default:
			$output	.=	'<div class="shop-banner half_height"'.$image_style.'>';
			$output	.=	'<div class="banner-content-wrapper">';
			$output	.=	'<div class="banner-content">';
			$output	.=	'<div class="overlay style3">';
			$output	.=	( $heading ) ? '<h2>'.esc_html($heading).'</h2>':'';
			$output	.=	($content) ? '<h5>'.esc_html($content).'</h5>':'';
			$output	.=	'<p><a href="'.esc_url($btn_link).'" target="'.esc_attr($target).'" class="button shaped bordered white">'.esc_html($button_text).'</a></p>';
			$output	.=	'</div>';
			$output	.=	'</div>';
			$output	.=	'</div>';
			$output	.=	'</div>';
			break;
	}

	return $output;

}
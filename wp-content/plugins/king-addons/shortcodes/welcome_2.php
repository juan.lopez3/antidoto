<?php
/**
 *
 * Welcome Style 2
 * @since 1.0.0
 * @version 1.0.0
 *
 */
add_shortcode('welcome_2', 'king_welcome_2');
function king_welcome_2( $atts, $content = null ) {

  extract( shortcode_atts( array(
    'animation' => '',
    'behind_image' => '',
    'animation_delay' => '',
    'front_image'  => '',
  ), $atts ) );

  $behind_img_url = ( is_numeric($behind_image) && !empty($behind_image) ) ? king_get_image_by_id($behind_image):'';
  $front_img_url  = ( is_numeric($front_image) && !empty($front_image) ) ? king_get_image_by_id($front_image):''; 
  $data_delay  = ( $animation_delay ) ? ' data-animation-delay="'.esc_attr($animation_delay).'"':'';
  ob_start();
  ?>

  <div class="welcome style2 <?php echo king_get_animation_class($animation); ?>">
    <div class="left-sec">
        <figure class="behind-img">
            <?php echo king_get_resized_image($behind_img_url, 399, 524, '', '', true); ?>
        </figure>
        <figure class="front-img">
            <?php echo king_get_resized_image($front_img_url, 283, 370, '', '', true); ?>
        </figure>
    </div>
    <div class="right-sec">
        <?php echo wp_kses_post($content); ?>
    </div>
  </div>

  <?php
  $html = ob_get_contents();
  ob_end_clean();
  return $html;
  return '<div '.king_get_animation_class($animation,true).' data-animation="'.$animation.'"'.$data_delay.'>'.do_shortcode($content).'</div>';
}


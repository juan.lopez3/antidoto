<?php
/**
 * Shortcode Title: Team members
 * Shortcode: team_members
 * Usage: [team_members animation="bounceInUp" member1="" member2="" memeber3="" member4=""]
 */
add_shortcode('team_members_3', 'king_team_members_3_func');

function king_team_members_3_func($atts, $content = null)
{

    extract(shortcode_atts(array(
		'animation' => '',
		'member1' => '',
		'member2' => '',
		'member3' => '',
		'member4' => '',
		'member5' => '',
	),
	$atts)); 
	
	ob_start();
	
	?>
	
	<div class="team-members style3 <?php echo king_get_animation_class($animation); ?>">
							
		<div class="row">
			<div class="col-md-6 pull-right">
				<div class="intro">
	                <?php echo rs_set_wpautop($content); ?>
	            </div>
        	</div>
			<?php king_team_member_item_style_3($member1, 'col-md-3 col-sm-6'); ?>
			<?php king_team_member_item_style_3($member2, 'col-md-3 col-sm-6'); ?>
		</div>

		<div class="row">
			<?php king_team_member_item_style_3($member3, 'col-md-3 col-sm-6'); ?>
			<?php king_team_member_item_style_3($member4, 'col-md-3 col-sm-6'); ?>
			<?php king_team_member_item_style_3($member5, 'col-md-3 col-sm-6'); ?>
	       <div class="col-md-3 col-sm-6">
	            <div class="hiring-block">
	                <a href="#" class="inner-frame">
	                    <div class="contents">
	                        <h5><?php _e('Join', 'king-addons'); ?></h5>
	                        <h6><?php _e('Our Team', 'king-addons');?></h6>
	                        <i class="fa fa-envelope"></i>
	                    </div>
	                </a>                                                
	            </div>
	        </div>
		</div>
		
	</div>

	<?php
	wp_reset_postdata();
	$html = ob_get_contents();
    ob_end_clean();
    return $html;
}


function king_team_member_item_style_3($member,$class) {
	
	global $post;
	
	if (empty($member)) {
		return;
	}
	
	$post = get_post($member);
	if (!$post || is_wp_error($post)) {
		return;
	}
	setup_postdata($post);
	
	?>
	<!-- Team Member -->
	<div class="<?php echo esc_attr($class); ?>">
        <div class="team-member style3">
            <figure>
                <?php king_the_resized_post_thumb(396, 383, get_the_title(), '', true); ?>
            </figure>
            <div class="details">
                <div class="details-inner">
                    <h2><?php the_title(); ?></h2>
                    <h3><?php echo get_post_meta($post->ID, 'team_position', true); ?></h3>
                    <ul class="social-media">
						<?php
							$xs_social_icons = array(
								'facebook',
								'twitter',
								'skype',
								'google',
								'vimeo',
								'linkedin',
								'instagram'
							);
							foreach ($xs_social_icons as $social_icon) {
								$social_url = get_post_meta($post->ID, 'xs_' . $social_icon, true);
								if ($social_url == '') {
									continue;
								} ?>
								<li><a href="<?php echo esc_url($social_url); ?>" target="_blank"><i class="icon-<?php echo esc_attr($social_icon); ?>"></i></a></li>
								<?php
							}
						?>
					</ul>
                </div>
            </div>
        </div>


	</div>
	<!-- /Team Member -->
	<?php
}
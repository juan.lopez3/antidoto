<?php
/**
 * Shortcode Title: List
 * Shortcode: list
 * Usage: [list animation="bounceInUp"][list_icon icon="home"]Your content...[/list_icon][/list]
 */

add_shortcode('list', 'king_list_func');

function king_list_func( $atts, $content = null ) {
	
	 extract(shortcode_atts(array(
	    'animation' => ''
    ), $atts));

	$pattern = '/<ul[^.]*<\/ul>/';
	if (preg_match($pattern, $content)) {
		return do_shortcode($content);
	}
	
	global $shortcode_list_item;
	
    $shortcode_list_item = array();
    do_shortcode($content);

	$items = '';
	foreach ($shortcode_list_item as $item) {
		$items .= '<li class="'.sanitize_html_class($item['list_style']).'">'.$item['content'].'</li>';


	}
    $shortcode_list_item = array();
	return '<ul class="list '.king_get_animation_class($animation).'">'.$items.'</ul>';
}

/**
 * Shortcode Title: List item
 * Shortcode: list_item
 * Usage: 
 */
add_shortcode('list_item', 'king_list_item_func');
function king_list_item_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
	    'list_style' => 'list-arrow'
    ), $atts));
    global $shortcode_list_item;
    $shortcode_list_item[] = array(
		'list_style' => $list_style, 
		'content' => trim(do_shortcode($content)));
}
<?php
/**
 * Shortcode Title: Pricing column
 * shortcode:  pricing_columns
 * Usage: [pricing_columns animation="fade" ]
 */

add_shortcode('pricing_column', 'king_pricing_column_func');
function king_pricing_column_func($atts, $content = null)
{
    extract(shortcode_atts(array(
        'animation' => '',
        'featured' => '',
        'title' => '',
        'currency' => '',
        'price' => '',
        'period' => '',
        'values' => '',
		'button_text' => '',
        'url' => '',
        'target' => ''
    ), $atts));

	$values_arr = array();
	
	if (!empty($values)) {
		$values_arr = preg_split("/\\r\\n|\\r|\\n/", $values);
	}
	
	$price = floatval($price);
	$whole = floor($price); 
	$fraction = number_format($price - $whole,2);
	$fraction = substr(strstr($fraction,'.'),1);
	
	ob_start(); ?>
	
	<table class="pricing-table style-king <?php echo king_get_animation_class($animation); ?>">
		<tr>
			<td class="pricing-table-item <?php echo ($featured == 'yes' ? 'featured' : '') ?>">
				<div class="table-header">
					<h3><?php echo esc_html($title); ?></h3>
					<?php if ($featured == 'yes'): ?>
						<div class="ribbon">
							<i class="icon-star"></i>
							<span><?php _e('Recommend', 'king-addons');?></span>
						</div>
					<?php endif; ?>
				</div>
				<div class="table-price">
					<span class="currency"><?php echo esc_html($currency); ?></span>
					<span class="price-main"><?php echo esc_html($whole); ?></span>
					<span class="price-secondary"><?php echo esc_html($fraction); ?><br><span class="period"><?php echo esc_html($period); ?></span></span>
				</div>
				<div class="table-content">
					<?php if (is_array($values_arr) && count($values_arr) > 0):
						foreach ($values_arr as $val): ?>
							<?php echo '<span class="value">'.wp_kses_data($val).'</span>'; ?>
						<?php endforeach;
					endif; ?>
				</div>
				<div class="table-footer">
					<a class="button light" href="<?php echo esc_url($url); ?>" target="<?php echo esc_attr($target); ?>"><?php echo esc_html($button_text); ?></a>
				</div>
			</td>
		</tr>
	</table>

	<?php
	$html = ob_get_contents();
    ob_end_clean();
    return $html;
}
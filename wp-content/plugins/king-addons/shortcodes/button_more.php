<?php
/**
 * Shortcode Title: Button
 * Shortcode: button
 * Usage: [button animation="bounceInUp" url="http://yourdomain.com" target="_blank"]Your content here...[/button]
 */
add_shortcode('button_more', 'king_button_more_func');

function king_button_more_func($atts, $content = null)
{
    extract(shortcode_atts(array(
		'animation'        => '',
		'url'              => '',
		'fill_style'       => 'filled',
		'shape'			   => 'btn-default',
		'background_color' => '',
		'border_color'     => '',
		'text_color'       => '',
		'target'           => ''
	),
	$atts));

	$background_color = ( $background_color ) ? 'background-color:'.esc_attr($background_color).';':'';
	$border_color     = ( $border_color ) ? 'border-color:'.esc_attr($border_color).';':'';
	$text_color       = ( $text_color ) ? 'color:'.esc_attr($text_color).';':'';

	$el_button_style	=	( $background_color || $border_color || $text_color ) ? ' style="'.$background_color.$border_color.$text_color.'"':'';

    return '<a href="'.esc_url($url).'" target="'.esc_attr($target).'" class="button '.sanitize_html_class($fill_style).' '.sanitize_html_class($shape).' shaped '.king_get_animation_class($animation).'"'.$el_button_style.'>'.$content.'</a>';
}
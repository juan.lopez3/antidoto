<?php
/**
 * Shortcode Title: Multi Posts
 * Shortcode: multi_posts
 * Usage: [multi_posts animation="" limit="2"]
 */
add_shortcode('multi_posts', 'king_multi_posts_func');

function king_multi_posts_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'limit' => 2,
		'animation' => ''
		),
	$atts));

	if (!intval($limit)) {
		$limit = 2;
	}
	ob_start();
	?>
	<div class="tabs <?php echo king_get_animation_class($animation); ?>">

		<div class="tab-header">
			<ul>
				<li><a href="#tab1"><?php _e('Popular', 'king'); ?></a></li>
				<li><a href="#tab2"><?php _e('Recent', 'king'); ?></a></li>
				<li><a href="#tab3"><?php _e('Comments', 'king'); ?></a></li>
			</ul>
		</div>

		<div class="tab-content">
			<div id="tab1" class="tab">
				<?php
				$r = new WP_Query( apply_filters( 'widget_posts_args', array('orderby' => 'comment_count DESC', 'posts_per_page' => $limit, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true) ) );
				if ($r->have_posts()) : ?>
					<ul class="posts-list">
						<?php  while ($r->have_posts()) : $r->the_post(); ?>
							<li class="post-item">
								<div class="featured-image">
									<?php king_the_resized_post_thumbnail('multipost-widget',get_the_title()); ?>
								</div>
								<div class="post-content">
									<a class="post-title" href="<?php echo esc_url(get_the_permalink());?>" title="<?php echo esc_attr(get_the_title());?>"><?php echo king_get_shortened_string(the_title(),4); ?></a>
									<ul class="post-meta">
										<li><?php the_time('M d, Y'); ?></li>
									</ul>
								</div>
							</li>
						<?php endwhile; ?>
					</ul>
					<?php
					wp_reset_postdata();
				endif; //have_posts()
				?>
			</div>

			<div id="tab2" class="tab">
				<?php
				$r = new WP_Query( apply_filters( 'widget_posts_args', array( 'posts_per_page' => $limit, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true  ) ) );
				if ($r->have_posts()) : ?>
					<ul class="posts-list">
						<?php  while ($r->have_posts()) : $r->the_post(); ?>
							<li class="post-item">
								<div class="featured-image">
									<?php king_the_resized_post_thumbnail('multipost-widget',get_the_title()); ?>
								</div>
								<div class="post-content">
									<a class="post-title" href="<?php echo esc_url(get_the_permalink());?>" title="<?php echo esc_attr(get_the_title());?>"><?php echo king_get_shortened_string(the_title(),4); ?></a>
									<ul class="post-meta">
										<li><?php the_time('M d, Y'); ?></li>
									</ul>
								</div>
							</li>
						<?php endwhile; ?>
					</ul>
					<?php
					wp_reset_postdata();
				endif; //have_posts()
				?>
			</div>

			<div id="tab3" class="tab">
				<?php
				$comments = get_comments( apply_filters( 'widget_comments_args', array( 'number' => $limit, 'status' => 'approve', 'post_status' => 'publish' ) ) );
				$i = 0;
				if ( $comments ) { 
					?>
					<ul class="posts-list">
						<?php foreach ( (array) $comments as $comment) { ?>
							<li class="post-item">
								<div class="featured-image">
									<?php echo king_get_resized_post_thumbnail($comment -> comment_post_ID,'multipost-widget', get_the_title($comment -> comment_post_ID)); ?>
								</div>
								<div class="post-content">
									<a class="post-title" href='<?php echo esc_url( get_comment_link($comment->comment_ID) ); ?>'><?php echo get_the_title($comment -> comment_post_ID); ?> </a>
									<p><?php echo king_get_shortened_string($comment->comment_content,5); ?></p>
								</div>
							</li>
						<?php } ?>
					</ul>
				<?php } ?>
			</div>
		</div>
	</div>
	<!-- /Tabs -->
	<?php
	$html =  ob_get_contents();

    ob_end_clean();
    return $html;
}
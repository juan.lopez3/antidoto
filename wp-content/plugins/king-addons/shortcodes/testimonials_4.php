<?php
/**
 * Shortcode Title: Testimonials Single
 * Shortcode: testimonials 2
 * Usage: [testimonials_2 animation="bounceInUp" category="3" limit="3" header="What people say" background=""]
 */
add_shortcode('testimonial_4', 'king_testimonials_4_func');

function king_testimonials_4_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
		'category'         => '',
		'characters_limit' => '',
		'limit'            => '',
		),
	$atts));

	wp_enqueue_script( 'king-owl-carousel');

	global $wp_query, $post;
	$args = array(
		'posts_per_page'  => $limit,
		'offset'          => 0,
		'orderby'         => 'date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'testimonial',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'				=> 1,
		'post_status'     => 'publish'
	);
	
	if (!empty($category)):
		$args['tax_query'] = array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'testimonial-category',
				'terms' => $category,
				'operator' => 'IN'
			)
		);
	endif;
	
	$tmp_query  = $wp_query;
  	$wp_query   = new WP_Query( $args );
	
	ob_start();

	?>

    <div class="testimonials-style2 <?php echo king_get_animation_class($animation); ?>">
        <div class="slider">
        	<?php 
        		while ( have_posts() ) : the_post(); 
        		$position = get_post_meta($post->ID, 'position', true);
				$company = get_post_meta($post->ID, 'company', true);
				$author = get_the_title($post -> ID);

				if (!empty($company) && !empty($position)) {
					$company = ', '.$company;
				}

				$post_content = stripslashes($post -> post_content);
			?>

            <div class="testimonial">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <p><?php 
							if (!empty($characters_limit)) {
								echo king_get_shortened_string_by_letters($post_content,intval($characters_limit));
							} else {
								echo wp_kses_post($post_content);
							}
							?></p>
                            <figure><?php king_the_resized_post_thumb(130, 130, get_the_title(), '', true); ?></figure>
                            <h5><?php echo esc_html($author); ?></h5>
                            <h6><?php echo esc_html($position.$company); ?></h6>
                        </div>
                    </div>
                </div>
            </div>

        	<?php endwhile; ?>
            
        </div>
        <div class="nav">
            <span class="next-slide">
                <span></span>
                <span class="next-next"></span>
                <span class="total"></span>
            </span>
            <span class="prev-slide">
                <span></span>
                <span class="prev-prev"></span>
                <span class="total"></span>
            </span>
        </div>
    </div>

    <?php
  	wp_reset_postdata();
  	$wp_query = $tmp_query;
  	$output = ob_get_clean();
  	return $output;
}
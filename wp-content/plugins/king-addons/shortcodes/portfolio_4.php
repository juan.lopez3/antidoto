<?php
/**
 * Shortcode Title: Portfolio 4
 * Shortcode: portfolio_4
 * Usage: [portfolio_4 animation="bounceInUp" category="6" limit="4" ]
 */
add_shortcode('portfolio_4', 'king_portfolio_4_func');

function king_portfolio_4_func($atts, $content = null) {
	global $post;

	extract(shortcode_atts(array(
		'animation' => '',
		'category'  => '',
		'limit'     => 4,
	), $atts));

	$html = '';
	$args = array(
		'posts_per_page' => $limit,
		'meta_query'     => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
		'offset'         => 0,
		'orderby'        => 'date',
		'order'          => 'ASC',
		'include'        => '',
		'exclude'        => '',
		'meta_key'       => '',
		'meta_value'     => '',
		'post_type'      => 'portfolio',
		'post_mime_type' => '',
		'post_parent'    => '',
		'paged'          => 1,
		'post_status'    => 'publish'
	);

	$col_size = array(
    	'col-md-6 full-height',
    	'col-lg-3 col-md-6 half-height',
    	'col-lg-3 col-md-6 half-height',
    	'col-md-6 half-height',
  	);

  	$image_size = array(
    	'ts-portfolio-4-full-height',
    	'ts-portfolio-4-half-height',
    	'ts-portfolio-4-half-height',
    	'ts-portfolio-4-half-height-wide',
  	);

	if (!empty($category)) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'portfolio-categories',
				'field' => 'id',
				'terms' => explode(',', $category)
			),
		);
	}

	$the_query = new WP_Query($args);

	if ($the_query->have_posts()) : ob_start();
	
		?>

		<section class="latest-works <?php echo king_get_animation_class($animation); ?>">
			<?php $i = 0; while ($the_query->have_posts()): $the_query->the_post(); $i = ($i < 4 ) ? $i : 0; ?>
				<?php $img_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $image_size[$i] ); ?>
				<article class="portfolio-item <?php echo sanitize_html_classes($col_size[$i]); ?>">
					<?php
						$item_cats = wp_get_post_terms(get_the_ID(), 'portfolio-categories');
						$cat1 = array();
						foreach ($item_cats as $item_cat) {
							$cat1[] = $item_cat->name;
						}
						
						$cat1_html = '';
						if (is_array($cat1)) {
							$cat1_html = implode(' ', $cat1);
						}
					?>
					<figure style="background-image:url(<?php echo esc_url($img_url[0]); ?>);">
                    	<?php the_post_thumbnail($image_size[$i]); ?>
                	</figure>
                	<div class="contents">
                    	<h3 class="title"><a href="<?php echo esc_url(get_the_permalink()); ?>"><?php the_title(); ?></a></h3>
                    	<h6 class="category"><?php echo esc_html($cat1_html); ?></h6>
               	 	</div><!-- /contents -->
				</article>
			<?php $i++; endwhile; wp_reset_postdata(); ?>
		</section>


	<?php endif;

	$html = ob_get_contents();
	ob_end_clean();

	return $html;
}

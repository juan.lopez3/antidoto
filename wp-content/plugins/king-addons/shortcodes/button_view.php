<?php
/**
 * Shortcode Title: Button
 * Shortcode: button
 * Usage: [button animation="bounceInUp" type="white" url="http://yourdomain.com" target="_blank"]Your content here...[/button]
 */
add_shortcode('button_view', 'king_button_view_func');

function king_button_view_func($atts, $content = null)
{
    extract(shortcode_atts(array(
		'animation' => '',
		'type' => '',
		'url' => '',
		'target' => ''
	),
	$atts));

    return '<a href="'.esc_url($url).'" target="'.esc_attr($target).'" class="button view-more unfilled '.(empty($type) ? 'black' : sanitize_html_classes($type) ).' '.king_get_animation_class($animation).'">'.$content.'</a>';
}
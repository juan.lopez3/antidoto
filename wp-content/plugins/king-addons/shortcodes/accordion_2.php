<?php
/**
 * Shortcode Title: Accordion 2
 * Shortcode: accordion_2
 * Usage: [accordion_2 animation="bounceInUp" open="yes"][accordion_2_toggle title="title 1"]Your content[/accordion_2_toggle][/accordion_2]
 */
add_shortcode('accordion_2', 'king_accordion_2_func');

function king_accordion_2_func($atts, $content = null) {

	extract(shortcode_atts(array(
		'open' => 'no',
		'colors' => '',
		'animation' => ''
	), $atts));

	global $accordion_array;
	$accordion_array = array();

	do_shortcode($content);
	$i = 0;

	$accordion_content = '';

	foreach ($accordion_array as $tab => $item) {

		$open_class = '';
		if ($open == "yes" && $i == 0)
		{
			$open_class = 'accordion-active';
		}

		$accordion_content .= '
			<div class="accordion '.$open_class.'">
				<div class="accordion-header">
					<div class="accordion-icon"></div>
					<div class="accordion-title-icon">
						<i class="'.$item['icon'].'"></i>
					</div>
					<h5>'.$item['title'].'</h5>
				</div>
				<div class="accordion-content">
					' . $item['content'] . '
				</div>
			</div>';
		$i++;
	}
	$html = '<div class="accordions style-king '.sanitize_html_classes($colors).' '.king_get_animation_class($animation).'">'.$accordion_content.'</div>';

	$accordion_array = array();
	return $html;
}
/**
 * Shortcode Title: Accordion Toggle
 * Shortcode: accordion_2_toggle
 * Usage: [accordion_2_toggle title="title 1"]Your content goes here...[/accordion_2_toggle]
 */
add_shortcode('accordion_2_toggle', 'king_accordion_2_toggle_func');

function king_accordion_2_toggle_func($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'icon' => '',
	), $atts));
	global $accordion_array;
	$accordion_array[] = array('title' => $title, 'icon' => $icon, 'content' => trim(do_shortcode($content)));
	return '';
}
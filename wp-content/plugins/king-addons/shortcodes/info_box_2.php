<?php
/**
 * Shortcode Title: Info Box 2
 * shortcode:  info_box_2
 * Usage: [info_box_2 animation="fade" icon="icon-search" content="Your content"]
 */

add_shortcode('info_box_2', 'king_info_box_2_func');
function king_info_box_2_func($atts, $content = null)
{
    extract(shortcode_atts(array(
        'icon_animation'          => '',
        'content_animation'       => '',
        'icon_animation_delay'    => '',
        'content_animation_delay' => '',
        'icon'                    => '',
		'icon_color' => ''
    ), $atts));
	
	$icon_animation_delay     = ( $icon_animation_delay ) ? ' data-wow-delay="'.esc_attr($icon_animation_delay).'s"':'';
	$content_animation_delay     = ( $content_animation_delay ) ? ' data-wow-delay="'.esc_attr($content_animation_delay).'s"':'';

    $output = '<div class="contact-info-box">';
    $output .= '<div class="icon'.king_get_animation_class($icon_animation).'"'.$icon_animation_delay.'>';
    $output .= '<i class="'.sanitize_html_classes($icon).'" '.(!empty($icon_color) ? 'style="color: '.esc_attr($icon_color).'"' : '').'></i>';
    $output	.=	'</div>';
    $output .= '<div class="info-content'.king_get_animation_class($content_animation).'"'.$content_animation_delay.'>';
    $output .= king_add_paragraph($content);
    $output .= '</div>';
    $output .= '</div>';

    return $output;
}
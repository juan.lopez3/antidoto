<?php
/*
 * Shortcode Name: Contact Form 3
 * Shortcode: contact_form_3
 * Usage: [contact_form_3 animation="fade" name_label="Name" email_label="your@email.com" message_label="Message" send_label="Send"  ]
 */

add_shortcode('contact_form_3', 'king_contact_form_3_func');

function king_contact_form_3_func($atts, $content = null)
{
    extract(
        shortcode_atts(array(
            'animation' => '',
            'name_label' => '',
            'email_label' => '',
            'message_label' => '',
            'send_label' => '',
        ), $atts)
    );
	
	ob_start();
	?>
	<form class="contact-form <?php echo king_get_animation_class($animation); ?>" data-sending="<?php echo esc_attr(__('Sending Message...','king-addons'));?>">
		<?php echo wp_nonce_field( 'contact_form_submission', 'contact_nonce', true,false); ?>
		<input type="hidden" name="contact-form-value" value="1" id=""/>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<input class="underlined" type="text" name="name" placeholder="<?php echo esc_attr($name_label); ?>*">
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
				<input class="underlined" type="text" name="email" placeholder="<?php echo esc_attr($email_label); ?>*">
			</div>
		</div>
		<textarea class="underlined" rows="4" name="msg" placeholder="<?php echo esc_attr($message_label); ?>"></textarea>
		<div class="align-center">
			<input class="underlined" type="submit" value="<?php echo esc_attr($send_label); ?>">
		</div>
		<div id="msg"></div>
	</form>
	<?php
	$html = ob_get_contents();
    ob_end_clean();
    return $html;
}






<?php
// Shortcode Title: Heading
// Shortcode: heading
// Usage: [heading type="1"]text...[/header]
function king_heading_func($atts, $content = null )
{
	extract(shortcode_atts(array(
	    'animation' => '',
         'color'=>'',
		'type' => 1,
		'align' => ''
    ), $atts));

	if (intval($type) < 1 || intval($type) > 6)
	{
		$type = 1;
	}

	switch ($align) {
		case 'center':
		case 'right':
			$align_class = 'class="align-'.sanitize_html_classes($align).' '.king_get_animation_class($animation).'"';
			break;
		default:
			$align_class = king_get_animation_class($animation,true);
	}

	$style = '';
    if(!empty($color)){
        $style = 'style="color:'.esc_attr($color).'"';
    }

	return '<h'.$type.' '.$align_class.'  '.$style.'  data-animation="'.esc_attr($animation).'">'.do_shortcode($content).'</h'.$type.'>';
}
add_shortcode('heading', 'king_heading_func');

// Shortcode Title: H1
// Shortcode: H1
// Usage: [H1]text...[/H1]
function king_h1_func($atts, $content = null )
{
	$content = do_shortcode($content);
	return '<h1>'.$content.'</h1>';
}
add_shortcode('H1', 'king_h1_func');
add_shortcode('h1', 'king_h1_func');

// Shortcode Title: H2
// Shortcode: H2
// Usage: [H2]text...[/H2]
function king_h2_func($atts, $content = null )
{
	$content = do_shortcode($content);
	return '<h2>'.$content.'</h2>';
}
add_shortcode('H2', 'king_h2_func');
add_shortcode('h2', 'king_h2_func');

// Shortcode Title: H3
// Shortcode: H3
// Usage: [H3]text...[/H3]
function king_h3_func($atts, $content = null )
{
	$content = do_shortcode($content);
	return '<h3>'.$content.'</h3>';
}
add_shortcode('H3', 'king_h3_func');
add_shortcode('h3', 'king_h3_func');

// Shortcode Title: H4
// Shortcode: H4
// Usage: [H4]text...[/H4]
function king_h4_func($atts, $content = null )
{
	$content = do_shortcode($content);
	return '<h4>'.$content.'</h4>';
}
add_shortcode('H4', 'king_h4_func');
add_shortcode('h4', 'king_h4_func');

// Shortcode Title: H5
// Shortcode: H5
// Usage: [H5]text...[/H5]
function king_h5_func($atts, $content = null )
{
	$content = do_shortcode($content);
	return '<h5>'.$content.'</h5>';
}
add_shortcode('H5', 'king_h5_func');
add_shortcode('h5', 'king_h5_func');
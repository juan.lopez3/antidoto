<?php
/**
 *
 * TS Latest News
 * @since 1.0.0
 * @version 1.1.0
 *
 */
function king_latest_news_2( $atts, $content = '', $id = '' ) {

  global $wp_query, $paged, $post;

  extract( shortcode_atts( array(
    'columns'       => 3,
    'animation'     => '',
    'cats'          => 0,
    'orderby'       => 'date',
    'exclude_posts' => '',
    'limit'         => '4'
  ), $atts ) );

  if( is_front_page() || is_home() ) {
    $paged = ( get_query_var('paged') ) ? intval( get_query_var('paged') ) : intval( get_query_var('page') );
  } else {
    $paged = intval( get_query_var('paged') );
  }

  // Query args
  $args = array(
    'paged'           => $paged,
    'orderby'         => $orderby,
    'posts_per_page'  => $limit,
    'meta_query'      => array(array('key' => '_thumbnail_id')), 
  );

  if( $cats ) {
    $args['tax_query'] = array(
      array(
        'taxonomy' => 'category',
        'field'    => 'ids',
        'terms'    => explode( ',', $cats )
      )
    );
  }

  if (!empty($exclude_posts)) {
  
  $exclude_posts_arr = explode(',',$exclude_posts);
    if (is_array($exclude_posts_arr) && count($exclude_posts_arr) > 0) {
      $args['post__not_in'] = array_map('intval',$exclude_posts_arr);
    }
  }
  
  $blog_align_class = array('image-right', 'image-right', 'image-left', 'image-left');

  $tmp_query  = $wp_query;
  $wp_query   = new WP_Query( $args );

  ob_start();
  
  ?>
  <div class="latest-posts style2">
  <?php
  $blog_list_count = 0; 
  while ( have_posts() ) : the_post(); ?>

    <article class="post <?php echo sanitize_html_class($blog_align_class[$blog_list_count]); ?> <?php echo king_get_animation_class($animation); ?>">
      <div class="contents">
          <div class="publish-date clearfix">
              <p><span data-hover="<?php esc_attr(the_time('d')); ?>"><?php the_time('d'); ?></span></p>
              <p>
                  <span data-hover="/<?php esc_attr(the_time('M')); ?>"><?php the_time('M'); ?></span>
                  <span data-hover="<?php esc_attr(the_time('Y')); ?>"><?php the_time('Y'); ?></span>
              </p>
          </div>
          <h3><a href="<?php echo esc_url(get_the_permalink()); ?>" data-hover="<?php esc_html(the_title()); ?>"><?php the_title(); ?></a></h3>
          <ul class="meta">
            <?php 
              $categories = get_the_category();
              if ( ! empty( $categories ) ) {
                foreach( $categories as $category ) {
                  echo '<li><a href="' . esc_url( get_category_link( $category->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'king-addon' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a></li>'.'';
                }
              }
            ?>
            <li><a href="#"><?php echo get_comments_number(); ?> <?php _e('Comments', 'king-addons');?></a></li>
          </ul>
          <ul class="social-media">
              <li>
                <a target="_blank" class="twitter-share-button"
                  href="https://twitter.com/share?url=<?php echo urlencode(get_the_permalink()); ?>&text=<?php echo urlencode(get_the_title()); ?>"
                  onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
                             return false;">
                   <i class="icon-twitter"></i>
                </a>
              </li>
              <li>
                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>"
                onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
                           return false;">
                 <i class="icon-facebook"></i>
                </a>
              </li>
          </ul>
      </div>
      <figure>
          <?php  king_the_resized_post_thumb(395, 382, get_the_title(), 'img-responsive', true); ?>
      </figure>
  </article>

  <?php $blog_list_count++; endwhile; ?>

  </div>

  <?php
  wp_reset_postdata();
  $wp_query = $tmp_query;

  $output = ob_get_clean();
  return $output;
}
add_shortcode( 'latest_news_2', 'king_latest_news_2' );

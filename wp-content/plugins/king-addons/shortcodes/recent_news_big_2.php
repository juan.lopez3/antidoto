<?php
/**
 * Shortcode Title: Recent News Big 2
 * Shortcode: recent_news_big_2
 * Usage: [recent_news_big_2 animation="fade" length="100" count=10 ],
 */

add_shortcode('recent_news_big_2', 'king_recent_news_big_2_func');

function king_recent_news_big_2_func($atts, $content = null)
{
    extract(shortcode_atts(array(
        'animation' => '',
        'header' => '',
        'limit' => '',
		'category' => '',
		'url' => ''
    ), $atts));

    $html = '';
    if (empty($limit)) {
        $limit = 1;
    }

    $latest = new Wp_Query(
        array(
            'post_type' => 'post',
            'posts_per_page' => $limit,
			'cat' => $category
        )
    );

    ob_start();

    if ($latest->have_posts()): ?>

		<div class="col-lg-10 col-md-10 col-sm-12 col-lg-push-1 col-md-push-1 <?php echo king_get_animation_class($animation); ?>">
			<div class="blog-post">
				<div class="blog-post-king">
					<h3><?php echo esc_html($header); ?></h3>
					<span class="separator"></span>

					<?php while ($latest->have_posts()): $latest->the_post(); ?>

						<div class="blog-post-meta">
							<span><?php echo get_the_date(get_option('date_format')); ?>, <?php _e('in', 'king'); ?> <?php echo king_post_categories(get_the_ID()); ?>, <?php _e('by', 'king'); ?> <?php the_author(); ?>, <?php comments_number( __('No comments', 'king'), __('1 comment', 'king'), __('% comments', 'king')) ;?></span>
						</div>
						<div class="blog-post-content">

							<h4 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr(get_the_title()); ?>"><?php the_title(); ?></a></h4>

							<!-- Post Image -->
							<div class="post-thumbnail">
								<?php king_the_resized_post_thumbnail('recent-news-big-2', get_the_title(), 'img-responsive'); ?>
							</div>
							<!-- /Post Image -->

							<ul class="social-media">
								<li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>"
										onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
										 <i class="icon-facebook-squared"></i>
									 </a>
								</li>
								<li>
									 <a target="_blank" class="twitter-share-button"
										href="https://twitter.com/share?url=<?php echo urlencode(get_the_permalink()); ?>&text=<?php echo urlencode(get_the_title()); ?>"
										onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
										 <i class="icon-twitter"></i>
									 </a>
								</li>
								<li>
									 <a href="https://plus.google.com/share?url=<?php echo urlencode(get_the_permalink()); ?>"
										onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
										 <i class="icon-google"></i>
									 </a>
								</li>
								<?php 
								$pin_image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'pinterest' );
								if ($pin_image): ?>
									 <li>
										 <a target="_blank" href="http://pinterest.com/pin/create%2Fbutton/?url=<?php echo urlencode(get_the_permalink()); ?>&media=<?php echo urlencode($pin_image[0]); ?>&description=<?php echo urlencode(get_the_title()); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
											 <i class="icon-pinterest"></i>
										 </a>
									 </li>
								<?php endif; ?>
								<li>
									 <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_the_permalink()); ?>&title=<?php echo urlencode(get_the_title()); ?>&summary=<?php echo urlencode(king_get_the_excerpt_theme(10)); ?>&source=<?php echo get_bloginfo( 'name' );?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=531,width=545');return false;">
										 <i class="icon-linkedin"></i>
									 </a>
								</li>
							</ul>

							<!-- Post Content -->
							<div class="post-content">
								<p><?php echo king_get_the_excerpt_theme(20); ?></p>
								<a href="<?php echo esc_url(get_the_permalink()); ?>" class="read-more"><?php _e('Continue Reading ...', 'king-addons');?></a>
							</div>
							<!-- /Post Content -->

						<?php  endwhile; ?>
					</div>
				</div>
			</div>

			<?php if (!empty($url)): ?>
				<div class="align-center">
					<a href="<?php echo esc_url($url);?>" class="button shaped"><?php _e('View Blog', 'king');?></a>
				</div>
			<?php endif; ?>
		</div>	
	<?php endif;

    wp_reset_postdata();
    $html = ob_get_contents();
    ob_end_clean();
	return $html;
}
<?php
/**
 * Shortcode Title: Blog
 * Shortcode: blog
 * Usage: [blog animation="fade" length="100" count=10 ],
 */
add_shortcode('blog', 'king_blog_func');

function king_blog_func($atts, $content = null) {
	extract(shortcode_atts(array(
		'animation' => '',
		'title_part1' => '',
		'title_part2' => '',
		'category' => '',
		'url' => ''
	), $atts));

	$latest = new Wp_Query(
		array(
			'post_type' => 'post',
			'posts_per_page' => 5,
			'cat' => $category
		)
	);

	ob_start();

	if ($latest->have_posts()): ?>

		<section id="king-blog" <?php echo king_get_animation_class($animation,true); ?>>
			<header class="text-uppercase" >
				<span><?php echo esc_html($title_part1); ?></span> <span class="site-text-color"><?php echo esc_html($title_part2); ?></span>
			</header>
			<div class="row" id="blog-container-box">
				
				<div class="col-md-6 left-sec">
					<div class="king-blog-effects-container blog-box-1">
						<?php if ($latest->have_posts()): 
							$latest->the_post(); ?>
							<?php king_blog_post_content('king-blog-hover-effects', 580, 650, true); ?>
						<?php endif; ?>
					</div>
				</div><!-- /col-md-6 -->
				<div class="col-md-6 right-sec">
					<div class="row">
						<div class="col-md-12">
							<div class="king-blog-effects-container blog-box-hor">
								<?php if ($latest->have_posts()): 
									$latest->the_post(); ?>
									<?php king_blog_post_content('king-blog-hover-effects', 580, 210); ?>
								<?php endif; ?>
							</div> <!-- /media-box div -->
						</div><!-- /col-md-12 -->
					</div><!-- /row -->
					<div class="box-container">	
						<div class="row">
							<div class="col-lg-6 col-md-12 blog-box-left">
								<div class="king-blog-effects-container blog-box-2">
									<?php if ($latest->have_posts()): 
										$latest->the_post(); ?>
										<?php king_blog_post_content('king-blog-hover-effects', 580, 860); //was 290x430 ?>
									<?php endif; ?>
								</div><!-- /blog-box-2 -->
							</div><!-- /.col-lg-12 -->
							<div class="col-lg-6 col-md-12 blog-box-right">
								<div class="king-blog-effects-container blog-box-3">
									<?php if ($latest->have_posts()): 
										$latest->the_post(); ?>
										<?php king_blog_post_content('king-blog-hover-effects', 580, 860); //was 290x430 ?>
									<?php endif; ?>
								</div><!-- /blog-box-3 -->
							</div><!-- /col-lg-12 -->
						</div><!-- /row -->
					</div><!-- /box-container -->
				</div><!-- /col-md-6 -->
			</div><!-- /#blog-container-box-->
			
			
			<?php if (!empty($url)): ?>
				<div class="text-center">
					<a href="<?php echo esc_url($url); ?>" class="btn btn-default blog-btn text-uppercase blog-btn">
						<?php _e('View Blog', 'king'); ?>
					</a>
				</div><!-- /blog view button div -->				
			<?php endif; ?>
				
		</section>
	<?php
	endif;

	wp_reset_postdata();
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}

function king_blog_post_content($class,$width,$height, $share_icons = false) {
	
	global $post;
	
	switch (get_post_format()):
		case 'video': ?>
			<div class="<?php echo sanitize_html_classes($class);?>">
				<?php
				$url = get_post_meta($post->ID, 'video_url', true);
				
				if (has_post_thumbnail()): ?>
					<!-- Post Image -->
					<div class="post-thumbnail">
						<div class="video-overlay">
							<div class="overlay-inner">
								<h4 class="post-title"><a href="<?php echo esc_attr(get_the_permalink()); ?>"><?php the_title(); ?></a></h4>
								<div class="blog-post-meta">
									<span><?php the_date(get_option('date_format'));?>, <?php _e('in', 'king'); ?> <?php echo get_the_category_list(' '); ?>, <?php _e('by', 'king');?> <span class="post-author"><?php the_author();?></span></span>
								</div>
								<a href="<?php echo esc_url($url); ?>" rel="prettyPhoto"><i class="fa fa-play"></i></a>
							</div>
						</div>
						<?php  king_the_resized_post_thumb($width, $height, get_the_title(), 'img-responsive', true); ?>
					</div>
					<!-- /Post Image -->
				<?php endif; ?>
				
			</div>
			<?php break;
		
		case 'quote': ?>
			<div class="<?php echo sanitize_html_classes($class);?>">
				<blockquote class="post-blockquote">
					<div class="quote-contents">
						<span class="quote-icon"></span>
						<p><?php echo get_post_meta(get_the_ID(),'quote_text',true);?></p>
						<p>- <?php _e('By','king'); ?> <?php echo get_post_meta(get_the_ID(),'author_text',true);?></p>
					</div>
				</blockquote>
			</div>
			<?php break;
			
		case 'link': ?>
			<div class="<?php echo sanitize_html_classes($class);?>">
				<div class="link-container">
					<?php $url = get_post_meta(get_the_ID(),'link',true);
					if (!empty($url)): ?>
						<blockquote class="post-blockquote style-link">
							<div class="link-contents">
								<span class="icon-container">
									<i class="fa fa-link"></i>
								</span>
								<p><?php echo esc_html(get_post_meta(get_the_ID(),'link_description',true)); ?></p>
								<a class="link" href="<?php echo esc_url($url); ?>" target="_blank"><?php echo esc_url($url); ?></a>
							</div>
						</blockquote>
						<!-- Post Image -->
						<div class="post-thumbnail">
							<?php  king_the_resized_post_thumb($width, $height, get_the_title(), 'img-responsive', true); ?>
						</div>
						<!-- /Post Image -->
					<?php endif; ?>
				</div>
			</div>
			
			<?php break;
		
		default: ?>
			<div class="<?php echo sanitize_html_classes($class);?>">
				<!-- Post Image -->
				<div class="post-thumbnail">
					<?php  king_the_resized_post_thumb($width, $height, get_the_title(), 'img-responsive', true); ?>
				</div>
				<!-- /Post Image -->
				<div class="overlay">
					<div class="overlay-inner">
						<h4 class="post-title"><a href="<?php echo esc_attr(get_the_permalink()); ?>"><?php the_title(); ?></a></h4>
						<div class="blog-post-meta">
							<span><?php the_date(get_option('date_format'));?>, <?php _e('in', 'king'); ?> <?php echo get_the_category_list(' '); ?>, <?php _e('by', 'king');?> <span class="post-author"><?php the_author();?></span></span>
						</div>
					
						<?php if ($share_icons):
							king_blog_post_share_icons();
						endif; ?>
					</div>
				</div>
				
			</div>
			<?php break;
	endswitch;
}

function king_blog_post_share_icons() { ?>
	
	<footer class="margin-top-35">
		<div class="socials-container">
			<ul class="social-media">
				<li>
					<a target="_blank" class="twitter-share-button"
					href="https://twitter.com/share?url=<?php echo urlencode(get_the_permalink()); ?>&text=<?php echo urlencode(get_the_title()); ?>"
					onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
										 return false;">
					 <i class="fa fa-twitter"></i>
					</a>
				</li>
				<li>
					<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>"
					onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
										 return false;">
					 <i class="fa fa-facebook"></i>
					</a>
				</li>
				<li>
					<a href="https://plus.google.com/share?url=<?php echo urlencode(get_the_permalink()); ?>"
					onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
										 return false;">
					 <i class="fa fa-google-plus"></i>
					</a>
				</li>
			</ul>
		</div>
		<div class="comments-counter">
			<a href="<?php echo esc_url(get_comments_link()); ?>"> <?php echo get_comments_number(); ?> <span class="fa fa-lg fa-comment-o"></span></a>
		</div>
	</footer>
			
<?php }

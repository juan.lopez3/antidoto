<?php
/*
 * Shortcode Title: Logos
 * Shortcode: logos
 *'Usage:[logos animation=""][logos_item]{content}[/logos_item][/logos]'
 */

add_shortcode('logos', 'king_logos_func');

function king_logos_func($atts, $content = null) {
	
	extract(shortcode_atts(array(
	    'animation' => ''
    ), $atts));
	
    global $shortcode_logos;
    $shortcode_logos = array(); // clear the array
    do_shortcode($content); // execute the '[tab]' shortcode first to get the title and content

	$items = '';
	foreach ($shortcode_logos as $logo) {

		$items .= '<li '.king_get_animation_class($animation, true).'>';
		if (!empty($logo['url'])) {
			$items .= '<a href="'.esc_url($logo['url']).'" target="'.esc_attr($logo['target']).'">';
		}
		
		$items .= '<img src="'.esc_url($logo['image']).'" alt="'.esc_attr($logo['title']).'" />';
		
		if (!empty($logo['url'])) {
			$items .= '</a>';
		}	
		$items .= '</li>';
	}
    $shortcode_logos = array();

	$html = '<ul class="shop-logos">'.$items.'</ul>';
	return $html;
}

add_shortcode('logos_item', 'king_logos_item_func');

function king_logos_item_func($atts, $content = null) {

    extract(shortcode_atts(array(
	    'title' => '',
	    'image' => '',
	    'url' => '',
	    'target' => '_self',
    ), $atts));
    global $shortcode_logos;
    $shortcode_logos[] = array(
		'title' => $title,
		'image' => king_get_image_by_id($image),
		'url' => $url,
		'target' => $target,
	);
}










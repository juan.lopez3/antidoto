<?php
/**
 * Shortcode Title: Testimonials Single
 * Shortcode: testimonials 2
 * Usage: [testimonials_2 animation="bounceInUp" category="3" limit="3" header="What people say" background=""]
 */
add_shortcode('testimonial_single', 'testimonial_single');

function testimonial_single( $atts, $content = null ) {
    extract(shortcode_atts(array(
		'category' => '',
		'style'    => 'style1',
		'box_background' => '',
		'image_shadow_color'	=> '',
		'characters_limit'	=> '',
		),
	$atts));

	global $wp_query, $post;
	$args = array(
		'posts_per_page'  => 1,
		'offset'          => 0,
		'orderby'         => 'date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'testimonial',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'				=> 1,
		'post_status'     => 'publish'
	);
	
	if (!empty($category)):
		$args['tax_query'] = array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'testimonial-category',
				'terms' => $category,
				'operator' => 'IN'
			)
		);
	endif;
	
	$tmp_query  = $wp_query;
  	$wp_query   = new WP_Query( $args );
	
	ob_start();

	switch ($style) {
		case 'style1':
			while ( have_posts() ) : the_post(); 

				$position = get_post_meta($post->ID, 'position', true);
				$company = get_post_meta($post->ID, 'company', true);
				$author = get_the_title($post -> ID);

				if (!empty($company) && !empty($position)) {
					$company = ', '.$company;
				}

				$post_content = stripslashes($post -> post_content);
			?>
				<div class="testimonial-single">
                    <figure>
					<div class="image-shadow"<?php echo ( !empty($image_shadow_color)  ? ' style="background-color:'.esc_attr($image_shadow_color).';"':'' ); ?>></div>
                        <?php the_post_thumbnail(); ?>
                    </figure>
                    <div class="testimonial-text">
                        <p><?php 
						if (!empty($characters_limit)) {
							echo king_get_shortened_string_by_letters($post_content,intval($characters_limit));
						} else {
							echo wp_kses_post($post_content);
						}
						?></p>
                    </div>
                    <p class="author"><?php echo esc_html($author); ?></p>
                    <p class="author-position"><?php echo esc_html($position.$company); ?></p>
				</div>
			<?php endwhile; 
			break;

		case 'style2':
		default: 
			while ( have_posts() ) : the_post();
			$position = get_post_meta($post->ID, 'position', true);
			$company = get_post_meta($post->ID, 'company', true);
			$author = get_the_title($post -> ID);

			if (!empty($company) && !empty($position)) {
				$company = ', '.$company;
			}

			$post_content = stripslashes($post -> post_content);
		?>
			<div class="testimonial-single small"<?php echo ( !empty($box_background ) ? ' style="background-color:'.esc_attr($box_background).';"':'' ); ?>>
                <figure>
				<div class="image-shadow"<?php echo ( !empty($image_shadow_color)  ? ' style="background-color:'.esc_attr($image_shadow_color).';"':'' ); ?>></div>
                    <?php the_post_thumbnail(); ?>
                </figure>
                <div class="testimonial-text">
                    <p><?php 
					if (!empty($characters_limit)) {
						echo king_get_shortened_string_by_letters($post_content,intval($characters_limit));
					} else {
						echo wp_kses_post($post_content);
					}
					?></p>
                </div>
                <p class="author"><?php echo esc_html($author); ?></p>
                <p class="author-position"><?php echo esc_html($position.$company); ?></p>
            </div>
		<?php endwhile; break;
	}
	
	wp_reset_query();
  	wp_reset_postdata();
  	$wp_query = $tmp_query;
  	$output = ob_get_clean();
  	return $output;
}
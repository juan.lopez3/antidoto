<?php
/*
 * Shortcode Name: Footer Map
 * Shortcode: contact_form_2
 * Usage: [contact_form_2 animation="fade" name_label="Name" name_icon="fa-user" email_label="your@email.com" email_icon="fa-envelope" message_label="Message" send_label="Send" clear_label="Clear" skin="1" button_align="left" ]
 */

add_shortcode('footer_map', 'king_footer_map_func');

function king_footer_map_func($atts, $content = null)
{
    extract(
        shortcode_atts(array(
            'animation' => '',
			'address' => '',
            'copyright_text' => '',
        ), $atts)
    );
	
	$marker_src = '';
	
	if (ot_get_option('add_google_maps_api') != 'disabled') {
		wp_enqueue_script('king-google-map-api');
	}
	wp_enqueue_script('king-cd-google-map');
	
	if (!empty($marker)) {
		$marker_src = king_get_image_by_id($marker);
	}
	
	
	ob_start();
	?>
	<section class="cd-google-map footer-map <?php echo king_get_animation_class($animation); ?>">
        <div class="google-container" data-address="<?php echo esc_attr($address); ?>"></div>
        <div class="map-overlay">
            <div class="overlay-inner">
                <div class="contents">
                    <?php echo rs_set_wpautop($content); ?>
                    <?php get_template_part('inc/social-icons'); ?>
                    <p><?php echo wp_kses_post($copyright_text); ?></p>
                </div>
            </div>
        </div>
        <div class="cd-zoom-in"></div>
        <div class="cd-zoom-out"></div>
    </section>
		
	<?php
	$html = ob_get_contents();
    ob_end_clean();
    return $html;
}






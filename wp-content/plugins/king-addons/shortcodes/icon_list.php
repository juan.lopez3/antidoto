<?php
/*
 * Shortcode Title: Icons list
 * Shortcode: icon_list
 *'Usage:[icon_list animation="bounceInUp"][icon_list_item icon="fa-search" icon_color="#FF0000" title="Your title" title_color="#D3D3D3" content_color="#D3D3D3" border_color="#D3D3D3"]Your content[/icon_list_item][/icon_list]'
 */
$global_icon_list_animation = null;

add_shortcode('icon_list_item', 'king_icon_list_item_func');

function king_icon_list_item_func($atts, $content = null)
{
    extract(shortcode_atts(array(
        'icon_animation'          => '',
        'icon_animation_delay'    => '',
        'content_animation'       => '',
        'content_animation_delay' => '',
        'title_animation'         => '',
        'title_animation_delay'   => '',
        'icon_style'              => 'filled',
        'icon_upload'             => '',
        'icon_color'              => '',
        'icon'                    => '',
        'title'                   => '',
        'title_color'             => '',
        'content_color'           => '',
        'border_color'            => '',
        'bg_color'                => '',
    ), $atts));

    global $rs_icon_list_style;
    $icon_color_style    = ( $icon_color ) ? ' style="color:'.esc_attr($icon_color).';"':'';
    $title_color_style   = ( $title_color ) ? ' style="color:'.esc_attr($title_color).';"':'';
    $content_color_style = ( $content_color ) ? ' style="color:'.esc_attr($content_color).';"':'';

    $customize    = ( $border_color || $bg_color ) ? true:false;
    $uniqid_class = '';

    if($customize) {
        $uniqid       = time().'-'.mt_rand();
        $custom_style = '';

        if($bg_color || $border_color ) {
            $custom_style .= '.icon-container-custom-'.$uniqid.':before {';
            $custom_style .= ( $bg_color ) ? 'background-color:'.$bg_color.' !important;':'';
            $custom_style .= ( $border_color ) ? 'border-color:'.$border_color.' !important;':'';
            $custom_style .= '}';
        }

        if($bg_color && $border_color ) {
            $custom_style .= '.icon-container-custom-'.$uniqid.':after {';
            $custom_style .= 'border-color:'.rs_hex2rgba($border_color, 0.5).' !important;';
            $custom_style .= '}';
        }

        if($bg_color ) {
            $custom_style .= '.icon-container-custom-'.$uniqid.':after {';
            $custom_style .= 'border-color:'.rs_hex2rgba($bg_color, 0.5).' !important;';
            $custom_style .= '}';
        }

        if($border_color ) {
            $custom_style .= '.icon-container-custom-'.$uniqid.':after {';
            $custom_style .= 'border-color:'.rs_hex2rgba($border_color, 0.5).' !important;';
            $custom_style .= '}';
        }

        king_add_inline_style( $custom_style );
        $uniqid_class = ' icon-container-custom-'. $uniqid;
    }

    $icon_animation_delay = ( $icon_animation_delay ) ? ' data-wow-delay="'.esc_attr($icon_animation_delay).'s"':'';
    
    $icon_html = '';
    if (!empty($icon_upload)) {
        $icon_upload = king_get_image_by_id($icon_upload);
        $icon_html  = '<div class="icon-container '.king_get_animation_class($icon_animation).$uniqid_class.'"'.$icon_animation_delay.'>';
        $icon_html  .= '<img class="icons" src="'.esc_url($icon_upload).'" />';
        $icon_html  .= '</div>';
    } else {
        $icon_html  = '<div class="icon-container '.king_get_animation_class($icon_animation).$uniqid_class.'"'.$icon_animation_delay.'>';
        $icon_html .= '<i '.$icon_color_style.' class="icons ' . sanitize_html_classes($icon) . '"></i>';
        $icon_html .= '</div>';
    }

    $content_animation_delay = ( $content_animation_delay ) ? ' data-wow-delay="'.esc_attr($content_animation_delay).'s"':'';
    $title_animation_delay = ( $title_animation_delay ) ? ' data-wow-delay="'.esc_attr($title_animation_delay).'s"':'';

    $output  = '<li>';
    $output .=  $icon_html;
    $output .=  '<div class="title '.king_get_animation_class($title_animation).'"'.$title_animation_delay.'>';
    $output .=  '<h3 '.$title_color_style.'>' . $title . '</h3>';
    $output .=  '</div>';
    $output .=  '<div class="content'.king_get_animation_class($content_animation).'"'.$content_animation_delay.'>';
    $output .=  '<p'.$content_color_style.'>'.$content.'</p>';
    $output .=  '</div>';
    $output .=  '</li>';
    
    return $output;
}

add_shortcode('icon_list', 'king_icon_list_func');



function king_icon_list_func($atts, $content = null)
{ 
    global $rs_icon_list_style;
    extract(shortcode_atts(array(
        'style'     => 'style1'
    ), $atts));

    global $rs_icon_list_style;
    $rs_icon_list_style = $style;

    return '<ul class="services-list '.sanitize_html_class($style).'">' . do_shortcode($content) . '</ul>';



}


<?php
/**
 * Shortcode Title: Counter 2
 * Shortcode: counter_2
 * Usage: [counter_2 animation="bounceInUp" color="#FF0000" title="Your title" quantity="1000"]
 */
add_shortcode('counter_2', 'king_counter_2_func');

function king_counter_2_func($atts, $content = null)
{
    extract(shortcode_atts(array(
            'icon_animation'           => '',
            'icon_animation_delay'     => '',
            'quantity_animation'       => '',
            'quantity_animation_delay' => '0',
            'title_animation'          => '',
            'title_animation_delay'    => '',
            'title_color'              => '',
            'quantity_color'           => '',
            'motion_blur'              => 'yes',
            'title'                    => '',
            'quantity'                 => '',
            'icon_type'                => 'font_icon',
            'icon'                     => '',
            'image'                    => '',
            'icon_color'               => ''
    ),
    $atts));
	$uniqid_class = '';
	$title_color_html = '';
	if (!empty($title_color)) {
		$title_color_html = 'style="color: '.esc_attr($title_color).'"';
	}

	$quantity_color_html = '';
	if (!empty($quantity_color)) {
		$quantity_color_html = 'style="color: '.esc_attr($quantity_color).'"';
	}

	$icon_color_style	=	( $icon_color ) ? ' style="color:'.esc_attr($icon_color).';"':'';

	$motion_class = '';
	if( $motion_blur == 'yes' && $quantity_color ) {
		$uniqid       = time().'-'.mt_rand();
		$custom_style = '';
		$motion_class = 'motion-blur-animation-active';
		$custom_style .= '.counter-box-'.$uniqid.'.motion-blur-animation-active .sc-counter.counter-animated {
			-webkit-animation: counterFadeIn-'.$uniqid.' 1s '.$quantity_animation_delay.'s alternate both ease;
		    -moz-animation: counterFadeIn-'.$uniqid.' 1s '.$quantity_animation_delay.'s alternate both ease;
		    -ms-animation: counterFadeIn-'.$uniqid.' 1s '.$quantity_animation_delay.'s alternate both ease;
		    animation: counterFadeIn-'.$uniqid.' 1s '.$quantity_animation_delay.'s alternate both ease;
		}';
		$custom_style .= '.counter-box-'.$uniqid.' .sc-counter{
			-webkit-animation-delay: '.$quantity_animation_delay.'s !important;
			-moz-animation-delay: '.$quantity_animation_delay.'s !important;
			-ms-animation-delay: '.$quantity_animation_delay.'s !important;
			animation-delay: '.$quantity_animation_delay.'s !important;

		}';
		

		$custom_style .= '@-webkit-keyframes counterFadeIn-'.$uniqid.' {
			0% { 
				opacity: 0;
				color: '.rs_hex2rgba($quantity_color, 0.2).';
				text-shadow: 0 5px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 10px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 17px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 20px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 30px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 -5px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 -10px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 -17px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 -20px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 -30px 0 '.rs_hex2rgba($quantity_color, 0.2).';
				-webkit-transform: translateY(-100%);
				-ms-transform:	 translateY(-100%);
				transform:		 translateY(-100%);
			}
			35% {
				opacity: 1;
			}
			100% {
				color: rgba(249,115,82,1);
				text-shadow: none;
				-webkit-transform: translateY(0);
				-ms-transform:	 translateY(0);
				transform:		 translateY(0);
			}
		}
		@keyframes counterFadeIn-'.$uniqid.' {
			0% { 
				opacity: 0;
				color: '.rs_hex2rgba($quantity_color, 0.2).';
				text-shadow: 0 5px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 10px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 17px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 20px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 30px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 -5px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 -10px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 -17px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 -20px 0 '.rs_hex2rgba($quantity_color, 0.2).',
				0 -30px 0 '.rs_hex2rgba($quantity_color, 0.2).';
				-webkit-transform: translateY(-100%);
				-ms-transform:	 translateY(-100%);
				transform:		 translateY(-100%);
			}
			35% {
				opacity: 1;
			}
			100% {
				color: '.rs_hex2rgba($quantity_color, 1).';
				text-shadow: none;
				-webkit-transform: translateY(0);
				-ms-transform:	 translateY(0);
				transform:		 translateY(0);
			}
		}';

		$uniqid_class = ' counter-box-'.$uniqid;
		king_add_inline_style( $custom_style );
	}
	$icon_animation_delay     = ( $icon_animation_delay ) ? ' data-wow-delay="'.esc_attr($icon_animation_delay).'s"':'';
	$quantity_animation_delay = ( $quantity_animation_delay ) ? ' data-wow-delay="'.esc_attr($quantity_animation_delay).'s"':'';
	$title_animation_delay    = ( $title_animation_delay ) ? ' data-wow-delay="'.esc_attr($title_animation_delay).'s"':'';
	$image_html               = '';
	if(is_numeric($image) && !empty($image)) {
		$image_src  = wp_get_attachment_image_src( $image, 'full' );
		if(isset($image_src[0])) {
			$image_html .= '<div class="icon-image'.king_get_animation_class($icon_animation).'"'.$icon_animation_delay.'>';
			$image_html .= '<img src="'.esc_url($image_src[0]).'" alt="icon" />';
			$image_html	.= '</div>';
		}
	}

	$output  = '<div class="counter-box '.$motion_class.$uniqid_class.'">';
	$output	.=	'<div class="icon '.king_get_animation_class($icon_animation).'"'.$icon_animation_delay.'>';
	if($icon_type == 'font_icon') {
		$output	.=	'<i class="icons ' . sanitize_html_classes($icon) . '"'.$icon_color_style.'></i>';
	} else {
		$output	.=	$image_html;
	}
	$output	.=	'</div>';
	$output	.=	'<div class="quantity-wrapper'.king_get_animation_class($quantity_animation).'"'.$quantity_animation_delay.'>';
	$output	.= '<span class="sc-counter" '.$quantity_color_html.''.$quantity_animation_delay.'>'.$quantity.'</span>';
	$output	.=	'</div>';
	$output	.=	'<div class="title'.king_get_animation_class($title_animation).'"'.$title_animation_delay.'>';
	$output	.=	'<h6 '.$title_color_html.'>'.$title.'</h6>';
	$output	.=	'</div>';	
	$output	.=	'</div>';	

    return $output;
}
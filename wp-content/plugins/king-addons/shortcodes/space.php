<?php
/**
 * Shortcode Title: Space
 * Shortcode: space
 * Usage: [space]
 */
add_shortcode('space', 'king_space_func');

function king_space_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		"height" => '20'
		),
	$atts));

	return '<div class="clearfix" style="height: '.(int)$height.'px"></div>';
}
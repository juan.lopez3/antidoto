<?php
/**
 * Shortcode Title: Team members
 * Shortcode: team_members
 * Usage: [team_members animation="bounceInUp" member1="" member2="" memeber3="" member4=""]
 */
add_shortcode('team_members', 'king_team_members_func');

function king_team_members_func($atts, $content = null)
{

    extract(shortcode_atts(array(
		'animation' => '',
		'member1' => '',
		'member2' => '',
		'member3' => '',
		'member4' => ''
	),
	$atts)); 
	
	ob_start();
	
	?>
	
	<div class="team-members <?php echo king_get_animation_class($animation); ?>">
							
		<div class="team-members-row">
			<?php king_team_member_item($member1, 'side-left'); ?>
			<?php king_team_member_item($member2, 'side-left'); ?>
		</div>

		<div class="team-members-row">
			<?php king_team_member_item($member3, 'side-right'); ?>
			<?php king_team_member_item($member4, 'side-right'); ?>
		</div>
		
	</div>

	<?php
	wp_reset_postdata();
	$html = ob_get_contents();
    ob_end_clean();
    return $html;
}


function king_team_member_item($member,$class) {
	
	global $post;
	
	if (empty($member)) {
		return;
	}
	
	$post = get_post($member);
	if (!$post || is_wp_error($post)) {
		return;
	}
	setup_postdata($post);
	
	?>
	<!-- Team Member -->
	<div class="team-member style-king <?php echo esc_attr($class); ?>">
		<div class="tm-image">
			<?php king_the_resized_post_thumbnail('team-member', get_the_title()); ?>
		</div>
		<div class="tm-info">
			<h5><?php the_title(); ?></h5>
			<span class="position"><?php echo get_post_meta($post->ID, 'team_position', true); ?></span>
			<span class="separator"></span>
			<ul class="social-media">
				<?php
					$xs_social_icons = array(
						'facebook',
						'twitter',
						'skype',
						'google',
						'vimeo',
						'linkedin',
						'instagram'
					);
					foreach ($xs_social_icons as $social_icon) {
						$social_url = get_post_meta($post->ID, 'xs_' . $social_icon, true);
						if ($social_url == '') {
							continue;
						} ?>
						<li><a href="<?php echo esc_url($social_url); ?>" target="_blank"><i class="icon-<?php echo esc_attr($social_icon); ?>"></i></a></li>
						<?php
					}
				?>
			</ul>
		</div>
	</div>
	<!-- /Team Member -->
	<?php
}
<?php
/**
 * Shortcode Title: Team members 2
 * Shortcode: team_members_2
 * Usage: [team_members_2 animation="bounceInUp" member1="" member2="" memeber3="" member4=""]
 */
add_shortcode('team_members_2', 'king_team_members_2_func');

function king_team_members_2_func($atts, $content = null)
{

    extract(shortcode_atts(array(
		'animation' => '',
		'member1' => '',
		'member2' => '',
		'member3' => '',
		'member4' => ''
	),
	$atts)); 
	
	ob_start();
	
	?>
	
	<div class="team-members <?php echo king_get_animation_class($animation); ?>">							
		<div class="team-members-row">
			<?php king_team_member_2_item($member1); ?>
			<?php king_team_member_2_item($member2); ?>
			<?php king_team_member_2_item($member3); ?>
			<?php king_team_member_2_item($member4); ?>
		</div>
	</div>

	<?php
	$html = ob_get_contents();
    ob_end_clean();
    return $html;
}


function king_team_member_2_item($member) {
	
	global $post;
	
	if (empty($member)) {
		return;
	}
	
	$post = get_post($member);
	if (!$post || is_wp_error($post)) {
		return;
	}
	setup_postdata($post);
	
	?>
	<!-- Team Member -->
	<div class="team-member style-king2">
		<div class="tm-image">
			<?php king_the_resized_post_thumbnail('team-member', get_the_title()); ?>
		</div>
		<div class="tm-info">
			<div>
				<div>
					<h5><?php the_title(); ?></h5>
					<span class="position"><?php echo get_post_meta($post->ID, 'team_position', true); ?></span>
					<span class="separator"></span>
					<ul class="social-media">
						<?php
							$xs_social_icons = array(
								'facebook',
								'twitter',
								'skype',
								'google',
								'vimeo',
								'linkedin',
								'instagram'
							);
							foreach ($xs_social_icons as $social_icon) {
								$social_url = get_post_meta($post->ID, 'xs_' . $social_icon, true);
								if ($social_url == '') {
									continue;
								} ?>
								<li><a href="<?php echo esc_url($social_url); ?>" target="_blank"><i class="icon-<?php echo sanitize_html_classes($social_icon); ?>"></i></a></li>
								<?php
							}
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- /Team Member -->
	<?php
	wp_reset_postdata();
}
<?php
/*
  * Shortcode Name : Column Offset
  * shortcode:  col_offset
  * Usage: [col_offset cols="5" offset="2"]
  */

add_shortcode('col_offset', 'king_col_offset_func');
function king_col_offset_func($atts, $content = null)
{
    extract(shortcode_atts(array(
        'cols' => '',
        'offset' => ''
    ), $atts));

    return '<div  class="col-lg-' . sanitize_html_classes($cols) . ' col-md-' . sanitize_html_classes($cols) . ' col-sm-' . sanitize_html_classes($cols) . ' col-lg-push-' . sanitize_html_classes($offset) . ' col-md-push-' . sanitize_html_classes($offset) . ' col-sm-push-' . sanitize_html_classes($offset) . '">' . do_shortcode($content) . '</div>';
}
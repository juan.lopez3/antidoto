<?php
/**
 * Shortcode Title: Image
 * Shortcode: image
 * Usage: [image animation="bounceInUp" size="half" align="alignleft" url="http://...." target="_blank"]image.png[/image]
 */
add_shortcode('image', 'king_image_func');

function king_image_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'animation'       => '',
		'animation_delay' => '',
		'size'            => '',
		'align'           => '',
		'url'             => '',
		'hover_effect'    => 'no',
		'tooltip_text'    => '',
		'target'          => ''
		),
	$atts));

	$animation_delay = ( $animation_delay ) ? ' data-wow-delay="'.esc_attr($animation_delay).'s"':'';
	
	$content = king_get_image_by_id(strip_tags($content));

	//wordpress is replacing "x" with special character in strings like 1920x1080
	//we have to bring back our "x"
	$content = str_replace('&#215;','x',$content);

	$class = ' img-responsive '.$align.' '.$size;
	$hover_effect_class = ( $hover_effect == 'yes' ) ? ' hover-effect-on':'';
	switch ($size) {
		case 'full':
			$image = king_get_resized_image_sidebar($content,array('full','one-sidebar','two-sidebars'), '', $class);
			break;

		case 'half':
			$image = king_get_resized_image_sidebar($content,array('half-full','half-one-sidebar','half-two-sidebars'), '', $class);
			break;

		case 'one_third':
			$image = king_get_resized_image_sidebar($content,array('third-full','third-one-sidebar','third-two-sidebars'), '', $class);
			break;

		case 'one_fourth':
			$image = king_get_resized_image_sidebar($content,array('fourth-full','fourth-one-sidebar','fourth-two-sidebars'), '', $class);
			break;

		default:
			$image = '<img src="'.esc_url($content).'" class="'.sanitize_html_classes($class).'" data-animation="'.$animation.'" alt="">';
			break;
	}
	
	$output = '';
	$output	.=	'<div class="vc_single_image-wrapper'.$hover_effect_class.' '.king_get_animation_class($animation).'"'.$animation_delay.'>';
	$output	.=  ( $tooltip_text ) ? '<h6 class="image-tooltip">'.esc_html($tooltip_text).'</h6>':'';
	$output	.=	( !empty($url)) ? '<a href="'.esc_url($href).'" target="'.esc_attr($target).'">':'';
	$output	.=	$image;
	$output	.=	( !empty($url)) ? '</a>':'';
	$output	.=	'</div>';

	return $output;
}
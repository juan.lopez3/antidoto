<?php
/**
 *
 * VC Row
 * @since 1.0.0
 * @version 1.0.0
 *
 */
function rs_row( $atts, $content = '', $key = '' ) {
  $defaults = array(
    'id'               => '',
    'class'            => '',
    'padding_top'      => '',
    'padding_bottom'   => '',
    'margin_top'       => '',
    'margin_bottom'    => '',
    'fluid'            => 'no',
    'attributes'       => '',
    'css'              => '',
    'st_ratio'         => '',
    'st_hr_offset'     => '',
    'st_ver_offset'    => '',
    'parallax'         => 'no'
  );

  extract( shortcode_atts( $defaults, $atts ) );

  $id                = ( $id ) ? ' id="'. esc_attr($id) .'"' : '';
  $class             = ( $class ) ? ' '. sanitize_html_classes($class) : '';
  $padding_top       = ( $padding_top ) ? ' '.sanitize_html_classes($padding_top) : '';
  $padding_bottom    = ( $padding_bottom ) ? ' '.sanitize_html_classes($padding_bottom) : '';
  $margin_top        = ( $margin_top ) ? ' '.sanitize_html_classes($margin_top) : '';
  $margin_bottom     = ( $margin_bottom ) ? ' '.sanitize_html_classes($margin_bottom) : '';
  $is_fluid          = ( $fluid == 'stretch_row_only' || $fluid == 'stretch_row_content') ? ' full-width':'';
  $attributes        = ( $attributes ) ? ' '.sanitize_html_classes(str_replace(',',' ',$attributes)) : '';
  $parallax_class    = ( $parallax == 'yes' ) ? ' parallax-bg':'';
  $st_ratio_data     = ( $st_ratio ) ? ' data-stellar-background-ratio="'.esc_attr($st_ratio).'"':'';
  $st_hr_offset_data = ( $st_hr_offset ) ? ' data-stellar-horizontal-offset="'.esc_attr($st_hr_offset).'"':'';
  $st_ver_offset_data = ( $st_ver_offset ) ? ' data-stellar-vertical-offset="'.esc_attr($st_ver_offset).'"':'';

  $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
  $css_class = ( $css_class ) ? ' '.sanitize_html_classes($css_class):'';

  $output  =  '<section '.$id.' class="section'.$is_fluid.$class.$parallax_class.$padding_top.$padding_bottom.$margin_top.$margin_bottom.$attributes.''.$css_class.'"'.$st_ratio_data.$st_ver_offset_data.$st_hr_offset_data.'>';
  $output .=  ( $fluid == 'stretch_row_only' ) ? '<div class="container">':'';
  $output .=  '<div class="row vc_row-fluid">';
  $output .=  do_shortcode(wp_kses_post($content));
  $output .=  ( $fluid == 'stretch_row_only' ) ? '</div>':'';
  $output .=  '</div>';
  $output .=  '</section>';

  return $output;
}
add_shortcode( 'vc_row', 'rs_row' );
add_shortcode( 'vc_row_inner', 'rs_row' );
add_shortcode( 'vc_section', 'rs_row' );

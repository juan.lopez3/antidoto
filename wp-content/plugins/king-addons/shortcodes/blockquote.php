<?php
/**
 * Shortcode Title: Blockquote
 * Shortcode: blockquote
 * Usage: [blockquote animation="bounceInUp" author="John"]Your content here...[/blockquote]
 */
add_shortcode('blockquote', 'king_blockquote_func');

function king_blockquote_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'style' => '',
		'author' => '',
		'animation' => ''
		),
	$atts));

	if ($style == 2) {
		return '<blockquote class="post-blockquote '.king_get_animation_class($animation).'" data-animation="'.$animation.'"><p>'.do_shortcode(nl2br($content)).'</p><p>-'.__('By','king').' '.$author.'</p></blockquote>';
	} else {
		return '<blockquote '.king_get_animation_class($animation,true).' data-animation="'.$animation.'"><p>'.do_shortcode(nl2br($content)).'</p><span class="author">'.$author.'</span></blockquote>';
	}
	
}
<?php
/**
 * Shortcode Title: Portfolio 3
 * Shortcode: portfolio_3
 * Usage: [portfolio_3 animation="bounceInUp" category_filter="" category="6" limit="6" ]
 */
add_shortcode('portfolio_3', 'king_portfolio_3_func');

function king_portfolio_3_func($atts, $content = null) {
	global $post, $wp_query;

	extract(shortcode_atts(array(
		'animation' => '',
		'category_filter' => 'yes',
		'category' => '',
		'filter_cats' => 0,
		'limit' => 12,
	), $atts));


	
	$output = '';
	$args = array(
		'posts_per_page' => $limit,
		'meta_query' => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
		'offset' => 0,
		'orderby' => 'date',
		'order' => 'DESC',
		'exclude' => '',
		'post_type' => 'portfolio',
		'post_mime_type' => '',
		'post_parent' => '',
		'paged' => 1,
		'post_status' => 'publish'
	);

	if (!empty($category)) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'portfolio-categories',
				'field' => 'id',
				'terms' => explode(',', $category)
			),
		);
	}

	$tmp_query  = $wp_query;
  	$wp_query   = new WP_Query( $args );

	//$the_query = new WP_Query($args);

	if (have_posts()) : ob_start();
	
		wp_enqueue_script('king-isotope');
		wp_enqueue_script('king-prettyphoto-js');
		wp_enqueue_style('king-prettyphoto-css');
	?>

		<!-- Projects Section -->
		<div class="section-inner-padding <?php echo king_get_animation_class($animation); ?>">
			
			<?php 
				if ($category_filter == 'yes') {
				    $filter_args = array(
				      'echo'     => 0,
				      'title_li' => '',
				      'style'    => 'none',
				      'taxonomy' => 'portfolio-categories',
				      'walker'   => new Walker_Portfolio_List_Categories(),
				    );

				    if( $filter_cats ) {

				      $exp_cats = explode(',',$filter_cats);
				      $new_cats = array();

				      foreach ( $exp_cats as $cat_value ) {
				        $has_children = get_term_children( intval($cat_value), 'portfolio-categories' );
				        if( ! empty( $has_children ) ) {
				            $new_cats[] = implode( ',', $has_children );
				        } else {
				            $new_cats[] = $cat_value;
				        }
				      }

				      $filter_args['include'] = implode( ',', $new_cats );

				    }

				    $filter_args = wp_parse_args( $args, $filter_args );
				}
			?>
			<div class="filters-bar style2">
				<ul class="blog-filters">
					<li class="filter is-checked" data-filter="*"><?php _e('All', 'king-addons');?></li>
					<?php echo wp_list_categories( $filter_args ); ?>	
				</ul>
			</div>
			
			<ul class="blogs-list">
				<?php
				$si = 1;
				while ( have_posts() ) : the_post();

					$item_cats = wp_get_post_terms(get_the_ID(), 'portfolio-categories');
					$cat = $cat1 = array();
					foreach ($item_cats as $item_cat) {
						$cat[] = 'category-' . $item_cat->slug;
						$cat1[] = $item_cat->name;
					}
					
					$cat_html = '';
					$cat1_html = '';
					if (is_array($cat)) {
						$cat_html = implode(' ', $cat);
					}

					if (is_array($cat1)) {
						$cat1_html = implode(' ', $cat1);
					}
					
					if (in_array($si,array(1,3,4,6,7,10))) {
						$excerpts = true;
						$image_size = 'portfolio-3-shortcode';
					} else {
						$excerpts = false;
						$image_size = 'portfolio-3-shortcode-v';
					}
					$si++;
					if ($si == 13) {
						$si = 1;
					}
					
					?>
					
					<li class="element-item <?php echo sanitize_html_classes($cat_html.' '.$image_size); ?>">
						<div class="blog-box style2">
							<figure>
								<?php king_the_resized_post_thumbnail($image_size, get_the_title()); ?>
								<figcaption>
									<div>
										<div>
											<span class="category"><?php echo esc_html($cat1_html); ?></span>
											<span class="separator"></span>
											<h6 class="post-title"><?php the_title(); ?></h6>
											<p><?php if ($excerpts == true):
												echo strip_tags(king_get_the_excerpt_theme(10));
											endif; ?></p>
											<a href="<?php echo esc_url(get_the_permalink()); ?>" class="king-eye-button"></a>
											<a href="<?php echo esc_url(king_img_url()); ?>" rel="prettyphoto" class="king-search-button"></a>
										</div>
									</div>
								</figcaption>
							</figure>
						</div> <!-- /blog-box -->
					</li>
				<?php endwhile; ?>
			</ul> <!-- /filter-list -->
		</div> 

	<?php endif;

	wp_reset_query();
  	wp_reset_postdata();
  	$wp_query = $tmp_query;

  	$output = ob_get_clean();
  	return $output;
}

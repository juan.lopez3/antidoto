<?php
/**
 * Shortcode Title: Testimonials 2
 * Shortcode: testimonials 2
 * Usage: [testimonials_2 animation="bounceInUp" category="3" limit="3" header="What people say" background=""]
 */
add_shortcode('testimonials_2', 'king_testimonials_2_func');

function king_testimonials_2_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
		'animation' => '',
		'category' => '',
		'limit' => '',
		'header' => '',
		'background' => ''
		),
	$atts));
	
	if (empty($limit)) {
		$limit = -1;
	}

	global $query_string, $post;
	$args = array(
		'posts_per_page'  => $limit,
		'offset'          => 0,
		'orderby'         => 'date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'testimonial',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'				=> 1,
		'post_status'     => 'publish'
	);
	
	if (!empty($category)):
		$args['tax_query'] = array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'testimonial-category',
				'terms' => $category,
				'operator' => 'IN'
			)
		);
	endif;
	
	$the_query = new WP_Query( $args );

	
	
	ob_start();
	if ( $the_query->have_posts() ) {
		global $post; 
		
		wp_enqueue_script('king-jquery-flexslider');
		?>
		
		<!-- Testimonials Slider -->
		<section class="full-width <?php echo king_get_animation_class($animation); ?>">
			<div class="flexslider testimonials-slider-fw">
				<ul class="slides">
					
					<?php 
					$n = 1;
					while ( $the_query->have_posts() )
					{
						$the_query->the_post();

						$position = get_post_meta($post->ID, 'position', true);
						$company = get_post_meta($post->ID, 'company', true);
						$author = get_the_title($post -> ID);

						if (!empty($company) && !empty($position)) {
							$company = ', '.$company;
						}

						$post_content = stripslashes($post -> post_content);
						
						$style_html = '';
						if (!empty($background)) {
							
							$background_src = king_get_image_by_id($background);
							if ($background_src) {
								$style_html = 'background: url('.esc_url($background_src).')';
							}
						}
						
						?>
						<li>
							<div class="testimonial-item" >
								<div class="testimonial-bg" <?php echo (!empty($style_html) ? 'style="'.$style_html.'"' : ''); ?>></div>
								<div class="container">
									<div class="testimonial-inner">
										<div class="testimonial-content">
											<h3><?php echo esc_html($header); ?></h3>
											<span class="separator">"</span>
											<p><?php echo esc_html($post_content); ?></p>
										</div>
										<div class="testimonial-author">
											<div class="avatar">
												<?php king_the_resized_post_thumbnail('testimonials', get_the_title()); ?>
												<span class="author-name"><?php echo esc_html($author); ?></span>
												<span class="author-position"><?php echo esc_html($position.$company); ?></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</li>
						<?php
						$n++;
					} ?>		
		
				</ul>
				<div class="testimonials-nav">
					<div class="container">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<ul class="testimonial-nav-list">
								<?php for ($i = 1; $i < $n; $i++): ?>
									<li></li>
								<?php endfor; ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- /Testimonials Slider -->
		<?php 
	}
	
	wp_reset_postdata();
    $html = ob_get_contents();
    ob_end_clean();
	return $html;
}
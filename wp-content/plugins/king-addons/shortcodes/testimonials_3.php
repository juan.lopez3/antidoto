<?php
/**
 * Shortcode Title: Testimonials 3
 * Shortcode: testimonials 3
 * Usage: [testimonials_3 animation="bounceInUp" category="3" limit="3"]
 */
add_shortcode('testimonials_3', 'king_testimonials_3_func');

function king_testimonials_3_func( $atts, $content = null ) {
    extract(shortcode_atts(array(
		'animation' => '',
		'style' => '',
		'category' => '',
		'limit' => '',
		'characters_limit' => ''
		),
	$atts));
	
	wp_enqueue_script( 'king-owl-carousel');
	
    if (empty($limit)) {
		$limit = -1;
	}

	global $query_string, $post;
	$args = array(
		'posts_per_page'  => $limit,
		'offset'          => 0,
		'orderby'         => 'date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'testimonial',
		'post_mime_type'  => '',
		'post_parent'     => '',
		'paged'				=> 1,
		'post_status'     => 'publish'
	);
	
	if (!empty($category)):
		$args['tax_query'] = array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'testimonial-category',
				'terms' => $category,
				'operator' => 'IN'
			)
		);
	endif;
	
	$the_query = new WP_Query( $args );

	if ($style == 2) {
		$class_container = 'testim-slides';
		$class_item = 'testimonial-block-2';
		$avatar_width = 320;
		$avatar_height = 290;
	} else {
		$class_container = 'single-slider';
		$class_item = 'testimonial-block';
		$avatar_width = 128;
		$avatar_height = 128;
	}
	
	ob_start();
	if ( $the_query->have_posts() ) {
		global $post; ?>
		
		<!-- Testimonials Slider -->
		<section class="section <?php echo king_get_animation_class($animation); ?>">
			<div class="<?php echo sanitize_html_class($class_container); ?> owl-carousel">
				
				<?php 
				$n = 1;
				while ( $the_query->have_posts() )
				{
					$the_query->the_post();

					$position = get_post_meta($post->ID, 'position', true);
					$company = get_post_meta($post->ID, 'company', true);
					$author = get_the_title($post -> ID);

					if (!empty($company) && !empty($position)) {
						$company = ', '.$company;
					}
					$post_content = stripslashes($post -> post_content);
					?>
					<div class="item">
						<div class="<?php echo sanitize_html_class($class_item); ?>">							
							<?php if ($style == 2) { ?>
								<figure class="avatar">
									<?php king_the_resized_post_thumb($avatar_width, $avatar_height, get_the_title(), '', true); ?>
									<figcaption>
										<p class="author-name"><?php echo esc_html($author); ?></p>
										<p class="author-position"><?php echo esc_html($position.$company); ?></p>
									</figcaption>
								</figure>
							<?php } else { ?>
								<figure class="avatar">
									<?php king_the_resized_post_thumb($avatar_width, $avatar_height, get_the_title(), '', true); ?>
								</figure>
								<p class="author-name"><?php echo esc_html($author); ?></p>
								<p class="author-position"><?php echo esc_html($position.$company); ?></p>
							<?php } ?>
							<div class="message">
								<p><?php 
								if (!empty($characters_limit)) {
									echo king_get_shortened_string_by_letters($post_content,intval($characters_limit));
								} else {
									echo wp_kses_post($post_content);
								}
								?></p>
							</div>
						</div>
					</div>
					<?php
				} ?>
				
			</div>
		</section>
		<!-- /Testimonials Slider -->
		<?php 
	}
	
	wp_reset_postdata();
    $html = ob_get_contents();
    ob_end_clean();
	return $html;
}
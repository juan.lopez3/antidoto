<?php
/**
 * Shortcode Title: Info Box 3
 * shortcode:  info_box_3
 * Usage: [info_box_3 animation="fade" icon="icon-search" content="Your content"]
 */

add_shortcode('info_box_3', 'king_info_box_3_func');
function king_info_box_3_func($atts, $content = null)
{
    extract(shortcode_atts(array(
        'animation' => '',
        'icon' => '',
        'icon_upload' => '',
		'icon_color' => ''
    ), $atts));

    $i_style =  '';
	$icon_html = '';
    if(!empty($icon_color)){
        $i_style = 'style="color:'.esc_attr($icon_color).'"';
    }
    if (!empty($icon_upload)) {
    	$icon_upload = king_get_image_by_id($icon_upload);
    	$icon_html = '<img class="icons" src="'.esc_url($icon_upload).'" />';
    } else {
    	$icon_html = '<i '.$i_style.' class="icons ' . sanitize_html_classes($icon) . '"></i>';
    }
    $html = '
		<div class="contact-info-box style2 '.king_get_animation_class($animation).'">
			<div class="icon">'.$icon_html.'</div>
			<div class="content-container">'.$content.'</div>
		</div>';
    return $html;
}
<?php
/**
 * Shortcode Title: Blog
 * Shortcode: blog
 * Usage: [blog animation="fade" length="100" count=10 ],
 */
add_shortcode('blog_3', 'king_blog_3_func');

function king_blog_3_func($atts, $content = null) {
	global $post;
	extract(shortcode_atts(array(
		'animation' => '',
		'category' => '',
		'url' => ''
	), $atts));

	$latest = new Wp_Query(
		array(
			'post_type' => 'post',
			'posts_per_page' => 5,
			'cat' => $category,
			'meta_query'     => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
		)
	);

	ob_start();

	if ($latest->have_posts()): ?>

		<section class="latest-posts" <?php echo king_get_animation_class($animation,true); ?>>
			<div class="row">
				
				<div class="col-md-6 left-sec">
					<article class="half-height">
						<?php if ($latest->have_posts()): 
							$latest->the_post(); ?>
							<?php $img_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'ts-blog-3-half-height') ?>
							<figure style="background-image:url(<?php echo esc_url($img_url[0]); ?>);">
                				<?php the_post_thumbnail(); ?>
            				</figure>
							<div class="contents">
		                        <h6 class="category"><?php echo get_the_category_list(' '); ?></h6>
		                        <h3 class="title"><a href="<?php echo esc_url(get_the_permalink()); ?>"><?php the_title(); ?></a></h3>
		                        <ul class="meta">
		                            <li><?php the_date(get_option('date_format'));?></li>
		                            <li><?php echo get_comments_number(); ?> <?php _e('Comments', 'king-addons'); ?></li>
		                        </ul>
		                        <?php king_blog_alt_post_share_icons(); ?>
                    		</div><!-- /contents -->
						<?php endif; ?>
					</article>
					<article class="half-height">
						<?php if ($latest->have_posts()): 
							$latest->the_post(); ?>
							<?php $img_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'ts-blog-3-half-height') ?>
							<figure style="background-image:url(<?php echo esc_url($img_url[0]); ?>);">
                				<?php the_post_thumbnail(); ?>
            				</figure>
							<div class="contents">
		                        <h6 class="category"><?php echo get_the_category_list(' '); ?></h6>
		                        <h3 class="title"><a href="<?php echo esc_url(get_the_permalink()); ?>"><?php the_title(); ?></a></h3>
		                        <ul class="meta">
		                            <li><?php the_date(get_option('date_format'));?></li>
		                            <li><?php echo get_comments_number(); ?> <?php _e('Comments', 'king-addons'); ?></li>
		                        </ul>
		                        <?php king_blog_alt_post_share_icons(); ?>
                    		</div><!-- /contents -->
						<?php endif; ?>
					</article>
				</div><!-- /col-md-6 -->

				<div class="col-lg-3 col-md-6 mid-sec">
					<article class="full-height">
						<?php if ($latest->have_posts()): 
							$latest->the_post(); ?>
							<?php $img_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'ts-blog-3-full-height') ?>
							<figure style="background-image:url(<?php echo esc_url($img_url[0]); ?>);">
                				<?php the_post_thumbnail(); ?>
            				</figure>
							<div class="contents">
		                        <h6 class="category"><?php echo get_the_category_list(' '); ?></h6>
		                        <h3 class="title"><a href="<?php echo esc_url(get_the_permalink()); ?>"><?php the_title(); ?></a></h3>
		                        <ul class="meta">
		                            <li><?php the_date(get_option('date_format'));?></li>
		                            <li><?php echo get_comments_number(); ?> <?php _e('Comments', 'king-addons'); ?></li>
		                        </ul>
		                        <?php king_blog_alt_post_share_icons(); ?>
		                        <div class="blog-content">
                            <p><?php king_the_excerpt_theme(30); ?></p>
                            <a href="<?php echo esc_url(get_the_permalink()); ?>" class="button shaped bordered">Read More</a>
                        </div>
                    		</div><!-- /contents -->
						<?php endif; ?>
					</article>
				</div><!-- /col-md-6 -->

				<div class="col-lg-3 col-md-6 right-sec">
					<article class="half-height">
						<?php if ($latest->have_posts()): 
							$latest->the_post(); ?>
							<?php $img_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'ts-blog-3-half-height-small') ?>
							<figure style="background-image:url(<?php echo esc_url($img_url[0]); ?>);">
                				<?php the_post_thumbnail(); ?>
            				</figure>
							<div class="contents">
		                        <h6 class="category"><?php echo get_the_category_list(' '); ?></h6>
		                        <h3 class="title"><a href="<?php echo esc_url(get_the_permalink()); ?>"><?php the_title(); ?></a></h3>
		                        <ul class="meta">
		                            <li><?php the_date(get_option('date_format'));?></li>
		                            <li><?php echo get_comments_number(); ?> <?php _e('Comments', 'king-addons'); ?></li>
		                        </ul>
		                        <?php king_blog_alt_post_share_icons(); ?>
                    		</div><!-- /contents -->
						<?php endif; ?>
					</article>
					<article class="half-height">
						<?php if ($latest->have_posts()): 
							$latest->the_post(); ?>


							<?php $img_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'ts-blog-3-half-height-small') ?>
							<figure style="background-image:url(<?php echo esc_url($img_url[0]); ?>);">
                				<?php the_post_thumbnail(); ?>
            				</figure>
							<div class="contents">
		                        <h6 class="category"><?php echo get_the_category_list(' '); ?></h6>
		                        <h3 class="title"><a href="<?php echo esc_url(get_the_permalink()); ?>"><?php the_title(); ?></a></h3>
		                        <ul class="meta">
		                            <li><?php the_date(get_option('date_format'));?></li>
		                            <li><?php echo get_comments_number(); ?> <?php _e('Comments', 'king-addons'); ?></li>
		                        </ul>
		                        <?php king_blog_alt_post_share_icons(); ?>
                    		</div><!-- /contents -->
						<?php endif; ?>
					</article>
				</div><!-- /col-md-6 -->



			</div><!-- /#blog-container-box-->
		
				
		</section>
	<?php
	endif;

	wp_reset_postdata();
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}

function king_blog_alt_post_share_icons() { ?>
	
	<ul class="social-media">
		<li>
			<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>"
			onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
								 return false;">
			 <i class="fa fa-facebook"></i>
			</a>
		</li>
		<li>
			<a target="_blank" class="twitter-share-button"
			href="https://twitter.com/share?url=<?php echo urlencode(get_the_permalink()); ?>&text=<?php echo urlencode(get_the_title()); ?>"
			onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
								 return false;">
			 <i class="fa fa-twitter"></i>
			</a>
		</li>
	</ul>
			
<?php }

<?php
/*
 * Shortcode Name: Contact Form 4
 * Shortcode: contact_form_4
 * Usage: [contact_form_4 animation="fade" name_label="Name" email_label="your@email.com" message_label="Message" send_label="Send"  ]
 */

add_shortcode('contact_form_4', 'king_contact_form_4_func');

function king_contact_form_4_func($atts, $content = null)
{
    extract(
        shortcode_atts(array(
            'animation'  => '',
            'name_label' => '',
            'shape'		 => 'btn-default',
            'fill_style' => 'filled',
			'background_color' => '',
			'border_color'     => '',
			'text_color'       => '',
            'email_label' => '',
            'message_label' => '',
            'send_label' => '',
        ), $atts)
    );

	ob_start();
	$uniqid_class = '';
	$custom_style = '';
	$customize		  = ( $background_color || $border_color || $text_color ) ? true:false;
	if($customize) {
		$uniqid       = time().'-'.mt_rand();
		$custom_style .=  '.btn-custom-'.$uniqid.'{';
        $custom_style .=  ( $background_color ) ? 'background-color:'.$background_color.' !important;':'';
        $custom_style .=  ( $border_color ) ? 'border-color:'.$border_color.' !important;':'';
        $custom_style .=  ( $text_color ) ? 'color:'.$text_color.' !important;':'';
        $custom_style .= '}';

        king_add_inline_style( $custom_style );

    	$uniqid_class = 'btn-custom-'. $uniqid;
	}
	?>
	<form class="contact-form centered-fields <?php echo king_get_animation_class($animation); ?>" data-sending="<?php echo esc_attr(__('Sending Message...','king-addons'));?>">
		<?php echo wp_nonce_field( 'contact_form_submission', 'contact_nonce', true,false); ?>
		<input type="hidden" name="contact-form-value" value="1" id=""/>
		<div class="row">
			<div class="col-md-12">
				<input class="underlined" type="text" name="name" placeholder="<?php echo esc_attr($name_label); ?>*">
			</div>
			<div class="col-md-12">
				<input class="underlined" type="text" name="email" placeholder="<?php echo esc_attr($email_label); ?>*">
			</div>
		</div>
		<textarea class="underlined" rows="1" name="msg" placeholder="<?php echo esc_attr($message_label); ?>"></textarea>
		<div class="align-center">
			<button class="button shaped <?php echo sanitize_html_class($uniqid_class); ?> <?php echo sanitize_html_class($shape); ?> <?php echo sanitize_html_class($fill_style); ?>" type="submit"><?php echo esc_html($send_label); ?></button>
		</div>
		<div id="msg"></div>
	</form>
	<?php
	$html = ob_get_contents();
    ob_end_clean();
    return $html;
}






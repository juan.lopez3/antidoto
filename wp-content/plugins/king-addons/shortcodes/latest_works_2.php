<?php
/**
 * Shortcode Title: Portfolio 3
 * Shortcode: portfolio_3
 * Usage: [portfolio_3 animation="bounceInUp" category_filter="" category="6" limit="6" ]
 */
add_shortcode('latest_works_2', 'king_latest_works_2_func');

function king_latest_works_2_func($atts, $content = null) {
	global $post, $wp_query;

	extract(shortcode_atts(array(
		'animation'       => '',
		'category'        => '',
		'category_filter' => 'yes',
		'filter_cats'     => 0,
		'limit'           => 4,
	), $atts));

	$item_col_class = array('col-lg-3 col-md-6 half-height', 'col-lg-3 col-md-6 half-height', 'col-md-6 half-height', 'col-md-6 half-height');
	$item_image_size = array('king-blog-3-half-height-small', 'ts-blog-3-half-height-small', 'ts-blog-3-half-height', 'ts-blog-3-half-height');
	
	$output = '';
	$args = array(
		'posts_per_page' => $limit,
		'meta_query' => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
		'offset' => 0,
		'orderby' => 'date',
		'order' => 'DESC',
		'exclude' => '',
		'post_type' => 'portfolio',
		'post_mime_type' => '',
		'post_parent' => '',
		'paged' => 1,
		'post_status' => 'publish'
	);

	if (!empty($category)) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'portfolio-categories',
				'field' => 'id',
				'terms' => explode(',', $category)
			),
		);
	}

	$tmp_query  = $wp_query;
  	$wp_query   = new WP_Query( $args );

	//$the_query = new WP_Query($args);

	if (have_posts()) : ob_start();
	
		wp_enqueue_script('king-isotope');
		?>

		<!-- Projects Section -->
		<section class="latest-works style2 filter-list <?php echo king_get_animation_class($animation); ?>">
			<?php 
				if ($category_filter == 'yes') {
				    $filter_args = array(
				      'echo'     => 0,
				      'title_li' => '',
				      'style'    => 'none',
				      'taxonomy' => 'portfolio-categories',
				      'walker'   => new Walker_Portfolio_List_Categories(),
				    );

				    if( $filter_cats ) {

				      $exp_cats = explode(',',$filter_cats);
				      $new_cats = array();

				      foreach ( $exp_cats as $cat_value ) {
				        $has_children = get_term_children( intval($cat_value), 'portfolio-categories' );
				        if( ! empty( $has_children ) ) {
				            $new_cats[] = implode( ',', $has_children );
				        } else {
				            $new_cats[] = $cat_value;
				        }
				      }

				      $filter_args['include'] = implode( ',', $new_cats );

				    }

				    $filter_args = wp_parse_args( $args, $filter_args );
				}
			?>
			<div class="col-md-6 half-height stamp">


				<?php if($content): ?>
				<div class="contents">
                	<?php echo wp_kses_post($content); ?>     
                </div>
            	<?php endif; ?>
				<ul class="filter-tabs">
					<li class="filter is-checked" data-filter="*"><?php _e('All', 'king-addons');?></li>
					<?php echo wp_list_categories( $filter_args ); ?>	
				</ul>
				<div class="filters-nav">
                    <span class="prev"></span>
                    <span class="next"></span>
                </div>
			</div>
			
			<?php
			$i = 0;
			while ( have_posts() ) : the_post();

				$item_cats = wp_get_post_terms(get_the_ID(), 'portfolio-categories');
				$cat = $cat1 = array();
				foreach ($item_cats as $item_cat) {
					$cat[] = 'category-' . $item_cat->slug;
					$cat1[] = $item_cat->name;
				}
				
				$cat_html = '';
				$cat1_html = '';
				if (is_array($cat)) {
					$cat_html = implode(' ', $cat);
				}

				if (is_array($cat1)) {
					$cat1_html = implode(' ', $cat1);
				}
				
				
				?>

				<article class="portfolio-item <?php echo sanitize_html_classes($item_col_class[$i]); ?> element-item <?php echo sanitize_html_classes($cat_html); ?>">
					<?php $img_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $item_image_size[$i]); ?>
					<figure style="background-image:url(<?php echo (isset($img_url[0]) ? esc_url($img_url[0]) : ''); ?>);">
        				<?php the_post_thumbnail(); ?>
    				</figure>
					<div class="contents">
					    <h3 class="title">
					        <a href="<?php echo esc_url(get_the_permalink()); ?>"><?php the_title(); ?></a>
					    </h3>
					    <h6 class="category"><?php echo esc_html($cat1_html); ?></h6>
					</div>
					<!-- /contents -->
        		</article>
				
			<?php $i++; endwhile; ?>
		</section> 

	<?php endif;

	wp_reset_query();
  	wp_reset_postdata();
  	$wp_query = $tmp_query;

  	$output = ob_get_clean();
  	return $output;
}

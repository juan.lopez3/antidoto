<?php
/**
 * Shortcode Title: Portfolio 2
 * Shortcode: portfolio_2
 * Usage: [portfolio_2 animation="bounceInUp" category="6" limit="6" ]
 */
add_shortcode('portfolio_2', 'king_portfolio_2_func');

function king_portfolio_2_func($atts, $content = null) {
	global $post;

	extract(shortcode_atts(array(
		'animation' => '',
		'title_part1' => '',
		'title_part2' => '',
		'category_filter' => 'yes',
		'category' => '',
		'filter_cats'=> 0,
		'limit' => 6,
	), $atts));


	
	$html = '';
	$args = array(
		'numberposts' => "",
		'posts_per_page' => $limit,
		'meta_query' => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
		'offset' => 0,
		'orderby' => 'date',
		'order' => 'DESC',
		'exclude' => '',
		'meta_key' => '',
		'meta_value' => '',
		'post_type' => 'portfolio',
		'post_mime_type' => '',
		'post_parent' => '',
		'paged' => 1,
		'post_status' => 'publish'
	);

	if (!empty($category)) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'portfolio-categories',
				'field' => 'id',
				'terms' => explode(',', $category)
			),
		);
	}

	$the_query = new WP_Query($args);

	if ($the_query->have_posts()) : ob_start();
	
		wp_enqueue_script('king-isotope');
		wp_enqueue_script('king-prettyphoto-js');
		wp_enqueue_style('king-prettyphoto-css');
		?>

		<!-- Projects Section -->
		<section class="full-width section big-padding-top bg-light-grey <?php echo king_get_animation_class($animation); ?>">

			<div class="container">
				<h3 class="section-heading"><?php echo esc_html($title_part1); ?> <span class="text-primary"><?php echo esc_html($title_part2); ?></span></h3>
				<?php 
					if ($category_filter == 'yes') {
					    $filter_args = array(
					      'echo'     => 0,
					      'title_li' => '',
					      'style'    => 'none',
					      'taxonomy' => 'portfolio-categories',
					      'walker'   => new Walker_Portfolio_List_Categories(),
					    );

					    if( $filter_cats ) {

					      $exp_cats = explode(',',$filter_cats);
					      $new_cats = array();

					      foreach ( $exp_cats as $cat_value ) {
					        $has_children = get_term_children( intval($cat_value), 'portfolio-categories' );
					        if( ! empty( $has_children ) ) {
					            $new_cats[] = implode( ',', $has_children );
					        } else {
					            $new_cats[] = $cat_value;
					        }
					      }

					      $filter_args['include'] = implode( ',', $new_cats );

					    }

					    $filter_args = wp_parse_args( $args, $filter_args );
					}
				?>
				<ul class="filter-tabs">
					<li class="filter is-checked" data-filter="*"><?php _e('All', 'king-addons');?></li>
					<?php echo wp_list_categories( $filter_args ); ?>	
				</ul>
			</div>
			<ul class="filter-list">
				<?php
				$si = 1;
				while ($the_query->have_posts()): $the_query->the_post();

					$item_cats = wp_get_post_terms(get_the_ID(), 'portfolio-categories');
					$cat = $cat1 = array();
					foreach ($item_cats as $item_cat) {
						$cat[] = 'category-' . $item_cat->slug;
						$cat1[] = $item_cat->name;
					}
					
					$cat_html = '';
					$cat1_html = '';
					if (is_array($cat)) {
						$cat_html = implode(' ', $cat);
					}

					if (is_array($cat1)) {
						$cat1_html = implode(' ', $cat1);
					}
					
					if (in_array($si,array(1,3,4))) {
						$image_size = 'portfolio-shortcode-h';
						$show_excerpt = true;
					} else {
						$image_size = 'portfolio-shortcode';
						$show_excerpt = false;
					}
					$si++;
					if ($si == 7) {
						$si = 1;
					}
					
					?>
					
					<li class="element-item <?php echo sanitize_html_classes($cat_html); ?>">
						<div class="project-box">
							<figure>
								<?php king_the_resized_post_thumbnail($image_size, get_the_title()); ?>
								<figcaption>
									<div>
										<span class="categ"><?php echo esc_html($cat1_html); ?></span>
										<h4><?php the_title(); ?></h4>
										<p><?php if ($show_excerpt == true):
											echo strip_tags(king_get_the_excerpt_theme(10));
										endif; ?></p>
										<a href="<?php echo esc_url(king_img_url()); ?>" class="btn" rel="prettyphoto"><?php _e('zoom', 'king'); ?></a>
										<a href="<?php echo esc_url(get_the_permalink()); ?>" class="btn"><?php _e('view project', 'king'); ?></a>
									</div>
								</figcaption>
							</figure>
						</div> <!-- /blog-box -->
					</li>
				<?php
				endwhile;
				wp_reset_postdata(); ?>
			</ul> <!-- /filter-list -->
		</section> 

	<?php endif;

	$html = ob_get_contents();
	ob_end_clean();

	return $html;
}

<?php
/*
 * Shortcode Name: Contact Form 2
 * Shortcode: contact_form_2
 * Usage: [contact_form_2 animation="fade" name_label="Name" name_icon="fa-user" email_label="your@email.com" email_icon="fa-envelope" message_label="Message" send_label="Send" clear_label="Clear" skin="1" button_align="left" ]
 */

add_shortcode('contact_form_2', 'king_contact_form_2_func');

function king_contact_form_2_func($atts, $content = null)
{
    extract(
        shortcode_atts(array(
            'animation' => '',
            'style' => '',
            'name_label' => '',
            'name_icon' => '',
            'email_label' => '',
            'email_icon' => '',
            'message_label' => '',
            'message_icon' => '',
            'send_label' => '',
            'background_type' => '',
            'background_image' => '',
			'address' => '',
            'marker' => '',
        ), $atts)
    );
	
	$marker_src = '';
	$background_src = '';
	
	if ($background_type == 'map') {
		if (ot_get_option('add_google_maps_api') != 'disabled') {
			wp_enqueue_script('king-google-map-api');
		}
		wp_enqueue_script('king-cd-google-map');
		
		if (!empty($marker)) {
			$marker_src = king_get_image_by_id($marker);
		}
		
	} else {
		if (!empty($background_image)) {
			$background_src = king_get_image_by_id($background_image);
		}
	}
	
	ob_start();
	?>
	<!-- New Contact Form SC -->
	<section class="section full-width king-contact <?php echo sanitize_html_classes($style); ?> <?php echo king_get_animation_class($animation); ?>">
		<?php 
		if ($background_type == 'map'): ?>
			<section class="cd-google-map">
				<div class="google-container" data-address="<?php echo esc_attr($address); ?>" data-marker="<?php echo esc_url($marker_src);?>"></div>
				<div class="cd-zoom-in"></div>
				<div class="cd-zoom-out"></div>
			</section>
		<?php elseif (!empty($background_src)): ?>
			<img src="<?php echo esc_url($background_src); ?>" alt="">
		<?php endif; ?>
		<form class="king-contact-form contact-form" data-sending="<?php echo esc_attr(__('Sending Message...','king-addons'));?>">
			<?php echo wp_nonce_field( 'contact_form_submission', 'contact_nonce', true,false); ?>
			<input type="hidden" name="contact-form-value" value="1" id=""/>
			<div class="container">
				<div class="contact-form-inner">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="input-container">
								<div class="iconic-input">
									<input type="text" name="name" placeholder="<?php echo esc_attr($name_label); ?>*">
									<i class="<?php echo esc_attr($name_icon);?>"></i>
								</div>
							</div>
							<div class="input-container">
								<div class="iconic-input">
									<input type="text" name="email" placeholder="<?php echo esc_attr($email_label); ?>*">
									<i class="<?php echo esc_attr($email_icon); ?>"></i>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6">
							<div class="input-container">
								<div class="iconic-input">
									<textarea rows="4" name="msg" placeholder="<?php echo esc_attr($message_label); ?>"></textarea>
									<i class="<?php echo esc_attr($message_icon); ?>"></i>
								</div>
							</div>
						</div>
					</div>
					<input type="submit" value="<?php echo esc_attr($send_label); ?>">
					<div id="msg"></div>
				</div>
			</div>
		</form>
	</section>
	<!-- /New Contact Form SC -->
	<?php
	$html = ob_get_contents();
    ob_end_clean();
    return $html;
}






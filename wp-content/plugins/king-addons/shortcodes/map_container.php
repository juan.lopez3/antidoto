<?php

/**

 * Shortcode Title: Map Container

 * Shortcode: map_container

 * Usage: [map_container address="" zoom="14" custom_marker="" full_width="no" grayscale="no" height="230" marker_offset="-140" first_page="no" last_page="yes"][wpgmappity id="x"][/map_container]

 */

add_shortcode('map_container', 'king_map_container_func');



function king_map_container_func( $atts, $content = null ) {

    

	extract(shortcode_atts(array(

		'full_width' => 'yes',

		//'grayscale' => 'no',

		//'zoom' => '14',

		'custom_marker' => '',

		'address' => '',

		'height' => '',

		'marker_offset' => '',

		'last_page' => '',

		'first_page' => ''

		), 

	$atts));

	

	$class = '';

	

	if ($full_width == 'yes') {

		$class .= ' full';

	}

	

	if ($last_page == 'yes') {

		$class .= ' last-page';

	}

	

	if ($first_page == 'yes') {

		$class .= ' first-page';

	}

	

	$styles = array();

	$styles_html_1 = '';

	if (!empty($height)) {

		$styles[] = 'height: '.(intval($height) + intval(abs($marker_offset))).'px';

		$styles_html_1 = 'height:'.esc_attr($height).'px';

	}

	

	if (!empty($marker_offset)) {

		$styles[] = 'margin-top: -'.intval(abs($marker_offset)).'px';

	}

	

	$styles_html_2 = '';

	if (count($styles) > 0) {

		$styles_html_2 = implode(';',$styles);

	}

	

	//map

	$map = '';

	

	if (!empty($address)) {

		if (ot_get_option('add_google_maps_api') != 'disabled') {
			wp_enqueue_script('king-google-map-api');
		}
		wp_enqueue_script('king-cd-google-map');


		ob_start(); ?>

		<div class="ts-map">

			<div class="cd-google-map" <?php echo ($styles_html_2 ? 'style="'.$styles_html_2.'"' : ''); ?>>
				<div class="google-container" data-address="<?php echo esc_attr($address); ?>" data-marker="<?php echo esc_url($custom_marker);?>"></div>
				<div class="cd-zoom-in"></div>
				<div class="cd-zoom-out"></div>
			</div>

		</div>

		 

		<?php 

		$map =  ob_get_contents();

		ob_end_clean();

	}

	

	return '

		<div class="sc-map '.$class.'" '.(!empty($styles_html_1) ? 'style="'.$styles_html_1.'"' : '').'>

			<div class="sc-map-container" '.(!empty($styles_html_2) ? 'style="'.$styles_html_2.'"' : '').'>

				'.$map.do_shortcode($content).'

			</div>

		</div>';

}
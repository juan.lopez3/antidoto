<?php

/**
 * Shortcode Title: Call to action 2
 * Shortcode: call_to_action_2
 * Usage: [call_to_action_2 animation="bounceInUp" title="Your title..." url="VC_URL_FORMAT" text_color="#FFFFFF"]Your title here...[/call_to_action_2]
 */
add_shortcode('call_to_action_2', 'king_call_to_action_2_func');

function king_call_to_action_2_func($atts, $content = null) {
	extract(shortcode_atts(array(
		'animation'     => '',
		'title'         => '',
		'button_url'    => '',
		'button_text'   => '',
		'text_color'    => '',
		'scroll_effect' => '',
		'boundaries'	=> 'exit',
		'from'			=> '',
		'to'			=> '',
		'opacity'		=> '',
		'trans_x'	    => '',
		'trans_y'	    => '',
		'trans_z'	    => ''
	), $atts));

	$href = $btn_title = $target = '';
	if ( function_exists( 'vc_parse_multi_attribute' ) ) {
		$parse_args     = vc_parse_multi_attribute( $button_url );
		$href           = ( isset( $parse_args['url'] ) ) ? $parse_args['url'] : '#';
		$btn_title      = ( isset( $parse_args['title'] ) ) ? $parse_args['title'] : '';
		$target         = ( isset( $parse_args['target'] ) ) ? trim( $parse_args['target'] ) : '_self';
	}

	$text_color_style = ( $text_color ) ? ' style="color:'.esc_attr($text_color).';"':'';

	$data_when = $data_from = $data_to = $data_opacity = $scroll_me_class = $data_x = $data_y = $data_z = '';
	if( $scroll_effect == 'yes' ) {
		$data_when      = 'data-when="'.esc_attr($boundaries).'"';
		$data_from      = 'data-from="'.esc_attr($from).'"';
		$data_to        = 'data-to="'.esc_attr($to).'"';
		$data_x			= 'data-translatex="'.esc_attr($trans_x).'"';
		$data_y			= 'data-translatey="'.esc_attr($trans_y).'"';
		$data_z			= 'data-translatez="'.esc_attr($trans_z).'"';
		$data_opacity   = 'data-opacity="'.esc_attr($opacity).'"';
		$scroll_me_class = ' animateme';
	}
	
	$html = '
		<div class="call-to-action'.$scroll_me_class.'" '.$data_when.$data_from.$data_to.$data_opacity.$data_x.$data_y.$data_z.$text_color_style.'>
			<h4>'.esc_attr($title).'</h4>
			<p>'.wp_kses_post($content).'</p>
			<a href="'.esc_url($href).'" title="'.esc_attr($btn_title).'" target="'.esc_attr($target).'" class="button">'.esc_attr($button_text).'</a>
		</div>';

	return $html;
}

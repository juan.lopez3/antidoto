<?php
/**
 * Shortcode Title: Welcome
 * Shortcode: welcome
 * Usage: [welcome animation="bounceInUp" background_image="image.png" overlay_image="image2.png" logo="logo.png" title="Lorem ipsum"]Your text...[/welcome]
 */
add_shortcode('welcome', 'king_welcome_func');

function king_welcome_func( $atts, $content = null ) {
	
	extract(shortcode_atts(array(
		'animation' => '',
		'animation_delay'	=> '',
		'background_image' => '',
		'overlay_image' => '',
		'logo' => '',
		'title' => '',
		),
	$atts));
	
	$background_src = '';
	if (!empty($background_image)) {
		$background_src = king_get_image_by_id($background_image);
	}
	$data_delay  = ( $animation_delay ) ? ' data-wow-delay="'.esc_attr($animation_delay).'"':'';
	ob_start();
	?>
	<div class="welcome clearfix <?php echo king_get_animation_class($animation); ?>">
		<div class="left-sec">
			<figure>
				<?php if (!empty($background_src)):
//					echo king_get_resized_image($background_src, 600, 665, '', '', true); 
                                        echo '<img src="'.$background_src.'">';
				endif; ?>
				<?php echo wp_get_attachment_image( $overlay_image, 'full', false, array('class' => 'logo')); ?>
			</figure>
		</div>
		<div class="right-sec">
			<div class="contents">
				<h4><?php echo esc_html($title); ?></h4>
				<figure class="logo-container">
					<?php echo wp_get_attachment_image( $logo, 'full' ); ?>
				</figure>
				<?php echo wp_kses_post($content); ?>
				</div>
		</div>
	</div>

	<?php
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
	return '<div '.king_get_animation_class($animation,true).' data-animation="'.$animation.'"'.$data_delay.'>'.do_shortcode($content).'</div>';
}
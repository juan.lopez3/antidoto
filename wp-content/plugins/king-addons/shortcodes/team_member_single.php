<?php
/**
 * Shortcode Title: Team members
 * Shortcode: team_members
 * Usage: [team_members animation="bounceInUp" member1="" member2="" memeber3="" member4=""]
 */
add_shortcode('team_members_single', 'king_team_members_single');

function king_team_members_single($atts, $content = null)
{

    extract(shortcode_atts(array(
		'animation' => '',
		'member_id' => '',
	),
	$atts)); 
	
	ob_start();
	king_team_member_item_one($member_id, $animation); 
	wp_reset_postdata();
	$html = ob_get_contents();
    ob_end_clean();
    return $html;
}


function king_team_member_item_one($member, $animation) {
	
	global $post;
	
	if (empty($member)) {
		return;
	}
	
	$post = get_post($member);
	if (!$post || is_wp_error($post)) {
		return;
	}
	setup_postdata($post);
	
	?>

	<div class="team-member-single <?php echo king_get_animation_class($animation); ?>">
        <figure>
            <?php
            //king_the_resized_post_thumbnail('team-member-single', get_the_title());
            echo get_the_post_thumbnail();
            ?>
        </figure>
        <ul class="social-media">
        	<?php
				$xs_social_icons = array(
					'facebook',
					'twitter',
					'skype',
					'google',
					'vimeo',
					'linkedin',
					'instagram'
				);
				foreach ($xs_social_icons as $social_icon) {
					$social_url = get_post_meta($post->ID, 'xs_' . $social_icon, true);
					if ($social_url == '') {
						continue;
					} ?>
					<li><a href="<?php echo esc_url($social_url); ?>" target="_blank"><i class="icon-<?php echo sanitize_html_classes($social_icon); ?>"></i></a></li>
					<?php
				}
			?>
        </ul>
    </div>
	<?php
}

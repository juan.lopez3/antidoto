<?php
/**
 * Shortcode Title: Skill bar
 * Shortcode: skillbar
 * Usage: [skillbar animation="bounceInUp" style="1"][skillbar_item percentage="80" title="Cooking" color="#FF0000"][/skillbar]
 */
add_shortcode('skillbar', 'king_skillbar_func');
$shortcode_skillbar_border_color = null;
$shortcode_skillbar_text_color = null;
$shortcode_skillbar_style = null;

function king_skillbar_func($atts, $content = null)
{
    extract(shortcode_atts(array(
        'animation' => '',
        'color' => '',
        'text_color' => '',
        'style' => ''
    ), $atts));

    global $shortcode_skillbar_style, $shortcode_skillbar_animation, $shortcode_skillbar_color, $shortcode_skillbar_text_color;
    $shortcode_skillbar_style = $style;
	
	if (empty($shortcode_skillbar_style)) {
		$shortcode_skillbar_style = 'style1';
	}
	
    $shortcode_skillbar_animation = $animation;
    $shortcode_skillbar_color = $color;
    $shortcode_skillbar_text_color = $text_color;
	
	if ($shortcode_skillbar_style == 'style3') {
		return '<div class="king-progressbars">'.do_shortcode($content).'</div>';
	} else {
		return do_shortcode($content);
	}
	
    
}

/**
 * Shortcode Title: Skill Bar item
 * Shortcode: skillbar_item
 * Usage: [skillbar_item percentage="80" title="Cooking" color="#FF0000"]
 */
add_shortcode('skillbar_item', 'king_skillbar_item_func');
function king_skillbar_item_func($atts, $content = null)
{
    extract(shortcode_atts(array(
        'percentage' => 0,
        'title' => '',
    ), $atts));

    global $shortcode_skillbar_style, $shortcode_skillbar_animation, $shortcode_skillbar_color, $shortcode_skillbar_text_color;

    $html = null;
    $rand = null;
    if ((int)$percentage > 100) {
        $percentage = 100;
    } else if ((int)$percentage < 1) {
        $percentage = 1;
    }

	$p_style = '';
    if (!empty($shortcode_skillbar_text_color)) {
        $p_style = 'style="color:' . esc_attr($shortcode_skillbar_text_color) . '"';
    }
	
	$s_style = '';
    if (!empty($shortcode_skillbar_color)) {
        $s_style = 'style="background-color:' . esc_attr($shortcode_skillbar_color) . '"';
    }

	switch ($shortcode_skillbar_style) {
		case 'style3':
			$html .= '
				<!-- King Progressbar -->
				<div class="king-progressbar ' . king_get_animation_class($shortcode_skillbar_animation) . '">
					<div class="progressbar-title">
						<p '.$p_style.'>' . $title . '</p>
					</div>
					<div class="progressbar-sc">
						<div class="progressbar" data-percent="' . esc_attr($percentage) . '">
							<div class="progress-width" '.$s_style.'></div>
							<span class="progress-percent" '.$p_style.'></span>
						</div>
					</div>
				</div>
				<!-- /King Progressbar -->
			';
			break;
		case 'style2':
		default: 
			$html .= '
				<div class="king-progressbar style2' . king_get_animation_class($shortcode_skillbar_animation) . '">
					<div class="progressbar-title">
						<p '.$p_style.'>' . $title . '</p>
						<span class="progress-percent" '.$p_style.'>'. $percentage .'</span>
					</div>
					<div class="progressbar-sc">
						<div class="progressbar" data-percent="' . esc_attr($percentage) . '">
							<div class="progress-width" '.$s_style.'></div>
						</div>
					</div>
				</div>';
	}
	
    

    return $html;
}
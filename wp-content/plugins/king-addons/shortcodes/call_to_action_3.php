<?php

/**
 * Shortcode Title: Call to action 2
 * Shortcode: call_to_action_2
 * Usage: [call_to_action_2 animation="bounceInUp" title="Your title..." url="VC_URL_FORMAT" text_color="#FFFFFF"]Your title here...[/call_to_action_2]
 */
add_shortcode('call_to_action_3', 'king_call_to_action_3_func');

function king_call_to_action_3_func($atts, $content = null) {
	extract(shortcode_atts(array(
		'animation'       => '',
		'animation_delay' => '',
		'promo_img'       => '',
		'button_url'      => '',
		'stellar_ratio'	  => '',
		'button_text'     => '',
		'hor_offset'	  => '',
		'ver_offset'	  => ''
	), $atts));

	$animation_delay    = ( $animation_delay ) ? ' data-wow-delay="'.esc_attr($animation_delay).'s"':'';
	$data_stellar_ratio = ( $stellar_ratio ) ? ' data-stellar-ratio="'.esc_attr($stellar_ratio).'"':'';
	$data_hor_offset    = ( $hor_offset ) ? ' data-stellar-horizontal-offset="'.esc_attr($hor_offset).'"':'';
	$data_ver_offset    = ( $ver_offset ) ? ' data-stellar-vertical-offset="'.esc_attr($ver_offset).'"':'';

	$href = $btn_title = $target = '';
	if ( function_exists( 'vc_parse_multi_attribute' ) ) {
		$parse_args     = vc_parse_multi_attribute( $button_url );
		$href           = ( isset( $parse_args['url'] ) ) ? $parse_args['url'] : '#';
		$title      = ( isset( $parse_args['title'] ) ) ? $parse_args['title'] : '';
		$target         = ( isset( $parse_args['target'] ) ) ? trim( $parse_args['target'] ) : '_self';
	}

	$output	 =	'<div class="call-to-action style2 '.king_get_animation_class($animation).'"'.$animation_delay.'>';
	$output	.=	'<div class="contents">';
	$output	.=	rs_set_wpautop($content);
	$output	.=	'<a href="'.esc_url($href).'" title="'.esc_attr($title).'" target="'.esc_attr($target).'" class="button shaped btn-square">'.esc_html($button_text).'</a>';
	$output	.=	'</div>';
	if(!empty($promo_img) && is_numeric($promo_img)) {
		$image_src  = wp_get_attachment_image_src( $promo_img, 'full' );
		if(isset($image_src[0])) {
			$output	.=	'<div class="parallax-element"'.$data_stellar_ratio.$data_ver_offset.$data_hor_offset.'>';
			$output	.=	'<img src="'.esc_url($image_src[0]).'" alt="">';
			$output	.=	'</div>';
			$output	.=	'</div>';
		}
	}

	return $output;
}

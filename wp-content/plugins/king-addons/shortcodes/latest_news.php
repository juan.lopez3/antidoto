<?php
/**
 * Shortcode Title: Latest news
 * Shortcode: latest_news
 * Usage: [latest_news animation="fade" length="100" count=10 ],
 */


add_shortcode('latest_news', 'king_latest_news_func');

function king_latest_news_func($atts, $content = null)
{
    extract(shortcode_atts(array(
        'animation' => '',
        'header' => '',
        'limit' => 9,
		'category' => '',
		'category_filter' => 'no',
		'url' => '',
		'link_label' => ''
    ), $atts));

    $html = '';
    
    $latest = new Wp_Query(
        array(
            'post_type' => 'post',
            'posts_per_page' => $limit,
			'cat' => $category
        )
    );
	if (!$link_label) {
		$link_label = __('View Blog', 'king-addons');
	}

    ob_start();

    if ($latest->have_posts()): 
		
		wp_enqueue_script('king-isotope');
		?>
		
		<!-- Blogs Section -->
		<div class="big-padding <?php echo king_get_animation_class($animation); ?>">
			<?php
			$categories = get_categories(  ); ?>

			<?php if ($category_filter == 'yes'): ?>
				<div class="filters-bar">
					<div class="cats-label"><?php _e('Categories:', 'king-addons'); ?></div>
					<ul class="blog-filters">
						<li class="filter is-checked" data-filter="*"><?php _e('All', 'king-addons'); ?></li>
						<?php if (is_array($categories) && !is_wp_error($categories)):
							foreach ($categories as $category): ?>
								<li class="filter" data-filter=".category-<?php echo esc_attr($category -> slug); ?>"><?php echo esc_html($category -> name); ?></li>
							<?php endforeach;
						endif; ?>
					</ul>
				</div>
			<?php endif; ?>
			
			<ul class="blogs-list">
				
				<?php while ($latest->have_posts()): $latest->the_post(); ?>
					
					<?php 
					$cat = array();
					$cat1 = array();
					
					$item_cats = wp_get_post_categories(get_the_ID());
					
					foreach ($item_cats as $cat_id) {
						$item_cat = get_category( $cat_id );
						$cat[] = 'category-' . $item_cat->slug;
						$cat1[] = $item_cat->name;
					} 
					
					$cat_html = '';
					if(is_array($cat)){
						$cat_html = implode(' ',$cat);
					}

					$cat1_html = '';
					if(is_array($cat1)){
						$cat1_html = implode(' ',$cat1);
					}
					
					?>
				
					<li class="element-item <?php echo sanitize_html_classes($cat_html); ?>">
						<div class="blog-box">
							<?php king_latest_news_post_format_content_(); ?>
							<div class="text">
								<h4><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></h4>
								<div class="blog-post-meta">
									<span><?php echo get_the_date(get_option('date_format')); ?>, <?php _e('in', 'king'); ?> <?php echo king_post_categories(get_the_ID()); ?>, <?php _e('by', 'king'); ?> <?php the_author(); ?></span>
								</div>								
								<p><?php king_the_excerpt_theme(18); ?></p>
								<ul class="social-media">
									<li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>"
											onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
											 <i class="fa fa-facebook"></i>
										 </a>
									</li>
									<li>
										 <a target="_blank" class="twitter-share-button"
											href="https://twitter.com/share?url=<?php echo urlencode(get_the_permalink()); ?>&text=<?php echo urlencode(get_the_title()); ?>"
											onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
											 <i class="fa fa-twitter"></i>
										 </a>
									</li>
									<li>
										 <a href="https://plus.google.com/share?url=<?php echo urlencode(get_the_permalink()); ?>"
											onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
											 <i class="fa fa-google-plus"></i>
										 </a>
									</li>
									<li>
										<a href="<?php echo esc_url(get_comments_link()); ?>" class="comments">
											<span><?php echo get_comments_number(); ?> </span>
											<i class="fa fa-comment-o"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</li>
				<?php  endwhile; ?>
			</ul>

			<?php if (!empty($url)): ?>
				<div class="align-center">
					<a href="<?php echo esc_url($url);?>" class="button shaped"><?php echo esc_html($link_label);?></a>
				</div>
			<?php endif; ?>			
		</div>
	<?php endif;

    wp_reset_postdata();
    $html = ob_get_contents();
    ob_end_clean();
	return $html;
}

function king_latest_news_post_format_content_() {
	
	switch (get_post_format()) {
		case 'link': ?>
			<figure>
				<?php  king_the_resized_post_thumb(360, 399, get_the_title(), '', true); ?>
				<figcaption>
					<div class="iconic"><a href="#"><i class="icon-link"></i></a></div>
					<p><?php echo esc_html(king_get_shortened_string(get_post_meta(get_the_ID(),'link_description',true),5));?></p>
					<?php $url = get_post_meta(get_the_ID(),'link',true);
					if (!empty($url)): ?>
						<p class="link"><a href="<?php echo esc_url($url); ?>" target="_blank"><?php echo esc_url($url); ?></a></p>
					<?php endif; ?>
				</figcaption>
			</figure>
			<?php break;
			
		case 'audio': ?>
			<?php $audio_url = get_post_meta(get_the_ID(), 'audio_url', true);
			if ($audio_url != ''): ?>
				<div class="audio-player-box">
					<audio>
						<source class="sc-audio-player" type="audio/mpeg" src="<?php echo esc_url($audio_url); ?>">
						<?php _e('Your browser does not support the audio element.','king'); ?>
					</audio>
				</div>
			<?php endif; ?>
			<?php break;
			
		case 'quote': ?>
			<blockquote class="blockquote">
				<p><?php echo esc_html(king_get_shortened_string(get_post_meta(get_the_ID(),'quote_text',true),15));?></p>
				<p class="author">- <?php _e('By','king'); ?> <?php echo esc_html(get_post_meta(get_the_ID(),'author_text',true));?></p>
			</blockquote>
			<?php break;
			
		case 'video': ?>
			<figure>
				<?php king_the_resized_post_thumb(360, 224, get_the_title(), '', true); 
				$url = get_post_meta(get_the_ID(),'video_url',true); ?>
				<?php if (!empty($url)) { ?>
					<figcaption>
						<div class="iconic-vid"><a href="<?php echo esc_url($url); ?>" rel="prettyphoto"><i class="icon-play"></i></a></div>
					</figcaption>
				<?php } ?>
			</figure>
		
			<?php break;
		
		default: ?>
			<figure>
				<?php  king_the_resized_post_thumb(360, 321, get_the_title(), '', true); ?>
			</figure>
	<?php }	
}
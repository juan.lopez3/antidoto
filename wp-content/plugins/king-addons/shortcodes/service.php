<?php
/**
 * Shortcode Title: Service
 * Shortcode: icon
 * Usage: [icon animation="bounceInUp" icon="icon1.png" con_color="" title="Your title" title_color="#FF0000" content_color=""]Your content here...[/icon]
 */
add_shortcode('service', 'king_service_func');

function king_service_func( $atts, $content = null ) {

	extract(shortcode_atts(array(
		'animation'       => '',
		'icon'            => '',
		'stroke_color'    => '',
		'title'           => '',
		'style'           => 'style1',
		'title_color'     => '',
		'content_color'   => '',
		'animation_delay' => ''
	),
	$atts));
	
	$title_style = '';
	$title_style_arr = array();
    
	if(!empty($title_color)){
        $title_style_arr[] = 'color:'.esc_attr($title_color);
	}
	if(count($title_style_arr) > 0){
        $title_style = 'style="'.implode(';', $title_style_arr).'"';
    }
	
	$content_style = '';
	$content_style_arr = array();
    
	if(!empty($content_color)){
        $content_style_arr[] = 'color:'.esc_attr($content_color);
	}
	if(count($content_style_arr) > 0){
        $content_style = 'style="'.implode(';', $content_style_arr).'"';
    }


  	$data_icon = '';
  	if ( is_numeric( $icon ) && !empty($icon) ) {
    	$image_src  = wp_get_attachment_image_src( $icon, 'full' );
	    if(isset($image_src[0])) {
	      $data_icon = ' data ='.esc_url($image_src[0]).'';
	    }
  	}

  	$data_stroke = ( $stroke_color ) ? ' data-stroke-color="'.esc_attr($stroke_color).'"':'';
  	$data_delay  = ( $animation_delay ) ? ' data-animation-delay="'.esc_attr($animation_delay).'"':'';
	
	
	$output  = '<div class="service '.sanitize_html_class($style).' '.king_get_animation_class($animation).'">';
	if($style != 'style3' ) {
		$output	.=	'<figure>'.wp_get_attachment_image( $icon, 'full' ).'</figure>';
	} else {
		$output	.=	'<div class="icon-container" data-animation-type="delayed"'.$data_stroke.$data_delay.'>';
		$output	.=	'<object type="image/svg+xml" '.$data_icon.'></object>';
		$output	.=	'</div>';
	}
	$output	.=	'<h5 '.$title_style.'>'.esc_html($title).'</h5>';
	$output	.=	'<p '.$content_style.'>'.$content.'</p>';
	$output	.=	'</div>';
	return $output;

}
<?php
/**
 * Shortcode Title: Text
 * Shortcode: text
 * Usage: [text animation="bounceInUp"]Your text here...[/text]
 */
add_shortcode('text', 'king_text_func');

function king_text_func( $atts, $content = null ) {
	
	extract(shortcode_atts(array(
		'animation' => ''
		),
	$atts));
	
	return '<div '.king_get_animation_class($animation,true).' data-animation="'.esc_attr($animation).'">'.do_shortcode($content).'</div>';
}
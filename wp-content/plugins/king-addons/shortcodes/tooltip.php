<?php
/**
 * Shortcode Tooltip
 * Shortcode: tooltip
 * Usage: [tooltip position="top" tooltip="tooltip title" link="http://google.com"]Your content goes here...[/tooltip]
 */

add_shortcode('tooltip', 'king_tooltip_func');
function king_tooltip_func($atts, $content = null)
{
    extract(shortcode_atts(array(
        'link' => '#',
        'tooltip' => '',
        'position' => 'top'
    ), $atts));

    return '<a title="' . esc_url($tooltip) . '" data-placement="' . esc_attr($position) . '" data-toggle="tooltip" class="hover-tooltip" href="'.esc_url($link).'" data-original-title="Text Tooltip">' . $content . '</a>';
}
<?php
/**
 * Shortcode Title: Portfolio
 * Shortcode: portfolio
 * Usage: [portfolio animation="bounceInUp" category="" hover_color="#FF0000"]
 */
add_shortcode('portfolio', 'king_portfolio_func');

function king_portfolio_func($atts, $content = null)
{
    global $post;

    extract(shortcode_atts(array(
		'animation' => '',
		'category' => '',
		'hover_color' => '',
	),
	$atts));
	
	$html = '';
    $args = array(
        'posts_per_page' => 4,
        'meta_query' => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
        'offset' => 0,
        'orderby' => 'date',
        'order' => 'DESC',
        'include' => '',
        'exclude' => '',
        'meta_key' => '',
        'meta_value' => '',
        'post_type' => 'portfolio',
        'post_mime_type' => '',
        'post_parent' => '',
        'paged' => 1,
        'post_status' => 'publish'
    );

    if (!empty($category)) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'portfolio-categories',
                'field' => 'id',
                'terms' => explode(',', $category)
            ),
        );
    }

	ob_start();
	
	$the_query = new WP_Query($args);
    if ($the_query->have_posts()): ?>

		<!-- Featured Projects -->
		<section class="full-width projects-section dark-gray-bg <?php echo king_get_animation_class($animation); ?>">
			<?php while ($the_query->have_posts()):  

				$the_query->the_post();

				$cat1 = array();
				$item_cats = wp_get_post_terms(get_the_ID(), 'portfolio-categories');
				foreach ($item_cats as $item_cat) {
					$cat1[] = $item_cat->name;
				}

				$cat1_html = '';
				if(is_array($cat1)){
					$cat1_html = implode(' ',$cat1);
				} ?>
				
				<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="project style-king">

						<div class="project-image wow animated fadeInLeft">
							<?php king_the_resized_post_thumbnail('featured_projects_5', get_the_title(), 'img-responsive'); ?>
							<div class="project-hover" <?php echo ($hover_color ? 'style="background-color: ' . king_hex_to_rgb($hover_color,'0.9') . '"' : ''); ?>>
								<div>
									<div>
										<span class="category"><?php echo king_get_shortened_string_by_letters($cat1_html, 60); ?></span>
										<span class="separator"></span>
										<h4 class="project-title"><?php echo king_get_shortened_string_by_letters(get_the_title(),25); ?></h4>
										<p><?php echo strip_tags(king_get_the_excerpt_theme(10)); ?></p>
										<a class="project-button" href="<?php echo esc_url(king_img_url()); ?>" rel="prettyPhoto"><?php _e('zoom', 'king'); ?></a>
										<a class="project-button" href="<?php the_permalink(); ?>"><?php _e('view project', 'king');?></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		
		</section>
		<!-- /Featured Projects -->
		<?php wp_reset_postdata(); ?>
    <?php endif;

    $html = ob_get_contents();
    ob_end_clean();
    return $html;
}
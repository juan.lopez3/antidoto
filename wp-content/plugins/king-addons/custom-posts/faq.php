<?php

    register_post_type ( 'faq', array (
        'labels' => array (
            'name' => __ ( 'FAQ', 'king-addons' ),
            'singular_name' => __ ( 'FAQ', 'king-addons' )
        ),
        'public' => true,
        'has_archive' => false,
        'rewrite' => true,
        'supports' => array (
            'title',
            'editor',
            'page-attributes'
        )
    ) );

    register_taxonomy ( "faq-categories", array (
        "faq"
    ), array (
        "hierarchical" => true,
        "label" => __( "Categories", 'king-addons' ),
		"singular_label" => __( "Category", 'king-addons' ),
        "rewrite" => true
    ) );
<?php
/**
 * Testimonial custom posty type
 */

$labels = array(
	'name' => __( 'Testimonials', 'king-addons' ),
	'singular_name' => __( 'Testimonial', 'king-addons' ),
	'add_new' => __( 'Add New', 'king-addons' ),
	'add_new_item' => __( 'Add New Testimonials', 'king-addons' ),
	'edit_item' => __( 'Edit Testimonials', 'king-addons' ),
	'new_item' => __( 'New Testimonials', 'king-addons' ),
	'all_items' => __( 'All Testimonials', 'king-addons' ),
	'view_item' => __( 'View Testimonials', 'king-addons' ),
	'search_items' => __( 'Search Testimonials', 'king-addons' ),
	'not_found' => __( 'No Testimonials found', 'king-addons' ),
	'not_found_in_trash' => __( 'No Testimonials found in Trash', 'king-addons' ),
	'parent_item_colon' => '',
	'menu_name' => __( 'Testimonials', 'king-addons' )
);

 $args = array(
	'labels'        => $labels,
	'public'        => true,
	'show_ui'       => true,
	'menu_position' => 30,
	'capability_type' => 'page',
	'supports'      => array( 'title', 'thumbnail', 'editor', 'page-attributes' ),
	'has_archive'   => true,
	 'rewrite' => 	true,
);

register_post_type ( 'testimonial', $args);

/**
 * Testimonial category
 */

$labels = array(
	'name'              => _x( 'Categories', 'taxonomy general name', 'king-addons' ),
	'singular_name'     => _x( 'Category', 'taxonomy singular name', 'king-addons' ),
	'search_items'      => __( 'Search categories', 'king-addons' ),
	'all_items'         => __( 'All Categories', 'king-addons' ),
	'parent_item'       => __( 'Parent Category', 'king-addons' ),
	'parent_item_colon' => __( 'Parent Category:', 'king-addons' ),
	'edit_item'         => __( 'Edit Category', 'king-addons' ),
	'update_item'       => __( 'Update Category', 'king-addons' ),
	'add_new_item'      => __( 'Add New Category', 'king-addons' ),
	'new_item_name'     => __( 'New Category Name', 'king-addons' ),
	'menu_name'         => __( 'Categories', 'king-addons' ),
);
$args = array(
	'labels' => $labels,
	'hierarchical' => true,
	"singular_label" => __( "Category", 'king-addons' ),
	"rewrite" => true
);
register_taxonomy( 'testimonial-category', 'testimonial', $args );
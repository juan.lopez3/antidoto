<?php

register_post_type ( 'food_menu', array (
    'labels' => array (
    'name' => __ ( 'Food Menu', 'king-addons' ),
    'singular_name' => __ ( 'Food Menu', 'king-addons' ),
    'add_new' => __ ( 'Add New', 'king-addons' ),
    'add_new_item' => __ ( 'Add New Item', 'king-addons' ),
    'edit_item' => __ ( 'Edit Item', 'king-addons' ),
    'new_item' => __ ( 'New Items', 'king-addons' ),
    'all_items' => __ ( 'All Items', 'king-addons' ),
    'view_item' => __ ( 'View Items', 'king-addons' ),
    'search_items' => __ ( 'Search Items', 'king-addons' ),
    'not_found' => __ ( 'No Items found', 'king-addons' ),
    'not_found_in_trash' => __ ( 'No Items found in Trash', 'king-addons' ),
    'parent_item_colon' => '',
    'menu_name' => __ ( 'Food Menu', 'king-addons' )
    ),
    'public' => true,
    'has_archive' => false,
    'rewrite' => true,
    'capability_type' => 'page',
    'supports' => array (
        'title',
        'editor',
        'thumbnail',
        'page-attributes'
    )
) );
    
register_taxonomy ( "menu-categories", array (
    "food_menu"
), array (
    "hierarchical" => true,
    "label" => __( "Categories", 'king-addons' ),
    "singular_label" => "Category",
    "rewrite" => true
) );
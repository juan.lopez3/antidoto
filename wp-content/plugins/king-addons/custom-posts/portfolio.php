<?php
/**
 * Portfolio custom posty type
 */
$labels = array(
	'name'               => _x( 'Portfolio', 'Items','king-addons' ),
	'singular_name'      => _x( 'Portfolio', 'Item','king-addons' ),
	'add_new'            => _x( 'Add New', 'Item','king-addons' ),
	'add_new_item'       => __( 'Add New Item','king-addons' ),
	'edit_item'          => __( 'Edit Item','king-addons' ),
	'new_item'           => __( 'New Item','king-addons' ),
	'all_items'          => __( 'All Items','king-addons' ),
	'view_item'          => __( 'View Item','king-addons' ),
	'search_items'       => __( 'Search Items','king-addons' ),
	'not_found'          => __( 'No Items found','king-addons' ),
	'not_found_in_trash' => __( 'No Items found in the Trash','king-addons' ),
	'parent_item_colon'  => '',
	'menu_name'          => __('Portfolio', 'king-addons')
);
$args = array(
	'labels'        => $labels,
	'description'   => __('Holds our products and product specific data', 'king-addons'),
	'public'        => true,
	'menu_position' => 21,
	'rewrite' 		=> array ('slug' => __ ( 'portfolio-item', 'king-addons' )),
	'supports'      => array( 'title', 'thumbnail','editor', 'comments' ),
	'has_archive'   => true,

);
register_post_type( 'portfolio', $args );

/**
 * Portflio category
 */
$labels = array(
	'name'              => _x( 'Categories', 'taxonomy general name', 'king-addons' ),
	'singular_name'     => _x( 'Category', 'taxonomy singular name', 'king-addons' ),
	'search_items'      => __( 'Search categories', 'king-addons' ),
	'all_items'         => __( 'All Categories', 'king-addons' ),
	'parent_item'       => __( 'Parent Category', 'king-addons' ),
	'parent_item_colon' => __( 'Parent Category:', 'king-addons' ),
	'edit_item'         => __( 'Edit Category', 'king-addons' ),
	'update_item'       => __( 'Update Category', 'king-addons' ),
	'add_new_item'      => __( 'Add New Category', 'king-addons' ),
	'new_item_name'     => __( 'New Category Name', 'king-addons' ),
	'menu_name'         => __( 'Categories' ),
);
$args = array(
	'labels' => $labels,
	'hierarchical' => true,
);
register_taxonomy( 'portfolio-categories', 'portfolio', $args );

$taxonomy_tags_args = array(
    'hierarchical'       => false,
    'label'              => 'Tags',
    'singular_label'     => 'Tags',
    'rewrite'            => array('slug'=> 'portfolio-tag' ),
);
register_taxonomy('portfolio-tag', array('portfolio'), $taxonomy_tags_args);

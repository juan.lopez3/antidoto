<?php
/**
 * Team custom post type
 */
$labels = array(
	'name'               => _x( 'Artista', 'Artista','king-addons' ),
	'singular_name'      => _x( 'Artista', 'Artista','king-addons' ),
	'add_new'            => _x( 'Add New', 'Team','king-addons' ),
	'add_new_item'       => __( 'Add New Team Member','king-addons' ),
	'edit_item'          => __( 'Edit Team Memeber','king-addons' ),
	'new_item'           => __( 'New Team Member','king-addons' ),
	'all_items'          => __( 'All Team Memebers','king-addons' ),
	'view_item'          => __( 'View Team','king-addons' ),
	'search_items'       => __( 'Search Team Member','king-addons' ),
	'not_found'          => __( 'No Member found','king-addons' ),
	'not_found_in_trash' => __( 'No Team Member found in the Trash','king-addons' ), 
	'parent_item_colon'  => '',
	'menu_name'          => __('Artista', 'king-addons'),
        
);
$args = array(
	'labels'        => $labels,
	'public'        => true,
	'capability_type' => 'post',
	'show_ui'       => true,
	'menu_position' => 21,
	'supports'      => array( 'title' ,'thumbnail', 'editor', ),
                         'taxonomies'=>array('category'),
	'has_archive'   => false,
	'rewrite' => 	true,
);

register_post_type( 'team', $args ); 

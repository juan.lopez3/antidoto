;(function ( $, window, document, undefined ) {
  'use strict';


  $.reloadPlugins = function() {

    $('.icon-select').chosen({
        template: function (text, value, templateData) {
            return ['<i class="' + templateData.icon + '"></i> ' + text].join('');
        }
    });
	$('.wpb_chosen').chosen();
	$('.chzn-drop').css({"width": "300px"});
  };

  var Shortcodes = vc.shortcodes;

  //
  // ATTS
  // -------------------------------------------------------------------------
  _.extend(vc.atts, {
    vc_efa_chosen:{
      parse:function ( param ) {
        var value = this.content().find('.wpb_vc_param_value[name=' + param.param_name + ']').val();
        return ( value ) ? value.join(',') : '';
      }
    },

    vc_icon: {
      parse:function( param ) {
        var value = this.content().find('.wpb_vc_param_value[name=' + param.param_name + ']').val();
        console.log(value);
        return ( value ) ? value : '';
      }
    },
  });

})( jQuery, window, document );

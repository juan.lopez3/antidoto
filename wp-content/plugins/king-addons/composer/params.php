<?php
/**
  * WPBakery Visual Composer Extra Params
  *
  * @package VPBakeryVisualComposer
  *
 */
  

function vc_icon($settings, $value) {

  $css_option = vc_get_dropdown_option( $settings, $value );
  
  $values = king_getFontAwesomeArray(true);
  $value = explode( ',', $value );

  
  $output  = '<select name="'. $settings['param_name'] .'" data-placeholder="'. $settings['placeholder'] .'" class="wpb_vc_param_value wpb_chosen chosen icon-select wpb-input wpb-rs-select  '. $settings['param_name'] .' '. $settings['type'] .' '. $css_option .'" data-option="'. $css_option .'">';

  foreach ( $values as $key => $val ) {
    $selected = ( in_array( $val, $value ) ) ? ' selected="selected"' : '';
    $output .= '<option data-icon="'. $key .'" value="'. $val .'"'. $selected .'>'.htmlspecialchars( $key ).'</option>';
  }

  $output .= '</select>' . "\n";
   
  return $output;
}


add_shortcode_param('vc_icon', 'vc_icon');

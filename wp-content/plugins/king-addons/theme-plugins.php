<?php
/*
Plugin Name: King Addons
Plugin URI: http://www.medialayout.com
Description: A part of King theme
Version: 1.0
Author: Medialayout Team
Author URI: http://www.medialayout.com
Text Domain: king-addons
*/

// Define Constants
defined('RS_ROOT') or define('RS_ROOT', dirname(__FILE__));

/**
 * Shortcodes
 */
if(!class_exists('RS_Shortcode') && class_exists('Vc_Manager'))
{
  /**
   * Main plugin class
   */
  class RS_Shortcode
  {

    private $assets_css;
    private $assets_js;

  /**
   * Construct
   */
    public function __construct() {
      add_action('init', array($this,'rs_init'),50);
      add_action('setup_theme', array($this,'rs_load_custom_post_types'),40);

    }

  /**
   * Plugin activation
   */
  public static function activate() {
    flush_rewrite_rules();
  }

  /**
   * Plugin deactivation
   */
  public static function deactivate() {
    flush_rewrite_rules();
  }

  /**
   * Init
   */
  public function rs_init() {

    if (!defined('KING_THEME_ACTIVATED') || KING_THEME_ACTIVATED !== true) {
       add_action( 'admin_notices', array($this,'rs_activate_theme_notice') );

    } else {


      $this->assets_css = plugins_url('/composer/assets/css', __FILE__);
      $this->assets_js  = plugins_url('/composer/assets/js', __FILE__);
      add_action( 'admin_print_scripts-post.php',   array($this, 'rs_load_vc_scripts'), 99);
      add_action( 'admin_print_scripts-post-new.php', array($this, 'rs_load_vc_scripts'), 99);
      add_action( 'admin_print_scripts-widgets.php', array($this, 'rs_load_widget_scripts'), 99);
      add_action('vc_load_default_params', array($this, 'rs_reload_vc_js'));
      $this->rs_vc_load_shortcodes();
      $this->rs_init_vc();
      $this->rs_vc_integration();
    }
  }

  /**
   * Print theme notice
   */
  function rs_activate_theme_notice() { ?>
    <div class="updated">
      <p><strong><?php _e('Please activate the King theme to use King Addons plugin.', 'king-addons'); ?></strong></p>
      <?php
      $screen = get_current_screen();
      if ($screen -> base != 'themes'): ?>
        <p><a href="<?php echo esc_url(admin_url('themes.php')); ?>"><?php _e('Activate theme', 'king-addons'); ?></a></p>
      <?php endif; ?>
    </div>
  <?php }

  /**
   * Init VC integration
   * @global type $vc_manager
   */
    public function rs_init_vc()
    {
      global $vc_manager;
      $vc_manager->setIsAsTheme();
      $vc_manager->disableUpdater();
      $list = array( 'page', 'post', 'portfolio' );
      $vc_manager->setEditorDefaultPostTypes( $list );
      $vc_manager->setEditorPostTypes( $list ); //this is required after VC update (probably from vc 4.5 version)
      //$vc_manager->frontendEditor()->disableInline(); // enable/disable vc frontend editior
      $vc_manager->automapper()->setDisabled();
    }

  /**
   * Load custom post types
   */
  public function rs_load_custom_post_types()
  {
    require_once(RS_ROOT .'/custom-posts/food.php');
    require_once(RS_ROOT .'/custom-posts/team.php');
    require_once(RS_ROOT .'/custom-posts/testimonial.php');
    require_once(RS_ROOT .'/custom-posts/portfolio.php');
    require_once(RS_ROOT .'/custom-posts/faq.php');
  }


  /**
   * Load shortcodes
   */
    public function rs_vc_load_shortcodes()
    {
      require_once RS_ROOT . '/shortcodes/accordion.php';
      require_once RS_ROOT . '/shortcodes/accordion_2.php';
      require_once RS_ROOT . '/shortcodes/map_container.php';
      require_once RS_ROOT . '/shortcodes/blog.php';
      require_once RS_ROOT . '/shortcodes/blog_3.php';
      require_once RS_ROOT . '/shortcodes/col-offset.php';
      require_once RS_ROOT . '/shortcodes/content_box.php';
      require_once RS_ROOT . '/shortcodes/contact_form_2.php';
      require_once RS_ROOT . '/shortcodes/contact_form_3.php';
      require_once RS_ROOT . '/shortcodes/contact_form_4.php';
      require_once RS_ROOT . '/shortcodes/blockquote.php';
      require_once RS_ROOT . '/shortcodes/project_gallery.php';
      require_once RS_ROOT . '/shortcodes/alerts.php';
      require_once RS_ROOT . '/shortcodes/banner.php';
      require_once RS_ROOT . '/shortcodes/button.php';
      require_once RS_ROOT . '/shortcodes/button_2.php';
      require_once RS_ROOT . '/shortcodes/button_view.php';
      require_once RS_ROOT . '/shortcodes/button_more.php';
      require_once RS_ROOT . '/shortcodes/columns.php';
      require_once RS_ROOT . '/shortcodes/call_to_action.php';
      require_once RS_ROOT . '/shortcodes/call_to_action_2.php';
      require_once RS_ROOT . '/shortcodes/call_to_action_3.php';
      require_once RS_ROOT . '/shortcodes/counter.php';
      require_once RS_ROOT . '/shortcodes/counter_2.php';
      require_once RS_ROOT . '/shortcodes/divider.php';
      require_once RS_ROOT . '/shortcodes/dropcaps.php';
      require_once RS_ROOT . '/shortcodes/food_menu.php';
      require_once RS_ROOT . '/shortcodes/food_menu_2.php';
      require_once RS_ROOT . '/shortcodes/form.php';
      require_once RS_ROOT . '/shortcodes/headers.php';
      require_once RS_ROOT . '/shortcodes/highlight.php';
      require_once RS_ROOT . '/shortcodes/icon.php';
      require_once RS_ROOT . '/shortcodes/icon_list.php';
      require_once RS_ROOT . '/shortcodes/info_box.php';
      require_once RS_ROOT . '/shortcodes/info_box_2.php';
      require_once RS_ROOT . '/shortcodes/info_box_3.php';
      require_once RS_ROOT . '/shortcodes/image.php';
      require_once RS_ROOT . '/shortcodes/latest_news.php';
      require_once RS_ROOT . '/shortcodes/latest_news_2.php';
      require_once RS_ROOT . '/shortcodes/latest_works.php';
      require_once RS_ROOT . '/shortcodes/latest_works_2.php';
      require_once RS_ROOT . '/shortcodes/list.php';
      require_once RS_ROOT . '/shortcodes/logos.php';
      require_once RS_ROOT . '/shortcodes/multi_posts.php';
      require_once RS_ROOT . '/shortcodes/person_slider.php';
      require_once RS_ROOT . '/shortcodes/portfolio.php';
      require_once RS_ROOT . '/shortcodes/portfolio_2.php';
      require_once RS_ROOT . '/shortcodes/portfolio_3.php';
      require_once RS_ROOT . '/shortcodes/portfolio_4.php';
      require_once RS_ROOT . '/shortcodes/pricing_column.php';
      require_once RS_ROOT . '/shortcodes/pricing_table.php';
      require_once RS_ROOT . '/shortcodes/recent_news_big.php';
      require_once RS_ROOT . '/shortcodes/recent_news_big_2.php';
      require_once RS_ROOT . '/shortcodes/service.php';
      require_once RS_ROOT . '/shortcodes/skillbar.php';
      require_once RS_ROOT . '/shortcodes/social_icons.php';
      require_once RS_ROOT . '/shortcodes/space.php';
      require_once RS_ROOT . '/shortcodes/special_text.php';
      require_once RS_ROOT . '/shortcodes/tabs.php';
      require_once RS_ROOT . '/shortcodes/blogtimeline.php';
      require_once RS_ROOT . '/shortcodes/team_members.php';
      require_once RS_ROOT . '/shortcodes/team_members_2.php';
      require_once RS_ROOT . '/shortcodes/team_members_3.php';
      require_once RS_ROOT . '/shortcodes/testimonials_2.php';
      require_once RS_ROOT . '/shortcodes/testimonials_3.php';
      require_once RS_ROOT . '/shortcodes/testimonials_4.php';
      require_once RS_ROOT . '/shortcodes/text.php';
      require_once RS_ROOT . '/shortcodes/tooltip.php';
      require_once RS_ROOT . '/shortcodes/welcome.php';
      require_once RS_ROOT . '/shortcodes/welcome_2.php';
      require_once RS_ROOT . '/shortcodes/video_player.php';
      require_once RS_ROOT . '/shortcodes/testimonial_single.php';
      require_once RS_ROOT . '/shortcodes/team_member_single.php';
      require_once RS_ROOT . '/shortcodes/footer_map.php';

      require_once RS_ROOT . '/shortcodes/vc_row.php';
      require_once RS_ROOT . '/shortcodes/vc_column.php';
      require_once RS_ROOT . '/shortcodes/vc_column_text.php';
    }

  /**
   * Visual composer integration
   */
    public function rs_vc_integration()
    {
      require_once( RS_ROOT .'/' .'composer/map.php' );
    }

  /**
   * Loand vc scripts
   */
    public function rs_load_vc_scripts()
    {
      wp_enqueue_style( 'rs-chosen',    $this->assets_css. '/chosen.css' );
      wp_enqueue_script( 'vc-script',   $this->assets_js . '/vc-script.js' ,      array('jquery'), '1.0.0', true );
      wp_enqueue_script( 'vc-chosen',   $this->assets_js . '/jquery.chosen.js' ,  array('jquery'), '1.0.0', true );
    }

  /**
   * Reload JS
   */
    public function rs_reload_vc_js()
    {
      echo '<script type="text/javascript">(function($){ $(document).ready( function(){ $.reloadPlugins(); }); })(jQuery);</script>';
    }

  } // end of class

  new RS_Shortcode;

  register_activation_hook( __FILE__, array( 'RS_Shortcode', 'activate' ) );
  register_deactivation_hook( __FILE__, array( 'RS_Shortcode', 'deactivate' ) );

} // end of class_exists



<?php
/*
Plugin Name: custom-type-artistas
*/

function registroart(){
   
register_post_type('artistas', array(
      'labels' => array('name' => 'artistas',
                        'singular_name' => 'artistas'
                       ),
      'description' => 'artistas',
      'public' => true,
       'show_ui'       => true,
      'menu_position' => 5,
      'supports' => array('title' ,'thumbnail', 'editor'),
      'taxonomies'=>array('category'),
   ));
    
 register_post_type('programacion', array(
      'labels' => array('name' => 'programacion',
                        'singular_name' => 'programacion'
                       ),
      'description' => 'programacion',
      'public' => true,
       'show_ui'       => true,
      'menu_position' => 5,
      'supports' => array('title')
   ));
}

add_action('init', 'registroart',1);
function __query_dias_programacion(){
    
    $arg_prog = array(
            'post_type'=>'programacion',
            /*'post_status' => 'publish',
            'meta_key'=>'dia_programacion',
            'orderby'=>'meta_value_num',
            'order'     => 'ASC',
            'meta_query' => array(
                'relation' => 'AND',
                array('key'=>'mes_programacion',
                    'value'=>  mes_castellano(date('m')),
                    'compare'=>'='
                    ),
                array('key'=>'anio',
                    'value'=> date('Y'),
                    'compare'=>'='
                    )
            )*/
        );
                                
    $query_prog = new WP_Query($arg_prog);
    return $query_prog;
    
}

function programacion_evento(){
    $output = "";
    
    $query_prog = __query_dias_programacion();
    
    if(!$query_prog->have_posts()){
        $output .= "no tenemos aún días para el evento";
        return $output;
    }
    
                 
    while ($query_prog->have_posts()){
        $output .= "<div class='item-programacion'>";
        $query_prog->the_post();
        $dia_programacion = get_field('dia_programacion');
        $mes_programacion = get_field('mes_programacion');
        $duracion = get_field('hora_evento').":".get_field('minutos_evento');
        
        $lugar = the_title("","",false);
        
        $costo = (get_field('costo_evento')==0)?'GRATIS':'$'.get_field('costo_evento');
        
        $output .= "<div class='fecha'>";
        $output .= "<div class='dia'>".$dia_programacion."</div>";
        $output .= "<div class='mes'>".$mes_programacion."</div>";
        $output .= "<div class='duracion'>".$duracion."</div>";
        $output .= "</div>";
        
        $output .= "<div class='lugar'>".$lugar."</div>";
        $output .= "<div class='costo'>".$costo."</div>";
     
        $output .= "</div>";
        
    }

    
    $output = "<div id='items_programacion'>".$output."</div>";
    
    wp_reset_query();
    return $output;

}


add_shortcode('programacion_evento', 'programacion_evento');

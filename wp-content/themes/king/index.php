<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package king
 * @since king 1.0
 */
get_header();
$url = king_get_theme_next_page_url();


$blog_style = ot_get_option('blog_style','list');


if ($blog_style == 'timeline'):
	
	wp_enqueue_script ( 'king-jquery-masonry2' );
	
    $posts_per_page = get_option('posts_per_page'); ?>
    <script>
        var timelinePerPage = <?php echo esc_js(intval($posts_per_page));?>;
        var timelineOffset = 0;
        var timelineOffsetNext = (   timelinePerPage * timelineOffset ) + timelinePerPage; // Current

        // Offset of page
        var currentMonth = null;
        var currentYear = null;
        var templateUrl = "<?php echo get_template_directory_uri();?>";

    </script>

    <section class="full-width-bg normal-padding medium-gray-bg ">
		<div class="timeline-container-wrap">
        <?php if (have_posts()) : ?>
        <?php /* Start the Loop */ ?>
        <?php
        $alternate_count = 0;
        $counter=0;
        while (have_posts()) :
			$counter++;

			the_post();

			if (!isset($this_month)) {
				$this_month = get_the_date("F");
				$this_year = get_the_date("Y");
			}
			$current_month = get_the_date("F");
			$current_year = get_the_date("Y");
			?>
			<?php if ($counter == 1): ?>
				<div class="timeline-date-tooltip">
					<span><?php echo get_the_date('F Y'); ?></span>
				</div>
				<div class="row timeline-row">
					<div class="masonry-container timeline-container">
			<?php else: ?>
				<?php
				if (($this_month == $current_month && $this_year == $current_year) || $counter == 1):

				else:
					$this_month = get_the_date("F");
					$this_year = get_the_date("Y");
					echo '
						</div></div>
						<div class="timeline-date-tooltip timeline-date-tooltip-top">
							<span>' . get_the_date('F Y') . '</span>
						</div>
						<div class="row timeline-row">
							<div class="masonry-container timeline-container">';
				endif;
			endif;
				?>
					<div class="col-sm-6 masonry-box" data-year="<?php echo get_the_date('Y'); ?>" data-month="<?php echo get_the_date('F'); ?>">
						<div class="blog-post masonry timeline">
							<!-- POST FOOTER -->
							<div class="post-footer">
								<?php echo get_avatar(get_the_author_meta('user_email'), '60'); ?>
								<span class="post-date">
									<span class="post-day"><?php echo get_the_date('d M'); ?> </span>
									<?php echo get_the_date('Y'); ?>
								</span>
								<ul class="post-meta">
									<li> <?php _e('By', 'king'); ?> <?php the_author(); ?> </li>
									<li><?php echo king_post_categories(get_the_ID()); ?> </li>
								</ul>
							</div>
							<!-- END POST FOOTER -->
							<?php get_template_part('inc/timeline-content/content', get_post_format()); ?>
						</div>
					</div>

				<?php endwhile;
				wp_reset_query();
				wp_reset_postdata();?>
				<?php else : //No posts were found ?>
					<?php get_template_part('no-results'); ?>
				<?php
				endif;
				?>
			</div>
		</div>
		</div> <!-- .masonry-container-wrapper -->		
		<div class="align-center load-more">
			<a class="button big blue button-load-more" id="timeline-load-more" href="#"><?php _e('Load More', 'king'); ?></a>
		</div>
	</section>

<?php elseif ($blog_style == 'grid'): 
	wp_enqueue_script ( 'king-jquery-masonry2' );
	
	?>
	<div class="king-blog blog-grid-home">
		<div class="container">
			<div class="masonry-container">
				<div class="grid-sizer"></div>
				<?php if (have_posts()) : ?>
					<div id="post-items">
						<div class="row">
							<?php /* Start the Loop */ 
							while (have_posts()) : the_post(); ?>
								<div class="masonry-box col-md-4">
									<?php   get_template_part('inc/king-content/content',get_post_format());?>
								</div>
							<?php endwhile;  ?>
						</div>
					</div>
					<?php wp_reset_query(); ?>
				<?php else : //No posts were found ?>
					<?php get_template_part('no-results'); ?>
				<?php endif; ?>
			</div>
			<div class="row">
				<div class="align-center load-more">
					<?php 
					if (!empty($url)): ?>
						<a class="button big blue button-load-more" id="load-more" href="<?php echo esc_url($url); ?>" data-loading="<?php _e('Loading posts', 'king'); ?>"><?php _e('Load More', 'king'); ?></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	
<?php elseif ($blog_style == 'king'): ?>
	<!-- King Blog -->
	<div class="king-blog">
		<div class="container">
			<div class="row">
				<div class="col-md-8 no-padding">
					<div class="king-blog-main">

						<?php
						global $wp_query;
						$max_num_pages = $wp_query->max_num_pages;
						?>

						<?php if (have_posts()) : 
							wp_enqueue_script ( 'king-fitvids' ); ?>
							<div class="king-blog-posts">
								<?php /* Start the Loop */
								while (have_posts()) : the_post(); ?>
									<?php get_template_part('inc/king-content/content', get_post_format()); ?>
								<?php endwhile; ?>
							</div><!-- /King Blog Posts -->

							<?php $url = king_get_theme_next_page_url($max_num_pages);
							if (!empty($url)): ?>
								<div class="align-center">
									<a href="<?php echo esc_url($url); ?>" data-loading="<?php echo esc_attr(__('Loading posts', 'king')); ?>" id="king-load-more" class="button shaped"><?php _e('Load More', 'king'); ?></a>
								</div>
							<?php endif; ?>
						<?php else : //No posts were found ?>
							<?php get_template_part('no-results'); ?>
						<?php endif; ?>
					</div>
				</div>
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div><!-- /King Blog -->
	
<?php else: ?>
	<div class="king-page">
		<!-- container -->
		<div class="container">
			<div class="row">
				<div class="col-md-8 small-padding">
					<section class="main-content">
						<?php if (have_posts()) : ?>
							<div id="post-items" class="row">
								<?php /* Start the Loop */
								while (have_posts()) : the_post(); ?>
									<?php get_template_part('content', get_post_format()); ?>
								<?php endwhile; ?>
							</div>
							<div class="row">
								<div class="align-center load-more">
									<?php 
									if (!empty($url)): ?>
										<a class="button big blue button-load-more" id="load-more" href="<?php echo esc_url($url); ?>" data-loading="<?php _e('Loading...', 'king'); ?>"><?php _e('Load More', 'king'); ?></a>
									<?php endif; ?>
								</div>
							</div>
						<?php else : //No posts were found ?>
							<?php get_template_part('no-results'); ?>
						<?php endif; ?>
					</section>
				</div>

				<?php get_sidebar(); ?>
			</div>
		</div>
		<!-- /container -->
	</div>
	<!-- /king-page -->
<?php endif; ?>
<?php get_footer();
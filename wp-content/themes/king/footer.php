<?php
/**
 * The Footer for our theme.
 *
 * @package king
 * @since king 1.0
 */
king_footer_page();

$show_shop_footer = get_post_meta($post->ID,'show_shop_footer',true);

if(empty($show_shop_footer)){
    $show_shop_footer = ot_get_option('show_shop_footer');
}
if($show_shop_footer == 'yes'):
?>
	<!-- Shop footer -->
	<section class="section normal-padding shop-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<?php dynamic_sidebar( 'shop-footer-area-1' ); ?>
				</div>
				<div class="col-md-3 col-sm-6">
					<?php dynamic_sidebar( 'shop-footer-area-2' ); ?>
				</div>
				<div class="col-md-6 col-sm-12">
					<?php dynamic_sidebar( 'shop-footer-area-3' ); ?>
				</div>
			</div>
		</div>
	</section>
	<!-- /Shop footer -->
<?php endif;?>
</div>
<!-- /Container -->
</section>
<!-- /Main Content -->


</div>
<!-- /Content Inner -->




<!-- Footer -->
<?php
$show_footer = get_post_meta($post->ID,'show_footer',true);

if(empty($show_footer)){
    $show_footer = ot_get_option('show_footer');
}
$footer_class = null;
$footer_style = 1;
$footer_btp_style = '';
$footer_style_opt = king_get_main_footer_style();

if ($footer_style_opt == 'alternative'):
	$footer_style = 2;
	$footer_class = 'footer-2';
elseif ($footer_style_opt == 'king' || ($footer_style_opt == 'king_alt') || ($footer_style_opt == 'king_alt_2')):
	$footer_style = $footer_style_opt;
	$footer_class = 'style-king';
elseif ($footer_style_opt == 'king_light'):
	$footer_style = 'king';
	$footer_btp_style = 'style-king';
	$footer_class = 'style-king style-light';
endif;

$hide_footer_widgets = king_get_opt('hide_footer_widgets');

if($show_footer == 'yes'):

?>
<footer id="footer" <?php echo !empty($footer_class) ? 'class="'.$footer_class.'"' : ''; ?>>
    <!-- Main Footer -->
	<?php if ($footer_style == 2): ?>		
		<div id="main-footer" class="smallest-padding">
				
			<div class="container">

				<div class="row">
					
					<?php get_sidebar('footer-2'); ?>

				</div>

			</div>

		</div>
	
	<?php else: //$footer_style = 1,king ?>
		
		<?php if ($hide_footer_widgets != 'yes'): ?>
			<div id="main-footer" class="smallest-padding">
				<div class="container">
					<div class="row">

						<?php get_sidebar('footer'); ?>

					</div>
				</div>
			</div>
		<?php endif; ?>
	<?php endif; ?>
	<!-- /Main Footer -->
    
	<?php if ($footer_style == 'king'): ?>
		<!-- Lower Footer -->
			<div id="lower-footer" class="king">
				<div class="container">
					<?php get_template_part('inc/social-icons'); ?>
					<span class="copyright"><?php echo ot_get_option('footer_text'); ?></span>
				</div>
			</div>
			<!-- /Lower Footer -->

	<?php elseif($footer_style == 'king_alt'): ?>
		<!-- Lower Footer -->
			<div id="lower-footer" class="king_alt">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<div class="left-sec">
								<span class="copyright"><?php echo ot_get_option('footer_text'); ?></span>
							</div>
						</div>
						<div class="col-md-6">
							<div class="right-sec">
								<?php get_template_part('inc/social-icons'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /Lower Footer -->

	<?php elseif($footer_style == 'king_alt_2'): ?>
		<!-- Lower Footer -->
			<div id="lower-footer" class="king_alt_2">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<?php echo do_shortcode(wp_kses_post(ot_get_option('footer_text_left'))); ?>
						</div>
						<div class="col-md-4">
							<?php echo do_shortcode(wp_kses_post(ot_get_option('footer_text_center'))); ?>
						</div>
						<div class="col-md-4">
							<span class="copyright"><?php echo ot_get_option('footer_text'); ?></span>
							<?php get_template_part('inc/social-icons'); ?>
						</div>
					</div>
				</div>
			</div>
			<!-- /Lower Footer -->

	<?php else: ?>	
		<!-- Lower Footer -->
		<div id="lower-footer" class="default-style">
			<div class="container">
				<span class="copyright"><?php echo ot_get_option('footer_text'); ?></span>
			</div>
		</div>
		<!-- /Lower Footer -->
	<?php endif; ?>
	
		
	
</footer>
<?php endif; ?>
<div id="back-to-top" <?php echo ($footer_btp_style == 'style-king' ? 'class="style-king"' : ''); ?>>
	<a href="#"></a>
</div>
<!-- /Footer -->


</div>
<!-- /Content Wrapper -->


<?php echo ot_get_option('scripts_footer'); ?>
<div class="media_for_js"></div>
<?php wp_footer(); ?>

</body>
</html>
<?php
/**
 * The Template for displaying single portfolio.
 *
 * @package king
 * @since king 1.0
 */


get_header();
$xs_portfolio_page = king_get_current_language_post_id(ot_get_option('portfolio_page'));
$xs_portfolio_link = get_page_link($xs_portfolio_page);

$xs_prev = king_mod_get_adjacent_post('prev', array('portfolio'));
$xs_next = king_mod_get_adjacent_post('next', array('portfolio'));

$xs_portfolio_style = get_post_meta($post->ID, 'portfolio_single_style', true);

if (empty($xs_portfolio_style) || $xs_portfolio_style == 'default') {
    $xs_portfolio_style = ot_get_option('portfolio_single_style');
}

$xs_post_format = get_post_format($post);

if ($xs_portfolio_style == 'extended'):
	
	wp_enqueue_script ( 'king-jquery-flexslider' );
	
    switch ($xs_post_format):
        case 'gallery':
            $gal_imgs = get_post_meta($post->ID, 'gallery_images', true); ?>
            <section class="full-width portfolio-extended-image">
                <?php if (is_array($gal_imgs)): ?>
                    <div class="flexslider main-flexslider light">
                        <ul class="slides">
                            <?php foreach ($gal_imgs as $img): ?>
                                <li class="align-center dark">
                                    <?php echo king_get_image($img['image'], 'img-responsive', $img['title'], 'full'); ?>                                    
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php else: ?>
                    <?php the_post_thumbnail('full', array('class' => 'img-responsive', 'alt' => get_the_title())); ?>
                <?php endif; ?>
            </section>
            <?php
            break;

        case 'video':
			
			wp_enqueue_script ( 'king-fitvids' );
            ?>
            <section class="full-width format-video portfolio-extended-image">
                <?php echo king_get_embaded_video(get_post_meta($post->ID, 'video_url', true)); ?>
            </section>
            <?php break;
        default:
            ?>
            <section class="full-width format-video portfolio-extended-image">
				<?php the_post_thumbnail('full', array('class' => 'img-responsive', 'alt' => get_the_title())); ?>
			</section>
			<?php break;
    endswitch; ?>
<?php endif; ?>

<?php
$titlebar_background = get_post_meta($post -> ID, 'titlebar_background', true);
$titlebar_background_url = '';
$titlebar_background_url_boxed = '';
$class = 'dark-blue-bg';
$class_boxed = '';
if (!empty($titlebar_background)):
	$titlebar_background_url = $titlebar_background;
	
	$title_background_position = get_post_meta(get_the_ID(), 'title_background_position', true);
    if (empty($title_background_position) || $title_background_position == 'default'):
        $title_background_position = ot_get_option('default_title_background_position');
    endif;
    if (!empty($title_background_position) && $title_background_position != 'default'):
        $class .= ' ' . $title_background_position;
    endif;

    $title_background_size = get_post_meta(get_the_ID(), 'title_background_size', true);
	
	if (empty($title_background_size) || $title_background_size == 'global'):
        $title_background_size = ot_get_option('default_title_background_size');
	endif;
	
	if (!empty($title_background_size) && $title_background_size != 'default'):
        $class .= ' ' . $title_background_size;
    endif;

endif;

if (is_singular(array('post', 'page')) && $page_body_class = get_post_meta(get_the_ID(), 'body_class', true) ):
	$body_class = $page_body_class;
else:
	$body_class = ot_get_option('body_class');
endif;

if (in_array($body_class,array('b1170','boxed-layout2'))):
	$titlebar_background_url_boxed = $titlebar_background_url;
	$titlebar_background_url = '';

	$class_boxed = $class;
	$class = '';
endif;

?>

<!-- Page Heading -->
<section class="full-width page-heading style2 portfolio-heading <?php echo sanitize_html_classes($class); ?>  <?php if($xs_portfolio_style == 'extended') echo 'portfolio-extended-heading'; ?>" <?php echo ('' != $titlebar_background_url ? 'style="background-image: url('.esc_url($titlebar_background_url).')"' : ''); ?>>
	<div class="container <?php echo sanitize_html_classes($class_boxed); ?>" <?php echo ('' != $titlebar_background_url_boxed ? 'style="background-image: url('.esc_url($titlebar_background_url_boxed).')"' : ''); ?>>
		<div class="row">
			<div class="col-sm-2">
				<a href="<?php echo esc_url($xs_portfolio_link); ?>" class="portfolio-button"></a>
			</div>
			<div class="col-sm-8">
				<h1><span class="extra-bold huge"><?php the_title(); ?></span></h1>
                <?php king_the_breadcrumbs(); ?>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 portfolio-arrows">
				<?php if (!empty($xs_prev) && $xs_prev->ID != ''): ?>
					<a href="<?php echo esc_url(get_permalink($xs_prev->ID)); ?>" class="portfolio-prev"></a>
				<?php endif; ?>
				<?php if (!empty($xs_next) && $xs_next->ID != ''): ?>
					<a class="portfolio-next" href="<?php echo esc_url(get_permalink($xs_next->ID)); ?>"></a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>
<!-- /Page Heading -->

<?php

switch ($xs_portfolio_style) {

    case 'extended':
        get_template_part('inc/portfolio-single/single', 'extended');
        break;
    case 'basic':
    default:
        get_template_part('inc/portfolio-single/single', 'basic');
        break;
}
get_footer(); ?>

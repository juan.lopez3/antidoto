<?php
/**
 * The template for displaying Comments.
 *
 * @package king
 * @since king 1.0
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */

global $post;
$show_comment_form = get_post_meta($post->ID,'show_comment_form',true);


if ( post_password_required() ) {
    return;
}
?>
<div  class="king-post-comments">
	<h3 class="section-heading"><?php _e('Comments', 'king'); ?> <span class="comments-count"><?php echo number_format_i18n(get_comments_number()); ?></span></h3>

	<div class="king-comments">
		
		<ul>
			<?php
				wp_list_comments( array(
					'callback' => 'king_comment', 
					'end-callback' => 'king_close_comment',
					'style'      => 'ul',
					'short_ping' => true,
				) );
			?>
		</ul>
	</div>
	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
		<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
			<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'king' ); ?></h1>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'king' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'king' ) ); ?></div>
		</nav><!-- #comment-nav-below -->
	<?php endif; // Check for comment navigation. ?>
	
	<?php if ( ! comments_open() ) : ?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'king' ); ?></p>
    <?php endif; ?>
		
	<?php
	if($show_comment_form != 'no'):
		$comment_args = array( 
			
			'title_reply'=> __('Leave A Reply', 'king'),
			'fields' => apply_filters( 'comment_form_default_fields', array(
			'author' => '
				<div class="iconic-input">
					<input placeholder="'.__('Name*', 'king').'" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" />
					<i class="icon-user"></i>
				</div>',
			'email'  => '
				<div class="iconic-input">
					<input id="email" placeholder="'.__('E-mail*', 'king').'" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"  />
					<i class="icon-mail-2"></i>
				</div>',
			'url'    => '' ) ),
			'comment_field' => '
				<div class="iconic-input">
					<textarea placeholder="'.__('Message', 'king').'" id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>
					<i class="icon-chat-1"></i>
				</div>',
			'submit'=>'',
			'comment_notes_after' => '',
			'comment_notes_before' => '',
			'label_submit'         => __( 'Send', 'king' ),
		);
		echo '<div class="king-comment-form">';
		comment_form($comment_args); 
		echo '</div>';
	endif;
	?>

</div>
<!-- /Post Comments -->
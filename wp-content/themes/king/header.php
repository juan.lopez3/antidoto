<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till </header>
 *
 * @package king
 * @since king 1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head >
        <meta charset="<?php bloginfo('charset'); ?>"/>
        <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0'>
        <link rel="profile" href="http://gmpg.org/xfn/11"/>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
        <?php king_favicon(); ?>
        <?php echo ot_get_option('scripts_header'); ?>
        <?php wp_head(); ?>		
    </head>
<body <?php body_class();?> >
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-W4C578"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W4C578');</script>
<!-- End Google Tag Manager -->
<?php 
$preloader = ot_get_option('preloader');
$preloader_text = ot_get_option('preloader_text');
if (!empty($preloader) && $preloader != 'disabled'): ?>
<div class="preloader-container">
    <div class="preloader-content">
        <svg class="preloader" xmlns="http://www.w3.org/2000/svg" version="1.1" width="600" height="200">
            <defs>
                <filter id="goo" x="-40%" y="-40%" height="200%" width="400%">
                    <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
                    <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -8" result="goo" />
                </filter>
            </defs>
            <g filter="url(#goo)">
                <circle class="dot" cx="50" cy="50" r="20" fill="#3fdbda" />
                <circle class="dot" cx="50" cy="50" r="20" fill="#3fdbda" />
            </g>
        </svg>
        <h5><?php echo esc_html($preloader_text); ?></h5>
    </div>
</div>
<?php endif; ?>

<?php
if (is_singular(array('post', 'page')) && $page_body_class = get_post_meta(get_the_ID(), 'body_class', true) ) {
	$body_class = $page_body_class;
}
else {
	$body_class = ot_get_option('body_class');
}

if ($body_class == 'boxed-layout2'): ?>
	<div class="boxed-layout-container">
		<div class="bl-top"></div>
		<div class="bl-left"></div>
		<div class="bl-right"></div>
		<div class="bl-bottom"></div>
	</div>
<?php elseif($body_class == 'page-border'): ?>
    <div class="page-frame">
        <div class="frame-top"></div>
        <div class="frame-right"></div>
        <div class="frame-bottom"></div>
        <div class="frame-left"></div>
    </div>
<?php endif; ?>


<!-- Content Wrapper -->
<div id="marine-content-wrapper" >

<?php if (ot_get_option('control_panel') == 'enabled_admin' && current_user_can('manage_options') || ot_get_option('control_panel') == 'enabled_all'): ?>
    <?php get_template_part('framework/control-panel'); ?>
<?php endif; ?>

<?php
$title_wrapper_added = false;
switch (king_get_main_menu_style()) {
	
	case 'style13':
        get_template_part('inc/header-13');
        break;
	
	case 'style14':
        get_template_part('inc/header-14');
        break;
	
	case 'style15':
        get_template_part('inc/header-15');
        break;

    case 'style17':
        get_template_part('inc/header-17');
        break;

    case 'style16':
    default:
        get_template_part('inc/header-16');
        break;
}
?>

<!-- Content Inner -->
<div id="marine-content-inner"  >

    <!-- Main Content -->
    <section id="main-content" >
        <!-- Container -->
        <div class="container">
			<?php if(get_post_type($post) != 'portfolio'):
				
				$slider_id = null;
				if (is_tax('product_cat')):
					$term_meta = get_option('taxonomy_'.get_queried_object()->term_id.'_metas');
					if (isset($term_meta['slider']) && !empty($term_meta['slider'])):
						$slider_id = $term_meta['slider'];
					endif;
				elseif (function_exists('is_shop') && is_shop()):
					$slider_id = get_post_meta(wc_get_page_id( 'shop' ), 'post_slider', true);
				elseif (is_singular()):
					$slider_id = get_post_meta(get_the_ID(), 'post_slider', true);
				endif;
				
				if ($slider_id):
					get_template_part('inc/header-image');
				elseif ($title_wrapper_added === false && (!function_exists('is_woocommerce') || !is_woocommerce() ) ):
					get_template_part('inc/top');
				endif;
			endif; ?>
		</div>
			

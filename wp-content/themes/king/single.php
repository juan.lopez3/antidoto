<?php
/**
 * The Template for displaying all single posts.
 *
 * @package king
 * @since king 1.0
 */

get_header();

switch (get_post_meta(get_the_ID(),'content_margin',true)) {
	case 'no_margin':
		$padding_class = 'no-padding';
		break;
	
	case 'only_bottom_margin':
		$padding_class = 'no-top-padding small-padding';
		break;
	
	case 'only_top_margin':
		$padding_class = 'no-bottom-padding small-padding';
		break;
	
	default:
		$padding_class = 'small-padding';
		break;
}

$class = king_check_if_any_sidebar(
	'col-md-12 '.$padding_class,
	'col-md-8 '.$padding_class,
	'');

if (king_get_single_post_sidebar_position() == 'left') {
	$class .= ' col-md-push-4';
}
?>

<!-- King Blog -->
<div class="king-blog">
	<div class="container">
		<div class="row">
			<div class="<?php echo sanitize_html_classes($class); ?>">
				<div class="king-blog-main blog-single">
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>
							<?php get_template_part('content','single');?>
						<?php endwhile; ?>	
						<?php
							// If comments are open or we have at least one comment, load up the comment template
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
						?>
					<?php else : //No posts were found ?>
						<?php get_template_part('no-results'); ?>
					<?php endif; ?>
				</div>
			</div>
			<?php king_get_single_post_sidebar('left'); ?>
			<?php king_get_single_post_sidebar('right'); ?>
		</div>
	</div>
</div><!-- /King Blog -->
<?php get_footer();
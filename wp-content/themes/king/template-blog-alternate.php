<?php
/*
 * Template Name: Blog - Alternate Style
 * 
 * @package king
 * @since king 1.0
 * 
 */
get_header();
$class = king_check_if_any_sidebar(
	'col-md-12 big-padding',
	'col-md-8 big-padding',
	'');

if (king_get_single_post_sidebar_position() == 'left') {
	$class .= ' col-md-push-4';
}

//adhere to paging rules
if (get_query_var('paged')) {
    $paged = get_query_var('paged');
} elseif (get_query_var('page')) { // applies when this page template is used as a static homepage in WP3+
    $paged = get_query_var('page');
} else {
    $paged = 1;
}

$posts_per_page = get_post_meta(get_the_ID(), 'number_of_items', true);
if (!$posts_per_page) {
    $posts_per_page = get_option('posts_per_page');
}

$blog_categories = get_post_meta(get_the_ID(), 'blog_categories', true);

global $query_string;
$args = array(
    'numberposts' => '',
    'posts_per_page' => $posts_per_page,
    'offset' => 0,
    'category__in' => is_array($blog_categories) ? $blog_categories : '',
    'orderby' => 'date',
    'order' => 'DESC',
    'include' => '',
    'exclude' => '',
    'meta_key' => '',
    'meta_value' => '',
    'post_type' => 'post',
    'post_mime_type' => '',
    'post_parent' => '',
    'paged' => $paged,
    'post_status' => 'publish'
);
query_posts($args);
?>
<!-- king-page -->
<div class="king-page">
	<!-- container -->
	<div class="container">
		<section class="row">
			<div class="<?php echo sanitize_html_classes($class); ?>">
				<?php if (have_posts()) : ?>
					<?php /* Start the Loop */ ?>
					<div id="post-items">
						<?php
						while (have_posts()) : the_post(); ?>
							<?php get_template_part('inc/blog-types/alternate'); ?>
						<?php endwhile; ?>
					</div>
					<div class="align-center load-more">
						<?php $url = king_get_theme_next_page_url();
						if (!empty($url)): ?>
							<a class="button big blue button-load-more" id="load-more-blog" href="<?php echo esc_url($url); ?>" data-loading="<?php _e('Loading posts', 'king'); ?>"><?php _e('Load More', 'king'); ?></a>
						<?php endif; ?>
					</div>
					<?php wp_reset_query(); ?>
				<?php else : //No posts were found ?>
					<?php get_template_part('no-results'); ?>
				<?php endif; ?>
			</div>
			<?php king_get_single_post_sidebar('left'); ?>
			<?php king_get_single_post_sidebar('right'); ?>
		</section>
	</div>
	<!-- /container -->
</div>
<!-- /king-page -->
<?php get_footer(); ?>
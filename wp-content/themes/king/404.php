<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package King
 * @since King 1.0
 */

get_header(); ?>
<!-- Main Content -->
<section class="error404-section">
	<!-- 404 Section -->
	<section class="section error404-content-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1><?php _e('404', 'king'); ?></h1>
					<h2><?php _e('Oops, This page couldn\'t be found!', 'king'); ?></h2>
					<a href="#" class="button shaped filled"><?php _e('Go back home', 'king'); ?></a>
				</div>
			</div>
		</div>
		<!-- / Container -->
	</section>
	<!-- 404 Section -->
	<!-- New Section -->
	<section class="section error404-content-bottom">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<?php echo do_shortcode(ot_get_option('404_content')); ?>
				</div>
				<div class="col-sm-6">
					<h3 class="section-heading"><?php _e('Search', 'king'); ?></h3>
					<form class="search-form" role="search" method="get" id="searchform" action="<?php echo home_url(); ?>">
						<p><?php _e("Can't find what you need? Take a moment and do a search below!", 'king'); ?></p>
						<div class="iconic-submit">
							<input type="text" name="s">
							<input type="submit" value="">
							<i class="icons icon-search"></i>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- /New Section -->
</section>
<!-- /Main Content -->

<?php get_footer(); ?>
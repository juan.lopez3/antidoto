<?php
/**
 * Template Name: Portfolio Full Width
 * 
 * @package king
 * @since king 1.0
 */
get_header();

wp_enqueue_script ( 'king-prettyphoto-js' );
wp_enqueue_script ( 'king-mixitup' );

$posts_per_page = get_post_meta(get_the_ID(), 'number_of_items', true);
if (!$posts_per_page) {
	$posts_per_page = -1;
}

if (get_query_var('paged')) {
    $paged = get_query_var('paged');
} elseif (get_query_var('page')) { // applies when this page template is used as a static homepage in WP3+
    $paged = get_query_var('page');
} else {
    $paged = 1;
}
$portfolio_categories = get_post_meta(get_the_ID(), 'portfolio_categories', true);
?>
<section class="full-width projects-section dark-gray-bg king-page">

	<div class="align-center">
		<div class="container">
			<h2 class="section-heading"><?php _e('Our Work','king');?></h2>
			<ul class="sorting-tags">
				<li class="filter" data-filter="all"><?php _e('All','king');?></li>
				<?php $folio_cats = get_terms('portfolio-categories');

				foreach ($folio_cats as $folio_cat):
					if (is_array($portfolio_categories) && !in_array($folio_cat -> term_id, $portfolio_categories)):
						continue;
					endif;
					?>
					<li class="filter" data-filter=".category-<?php echo esc_attr($folio_cat->slug); ?>"><?php echo esc_html($folio_cat->name); ?></li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<div id="projects-container" class="full-width-portfolio">
		<?php
		$args = array(
			'numberposts' => '-1',
			'posts_per_page' => $posts_per_page,
			'offset' => 0,
			'meta_query' => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
			'cat' => '',
			'orderby' => 'id',
			'order' => 'DESC',
			'include' => '',
			'exclude' => '',
			'meta_key' => '',
			'meta_value' => '',
			'post_type' => 'portfolio',
			'post_mime_type' => '',
			'post_parent' => '',
			'paged' => $paged,
			'post_status' => 'publish'
		);
		
		if (is_array($portfolio_categories)):
			$args['tax_query'] = array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'portfolio-categories',
					'terms' => $portfolio_categories,
					'operator' => 'IN'
				)
			);
		endif;
		
		query_posts($args);

		?>
		<?php if (have_posts()) : ?>
			<?php while (have_posts()):
				the_post();
				$item_cats = wp_get_post_terms(get_the_ID(), 'portfolio-categories');
				$cat = array();
				$cat1 = array();
				foreach ($item_cats as $item_cat) {
					if (is_array($portfolio_categories) && !in_array($item_cat -> term_id, $portfolio_categories)):
						continue;
					endif;
					$cat[] = 'category-' . $item_cat->slug;
					$cat1[] = $item_cat->name;
				}
				?>
				<!-- Project -->
				<div class="col-md-3 col-sm-6 mix <?php echo sanitize_html_classes(implode(' ', $cat)); ?>">
					<div class="project style-king">

						<div class="project-image wow animated fadeInLeft">
							<?php the_post_thumbnail('king-featured_projects_5', array('class' => 'img-responsive', 'alt' => get_the_title())); ?>
							<div class="project-hover">
								<div>
									<div>
										<h4 class="project-title"><?php echo king_get_shortened_string_by_letters(get_the_title(),25); ?></h4>
										<a class="project-button" href="<?php echo esc_url(king_img_url()); ?>" rel="prettyPhoto"><?php _e('zoom', 'king'); ?></a>
										<a class="project-button" href="<?php echo esc_url(get_permalink()); ?>"><?php _e('view project', 'king');?></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /Project -->
			<?php endwhile;
		endif;
		wp_reset_postdata();
		wp_reset_query();
		?>
	</div>
</section>
<section class="container">
	<div class="row">
		<?php if (have_posts()): 
			while (have_posts()): the_post(); ?>
				<div class="col-sm-12">
					<?php the_content(); ?>
				</div>
			<?php endwhile; 
		endif; ?>
	</div>
</section>
<?php get_footer(); ?>
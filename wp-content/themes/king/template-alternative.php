<?php
/**
 * Template Name: Alternative
 *
 * @package king
 * @since king 1.0
 */

get_header(); 

$class = king_check_if_any_sidebar(
	'col-md-12 big-padding',
	'col-md-8 big-padding',
	'');

if (king_get_single_post_sidebar_position() == 'left') {
	$class .= ' col-md-push-4';
}
?>
<!-- king-page -->
<div class="king-page">
	<!-- container -->
	<div class="container">
		<div class="row">
			<section class="<?php echo sanitize_html_classes($class); ?>">
				<?php if (have_posts()) : ?>
					<?php /* Start the Loop */ ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>
					<?php if (get_post_meta($post->ID, 'show_comment_form', true) != 'no'):
						if (comments_open() || '0' != get_comments_number() ): ?>
							<div class="row">
								<div class="col-sm-12"><?php comments_template('', true); ?></div>
							</div>
						<?php endif;
					endif;
					?>
				<?php else : //No posts were found ?>
					<?php get_template_part('no-results'); ?>
				<?php endif; ?>
			</section>
			<?php king_get_single_post_sidebar('left'); ?>
			<?php king_get_single_post_sidebar('right'); ?>
		</div>
	</div>
	<!-- /container -->
</div>
<!-- /king-page -->
<?php get_footer();
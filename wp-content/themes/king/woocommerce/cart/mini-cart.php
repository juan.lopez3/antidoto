<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

global $woocommerce;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>

<?php do_action( 'woocommerce_before_mini_cart' ); ?>


<?php if ( sizeof( WC()->cart->get_cart() ) == 0 ) { ?>
	<div class="sc-header">
		<h4><?php _e('Cart is empty', 'king'); ?></h4>
	</div>
<?php } else { ?>
	<div class="sc-header">
		<h4><?php echo sprintf(_n('%d item <span>in cart</span>', '%d items <span>in cart</span>', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count); ?></h4>
		<a href="<?php echo esc_url( get_permalink( woocommerce_get_page_id( 'shop' ) ) ); ?>?empty-cart=1" class="sc-remove-button"><i class="icons icon-cancel-circled"></i></a>
	</div>
	<div class="sc-content">
		<?php
		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
			$product_price = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				?>
				<div class="sc-item">
					<div class="featured-image">
						<?php echo get_the_post_thumbnail($_product-> id, 'king-square', array('alt' => '')); ?>
					</div>
					<div class="item-info">
						<?php
						if ( ! $_product->is_visible() )
							echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );
						else
							echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s" class="title">%s</a>', esc_url($_product->get_permalink()), $_product->get_title() ), $cart_item, $cart_item_key );
						?>

						<span class="price">
							<?php echo apply_filters( 'woocommerce_widget_cart_item_quantity', '<span class="quantity">' . sprintf( '%s &times; %s', $cart_item['quantity'], $product_price ) . '</span>', $cart_item, $cart_item_key ); ?>
							
						</span>

						<?php // Meta data
						echo WC()->cart->get_item_data( $cart_item ); ?>
					</div>
				</div>
				<?php
			} //if
		} //foreach ?>
		</div>
		<div class="sc-footer">
			<a href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>" class="button view-cart-button"><?php _e('View Cart', 'king'); ?></a>
			<a href="<?php echo esc_url($woocommerce->cart->get_checkout_url()); ?>" class="button checkout-button"><?php _e('Checkout', 'king'); ?></a>
		</div>
<?php } ?>

<?php do_action( 'woocommerce_after_mini_cart' ); ?>

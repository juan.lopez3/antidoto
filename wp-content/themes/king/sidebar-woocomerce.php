<?php
/**
 * The Sidebar containing the left widget areas.
 *
 * @package king
 * @since king 1.0
 */
?>
<!-- Sidebar -->
<div class="col-md-3 col-md-offset-1 small-padding">
	<section class="sidebar">
		<?php if (is_active_sidebar( 'shop' )): ?>
			<?php dynamic_sidebar( 'shop' ); ?>
		<?php endif; ?>
	</section>
</div>
<!-- /Sidebar -->
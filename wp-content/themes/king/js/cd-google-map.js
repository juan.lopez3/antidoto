jQuery(document).ready(function($){
	if ( $('.google-container').length ) {
		var googleContainer = document.querySelectorAll('.google-container');
		Array.prototype.forEach.call(googleContainer, function(el, i){

			//set your google maps parameters
			var latitude = -37.823323,
				longitude = 145.046120,
				el = el,
				address = el.getAttribute('data-address'),
				map_zoom = 17;

			//google map custom marker icon - .png fallback for IE11
			var marker_url = el.getAttribute('data-marker');
				
			//define the basic color of your map, plus a value for saturation and brightness
			var	main_color = '#222a2c',
				saturation_value= -50,
				brightness_value= -5;

			//we define here the style of the map
			var style= [ 
				{
					//set saturation for the labels on the map
					elementType: "labels",
					stylers: [
						{saturation: saturation_value}
					]
				},  
			    {	//poi stands for point of interest - don't show these lables on the map 
					featureType: "poi",
					elementType: "labels",
			        stylers: [
			            {visibility: "on"}
			        ]
				},
				{
					//don't show highways lables on the map
			        featureType: 'road.highway',
			        elementType: 'labels',
			        stylers: [
			            {visibility: "on"}
			        ]
			    }, 
				{ 	
					//show local road lables on the map
					featureType: "road.local", 
					elementType: "labels.icon", 
					stylers: [
						{visibility: "on"} 
					] 
				},
				{ 
					//show arterial road lables on the map
					featureType: "road.arterial", 
					elementType: "labels.icon", 
					stylers: [
						{visibility: "on"}
					] 
				},
				{
					//don't show road lables on the map
					featureType: "road",
					elementType: "geometry.stroke",
					stylers: [
						{visibility: "on"},
						{hue: main_color},
						{saturation: saturation_value}
					]
				}, 
				//style different elements on the map
				{ 
					featureType: "transit", 
					elementType: "geometry.fill", 
					stylers: [
						{ hue: main_color },
						{ visibility: "on" }, 
						{ lightness: brightness_value }, 
						{ saturation: saturation_value }
					]
				}, 
				{
					featureType: "poi",
					elementType: "geometry.fill",
					stylers: [
						{ hue: main_color },
						{ visibility: "on" }, 
						{ lightness: brightness_value }, 
						{ saturation: saturation_value }
					]
				},
				{
					featureType: "poi.government",
					elementType: "geometry.fill",
					stylers: [
						{ hue: main_color },
						{ visibility: "on" }, 
						{ lightness: brightness_value }, 
						{ saturation: saturation_value }
					]
				},
				{
					featureType: "poi.sport_complex",
					elementType: "geometry.fill",
					stylers: [
						{ hue: main_color },
						{ visibility: "on" }, 
						{ lightness: brightness_value }, 
						{ saturation: saturation_value }
					]
				},
				{
					featureType: "poi.attraction",
					elementType: "geometry.fill",
					stylers: [
						{ hue: main_color },
						{ visibility: "on" }, 
						{ lightness: brightness_value }, 
						{ saturation: saturation_value }
					]
				},
				{
					featureType: "poi.business",
					elementType: "geometry.fill",
					stylers: [
						{ hue: main_color },
						{ visibility: "on" }, 
						{ lightness: brightness_value }, 
						{ saturation: saturation_value }
					]
				},
				{
					featureType: "transit",
					elementType: "geometry.fill",
					stylers: [
						{ hue: main_color },
						{ visibility: "on" }, 
						{ lightness: brightness_value }, 
						{ saturation: saturation_value }
					]
				},
				{
					featureType: "transit.station",
					elementType: "geometry.fill",
					stylers: [
						{ hue: main_color },
						{ visibility: "on" }, 
						{ lightness: brightness_value }, 
						{ saturation: saturation_value }
					]
				},
				{
					featureType: "landscape",
					stylers: [
						{ hue: main_color },
						{ visibility: "on" }, 
						{ lightness: brightness_value }, 
						{ saturation: saturation_value }
					]
					
				},
				{
					featureType: "road",
					elementType: "geometry.fill",
					stylers: [
						{ hue: main_color },
						{ visibility: "on" }, 
						{ lightness: brightness_value }, 
						{ saturation: saturation_value }
					]
				},
				{
					featureType: "road.highway",
					elementType: "geometry.fill",
					stylers: [
						{ hue: main_color },
						{ visibility: "on" }, 
						{ lightness: brightness_value }, 
						{ saturation: saturation_value }
					]
				}, 
				{
					featureType: "water",
					elementType: "geometry",
					stylers: [
						{ hue: main_color },
						{ visibility: "on" }, 
						{ lightness: brightness_value }, 
						{ saturation: saturation_value }
					]
				}
			];
				
			//set google map options
			var map_options = {
		      	center: new google.maps.LatLng(latitude, longitude),
		      	zoom: map_zoom,
		      	panControl: false,
		      	zoomControl: false,
		      	mapTypeControl: false,
		      	streetViewControl: false,
		      	mapTypeId: google.maps.MapTypeId.ROADMAP,
		      	scrollwheel: false,
		      	styles: [
		      		{
		      			"featureType": "all",
      			        "elementType": "all",
      			        "stylers": [
      			            {
      			                "saturation": "-70"
      			            },
      			            {
      			                "lightness": "-30"
      			            },
      			            {
      			                "gamma": "1.7"
      			            },
      			            {
      			                "weight": "1.65"
      			            }
      			        ]
		      		}
		      	]
		    }
		    //inizialize the map
		    // var gmaps = /(google-container-)\d/g;
			var map = new google.maps.Map(el, map_options);
				
			//center map on location
			var geocoder = new google.maps.Geocoder();
			geocoder.geocode({"address": address}, function(results, status) {
			if(status == google.maps.GeocoderStatus.OK) 
			{
				result = results[0].geometry.location;
				latitude = results[0].geometry.location.lat(),
				longitude = results[0].geometry.location.lng();

				map.setCenter(new google.maps.LatLng(latitude + 0.0004, longitude));
				
				//add a custom marker to the map	
				var marker = new google.maps.Marker({
					position: result,
					map: map,
					visible: true,
					icon: marker_url
				});
			};
		});
	  });
	};
});

  
<?php
/*
 * Template Name: FAQ
 * 
 * @package king
 * @since king 1.0
 */

get_header();

wp_enqueue_script ( 'king-mixitup' );

$class = king_check_if_any_sidebar(
	'col-md-12',
	'col-md-8',
	'');

if (king_get_single_post_sidebar_position() == 'left') {
	$class .= ' col-md-push-4';
}

$posts_per_page = get_post_meta(get_the_ID(), 'number_of_items', true);
if (!$posts_per_page) {
	$posts_per_page = -1;
}

if (get_query_var('paged')) {
    $paged = get_query_var('paged');
} elseif (get_query_var('page')) { // applies when this page template is used as a static homepage in WP3+
    $paged = get_query_var('page');
} else {
    $paged = 1;
}
?>
<div class="king-page">
	<!-- container -->
	<div class="container">
		<div class="faq-filters">
			<ul>
				<li class="filter" data-filter="all"><?php _e('All', 'framework'); ?></li>
				<?php $cats = get_terms('faq-categories');
				foreach ($cats as $cat):
					?>
					<li class="filter" data-filter=".faq-<?php echo esc_attr($cat->slug); ?>"><?php echo esc_html($cat->name); ?></li>
				<?php endforeach; ?>
			</ul>
		</div>
		<div class="row">
			<div class="<?php echo sanitize_html_classes($class); ?> small-padding">
				<section class="main-content">
					<div class="accordions faq-accordions">
						<?php
							$args = array(
								'posts_per_page' => $posts_per_page,
								'offset' => 0,
								'cat' => '',
								'orderby' => 'menu_order',
								'order' => 'ASC',
								'include' => '',
								'exclude' => '',
								'meta_key' => '',
								'meta_value' => '',
								'post_type' => 'faq',
								'post_mime_type' => '',
								'post_parent' => '',
								'paged' => $paged,
								'post_status' => 'publish'
							);
							query_posts($args);
						?>

						<?php if (have_posts()) : ?>
							<?php while (have_posts()):
								the_post();
								$item_cats = wp_get_post_terms(get_the_ID(), 'faq-categories');
								$cat = array();
								foreach ($item_cats as $item_cat):
									$cat[] = 'faq-' . $item_cat->slug;
								endforeach;
								?>
								<!-- Accordion -->
								<div class="accordion mix <?php echo sanitize_html_classes(implode(' ',$cat)); ?>">
									<div class="accordion-header">
										<div class="accordion-icon"></div>
										<h5><?php the_title(); ?></h5>
									</div>
									<div class="accordion-content"><?php the_content(); ?></div>		
								</div>
								<!-- /Accordion -->
							<?php endwhile;
						endif;
						wp_reset_query();
						wp_reset_postdata(); ?>
					</div>
				</section>
			</div>
			<?php king_get_single_post_sidebar('left'); ?>
			<?php king_get_single_post_sidebar('right'); ?>
		</div>
	</div>
	<!-- /container -->
</div>
<!-- /king-page -->	
<?php get_footer(); ?>
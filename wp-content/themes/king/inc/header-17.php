<?php
/**
 * Header style 13 template
 *
 * @package king
 * @since king 1.0
 */
?>
<!-- Header -->
<form role="search" method="get" class="fullscreen-search-form" id="searchform" action="<?php echo esc_url(home_url()); ?>">
	<span class="form-close-btn"><i class="fa fa-remove"></i></span>
	<div class="search-form-inner">
		<input type="text" name="s" placeholder="<?php echo esc_attr('Start Typing..', 'king');?>">
		<label for="s"><?php _e('Hit Enter To Begin Your Search', 'king');?></label>
		<button type="submit" value=""><i class="icons icon-search"></i></button>
	</div>
</form>
<header id="header" class="style-king header-style-5 <?php echo sanitize_html_classes(king_get_opt('sticky_menu_color')); ?> <?php king_the_logo('', true); ?> <?php echo sanitize_html_classes(king_get_main_menu_color() == 'header-light' ? 'style-light' : 'style-dark'); ?>">
	
	<?php king_show_king_preheader(); ?>
	
	<!-- Main Header -->
	<div id="main-header">
		<div class="container">
			<div class="row">

				<!-- Logo -->
				<div class="col-lg-3 col-md-3 col-sm-3 logo">
					<?php king_the_logo(); ?>
					<div id="main-nav-button">
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
				
				<div class="col-sm-9 align-right">
					<!-- Main Navigation -->
                    <?php 
					if (has_nav_menu('primary')):
						wp_nav_menu(array(
							'theme_location'	=> 'primary',
							'container'			=> false,
							'menu_id'			=> 'main-nav',
							'depth'				=> 3,
							'walker'			=> has_nav_menu('primary') ? new king_walker_nav_menu : null
						));
					endif; ?>
					<!-- /Main Navigation -->
					
					<!-- Sideheader Button -->
					<div id="sideheader-button">
						<span></span>
						<span></span>
						<span></span>
					</div>
					<!-- /Sideheader Button -->

					<!-- Search Box -->
					<div id="search-box" class="align-right">
						<i class="icons icon-search"></i>
					</div>
					<!-- /Search Box -->
				</div>
			</div>
		</div>
	</div>
	<!-- /Main Header -->

</header>
<!-- /Header -->

<?php get_sidebar('header'); ?>
  <?php
/**
 * The template for displaying quote post format content
 *
 * @package king
 * @since king 1.0
 */

$terms = wp_get_post_terms($post->ID, 'category');
$term_slugs = array();
if (count($terms) > 0):
	foreach ($terms as $term):
		$term_slugs[] = $term->slug;
	endforeach;
endif;

$post_class = array('post');
if (get_post_format()) {
	$post_class[] = 'format-' . get_post_format();
}
if (count($term_slugs) > 0) {
	$post_class = array_merge($post_class, $term_slugs);
}
?>
<article <?php post_class($post_class); ?>>

	<header>
		<div class="link-container">
			<?php $url = get_post_meta(get_the_ID(),'link',true);
			if (!empty($url)): ?>
				<blockquote class="post-blockquote style-link">
					<div class="link-contents">
						<span class="icon-container">
							<i class="fa fa-link"></i>
						</span>
						<p><?php echo esc_html(get_post_meta(get_the_ID(),'link_description',true)); ?></p>
						<a class="link" href="<?php echo esc_url($url); ?>" target="_blank"><?php echo esc_url($url); ?></a>
					</div>
				</blockquote>
				<?php if (has_post_thumbnail()): ?>
					<!-- Post Image -->
					<div class="post-thumbnail" style="background-image: url(<?php echo esc_url(king_get_image_by_id(get_post_thumbnail_id(get_the_ID()),'king-default')); ?>)">
						<?php the_post_thumbnail('king-default', array('class' => 'img-responsive')); ?>
					</div>
					<!-- /Post Image -->
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</header>
	<div class="post-content">
		<div class="post-details">
			<h4 class="post-title">
				<span class="post-format">
					<?php king_post_icon(); ?>
				</span>						
				<a href='<?php echo esc_url(get_permalink()); ?>' title=""><?php the_title(); ?></a>
			</h4>

		</div>
		<p class="latest-from-blog_item_text">
			<?php
			$excerpt_length = get_post_meta(get_the_ID(), 'excerpt_length', true);
			if (empty($excerpt_length)):
				$excerpt_length = 'regular';
			endif;
			echo king_get_the_excerpt_theme($excerpt_length); ?>
		</p>
		<a class="read-more big" href='<?php echo esc_url(get_permalink()); ?>' title=""><?php _e('Read more', 'king'); ?></a>
	</div>
</article>


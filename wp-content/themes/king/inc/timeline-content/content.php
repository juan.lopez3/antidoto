 <?php
/**
 * The template for displaying video post format content
 *
 * @package king
 * @since king 1.0
 */

 wp_enqueue_script ( 'king-prettyphoto-js' );
 
$terms = wp_get_post_terms($post->ID, 'category');
$term_slugs = array();
if (count($terms) > 0):
	foreach ($terms as $term):
		$term_slugs[] = $term->slug;
	endforeach;
endif;

$post_class = array('post');
if (get_post_format()) {
	$post_class[] = 'format-' . get_post_format();
}
if (count($term_slugs) > 0) {
	$post_class = array_merge($post_class, $term_slugs);
}
?>
<article <?php post_class($post_class); ?>>
	<?php
   if (has_post_thumbnail()): ?>


	   <div class="post-thumbnail">

		   <?php the_post_thumbnail('king-default', array('class' => 'img-responsive')); ?>

		   <div class="post-hover">
			   <a class="link-icon" href="<?php echo esc_url(get_permalink()); ?>"></a>
			   <a class="search-icon" href="<?php echo esc_url(king_img_url()); ?>" rel="prettyPhoto"></a>
		   </div>

	   </div>


	<?php endif; ?>
	<div class="post-content">
		<div class="post-details">
			<h4 class="post-title">
				<span class="post-format">
					<?php king_post_icon(); ?>
				</span>						
				<a href='<?php echo esc_url(get_permalink()); ?>' title=""><?php the_title(); ?></a>
			</h4>

		</div>
		<p class="latest-from-blog_item_text">
			<?php
			$excerpt_length = get_post_meta(get_the_ID(), 'excerpt_length', true);
			if (empty($excerpt_length)):
				$excerpt_length = 'regular';
			endif;
			echo king_get_the_excerpt_theme($excerpt_length); ?>
		</p>
		<a class="read-more big" href='<?php echo esc_url(get_permalink()); ?>' title=""><?php _e('Read more', 'king'); ?></a>
	</div>
	<div class="clear"></div>
</article>




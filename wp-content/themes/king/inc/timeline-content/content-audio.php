<?php
/**
 * The template for displaying video post format content
 *
 * @package king
 * @since king 1.0
 */

$terms = wp_get_post_terms($post->ID, 'category');
$term_slugs = array();
if (count($terms) > 0):
	foreach ($terms as $term):
		$term_slugs[] = $term->slug;
	endforeach;
endif;

$post_class = array('post');
if (get_post_format()) {
	$post_class[] = 'format-' . get_post_format();
}
if (count($term_slugs) > 0) {
	$post_class = array_merge($post_class, $term_slugs);
}
?>
<article <?php post_class($post_class); ?>>

	
	<header>
		<?php $audio_url = get_post_meta(get_the_ID(), 'audio_url', true);
		if ($audio_url != ''): ?>
			<div class="audio-player-box">
				<audio>
					<source class="sc-audio-player" type="audio/mpeg" src="<?php echo esc_url($audio_url); ?>">
					<?php _e('Your browser does not support the audio element.','king'); ?>
				</audio>
			</div>
		<?php endif; ?>
	</header>
	

	<div class="post-content">
		<div class="post-details">
			<h4 class="post-title">
				<span class="post-format">
					<?php king_post_icon(); ?>
				</span>
				<a href='<?php echo esc_url(get_permalink()); ?>' title=""><?php the_title(); ?></a>
			</h4>

		</div>
		<p class="latest-from-blog_item_text">
			<?php
			$excerpt_length = get_post_meta(get_the_ID(), 'excerpt_length', true);
			if (empty($excerpt_length)):
				$excerpt_length = 'regular';
			endif;
			echo king_get_the_excerpt_theme($excerpt_length); ?>
		</p>
		<a class="read-more big" href='<?php echo esc_url(get_permalink()); ?>' title=""><?php _e('Read more', 'king'); ?></a>
	</div>
	<div class="clear"></div>
</article>





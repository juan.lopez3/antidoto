  <?php
/**
 * The template for displaying video post format content
 *
 * @package king
 * @since king 1.0
 */

$terms = wp_get_post_terms($post->ID, 'category');
$term_slugs = array();
if (count($terms) > 0):
	foreach ($terms as $term):
		$term_slugs[] = $term->slug;
	endforeach;
endif;

$post_class = array('post');
if (get_post_format()) {
	$post_class[] = 'format-' . get_post_format();
}
if (count($term_slugs) > 0) {
	$post_class = array_merge($post_class, $term_slugs);
}
?>
<article <?php post_class($post_class); ?>>

	<header>
		<blockquote class="post-blockquote">
			<p><?php echo get_post_meta(get_the_ID(),'quote_text',true);?></p>
			<span class="author"><?php echo get_post_meta(get_the_ID(),'author_text',true);?></span>
		</blockquote>
	</header>
</article>


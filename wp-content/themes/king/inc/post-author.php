<?php
/**
 * Post author info
 *
 * @package king
 * @since king 1.0
 */
?>

<!-- Post Author -->
<div class="post-author">

	<?php
	echo get_avatar( get_the_author_meta( 'user_email'), '70');
	?>
	<h3><?php echo sprintf(__('About %s', 'king'), get_the_author()); ?></h3>

	<?php echo  get_the_author_meta( 'description');?>

</div>
<!-- /Post Author -->

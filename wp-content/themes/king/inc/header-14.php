<?php
/**
 * Header style 14 template
 *
 * @package king
 * @since king 1.0
 */
?>

<header id="header" class="style-king <?php king_the_logo('', true); ?> <?php echo sanitize_html_classes(king_get_opt('sticky_menu_color')); ?> columns3 <?php echo sanitize_html_classes(king_get_main_menu_color()); ?>">
    <!-- Main Header -->
    <div id="main-header">	
		<div class="container">
			<div class="row">
				<div class="left-column col-md-5">
					<!-- Main Navigation -->
					<?php 
					if (has_nav_menu('primary-left')):
						wp_nav_menu(array(
							'theme_location'	=> 'primary-left',
							'container'			=> false,
							'menu_id'			=> 'main-nav',
							'menu_class'		=> 'menu',
							'depth'				=> 3,
							'walker'			=> new king_walker_nav_menu
						));
					endif; ?>
				</div>
				<!-- /Main Navigation -->
				<div class="middle-column col-md-2">
					<?php king_the_logo(); ?>
					<div id="main-nav-button">
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
				<div class="right-column col-md-5">
					<!-- Main Navigation -->
					<?php 
					if (has_nav_menu('primary-right')):
						wp_nav_menu(array(
							'theme_location'	=> 'primary-right',
							'container'			=> false,
							'menu_id'			=> 'main-nav',
							'menu_class'		=> 'menu',
							'depth'				=> 3,
							'walker'			=> new king_walker_nav_menu
						));
					endif; ?>
					<!-- /Main Navigation -->
				</div>
			</div>
		</div>
	</div>
</header>
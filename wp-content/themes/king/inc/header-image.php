<?php
/**
 * Header image/slider
 *
 * @package king
 * @since king 1.0
 */

$slider = null;

$force_slider_id = null;
if (is_tax('product_cat')) {
	$slider_id = null;
	$term_meta = get_option('taxonomy_'.get_queried_object()->term_id.'_metas');
	
	if (isset($term_meta['slider']) && !empty($term_meta['slider'])) {
		$slider = true;
		$force_slider_id = $term_meta['slider'];
		
		
	}
} else if (function_exists('is_woocommerce') && is_woocommerce()) {
	$slider_id = get_post_meta(wc_get_page_id( 'shop' ), 'post_slider', true);
	if ($slider_id) {
		$slider = true;
		$slider_page_id = wc_get_page_id( 'shop' );	
	}
	
} else if (is_home() || is_page() || is_single()) {	
	$slider = null;
	$slider_id = get_post_meta(get_the_ID(), 'post_slider', true);
	if ($slider_id) {
		$slider = true;
		$slider_page_id = get_the_ID();
	}
}

if (!empty($slider)): ?>
	</div>
	<div class="container">
		<section id="slider" class="section full-width">
			<?php 
			if (!empty($force_slider_id)):
				echo king_get_post_slider(null,$force_slider_id);
			else:
				echo king_get_post_slider($slider_page_id);
			endif;
			?>
			<script>
				/* Fix The Revolution Slider Loading Height issue */
				jQuery(document).ready(function($){
					$('.rev_slider_wrapper').each(function(){
						$(this).css('height','');
						var revStartHeight = parseInt($(' > .rev_slider', this).css('height'));
						$(this).height(revStartHeight);
						$(this).parents('#slider').height(revStartHeight);
						
						$(window).load(function(){
							$('#slider').css('height','');
						});
					});
				});
			</script>
	    </section>
    </div><!-- /container -->
	<div class="container">
<?php endif;?>
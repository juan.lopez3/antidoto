<?php
/**
 * Shortcodes
 *
 * @package framework
 * @since framework 1.0
 */


/* PLEASE ADD every new shortcode to the get_shortcodes_help function below (all except columns shortcodes which are includes in inc/tinymce.js.php */

/**
 * Get shortcodes list
 *
 */
function king_get_shortcodes_list()
{
    $aHelp = array();

//adding custom items which are not shortcodes but are required for popup.php (eg. nav-menus.php icons)
    if (isset($_GET['custom_popup_items']) && $_GET['custom_popup_items'] == 1 && function_exists('king_get_custom_popup_items')) {
        $custom_items = king_get_custom_popup_items();
        if (is_array($custom_items)) {
            $aHelp = array_merge($aHelp, $custom_items);
        }
    }

    return $aHelp;
}

function king_get_percentage_select_values($add_zero = false)
{
    $a = array();

    if ($add_zero) {
        $start_from = 0;
    } else {
        $start_from = 1;
    }

    for ($i = $start_from; $i <= 100; $i++) {
        $a[$i] = $i;
    }
    return $a;
}

function king_get_animation_effects_settings()
{

    return array(
        'type' => 'select',
        'label' => __('Animation', 'framework'),
        'desc' => '',
        'values' => king_get_animation_effects_list()
    );
}
function king_get_vc_animation_effects_settings($label = '', $param_name = '')
{

    $label      = ( empty($label)) ? 'Animation':$label;
    $param_name = ( empty($param_name)) ? 'animation':$param_name;
	return array(
		'type' => 'dropdown',
		'heading' => $label,
		'param_name' => $param_name,
		'admin_label' => true,
		'value' => king_get_animation_effects_list(true),
		'description' => ''
	);
}

function king_get_animation_effects_list($flip = false)
{

    $animations = array(
        'bounce',
        'flash',
        'pulse',
        'shake',
        'swing',
        'tada',
        'wobble',
        'bounceIn',
        'bounceInDown',
        'bounceInLeft',
        'bounceInRight',
        'bounceInUp',
        'bounceOut',
        'bounceOutDown',
        'bounceOutLeft',
        'bounceOutRight',
        'bounceOutUp',
        'fadeIn',
        'fadeInDown',
        'fadeInDownBig',
        'fadeInLeft',
        'fadeInLeftBig',
        'fadeInRight',
        'fadeInRightBig',
        'fadeInUp',
        'fadeInUpBig',
        'fadeOut',
        'fadeOutDown',
        'fadeOutDownBig',
        'fadeOutLeft',
        'fadeOutLeftBig',
        'fadeOutRight',
        'fadeOutRightBig',
        'fadeOutUp',
        'fadeOutUpBig',
        'flip',
        'flipInX',
        'flipInY',
        'flipOutX',
        'flipOutY',
        'lightSpeedIn',
        'lightSpeedOut',
        'rotateIn',
        'rotateInDownLeft',
        'rotateInDownRight',
        'rotateInUpLeft',
        'rotateInUpRight',
        'rotateOut',
        'rotateOutDownLeft',
        'rotateOutDownRight',
        'rotateOutUpLeft',
        'rotateOutUpRight',
        'slideInDown',
        'slideInLeft',
        'slideInRight',
        'slideOutLeft',
        'slideOutRight',
        'slideOutUp',
        'hinge',
        'rollIn',
        'rollOut'
    );
    $animation_effects = array();
	
	if ($flip == true) {
		$animation_effects[__('None', 'framework')] = '';
	} else {
		$animation_effects[''] = __('None', 'framework');
	}
	
    
    foreach ($animations as $animation) {
        $animation_effects[$animation] = $animation;
    }

    return $animation_effects;
}
<?php
/**
 * Top secion of the theme, includes page header or image slider
 *
 * @package king
 * @since king 1.0
 */
if (is_tag()) {
    $title = sprintf(__('Posts Tagged "%s"', 'king'), single_tag_title('', false));
} elseif (is_day()) {
    $title = sprintf(esc_html__('Posts made in %s', 'king'), get_the_time('F jS, Y'));
} elseif (is_month()) {
    $title = sprintf(esc_html__('Posts made in %s', 'king'), get_the_time('F, Y'));
} elseif (is_year()) {
    $title = sprintf(esc_html__('Posts made in %s', 'king'), get_the_time('Y'));
} elseif (is_search()) {
    $title = sprintf(esc_html__('Search results for %s', 'king'), get_search_query());
} elseif (is_category()) {
    $title = single_cat_title('', false);
} elseif (is_author()) {
    global $wp_query;
    $curauth = $wp_query->get_queried_object();
    $title = sprintf(__('Posts by %s', 'king'), $curauth->nickname);
} elseif (is_single()) {
    
	$title = get_the_title();
    
} elseif (is_page()) {
    $title = get_the_title();
} else if (is_404()) {
    $title = __('404 Page Not Found', 'king');
} else if (function_exists('is_woocommerce') && is_woocommerce()) {
    $title = __('Shop', 'king');
} else {
    $title = get_bloginfo('name');
}

$titlebar = get_post_meta(is_singular() ? get_the_ID() : null, 'titlebar', true);
$no_titlebar = false;

$image = get_post_meta(is_singular() ? get_the_ID() : null, 'titlebar_background', true);
$titlebar_bg = get_post_meta(is_singular() ? get_the_ID() : null, 'titlebar_bg', true);


switch ($titlebar) {
    case 'title':
        $show_title = true;
        $show_breadcrumbs = false;
        break;

    case 'breadcrumbs':
        $show_title = true;
        $show_breadcrumbs = true;
        break;

    case 'no_titlebar':
        $no_titlebar = true;
        break;

    default:
        $show_title = true;
        $show_breadcrumbs = true;
}

$class = '';

$titlebar_style = get_post_meta(is_singular() ? get_the_ID() : null, 'titlebar_style', true);

if (function_exists('is_woocommerce') && is_woocommerce()) {
    //$titlebar_style = ot_get_option('default_shop_titlebar_style');
    return;
}

if (empty($titlebar_style) || $titlebar_style == 'default') {
    $titlebar_style = ot_get_option('default_titlebar_style');
}

if (empty($image)) {
	if (function_exists('is_woocommerce') && is_woocommerce()) {
		$image = ot_get_option('default_shop_title_background');
	}
	if (empty($image)) {
		$image = ot_get_option('default_title_background');
	}
}
if (!empty($image)) {
    $title_background_position = ot_get_option('default_title_background_position');
    
    if (!empty($title_background_position)) {
        $class .= ' ' . $title_background_position;
    }

    $title_background_size = ot_get_option('default_title_background_size');
	
	if (!empty($title_background_size) && $title_background_size != 'default') {
        $class .= ' ' . $title_background_size;
    }
}

$styles = array();
if (!empty($image)) {
     $styles[] = 'background-image: url(' . esc_url($image) . ')';
}


if (!empty($titlebar_bg)) {
     $styles[] = 'background-color: '.esc_attr($titlebar_bg);
}

if ($no_titlebar === false):

if (isset($_GET['switch_layout']) && !empty($_GET['switch_layout'])) {
	$body_class = $_GET['switch_layout'];
} elseif (is_singular(array('post', 'page')) && $page_body_class = get_post_meta(get_the_ID(), 'body_class', true) ) {
	$body_class = $page_body_class;
} else {
	$body_class = ot_get_option('body_class');
}

$container_class = ( $body_class == 'b1170' || $body_class == 'b960' || $body_class == 'boxed-layout2') ? 'container':'';
	
	if ((king_get_main_menu_style() == 'style16') && !function_exists('is_woocommerce')): ?>
		<!-- Title Bar -->
		<div class="page-title-bar style-king" <?php echo (count($styles) > 0) ? 'style="'.implode(';',$styles).'"' : ''; ?>>
			<?php if ($show_title): ?>
				<div class="container">
					<h1><?php echo wp_kses_data($title); ?></h1>
				</div>
			<?php endif; ?>
		</div>
		<!-- /Title Bar -->
	<?php else:
		switch ($titlebar_style):
			case 'style2': ?>
				<section class="full-width <?php echo sanitize_html_class($container_class); ?> page-heading <?php echo sanitize_html_classes($class); ?>" <?php echo (count($styles) > 0) ? 'style="'.implode(';',$styles).'"' : ''; ?>>
					<div class="container">
						<div class="row">
							<div class="col-md-12">
							<?php if ($show_title): ?>
								<h1><?php echo wp_kses_data($title); ?></h1>
							<?php endif;
							if ($show_breadcrumbs && ot_get_option('show_breadcrumbs') != "no"): ?>
								<?php king_the_breadcrumbs(); ?>
							<?php endif;?>
							</div>
						</div>
					</div>
				</section>
				<?php break;

			case 'style3': ?>
				<section class="full-width <?php echo sanitize_html_class($container_class); ?> style2 page-heading header_bg <?php echo sanitize_html_classes($class); ?>" <?php echo (count($styles) > 0) ? 'style="'.implode(';',$styles).'"' : ''; ?>>
					<div class="container">
						<div class="row">
							<?php if ($show_title): ?>
								<div class="col-lg-12">
									<?php
									$icon = get_post_meta(is_singular() ? get_the_ID() : null,'page_title_icon_upload', true);
									if (!empty($icon)): ?>
										<img src="<?php echo esc_url($icon); ?>" />
									<?php elseif (empty($icon)): 
										$icon = get_post_meta(is_singular() ? get_the_ID() : null,'page_title_icon', true);
										if (!empty($icon)): ?>
											<i class="icons <?php echo sanitize_html_classes($icon); ?>"></i>
										<?php else: ?>	
											<i class="icons icon-sun-2"></i>
										<?php endif;
									endif; ?>
									<h1><span class="extra-bold"><?php echo wp_kses_data($title); ?></span></h1>

								</div>
								<?php echo get_post_meta(is_singular() ? get_the_ID() : null, 'titlebar_text', true); ?>
							<?php endif;
							if ($show_breadcrumbs && ot_get_option('show_breadcrumbs') != "no"): ?>
								<div class="col-lg-12  portfolio-bc">
									<?php king_the_breadcrumbs(); ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</section>
				<?php break;

			case 'style1':
			default: ?>
				<section class="full-width <?php echo sanitize_html_class($container_class); ?> style3 page-heading <?php echo sanitize_html_classes($class); ?>" <?php echo (count($styles) > 0) ? 'style="'.implode(';',$styles).'"' : ''; ?>>
					<div class="container">
						<div class="row">
							<?php
							if ($show_title): ?>
								<div class="col-md-12">
									<h1><?php echo wp_kses_data($title); ?></h1>
									<?php echo get_post_meta(is_singular() ? get_the_ID() : null, 'titlebar_text', true); ?>
								<?php endif;
								if ($show_breadcrumbs && ot_get_option('show_breadcrumbs') != "no"): ?>
									<?php king_the_breadcrumbs(); ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</section>
				<?php
				break;
		endswitch;
	endif;
endif;

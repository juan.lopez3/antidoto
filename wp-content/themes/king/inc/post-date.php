<?php

/**

 * Post date

 *

 * @package king

 * @since king 1.0

 */

?>

<span class="post-date">

	<span class="post-day"><?php echo get_the_date('d');?></span><br/>

	<?php echo get_the_date('M, Y');?>

</span>



<span class="post-format">

	<?php king_post_icon(); ?>

</span>



<?php echo get_avatar( get_the_author_meta( 'user_email'), '70'); ?>

<span class="author"><?php _e('By','king');?> <?php the_author();?></span>
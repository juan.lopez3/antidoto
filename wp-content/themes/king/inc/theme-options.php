<?php

/**
 * Theme options
 *
 * @package framework
 * @since framework 1.0
 */

require_once (get_template_directory() . '/framework/fonts/google-fonts.json.php');
require_once (get_template_directory() . '/framework/fonts/fontello.php');

/**
 * Initialize the options before anything else.
 */
add_action('admin_init', 'king_custom_theme_options', 1);

/**
 * Initalize theme options scripts
 */
add_action('admin_enqueue_scripts', 'king_framework_theme_options_scripts');

function king_framework_theme_options_scripts() {

	wp_enqueue_script('jquery-ui-core');
	wp_enqueue_script('jquery-ui-mouse');
	wp_enqueue_script('jquery-ui-widget');
	wp_enqueue_script('jquery-ui-slider');
	wp_enqueue_style('king-jquery-ui-my-theme', get_template_directory_uri() . "/framework/css/jquery-ui/jquery.ui.my-theme.css", false);

	wp_enqueue_script('king-theme_options', get_template_directory_uri() . '/framework/js/theme-options.js', array('jquery'));
	wp_enqueue_script('king-screwdefaultbuttons', get_template_directory_uri() . '/framework/js/jquery.screwdefaultbuttonsV2.min.js', array('jquery'));
}

/**
 * Build the custom settings & update OptionTree.
 */
function king_custom_theme_options() {
	/**
	 * Get a copy of the saved settings array.
	 */
	$saved_settings = get_option('option_tree_settings', array());

	$user_sidebars = ot_get_option('user_sidebars');
	$sidebar_choices = array();
	$sidebar_choices[] = array(
		'label' => __('Main', 'framework'),
		'value' => 'main',
		'src' => ''
	);
	if (is_array($user_sidebars)) {
		foreach ($user_sidebars as $sidebar) {
			$sidebar_choices[] = array(
				'label' => $sidebar['title'],
				'value' => sanitize_title($sidebar['title']),
				'src' => ''
			);
		}
	}

	
	
	/**
	 * Custom settings array that will eventually be
	 * passes to the OptionTree Settings API Class.
	 */
	$custom_settings = array(
		'sections' => array(
			array(
				'id' => 'general_settings',
				'title' => __('General Settings', 'framework')
			),
			array(
				'id' => 'preheader',
				'title' => __('Preheader', 'framework')
			),
			array(
				'id' => 'header',
				'title' => __('Header', 'framework')
			),
			array(
				'id' => 'footer',
				'title' => __('Footer', 'framework')
			),
			array(
				'id' => 'pages',
				'title' => __('Pages', 'framework')
			),
			array(
				'id' => 'sidebars',
				'title' => __('Sidebars', 'framework')
			),
			array(
				'id' => 'integration',
				'title' => __('Integration', 'framework')
			),
			array(
				'id' => 'social',
				'title' => __('Contacts & Social', 'framework')
			),
			array(
				'id' => 'fonts',
				'title' => __('Fonts', 'framework')
			),
			array(
				'id' => 'elements_color',
				'title' => __('Elements Color', 'framework')
			),
			array(
				'id' => 'translations',
				'title' => __('Translations', 'framework')
			),
			array(
				'id' => 'import',
				'title' => __('Import', 'framework')
			)
		)
	);
	$custom_settings['settings'] = array();
	
	foreach ($custom_settings['sections'] as $section) {
		if (file_exists(get_template_directory().'/inc/options/'.$section['id'].'.php')) {
			$options = array();
			require_once (get_template_directory().'/inc/options/'.$section['id'].'.php');
			
			if (is_array($options)) {
				$custom_settings['settings'] = array_merge($custom_settings['settings'],$options);
			}
		}
	}
	
	
	/* allow settings to be filtered before saving */
	$custom_settings = apply_filters('option_tree_settings_args', $custom_settings);

	/* settings are not the same update the DB */
	if ($saved_settings !== $custom_settings) {
		update_option('option_tree_settings', $custom_settings);
	}
}

/**
 * Get font choices for theme options
 * @param bool $return_string if true returned array is strict, example array item: font_name => font_label
 * @return array
 */
function king_get_font_choices($return_strict = false) {
	$aFonts = array(
		array(
			'value' => 'default',
			'label' => __('Default', 'framework'),
			'src' => ''
		),
		array(
			'value' => 'Verdana',
			'label' => 'Verdana',
			'src' => ''
		),
		array(
			'value' => 'Geneva',
			'label' => 'Geneva',
			'src' => ''
		),
		array(
			'value' => 'Arial',
			'label' => 'Arial',
			'src' => ''
		),
		array(
			'value' => 'Arial Black',
			'label' => 'Arial Black',
			'src' => ''
		),
		array(
			'value' => 'Trebuchet MS',
			'label' => 'Trebuchet MS',
			'src' => ''
		),
		array(
			'value' => 'Helvetica',
			'label' => 'Helvetica',
			'src' => ''
		),
		array(
			'value' => 'sans-serif',
			'label' => 'sans-serif',
			'src' => ''
		),
		array(
			'value' => 'Georgia',
			'label' => 'Georgia',
			'src' => ''
		),
		array(
			'value' => 'Times New Roman',
			'label' => 'Times New Roman',
			'src' => ''
		),
		array(
			'value' => 'Times',
			'label' => 'Times',
			'src' => ''
		),
		array(
			'value' => 'serif',
			'label' => 'serif',
			'src' => ''
		),
		array(
			'value' => 'Nella Sue',
			'label' => 'Nella Sue',
			'src' => ''
		)
	);

	
	$aFonts[] = array(
		'value' => 'google_web_fonts',
		'label' => '---Google Web Fonts---',
		'src' => ''
	);

	$google_fonts = king_get_google_fonts();
	
	if (!empty($google_fonts)) {

		$aGoogleFonts = json_decode($google_fonts, true);

		if (isset($aGoogleFonts['items']) && is_array($aGoogleFonts['items'])) {
			foreach ($aGoogleFonts['items'] as $aGoogleFont) {
				$aFonts[] = array(
					'value' => 'google_web_font_' . $aGoogleFont['family'],
					'label' => $aGoogleFont['family'],
					'src' => ''
				);
			}
		}
	}
	

	if ($return_strict) {
		$aFonts2 = array();
		foreach ($aFonts as $font) {
			$aFonts2[$font['value']] = $font['label'];
		}
		return $aFonts2;
	}
	return $aFonts;
}

/**
 * Get background patterns
 * @param bool $control_panel if true return array for control panel (front end)
 * @return type
 */
function king_get_background_patterns($control_panel = false)
{
	$patterns = array();


	if ($control_panel === false)
	{
		$patterns[] = array(
			'value' => 'none',
			'label' => __('None', 'framework'),
			'src' => ''
		);

		$patterns[] = array(
			'value' => 'image',
			'label' => __('Image (choose below)', 'framework'),
			'src' => ''
		);
	}

	$patterns[] = array(
		'value' => 'cartographer.png',
		'label' => __('Cartographer', 'framework'),
		'src' => ''
	);
	$patterns[] = array(
		'value' => 'concrete_wall.png',
		'label' => __('Concrete Wall', 'framework'),
		'src' => ''
	);
	$patterns[] = array(
		'value' => 'dark_wall.png',
		'label' => __('Dark Wall', 'framework'),
		'src' => ''
	);
	$patterns[] = array(
		'value' => 'dark_wood.png',
		'label' => __('Dark Wood', 'framework'),
		'src' => ''
	);
	$patterns[] = array(
		'value' => 'irongrip.png',
		'label' => __('Irongrip', 'framework'),
		'src' => ''
	);
	$patterns[] = array(
		'value' => 'purty_wood.png',
		'label' => __('Purty Wood', 'framework'),
		'src' => ''
	);
	$patterns[] = array(
		'value' => 'px_by_Gre3g.png',
		'label' => __('PX', 'framework'),
		'src' => ''
	);
	return $patterns;
}

/**
 * Get menu background transparency values
 * @return int
 */
function king_get_menu_background_transparency_values()
{
	$values = array();
	for ($i = 0; $i <= 100; $i ++)
	{
		$v = $i;
		$v = 100 - $i;
		if ($v == 100)
		{
			$v = 1;
		}
		else
		{
			if ($v < 10)
			{
				$v = '0'.$v;
			}
			$v = '0.'.$v;
		}
		$values[] = array(
			'value' => $v,
			'label' => $i.'%',
			'src' => ''
		);
	}
	return $values;
}

/**
 * Get preheader types
 * @return array
 */
function king_get_preheader_types() {
	
	$preheader_items = array(
		array(
			'value' => 'text',
			'label' => __('Text', 'framework'),
			'src' => ''
		),
		array(
			'value' => 'date',
			'label' => __('Date', 'framework'),
			'src' => ''
		),
	);
	
	return $preheader_items;
}

/**
 * Adds se
 */
function king_add_settings($custom_settings) {
	
	return $custom_settings;
}
<?php
/**
 * The template for displaying quote post format content
 *
 * @package king
 * @since king 1.0
 */
?>

<article class="blog-post">
	<div class="blog-post-king">
		<div class="blog-post-content">

			<header>
				<blockquote class="post-blockquote">
					<p><?php echo get_post_meta(get_the_ID(),'quote_text',true);?></p>
					<p>- <?php _e('By','king'); ?> <?php echo get_post_meta(get_the_ID(),'author_text',true);?></p>
				</blockquote>

				<h4 class="post-title"><a href="<?php echo esc_attr(get_the_permalink()); ?>"><?php the_title(); ?></a></h4>
				<div class="blog-post-meta">
					<span><?php the_date(get_option('date_format'));?>, <?php _e('in', 'king'); ?> <?php echo get_the_category_list(' '); ?>, <?php _e('by', 'king');?> <span class="post-author"><?php the_author();?></span></span>
				</div>
			</header>

			<!-- Post Content -->
			<div class="post-content">
				<p><?php
					$excerpt_length = get_post_meta(CURRENT_ID, 'excerpt_length', true);
					if (empty($excerpt_length)):
						$excerpt_length = 'regular';
					endif;
					echo king_get_the_excerpt_theme($excerpt_length); ?>
				</p>
			</div>
			<!-- /Post Content -->

			<footer>
				<div class="socials-container">
					<?php king_share_buttons_king(array('facebook','twitter','google_plus')); ?>
				</div>
				<div class="comments-counter">
					<a href="<?php echo esc_url(get_comments_link()); ?>"><?php comments_number( '0', '1', '%') ;?></a>
				</div>
			</footer>

		</div>
	</div>
</article><!-- /Blog Post -->
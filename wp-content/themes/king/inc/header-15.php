<?php
/**
 * Header style 15 template
 *
 * @package king
 * @since king 1.0
 */
?>
<!-- Header -->
<header id="king-header" class="header-style15 <?php echo sanitize_html_classes(king_get_opt('sticky_menu_color')); ?> <?php king_the_logo('', true); ?>">

	<!-- Header Upper Area -->
	<div class="header-upper-area">
		<!-- Logo -->
		<?php king_the_logo() ?>
	</div>
	<!-- /Header Upper Area -->

	<!-- Header Left Area -->
	<div class="header-left-area">
		<div>
			<div>
				<?php get_template_part('inc/social-icons'); ?>
			</div>
		</div>
	</div>
	<!-- /Header Left Area -->

	<!-- Header Right Area -->
	<div class="header-right-area">
		<div>
			<div>

				<div id="navigation-lightbox-button">
					<span></span>
					<span></span>
					<span></span>
				</div>

			</div>
		</div>
	</div>
	<!-- /Header Right Area -->

	<!-- Nav lightbox -->
	<div id="navigation-lightbox">
		<div>
			<div>
				<div id="main-nav-button">
					<span></span>
					<span></span>
					<span></span>
				</div>
				<!-- Main Navigation -->
				<?php 
				if (has_nav_menu('primary')):
					wp_nav_menu(array(
						'theme_location'	=> 'primary',
						'container'			=> false,
						'menu_id'			=> 'main-nav',
						'menu_class'		=> 'menu',
						'depth'				=> 3,
						'walker'			=> new king_walker_nav_menu
					));
				endif; ?>
                <!-- /Main Navigation -->
			</div>
		</div>
	</div>
	<!-- /Nav lightbox -->
</header>
<!-- /Header -->
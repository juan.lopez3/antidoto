<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package king
 * @since king 1.0
 */

/**
 * Display site logo
 * @param string $style
 */
function king_the_logo($style = '', $class = false) { 
	
	$logo_url = null;
	$logo_sticky_url = null;
	if (is_page()) {
		$logo_url = get_post_meta(get_the_ID(), 'logo_url', true);
		$logo_sticky_url = get_post_meta(get_the_ID(), 'sticky_logo_url', true);
	}
	if (empty($logo_url) && function_exists('is_woocommerce') && (
			is_shop() ||
			is_woocommerce() ||
			is_product_category() ||
			is_product_tag() ||
			is_product() ||
			is_cart() ||
			is_checkout() ||
			is_account_page()
		)) {
		$logo_url = get_post_meta(get_option('woocommerce_shop_page_id'), 'logo_url', true);
		$logo_sticky_url = get_post_meta(get_option('woocommerce_shop_page_id'), 'sticky_logo_url', true);
	}
	
	if (empty($logo_url)) {
		if ($style == 'light') {
			$logo_url = ot_get_option('logo_light_url');
		} else {
			$logo_url = ot_get_option('logo_url');
		}	
	}

	if(empty($logo_sticky_url)) {
		$logo_sticky_url = ot_get_option('sticky_logo_url');
	}


	if($class == false):
	?>
		<a href='<?php echo esc_url(home_url('/')); ?>' title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
			<?php
				if (!empty($logo_url)) {
					echo '<img class="logo" src="' . esc_url($logo_url) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '">';
				} else {
					echo '<img class="logo" src="' . get_template_directory_uri() . '/img/king-logo.png" alt="' . esc_attr(get_bloginfo('name','display')) . '">';
				}

				if(!empty($logo_sticky_url)) {
					echo '<img class="logo sticky-logo" src="' . esc_url($logo_sticky_url) . '" alt="' . esc_attr(get_bloginfo('name', 'display')) . '">';
				}
			?>
		</a>
	<?php
	else:
		if(!empty($logo_sticky_url)) {
			echo __('have-sticky-logo', 'king');
		}
	endif;
}

/**
 * Display site favicon
 * @param string $style
 */
function king_favicon() {
	if(function_exists('wp_site_icon') && has_site_icon() ) { wp_site_icon(); return; }
	if (ot_get_option('favicon')):
    	echo '<link rel="shortcut icon" href="'.esc_url(ot_get_option('favicon')).'" type="image/x-icon"/>';
    endif;
}


/**
*
* @return none
* @param  css
* compress css
*
**/
if( !function_exists('king_css_compress')) {

  function king_css_compress($css) {
    $css  = preg_replace( '!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css );
    $css  = str_replace( ': ', ':', $css );
    $css  = str_replace( array( "\r\n", "\r", "\n", "\t", '  ', '    ', '    ' ), '', $css );
    return $css;
  }

}

/**
* @return null
* @param none
* Add inline styles
**/
if( !function_exists('king_inline_styles')) {

  function king_inline_styles() {

    global $king_inline_styles;

    if ( ! empty( $king_inline_styles ) ) {
      echo '<style id="custom-shortcode-css" type="text/css">'. king_css_compress( join( '', $king_inline_styles ) ) .'</style>';
    }
  }
}

/**
*
* @return none
* @param  style
* add inline css
*
**/
global $king_inline_styles;
$king_inline_styles = array();
if( ! function_exists( 'king_add_inline_style' ) ) {
  function king_add_inline_style( $style ) {
    global $king_inline_styles;
    array_push( $king_inline_styles, $style );
  }
}



/**
 * Getting google web fonts
 * @return type
 */
function king_get_used_googe_web_fonts()
{
    $fonts = array(
        'content_font' => ot_get_option('content_font'),
        'title_font' => ot_get_option('title_font'),
        'menu_font' => ot_get_option('menu_font'),
        'headers_font' => ot_get_option('headers_font')
    );
    //get fonts from page content
    if (is_page()) {
        $page = get_page(get_the_ID());
        preg_match_all('/google_web_font_[a-zA-Z0-9. ]*+/i', $page->post_content, $matches);

        if (isset($matches[0]) && is_array($matches[0])) {
            foreach ($matches[0] as $font) {
                $fonts[] = $font;
            }
        }
		
		$cf_font_style = get_post_meta(get_the_ID(), 'footer_cf_font_style', true);
		if (!empty($cf_font_style) && strstr($cf_font_style, 'google_web_font_') && get_post_meta(get_the_ID(), 'footer_style', true) == 'alternative') {
			$fonts[] = $cf_font_style;
		}
    }

    $fonts_to_get = array();
	$fonts_to_get[] = 'Lato';
    $local_fonts_to_load = array();
    $fonts_return = array();
    foreach ($fonts as $key => $font) {
        if (empty($font)) {
            continue;
        }
        $tmp = $font;
        if (strstr($font, 'google_web_font_')) {
            $tmp = str_replace('google_web_font_', '', $tmp);
            $fonts_to_get[] = $tmp;
        }

        $fonts_return[$key] = $tmp;
    }

    $fonts_to_get = array_unique($fonts_to_get);
    $subset = '';
    $character_sets = ot_get_option('character_sets');
    if (is_array($character_sets) && count($character_sets) > 0) {
        $subset = '&subset=latin,' . implode(',', $character_sets);
    }

    if (count($fonts_to_get) > 0) {
        $protocol = is_ssl() ? 'https' : 'http';

        foreach ($fonts_to_get as $font) {
			
			$font_url = $protocol.'://fonts.googleapis.com/css?family='.urlencode($font).':400,800,300,700,900';
            ?>
            <link href="<?php echo esc_url($font_url); ?>" rel="stylesheet" type="text/css">
        <?php
        }
    }

    if (count($local_fonts_to_load) > 0) {
        ?>
        <style type="text/css">
            <?php
            foreach ($local_fonts_to_load as $font)
            {?>
            @font-face {
                font-family: '<?php echo esc_attr($font); ?>';
                src: url("<?php echo get_template_directory_uri()?>/font/<?php echo esc_attr($font); ?>.otf") format("opentype");
            }

            <?php
            } ?>
        </style>
    <?php
    }

    return $fonts_return;
}

if (!function_exists('king_theme_styles')) :

    function king_theme_styles()
    {
        $fonts = king_get_used_googe_web_fonts();
		
		$content_font = isset($fonts['content_font']) ? $fonts['content_font'] : '';
        $title_font = isset($fonts['title_font']) ? $fonts['title_font'] : '';
        $menu_font = isset($fonts['menu_font']) ? $fonts['menu_font'] : '';
        $headers_font = isset($fonts['headers_font']) ? $fonts['headers_font'] : '';
        ?>
        <style type="text/css">
            <?php if (!empty( $content_font ) && $content_font != 'default'): ?>
				body {
					font-family: '<?php echo esc_attr($content_font); ?>';
				}
            <?php endif; ?>

            <?php if (ot_get_option('content_font_size')): ?>
				body,
				p {
					font-size: <?php echo esc_attr(ot_get_option('content_font_size')); ?>px;
				}
            <?php endif; ?>

            <?php if (!empty( $title_font ) && $title_font != 'default'): ?>
				.page-heading h1 {
					font-family: '<?php echo esc_attr($title_font); ?>';
				}
            <?php endif; ?>

            <?php if (ot_get_option('title_font_size')): ?>
				.page-heading h1 {
					font-size: <?php echo esc_attr(ot_get_option('title_font_size')); ?>px;
				}
            <?php endif; ?>

            <?php if (!empty( $menu_font ) && $menu_font != 'default'): ?>
				#main-nav>li>a, 
				#header div.menu>ul>li>a,
				#main-nav li ul li a, 
				#main-nav .mega-menu li span, 
				#main-nav .mega-menu-footer span, 
				#side-nav-menu>li>a, 
				#side-nav li ul li>a, 
				#side-nav .mega-menu li span, 
				#side-nav .mega-menu-footer span {
					font-family: '<?php echo esc_attr($menu_font); ?>';
				}
            <?php endif; ?>

            <?php if (ot_get_option('menu_font_size')): ?>
				#main-nav>li>a, 
				#header div.menu>ul>li>a {
					font-size: <?php echo esc_attr(ot_get_option('menu_font_size')); ?>px;
				}
            <?php endif; ?>

            <?php if (!empty( $headers_font ) && $headers_font != 'default'): ?>
				h1,
				h2,
				h3,
				h4,
				h5,
				h6,
				aside h5 {
                font-family: '<?php echo esc_attr($headers_font); ?>';
            }
            <?php endif; ?>

            <?php if (ot_get_option('h1_size')): ?>
				h1 {
                font-size: <?php echo esc_attr(ot_get_option('h1_size')); ?>px;
            }
            <?php endif; ?>

            <?php if (ot_get_option('h2_size')): ?>
				h2 {
                font-size: <?php echo esc_attr(ot_get_option('h2_size')); ?>px;
            }
            <?php endif; ?>

            <?php if (ot_get_option('h3_size')): ?>
				h3 {
                font-size: <?php echo esc_attr(ot_get_option('h3_size')); ?>px;
            }
            <?php endif; ?>

            <?php if (ot_get_option('h4_size')): ?>
				h4 {
                font-size: <?php echo esc_attr(ot_get_option('h4_size')); ?>px;
            }
            <?php endif; ?>

            <?php if (ot_get_option('h5_size')): ?>
				h5 {
                font-size: <?php echo esc_attr(ot_get_option('h5_size')); ?>px;
            }
            <?php endif; ?>

            <?php if (ot_get_option('h6_size')): ?>
				h6 {
                font-size: <?php echo esc_attr(ot_get_option('h6_size')); ?>px;
            }
            <?php endif; ?>

            <?php 
			
			
			if (is_singular(array('post', 'page')) && $page_body_class = get_post_meta(get_the_ID(), 'body_class', true) ) {
				$body_class = $page_body_class;
			}
			else {
				$body_class = ot_get_option('body_class');
			}
			
			if (
				in_array($body_class,array('b1170','boxed-layout2')) && (!isset($_GET['switch_layout']) || in_array($_GET['switch_layout'],array('b1170','boxed-layout2'))) ||
				isset($_GET['switch_layout']) && ($_GET['switch_layout'] == 'b1170') ||
				(king_check_if_use_control_panel_cookies() && isset($_COOKIE['theme_body_class']) && in_array($_COOKIE['theme_body_class'],array('b1170','boxed-layout2') ) )
			): ?>
				body.b1170, body.b960, body.boxed-layout2 {
					<?php if (isset($_GET['switch_layout']) && ($_GET['switch_layout'] == 'b1170' ) ): ?> 
						background-image: url(<?php echo get_template_directory_uri(); ?>/img/body-img/img8.jpg);
						background-attachment: fixed;
						
					<?php elseif (king_check_if_control_panel() && isset($_COOKIE['theme_background']) && !empty($_COOKIE['theme_background'])): ?> 
						background-image: url(<?php echo get_template_directory_uri(); ?>/img/<?php echo esc_attr($_COOKIE['theme_background']); ?>);
						background-repeat: no-repeat;
						background-position: center;
						background-attachment: fixed;

					<?php elseif (is_singular(array('post', 'page')) && get_post_meta(get_the_ID(), 'background_image', true) != '' ): ?> 
						background-image: url(<?php echo get_post_meta(get_the_ID(), 'background_image', true); ?>);
						
						<?php if (ot_get_option('background_attachment') != '' ): ?> 
							background-attachment: <?php echo ot_get_option('background_attachment'); ?>;
						<?php endif; ?>
						
					<?php elseif (ot_get_option('background_pattern') == 'image' && ot_get_option('background_image') != '' ): ?> 
						background-image: url(<?php echo ot_get_option('background_image'); ?>);

						<?php if (ot_get_option('background_attachment') != '' ): ?> 
							background-attachment: <?php echo ot_get_option('background_attachment'); ?>;
						<?php endif; ?>

					<?php elseif (ot_get_option('background_pattern') != 'none' ): ?> 
						background-image: url(<?php echo get_template_directory_uri(); ?>/img/body-bg/<?php echo ot_get_option('background_pattern'); ?>);
						<?php if (ot_get_option('background_attachment') != '' ): ?> 
							background-attachment: <?php echo ot_get_option('background_attachment'); ?>;
						<?php endif; ?> 
					<?php endif; ?> 
					<?php if (ot_get_option('background_color') != '' ): ?> 
							background-color: <?php echo ot_get_option('background_color'); ?>;
					<?php endif; ?> 
					<?php if (ot_get_option('background_repeat') != '' ): ?> 
							background-repeat: <?php echo ot_get_option('background_repeat'); ?>;
					<?php endif; ?> 
					<?php if (ot_get_option('background_position') != '' ): ?> 
							background-position: <?php echo ot_get_option('background_position'); ?> top;
					<?php endif; ?> 
					<?php if (ot_get_option('background_size') == 'browser' ): ?> 
						-webkit-background-size: cover;
						-moz-background-size: cover;
						-o-background-size: cover;
						background-size: cover;
					<?php endif; ?>
				}
				
				<?php if (
						(king_check_if_control_panel() && isset($_COOKIE['theme_background']) && !empty($_COOKIE['theme_background'])) ||
						ot_get_option('background_pattern') == 'image' && ot_get_option('background_image') != '' ||
						ot_get_option('background_pattern') != 'none' && ot_get_option('background_pattern') != 'image' ||
						ot_get_option('background_color') != ''
					): ?>
					.boxed-layout-container div {
						background-color: transparent;
					}
				<?php endif; ?>
            <?php endif; ?>

            img.logo {
				<?php if (ot_get_option('logo_top_margin')): ?> 
					margin-top: <?php echo ot_get_option('logo_top_margin'); ?>px;
				<?php endif; ?> 
				<?php if (ot_get_option('logo_left_margin')): ?> 
					margin-left: <?php echo ot_get_option('logo_left_margin'); ?>px;
				<?php endif; ?> 
				<?php if (ot_get_option('logo_bottom_margin')): ?> 
					margin-bottom: <?php echo ot_get_option('logo_bottom_margin'); ?>px;
				<?php endif; ?>
            }

            <?php
            if (is_page()) {
                $page_title_color = get_post_meta(get_the_ID(),'page_title_color', true);
                if (empty($page_title_color)) {
                    $page_title_color = ot_get_option('page_title_color');
                }
                if (!empty($page_title_color)) { ?>
                    /* page_title_color */
					.page-heading h1, 
                    .page-heading.style3 h1, 
					.page-heading.style2 h1 {
                        color: <?php echo esc_attr($page_title_color); ?>;
                    }
                <?php } ?>
            <?php }
            
            if (ot_get_option('megamenu_background')) { ?>
                #main-nav .mega-menu>ul,
                .headerstyle1 #main-nav .mega-menu>ul,
                .headerstyle4 #main-nav .mega-menu>ul,
                .headerstyle5 #main-nav .mega-menu>ul,
                .headerstyle8 #main-nav .mega-menu>ul {
                    background-image: url(<?php echo esc_url(ot_get_option('megamenu_background')); ?>);
                }
            <?php }            
            ?>
        </style>
        <style type="text/css" id="dynamic-styles">
            <?php king_the_theme_dynamic_styles(false); ?>
        </style>
        <?php if (ot_get_option('custom_css')): ?>
        <style type="text/css">
            <?php echo ot_get_option('custom_css'); ?>
        </style>

    <?php endif; ?>
		
	<style><?php

        /* ALTERNATE SLIDER TEMPLATE */
        global $post;
		
		$slider_content_bg1 = get_post_meta(is_singular() ? get_the_ID() : null, 'slider_content_bg', true);
        $slider_content_bg2 = get_post_meta(is_singular() ? get_the_ID() : null, 'slider_content_bg_2', true);
        $slider_content_bg_transparency = get_post_meta(is_singular() ? get_the_ID() : null, 'slider_content_bg_transparency', true);
        if (!empty($slider_content_bg1) || !empty($slider_content_bg2)):?>

           .alternate-slider-bg {
				background-image: -webkit-gradient(
				  linear, right top, left top, from(<?php echo king_hex_to_rgb($slider_content_bg1,$slider_content_bg_transparency / 100); ?>),
				  to(<?php echo king_hex_to_rgb($slider_content_bg2, $slider_content_bg_transparency / 100); ?>)
				);
				background-image: -moz-linear-gradient(
				  right center,
				  <?php echo king_hex_to_rgb($slider_content_bg1,$slider_content_bg_transparency / 100); ?> 20%, <?php echo king_hex_to_rgb($slider_content_bg2, $slider_content_bg_transparency / 100); ?> 95%
				);
				filter: progid:DXImageTransform.Microsoft.gradient(
				  gradientType=1, startColor=<?php echo esc_attr($slider_content_bg1); ?>, endColorStr=<?php echo esc_attr($slider_content_bg2); ?>
				);
				-ms-filter: progid:DXImageTransform.Microsoft.gradient(
				  gradientType=1, startColor=<?php echo esc_attr($slider_content_bg1); ?>, endColorStr=<?php echo esc_attr($slider_content_bg2); ?>
				);
			}
         <?php endif;

		$page_title_color = get_post_meta(is_singular() ? get_the_ID() : null, 'page_title_color', true);
		$titlebar_bg = get_post_meta(is_singular() ? get_the_ID() : null, 'titlebar_bg', true);
		$titlebar_background = get_post_meta(is_singular() ? get_the_ID() : null, 'titlebar_background', true);
		$breadcrumb_color = get_post_meta(is_singular() ? get_the_ID() : null, 'breadcrumb_color', true);

		$heading_box=array();
		if(!empty($titlebar_bg)){
			$heading_box[] = 'background-color:'.esc_attr($titlebar_bg);
		}else{
			$heading_box[] = 'background-color:'.ot_get_option('page_title_background_color');
		}

		if(!empty($titlebar_background)){
			$heading_box[] = 'background-image:'.esc_attr($titlebar_background);
		}

		if(is_array($heading_box)){
			echo '.portfolio-heading,.page-heading{'.implode(';',$heading_box).'}';
		}

		if(!empty($page_title_color)){
			 echo '.portfolio-heading h1 span,.page-heading h1{color:'.esc_attr($page_title_color).'!important} ';
		}else{
			echo '.portfolio-heading h1 span,.page-heading h1{color:'.ot_get_option('page_title_color').'!important} ';
		}

		if(!empty($breadcrumb_color)){
			echo '.breadcrumbs a, .breadcrumbs span, .breadcrumbs {
			color:'. $breadcrumb_color.'!important}';
		}
		
        /*  FOR FOOTER  */
        $lower_footer_bg = ot_get_option('copyrights_bar_background');
        if(!empty($lower_footer_bg)){
            echo '#lower-footer{background-color:'.esc_attr($lower_footer_bg).'}';
        }

        $copyrights_bar_text_color = ot_get_option('copyrights_bar_text_color');
        if(!empty($copyrights_bar_text_color)){
            echo '#lower-footer{color:'.esc_attr($copyrights_bar_text_color).'}';
        }

        $footer_background_color =  ot_get_option('footer_background_color');
        if(!empty($footer_background_color)){
            echo '#main-footer{background-color:'.esc_attr($footer_background_color).'}';
        }

        $footer_main_text_color = ot_get_option('footer_main_text_color');
        if(!empty($footer_main_text_color)){
            echo '#main-footer{color:'.esc_attr($footer_main_text_color).'}';
        }

        $footer_headers_color = ot_get_option('footer_headers_color');
        if(!empty($footer_headers_color)){
            echo '#main-footer h4{color:'.esc_attr($footer_headers_color).'}';
        }
        /*  END FOOTER STYLES  */

        $main_body_text_color = ot_get_option('main_body_text_color');
        if(!empty($main_body_text_color)){
            echo '#main-content p{color:'.esc_attr($main_body_text_color).'}';
        }
        $main_body_background_color = ot_get_option('main_body_background_color');
        if(!empty($main_body_background_color)){
            echo 'body, #main-content { background: '.esc_attr($main_body_background_color).' !important   }';

        }
		$content_background_color = king_get_opt('content_background_color');
        if(!empty($content_background_color)){
//             echo '.king-blog, .king-page, .king-page .post-content, .king-blog-main.blog-single article.blog-post { background-color: '.$content_background_color.' !important   }';
        }
		$sidebar_background_color = king_get_opt('sidebar_background_color');
        if(!empty($sidebar_background_color)){
            echo '.king-blog-sidebar:before { background-color: '.esc_attr($sidebar_background_color).' !important   }';
        }
		
		$footer_page_background_color = king_get_opt('footer_page_background_color');
        if(!empty($footer_page_background_color)){
            echo '.footer-page { background-color: '.esc_attr($footer_page_background_color).' !important   }';
        }
		?>
        </style>
    <?php
}

    add_action('wp_head', 'king_theme_styles');
endif;


function king_the_theme_dynamic_styles($ajax_request = true)
{
    $main_color = ot_get_option('main_color');

    //change color if control panel is enabled
    if (king_check_if_control_panel()) {
        if (isset($_GET['main_color']) && !empty($_GET['main_color'])) {
            setcookie('theme_main_color', $_GET['main_color'], null, '/');
            $_COOKIE['theme_main_color'] = $_GET['main_color'];
            $main_color = $_COOKIE['theme_main_color'];
        }

        if (king_check_if_use_control_panel_cookies() && isset($_COOKIE['theme_main_color']) && !empty($_COOKIE['theme_main_color'])) {
            $main_color = $_COOKIE['theme_main_color'];
        }
    }
    ?>
    <?php if (1 == 2): //fake <style> tag, reguired only for editor formatting, please don't remove ?>
    <style>
<?php endif; ?>

    <?php if (ot_get_option('main_body_background_color')): ?>
    .page-wrapper,
    header {
    background:
<?php echo ot_get_option('main_body_background_color'); ?>
}
<?php endif; ?>

    <?php if ($main_color): ?>
    /* main_color */
	a,
	blockquote span, 
	blockquote span a,
	.welcome .right-sec .contents h4,
	.text-primary,
	span.light-orange,
	.site-text-color,
	.pricing-table.style-king .featured .table-price .price-main, 
	.pricing-table.style-king .featured .table-price .price-secondary,
	.contact-info-box .icon,
	.king-blog-effects-container .post-author,
	.page-heading.style3 .breadcrumbs .current,
	.accordion-active .accordion-header h5,
	span.comments-count,
	.king-comments .comment-author span,
	.team-members .intro .more-link:hover,
	span.post-day,
	.king-page .sidebar ul li a:hover,
	.king-blog-sidebar .categories a:hover, .king-blog-sidebar .widget > ul li a:hover,
	.blog-post-king .blog-post-meta a,
	.king-blog .blog-post .post-title a:hover,
	.blog-post footer .comments-counter a,
	.shop-product .product-buttons a.product_type_grouped, .shop-product .product-buttons a.add_to_cart_button,
	.shop-product .product-info .price.blue,
	.woocommerce .shop-product-details .price ins .amount,
	#header.style-light #main-nav > li.current-menu-item > a, #header.header-light #main-nav > li.current-menu-item > a,
	.latest-posts.style2 article .contents a:hover,
	.tp-caption a:hover,
	.tab-header ul li.active-tab a,.king-blog-effects-container .comments-counter a:hover,
	.blog-post footer .social-media li a:hover,
	#king-sideheader #wp-calendar a, .king-blog #wp-calendar a, .king-page #wp-calendar a,
	.blog-post-king .blog-post-meta .post-author,
	.blog-post footer .comments-counter a:hover:after,
	.king-comments .comment-reply-link:hover,
	.king-comments .comment-reply-link:hover .fa,
	.king-comments .comment-edit-link:hover,
	.king-comments .comment-edit-link:hover .fa,
	.blog-post-list .post-content h4 a:hover,
	.project-description .social-media li a:hover
	{
        color: <?php echo esc_attr($main_color); ?>;
    }
	
	ul.arrow-list a:hover, 
	.shop-footer .shop-widget ul a:hover {
		color: <?php echo esc_attr($main_color); ?>;
		border-bottom: 1px solid <?php echo esc_attr($main_color); ?>;
	}

	svg, svg path {
		stroke: <?php echo esc_attr($main_color); ?>;
	}
	
	ul.services-list:not(.style2) li h3,
	h3.special-text span,
	.widget-latest-posts .post-title:hover,
	.king-blog-effects-container .post-title a:hover,
	.king-blog-effects-container .socials-container .social-media li a:hover,
	.shop-filters span.active, .shop-filters span:hover,
	.shopping-cart-dropdown .sc-footer a.button.checkout-button,
	#preheader a:hover,
	.team-members .hiring-block:hover .inner-frame h5,
	.team-members .hiring-block:hover .inner-frame h6,
	.team-members .hiring-block:hover .inner-frame,
	.counter-box .icon i,.error404-content-bottom a:hover,
	#king-sideheader .widget ul li a:hover,
	#main-footer .widget-latest-posts .post-title:hover {
        color: <?php echo esc_attr($main_color); ?> !important;
    }
	
	.team-members-row .team-member:hover,
	.call-to-action:not(.style2) .button,
	.king-blog-effects-container .post-blockquote:not(.style-link),
	.blog-btn:hover, .blog-btn:focus, .blog-btn:active,
	.pricing-table.style-king .ribbon,
	.vc_custom_1439066642566,
	.king-progressbar .progressbar .progress-width,
	.style2 .tab-header ul li.active-tab,
	.testimonial-single .image-shadow,
	.button.shaped.filled,
	.pricing-table.style-king .table-footer .button:hover,
	.info-box a.button:hover,
	.latest-posts.style2 article .social-media li a:hover,
	.blog-post header .post-blockquote:not(.style-link),
	.widget_calendar #today,
	.load-more .button-load-more:hover,
	.timeline-date-tooltip span,
	.load-more .button-load-more:hover,
	.project-box figcaption a.btn:hover,
	.king-contact-form input[type="submit"]:hover,
	.project.style-king .project-hover .project-button:hover,
	.bg-section-accent,#back-to-top a:hover
    {
        background-color: <?php echo esc_attr($main_color); ?> !important;
    }

	a.button:hover, 
	input[type="submit"].blue:hover, 
	.get-in-touch.light input[type="submit"]:hover,
	.ribbon.onsale,.team-members .intro .more-link:hover:before {
		background-color: <?php echo king_change_color($main_color, 10); ?>;
	}
	
	.service-icon .icons, 
	.style2 .tab-header ul li.active-tab .icons, 
	.blue-bg .icons, 
	.darker-blue-bg p, 
	.darker-blue-bg .icons {
		color: <?php echo king_change_color($main_color, 60); ?>;
	}
	
	.service .content_box, 
	span.testimonial-job, 
	.sorting-tags div.filter.active, 
	.sorting-tags div.filter:hover {
		color: <?php echo king_change_color($main_color, 20); ?>;
	}
	
	.services-list li,
	.team-members .intro .more-link:hover,
	.error404-content-bottom ul li a:hover {
		border-bottom-color: <?php echo esc_attr($main_color); ?>;
	}
	
	#navigation-lightbox .mega-menu > .sub-menu, #navigation-lightbox #main-nav > li .sub-menu, 
	#header.style-king .mega-menu > .sub-menu, #header.style-king #main-nav > li .sub-menu,
	.tab-header ul li.active-tab,
	.timeline-date-tooltip span:after {
		border-top-color: <?php echo esc_attr($main_color); ?>;
	}
	
	.team-members-row .team-member:hover .tm-info:before {
		border-right-color: <?php echo esc_attr($main_color); ?>;
	}

	.team-members-row .team-member.side-right:hover .tm-info:before {
		border-left-color: <?php echo esc_attr($main_color); ?>;
	}
	
	.blog-btn:hover, .blog-btn:focus, .blog-btn:active,
	.project-description .social-media li a:hover,
	.king-page .sidebar ul li a:hover,
	.load-more .button-load-more:hover,
	.blog-post footer .social-media li a:hover,
	.load-more .button-load-more:hover,
	.pricing-table.style-king .table-footer .button:hover,
	.project-box figcaption a.btn:hover,
	.woocommerce .shop-product-submit .qty,
	.latest-posts article .social-media li a:hover,
	.team-members .intro .more-link:hover,
	.team-members .hiring-block:hover .inner-frame h5,
	.team-members .hiring-block:hover .inner-frame h6,
	.team-members .hiring-block:hover .inner-frame,
	.team-members .hiring-block .inner-frame:before,
	#king-sideheader .social-media ul li a:hover, #header.style-king .social-media li a:hover,
	.call-to-action:not(.style2) .button,
	.king-contact-form input[type="submit"]:hover,
	.project.style-king .project-hover .project-button:hover,
	#king-sideheader .social-media li a:hover, .widget_social_media .social-media li a:hover  {
		border-color: <?php echo esc_attr($main_color); ?> !important;
	}
	
	<?php
	$main_color_lighter = king_change_color($main_color, 10);
	?>
	
	.alternate-slider-bg {
		background-image: -webkit-gradient(
		  linear, right top, left top, from(<?php echo king_hex_to_rgb($main_color,'0.8'); ?>),
		  to(<?php echo king_hex_to_rgb($main_color_lighter, '0.8'); ?>)
		);
		background-image: -moz-linear-gradient(
		  right center,
		  <?php echo king_hex_to_rgb($main_color,'0.8'); ?> 20%, <?php echo king_hex_to_rgb($main_color_lighter,'0.8'); ?> 95%
		);
		filter: progid:DXImageTransform.Microsoft.gradient(
		  gradientType=1, startColor=<?php echo esc_attr($main_color); ?>, endColorStr=<?php echo esc_attr($main_color_lighter); ?>
		);
		-ms-filter: progid:DXImageTransform.Microsoft.gradient(
		  gradientType=1, startColor=<?php echo esc_attr($main_color); ?>, endColorStr=<?php echo esc_attr($main_color_lighter); ?>
		);
	}

<?php endif; ?>

    <?php if (ot_get_option('main_body_text_color')): ?>
    /* main_body_text_color */
		body {
			color: <?php echo esc_attr(ot_get_option('main_body_text_color')); ?>
		;
		}
	<?php endif; ?>

    <?php if (ot_get_option('header_background_color')): ?>
		/* header_background_color */
		#main-header, 
		#lower-header {
			background-color: <?php echo esc_attr(ot_get_option('header_background_color')); ?>;
		}
	<?php endif; ?>

    <?php if (ot_get_option('page_title_background_color')): ?>
		/* page_title_background_color */
		.page-header {
			background-color: <?php echo esc_attr(ot_get_option('page_title_background_color')); ?>;
		}
	<?php endif; ?>

    <?php if (ot_get_option('menu_background_color')): ?>
		/* menu_background_color */
		#lower-header,
		body.b1170 #lower-header>.container, 
		body.b960 #lower-header>.container, 
		#header.style3 #lower-header, 
		#header.style4 #lower-header, 
		#header.style5 #lower-header, 
		#header.style6 #lower-header {
			background-color: <?php echo king_hex_to_rgb(ot_get_option('menu_background_color'),'.67'); ?>;
		}
		
		body.b1170 #header.style2 #main-header>.container, 
		body.b960 #header.style2 #main-header>.container, 
		#header.style2 #main-header, 
		#header.style8 #main-header {
			background-color: <?php echo king_hex_to_rgb(ot_get_option('menu_background_color'),'.56'); ?>;
		}
		
	<?php endif; ?>
		
	<?php if (ot_get_option('sub_menu_background_color')): ?>
		
		.headerstyle1 #main-nav li ul li a, 
		.headerstyle1 #header div.menu>ul li ul li a, 
		.headerstyle5 #main-nav li ul li a, 
		.headerstyle5 #header div.menu>ul li ul li a, 
		.headerstyle8 #main-nav li ul li a, 
		.headerstyle8 #header div.menu>ul li ul li a,
		
		.headerstyle1 #main-nav li ul li.current-menu-item>a, 
		.headerstyle1 #main-nav li ul li.current-menu-ancestor>a, 
		.headerstyle1 #header div.menu>ul li ul li.current-menu-item>a, 
		.headerstyle1 #header div.menu>ul li ul li.current-menu-ancestor>a, 
		.headerstyle1 #main-nav li ul li:hover>a, 
		.headerstyle1 #header div.menu>ul li ul li:hover>a, 
		.headerstyle5 #main-nav li ul li.current-menu-item>a, 
		.headerstyle5 #main-nav li ul li.current-menu-ancestor>a, 
		.headerstyle5 #header div.menu>ul li ul li.current-menu-item>a, 
		.headerstyle5 #header div.menu>ul li ul li.current-menu-ancestor>a, 
		.headerstyle5 #main-nav li ul li:hover>a, 
		.headerstyle5 #header div.menu>ul li ul li:hover>a, 
		.headerstyle8 #main-nav li ul li.current-menu-item>a, 
		.headerstyle8 #main-nav li ul li.current-menu-ancestor>a, 
		.headerstyle8 #header div.menu>ul li ul li.current-menu-item>a, 
		.headerstyle8 #header div.menu>ul li ul li.current-menu-ancestor>a, 
		.headerstyle8 #main-nav li ul li:hover>a, 
		.headerstyle8 #header div.menu>ul li ul li:hover>a,
		
		.headerstyle1 #main-nav .mega-menu>ul, 
		.headerstyle4 #main-nav .mega-menu>ul, 
		.headerstyle5 #main-nav .mega-menu>ul, 
		.headerstyle8 #main-nav .mega-menu>ul, 
		
		#main-nav .mega-menu>ul, 
		#main-nav .mega-menu-footer, 
		.headerstyle1 #main-nav .mega-menu-footer, 
		.headerstyle4 #main-nav .mega-menu-footer, 
		.headerstyle5 #main-nav .mega-menu-footer, 
		.headerstyle8 #main-nav .mega-menu-footer
		{
			background-color: <?php echo esc_attr(ot_get_option('sub_menu_background_color')); ?>;
		}
		
		#main-nav li ul li a, 
		#header div.menu>ul li ul li a {
			background: <?php echo ot_get_option('sub_menu_background_color'); ?>;
			background-color: <?php echo king_hex_to_rgb(ot_get_option('sub_menu_background_color'),'.89'); ?>;
		}
		
		
		#header.style3 #lower-header #main-nav li ul li.current-menu-item>a, 
		#header.style3 #lower-header #main-nav li ul li.current-menu-ancestor>a, 
		#header.style4 #lower-header #main-nav li ul li.current-menu-item>a, 
		#header.style4 #lower-header #main-nav li ul li.current-menu-ancestor>a, 
		#header.style3 #lower-header div.menu>ul li ul li.current-menu-item>a, 
		#header.style3 #lower-header div.menu>ul li ul li.current-menu-ancestor>a, 
		#header.style4 #lower-header div.menu>ul li ul li.current-menu-item>a, 
		#header.style4 #lower-header div.menu>ul li ul li.current-menu-ancestor>a,
		
		#main-nav li ul li:hover>a, 
		#header div.menu>ul li ul li:hover>a, 
		
		#main-nav li ul li.current-menu-item>a, 
		#main-nav li ul li.current-menu-ancestor>a, 
		#header div.menu>ul li ul li.current-menu-item>a, 
		#header div.menu>ul li ul li.current-menu-ancestor>a
		{
			background: <?php echo esc_attr(ot_get_option('sub_menu_background_color')); ?>;
			background-color: <?php echo king_hex_to_rgb(ot_get_option('sub_menu_background_color'),'.95'); ?>;
		}
		
	<?php endif; ?>

    <?php if (ot_get_option('headers_text_color')): ?>
		/* headers_text_color */
		#main-content h1,
		#main-content h2,
		#main-content h3,
		#main-content h4,
		#main-content h5,
		#main-content h6 {
			color: <?php echo ot_get_option('headers_text_color'); ?>;
		}
	<?php endif; ?>
    
    <?php if(ot_get_option('preloader_dot_color')): ?>
    	svg.preloader circle.dot { 
    		fill: <?php echo ot_get_option('preloader_dot_color'); ?>
    	}
    <?php endif; ?>

    <?php if(ot_get_option('preloader_dot_color_second')): ?>
    	svg.preloader circle.dot:nth-of-type(2) { 
    		fill: <?php echo ot_get_option('preloader_dot_color_second'); ?>
    	}
    <?php endif; ?>

    <?php if (ot_get_option('footer_background_color')): ?>
		/* footer_background_color */
		#main-footer {
			background-color: <?php echo esc_attr(ot_get_option('footer_background_color')); ?>;
		}
		
		#main-footer input[type="text"], 
		#main-footer input[type="password"], 
		#main-footer textarea {
			background-color:  <?php echo king_change_color(ot_get_option('footer_background_color'),10); ?>;
		}
		
		#main-footer input[type="text"]:focus, 
		#main-footer input[type="password"]:focus, 
		#main-footer textarea:focus {
			background-color:  <?php echo king_change_color(ot_get_option('footer_background_color'),15); ?>;
		}
	<?php endif; ?>

    <?php if (ot_get_option('footer_headers_color')): ?>
		#footer h4 {
			color: <?php echo esc_attr(ot_get_option('footer_headers_color')); ?>;
		}
	<?php endif; ?>

    <?php if (ot_get_option('footer_main_text_color')): ?>
		/* footer_main_text_color */
		#main-footer, 
		#main-footer .blog-post .post-title {
			color: <?php echo esc_attr(ot_get_option('footer_main_text_color')); ?>;
		}
	<?php endif; ?>

    <?php if (ot_get_option('copyrights_bar_background')): ?>
		/* copyrights_bar_background */
		#lower-footer, 
		#footer.style-king #lower-footer {
			background-color: <?php echo esc_attr(ot_get_option('copyrights_bar_background')); ?>;
		}
	<?php endif; ?>

    <?php if (ot_get_option('copyrights_bar_text_color')): ?>
		/* copyrights_bar_text_color */
		#lower-footer,
		#footer.style-king #lower-footer .copyright,
		#footer.style-king #lower-footer .social-media li a {
			color: <?php echo esc_attr(ot_get_option('copyrights_bar_text_color')); ?>;
			border-color: <?php echo esc_attr(ot_get_option('copyrights_bar_text_color')); ?>;
		}
	<?php endif; ?>

    <?php if (1 == 2): //fake </style> tag, reguired only for editor formatting, please don't remove ?>
    </style>
<?php endif; ?>
    <?php
    if ($ajax_request === true) {
        die();
    }
}

function king_the_theme_dynamic_styles_ajax_request()
{
    king_the_theme_dynamic_styles(true);
}

add_action('wp_ajax_nopriv_the_theme_dynamic_styles', 'king_the_theme_dynamic_styles_ajax_request');
add_action('wp_ajax_the_theme_dynamic_styles', 'king_the_theme_dynamic_styles_ajax_request');


if (!function_exists('king_theme_navi')):

    /**
     * Posts annd search pagination
     *
     * @since king 1.0
     */
    function king_theme_navi()
    {
        $args = array(
            'container' => 'ul',
            'container_id' => 'pager',
            'container_class' => 'post-pagination',
            'items_wrap' => '<li class="%s"><span></span>%s</li>',
            'item_class' => '',
            'item_active_class' => '',
            'list_item_active_class' => 'active',
            'item_prev_class' => 'prev-page',
            'item_next_class' => 'next-page',
            'prev_text' => '',
            'next_text' => ''
        );
        king_wp_custom_corenavi($args);
    }
endif; //ends check for theme_navi

if ( ! function_exists( 'king_comment' ) ) :
/**
 * Comments and pingbacks. Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since king 1.0
 */
function king_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
			?>
			<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
				<p><?php _e( 'Pingback:', 'king' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'king' ), ' ' ); ?></p>
			</li>
			<?php
		break;
	
		default :
			?>
			<!-- Comment Item -->
			<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
					
				<div class="comment-inner">
					<div class="comment-avatar">
						<?php echo get_avatar( $comment, 70 ); ?>
					</div>
					<div class="comment-content">
						<header>
							<div class="comment-author-info">
								<span class="comment-author"><span><?php comment_author_link();?></span> <?php _e('says:', 'king');?></span>
								<span class="date"><?php echo comment_date(get_option('date_format')) ?>, <?php echo comment_date(get_option('time_format')) ?></span>
							</div>
							<div class="comment-reply">
								<?php 
								
								$icon = '<i class="fa fa-comment"></i> ';
								
								$args_2 = array(
									'depth' => $depth, 
									'max_depth' => 3,
									'reply_text'    => $icon.__( 'Reply', 'king' ),
									'reply_to_text' => $icon.__( 'Reply to %s', 'king' ),
									'login_text'    => $icon.__( 'Log in to Reply', 'king' ),
								);
								
								echo get_comment_reply_link( array_merge($args,$args_2) );
								
								edit_comment_link( '<i class="fa fa-edit"></i> '.__( 'Edit', 'king' ));?>
								
								
							</div>
						</header>
						
						<?php if ( $comment->comment_approved == '0' ) : ?>
							<em><?php _e( 'Your comment is awaiting moderation.', 'king' ); ?></em>
						<?php endif; ?>
						<?php comment_text(); ?>
						
					</div>
				</div>
			<?php
			break;
	endswitch;
}

endif; // ends check for king_comment()

if (!function_exists('king_close_comment')):
/**
 * Close comment
 * 
 * @since king 1.0
 */
function king_close_comment() { ?>		
			</li>
			<!-- End Comment Item -->
<?php }

endif; // ends check for king_close_comment()

if (!function_exists('king_get_theme_navi_array')):

    /**
     * Get Posts annd search pagination array
     *
     * @since king 1.0
     */
    function king_get_theme_navi_array()
    {
        $args = array(
            'container' => '',
            'container_id' => 'pager',
            'container_class' => 'clearfix',
            'items_wrap' => '%s',
            'item_class' => 'page',
            'item_active_class' => 'active',
            'item_prev_class' => 'prev-page',
            'item_next_class' => 'next-page',
            'prev_text' => '',
            'next_text' => '',
            'next_prev_only' => false,
            'type' => 'array'
        );
        return king_wp_custom_corenavi($args);
    }
endif; //ends check for theme_navi

if (!function_exists('king_get_theme_next_page_url')):

    /**
     * Get next page url
     *
     * @since king 1.0
     */
    function king_get_theme_next_page_url($max_num_pages = null)
    {
        $args = array(
            'container' => '',
            'container_id' => 'pager',
            'container_class' => 'clearfix',
            'items_wrap' => '%s',
            'item_class' => 'page',
            'item_active_class' => 'active',
            'item_prev_class' => 'prev-page',
            'item_next_class' => 'next-page',
            'prev_text' => '',
            'next_text' => '',
            'next_prev_only' => true,
            'type' => 'array',
			'max_num_pages' => $max_num_pages
        );
        $pages = king_wp_custom_corenavi($args);

		if (isset($pages['links']['next'])) {
            preg_match('/href="(.*)"/', $pages['links']['next'], $matches);

            if (isset($matches[1]) && !empty($matches[1])) {
                return $matches[1];
            }
        }
        return false;
    }
endif;

if (!function_exists('king_get_theme_prev_page_url')):

    /**
     * Get prev page url
     *
     * @since king 1.0
     */
    function king_get_theme_prev_page_url()
    {
        $args = array(
            'container' => '',
            'container_id' => 'pager',
            'container_class' => 'clearfix',
            'items_wrap' => '%s',
            'item_class' => 'page',
            'item_active_class' => 'active',
            'item_prev_class' => 'prev-page',
            'item_next_class' => 'next-page',
            'prev_text' => '',
            'next_text' => '',
            'next_prev_only' => true,
            'type' => 'array'
        );
        $pages = king_wp_custom_corenavi($args);
		
		if (isset($pages['links']['prev'])) {
            preg_match('/href="(.*)"/', $pages['links']['prev'], $matches);

            if (isset($matches[1]) && !empty($matches[1])) {
                return $matches[1];
            }
        }
        return false;
    }
endif;

if (!function_exists('king_the_king_navi')):

    /**
     * Show corp navi
     *
     * @since king 1.0
     */
    function king_the_king_navi()
    {
        $pagination = king_get_theme_navi_array();
        if (is_array($pagination['links']) && count($pagination['links']) > 0) {
            ?>
            <ul class='post-pagination'>
                <?php foreach ($pagination['links'] as $key => $item): ?>

                    <?php if ($key == 'prev' || $key == 'next'): ?>
                        
                            <?php echo '<li>'.$item.'</li>'; ?>
                        
                    <?php else: ?>
                        <li <?php echo($pagination['active'][(int)$key] === true ? 'class="active"' : '') ?>>
                            <?php echo '<span></span>'.$item; ?>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        <?php
        }
    }
endif; //ends check for theme_navi

function king_the_breadcrumbs($delimiter=null)
{

    /* === OPTIONS === */
    $text['home'] = __('Home', 'king'); // text for the 'Home' link
    $text['category'] = __('Archive by Category "%s"', 'king'); // text for a category page
    $text['search'] = __('Search Results for "%s" Query', 'king'); // text for a search results page
    $text['tag'] = __('Posts Tagged "%s"', 'king'); // text for a tag page
    $text['author'] = __('Posts by %s', 'king'); // text for an author page
    $text['404'] = __('404 Page Not Found', 'king'); // text for the 404 page

    $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
    $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
    if(empty($delimiter)){
        $delimiter = '<span class="delimiter">|</span> '; // delimiter between crumbs
    }

    $before = '<span class="current">'; // tag before the current crumb
    $after = '</span>'; // tag after the current crumb
    /* === END OF OPTIONS === */

    global $post;
    $homeLink = home_url() . '/';
    $linkBefore = '<span typeof="v:Breadcrumb">';
    $linkAfter = '</span>';
    $linkAttr = ' rel="v:url" property="v:title"';
    $link = $linkBefore . '<a' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;

    if (is_home() || is_front_page()) {

        if ($showOnHome == 1) echo '<p class="breadcrumbs"><a href="' . esc_url($homeLink) . '">' . $text['home'] . '</a></p>';

    } else {

        echo '<p class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">' . sprintf($link, esc_url($homeLink), $text['home']) . $delimiter;

        if (function_exists('is_shop') && is_shop()) {
            echo $before . __('Shop', 'king') . $after;
        } else if (is_category()) {
            $thisCat = get_category(get_query_var('cat'), false);
            if ($thisCat->parent != 0) {
                $cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
                $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                echo $cats;
            }
            echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;

        } elseif (is_search()) {
            echo $before . sprintf($text['search'], get_search_query()) . $after;

        } elseif (is_day()) {
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F')) . $delimiter;
            echo $before . get_the_time('d') . $after;

        } elseif (is_month()) {
            echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
            echo $before . get_the_time('F') . $after;

        } elseif (is_year()) {
            echo $before . get_the_time('Y') . $after;

        } elseif (is_single() && !is_attachment()) {
            if (get_post_type() != 'post') {
                $post_type = get_post_type_object(get_post_type());

                if ($post_type->query_var == 'product') {
                    $label = __('Shop', 'king');
                } else {
                    $label = $post_type->labels->singular_name;
                }
                $slug = $post_type->rewrite;

                $portfolio_page_id = null;
                if (get_post_type() == 'portfolio') {
                    $portfolio_page_id = ot_get_option('portfolio_page');
                }
                if (!empty($portfolio_page_id)) {
                    echo '<a href="' . esc_url(get_permalink($portfolio_page_id)) . '">' . get_the_title($portfolio_page_id) . '</a>';
                } else {
                    printf($link, get_post_type_archive_link($post_type->name), $label);
                }
                if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
            } else {
                $cat = get_the_category();
                $cat = isset($cat[0]) ? $cat[0] : null;
                $cats = get_category_parents($cat, TRUE, $delimiter);
                if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                echo $cats;
                if ($showCurrent == 1) echo $before . get_the_title() . $after;
            }

        } elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
            $post_type = get_post_type_object(get_post_type());
            echo $before . $post_type->labels->singular_name . $after;

        } elseif (is_attachment()) {
            $parent = get_post($post->post_parent);
            $cat = get_the_category($parent->ID);
            $cat = isset($cat[0]) ? $cat[0] : null;
            $cats = get_category_parents($cat, TRUE, $delimiter);
            if (!is_wp_error($cats)) {
                $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                echo $cats;
                printf($link, get_permalink($parent), $parent->post_title);
            }
            if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

        } elseif (is_page() && !$post->post_parent) {
            if ($showCurrent == 1) echo $before . get_the_title() . $after;

        } elseif (is_page() && $post->post_parent) {
            $parent_id = $post->post_parent;
            $breadcrumbs = array();
            while ($parent_id) {
                $page = get_page($parent_id);
                $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
                $parent_id = $page->post_parent;
            }
            $breadcrumbs = array_reverse($breadcrumbs);
            for ($i = 0; $i < count($breadcrumbs); $i++) {
                echo $breadcrumbs[$i];
                if ($i != count($breadcrumbs) - 1) echo $delimiter;
            }
            if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

        } elseif (is_tag()) {
            echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;

        } elseif (is_author()) {
            global $author;
            $userdata = get_userdata($author);
            echo $before . sprintf($text['author'], $userdata->display_name) . $after;

        } elseif (is_404()) {
            echo $before . $text['404'] . $after;
        }
        echo '</p>';

    }
}

/**
 * Get post slider
 * @since king 1.0
 */
if (!function_exists('king_get_post_slider')) {
    function king_get_post_slider($post_id, $force_slider = null)
    {
		if ($force_slider == null) {
			$a = get_post_meta($post_id, 'post_slider', true);
		} else {
			$a = $force_slider;
		}

		//layerslider
        if (strstr($a, 'LayerSlider')) {
            $id = str_replace('LayerSlider-', '', $a);
            return do_shortcode('[layerslider id="' . $id . '"]');
		
		//revslider
        } else if (strstr($a, 'revslider')) {
            $id = str_replace('revslider-', '', $a);
            return do_shortcode('[rev_slider ' . $id . ']');
		
		//masterslider
        } else if (strstr($a, 'masterslider')) {
            $id = str_replace('masterslider-', '', $a);
            return do_shortcode('[masterslider id="' . $id . '"]');
        }
		
    }
}


/**
 * Share buttons
 * Can be included in the loop only
 * @since king 1.0
 */

function king_share_buttons($class = '')
{
    ?>
    <div class="ts-share">
        <ul class="social-icons style2 <?php echo sanitize_html_classes($class); ?>">
            <li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(esc_url(get_permalink())); ?>"
                   onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                    <i class="icon-facebook-squared"></i>
                </a>
			</li>
            <li>
				<a target="_blank" class="twitter-share-button"
                   href="https://twitter.com/share?url=<?php echo urlencode(esc_url(get_permalink())); ?>&text=<?php echo urlencode(get_the_title()); ?>"
                   onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                    <i class="icon-twitter"></i>
				</a>
			</li>
            <li>
				<a href="https://plus.google.com/share?url=<?php echo urlencode(esc_url(get_permalink())); ?>"
                   onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
					<i class="icon-google"></i>
                </a>
            </li>
			<?php 
			$pin_image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'pinterest' );
			if ($pin_image): ?>
				<li>
					<a target="_blank" href="http://pinterest.com/pin/create%2Fbutton/?url=<?php echo urlencode(esc_url(get_permalink())); ?>&media=<?php echo urlencode($pin_image[0]); ?>&description=<?php echo urlencode(get_the_title()); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
						<i class="icon-pinterest"></i>
					</a>
				</li>
			<?php endif; ?>
			<li>
				<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(esc_url(get_permalink())); ?>&title=<?php echo urlencode(get_the_title()); ?>&summary=<?php echo urlencode(king_get_the_excerpt_theme(10)); ?>&source=<?php echo get_bloginfo( 'name' );?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=531,width=545');return false;">
					<i class="icon-linkedin"></i>
				</a>
			</li>	
        </ul>
    </div>

<?php
}

/**
 * Share buttons
 * Can be included in the loop only
 * @since king 1.0
 */

function king_share_buttons_king($active = false)
{
	
	
    ?>
	<ul class="social-media">
		<?php if ($active === false || (is_array($active) && in_array('twitter',$active) ) ): ?>
			<li>
				<a target="_blank" class="twitter-share-button"
				   href="https://twitter.com/share?url=<?php echo urlencode(esc_url(get_permalink())); ?>&text=<?php echo urlencode(get_the_title()); ?>"
				   onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
					<i class="icon-twitter"></i>
				</a>
			</li>
		<?php endif; ?>
		
		<?php if ($active === false || (is_array($active) && in_array('facebook',$active) ) ): ?>
			<li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(esc_url(get_permalink())); ?>"
				   onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
					<i class="icon-facebook"></i>
				</a>
			</li>
		<?php endif; ?>
		
		<?php if ($active === false || (is_array($active) && in_array('pinterest',$active) ) ): ?>
			<?php 
			$pin_image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'king-pinterest' );
			if ($pin_image): ?>
				<li>
					<a target="_blank" href="http://pinterest.com/pin/create%2Fbutton/?url=<?php echo urlencode(esc_url(get_permalink())); ?>&media=<?php echo urlencode($pin_image[0]); ?>&description=<?php echo urlencode(get_the_title()); ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
						<i class="icon-pinterest"></i>
					</a>
				</li>
			<?php endif; ?>
		<?php endif; ?>
		
		<?php if ($active === false || (is_array($active) && in_array('google_plus',$active) ) ): ?>
		<li>
			<a href="https://plus.google.com/share?url=<?php echo urlencode(esc_url(get_permalink())); ?>"
			   onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
				<i class="icon-google"></i>
			</a>
		</li>
		<?php endif; ?>
		
		<?php if ($active === false || (is_array($active) && in_array('linkedin',$active) ) ): ?>
		<li>
			<a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(esc_url(get_permalink())); ?>&title=<?php echo urlencode(get_the_title()); ?>&summary=<?php echo urlencode(king_get_the_excerpt_theme(10)); ?>&source=<?php echo get_bloginfo( 'name' );?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=531,width=545');return false;">
				<i class="icon-linkedin"></i>
			</a>
		</li>
		<?php endif; ?>
		
		<?php if ($active === false || (is_array($active) && in_array('comments',$active) ) ): ?>
			<li><a href="<?php echo esc_url(get_comments_link()); ?>"><i class="icon-chat"></i></a></li>
		<?php endif; ?>
	</ul>
<?php
}

/**
 * Outputs the pre header
 * @param int $cols
 */
function king_show_preheader()
{

}

/**
 * Outputs king pre header
 * @param int $cols
 */
function king_show_king_preheader()
{

    global $woocommerce;

    $preheader = ot_get_option('preheader');
	
    $show_preheader  = ot_get_option('show_preheader');
    $preheader_style = ot_get_option('preheader_style');
    $preheader_layout = ot_get_option('preheader_layout');

		$show_post_preheader = get_post_meta(is_singular() ? get_the_ID() : null, 'show_preheader', true);
    if (in_array($show_post_preheader, array('yes', 'no'))) {
        $show_preheader = $show_post_preheader;
    }

    $post_preheader_style = get_post_meta(is_singular() ? get_the_ID() : null, 'preheader_style', true);
    if (in_array($post_preheader_style, array('page-preheader', 'shop-preheader'))) {
        $preheader_style = $post_preheader_style;
    }

    $post_preheader_layout = get_post_meta(is_singular() ? get_the_ID() : null, 'preheader_layout', true);
   
    if (in_array($post_preheader_layout, array('with-container', 'full-width-without-container'))) {
        $preheader_layout = $post_preheader_layout;
    }
	
	$show_preheader_social_icons  = king_get_opt('show_preheader_social_icons');
	
	if ($show_preheader == 'yes'): 
		switch ($preheader_style) {
			case 'page-preheader': ?>
				<div id="preheader" class="<?php echo sanitize_html_classes($preheader_layout); ?>">
					<?php if($preheader_layout == 'with-container'): ?><div class="container"><?php endif; ?>
						<div class="king-preheader">
							<div class="row">
								<div class="col-lg-8 col-md-8 col-sm-8">
									<!-- Text List -->
									<?php
									if (is_array($preheader) && count($preheader) > 0):
										
									?>
										<ul class="text-list">
											<?php foreach ($preheader as $item): 
												
												switch ($item['type']):
													case 'date':
														echo '<li> '.date(get_option('date_format')).'</li>';
														break;

													default: ?>
														<li><?php echo wp_kses_post($item['text']); ?></li>
														<?php
												endswitch; ?>
											<?php endforeach; ?>
										</ul>
									<?php endif; ?>
									</ul>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 align-right">
									<?php get_template_part('inc/social-icons'); ?>
								</div>
							</div>
						</div>
					<?php if($preheader_layout == 'with-container'): ?></div><?php endif; ?>
				</div>
				<?php break;
			case 'shop':
			default: ?>
				<!-- Preheader -->
				<div id="preheader">
					<div class="container">
						<div class="king-preheader">
							<div class="row">
								<div class="col-lg-8 col-md-8 col-sm-8">
									<?php
										$menu_id = null;
										// if (strstr($item['type'],'menu_')) {
										// 	$menu_id = str_replace('menu_', '', $item['type']);
										// 	$item['type'] = 'menu';
										// } 
									
										if (has_nav_menu( 'shop-header' )) {
											$defaults = array(
												'container'			=> false,
												'container_class'	=> 'menu',
												'fallback_cb'		=> '',
												'menu'				=> $menu_id,
												'theme_location'	=> 'shop-header',
												'depth' 			=> 1
											);
											wp_nav_menu( $defaults );
										}
									?>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 align-right">

									<?php if (is_object($woocommerce)) { 

										ob_start();
									    woocommerce_mini_cart();
									    $mini_cart = ob_get_clean();
    								?>
															
									<div class="cart-menu-item"><a href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>"><span class="cart-count"><?php echo esc_html($woocommerce->cart->cart_contents_count); ?> <?php _e('ITEMS(S)', 'king');?></span> - <span class="cart-total"><?php echo $woocommerce->cart->get_cart_total(); ?></span></a>
										<div class="shopping-cart-dropdown"><?php echo wp_kses_post($mini_cart); ?></div>											
									</div>
															
									<?php } else {
										get_template_part('inc/social-icons'); ?>
										<!-- Social Media -->
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php break;
		}

		?>
		
		<!-- /Preheader -->

    <?php endif;
}

/**
 * WooCoomerce recently viewed
 * @global type $product
 * @return type
 */
function king_woocommerce_recently_viewed() {
	
	$viewed_products = ! empty( $_COOKIE['woocommerce_recently_viewed'] ) ? (array) explode( '|', $_COOKIE['woocommerce_recently_viewed'] ) : array();
	$viewed_products = array_filter( array_map( 'absint', $viewed_products ) );

	if ( empty( $viewed_products ) )
		return;

	$query_args = array( 
		'posts_per_page' => 6, 
		'no_found_rows' => 1, 
		'post_status' => 'publish', 
		'post_type' => 'product', 
		'post__in' => $viewed_products, 
		'orderby' => 'rand' 
	);

	$query_args['meta_query'] = array();
	$query_args['meta_query'][] = WC()->query->stock_status_meta_query();
	$query_args['meta_query'] = array_filter( $query_args['meta_query'] );
	
	$r = new WP_Query($query_args);

	if ( $r->have_posts() ): 
		
		 ?>

		<!-- Recently Viewed -->
		<section class="section full-width-bg normal-padding light-gray-bg">

			<div class="row">

				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="recent-products-header">
						<h5><strong><?php _e('RECENTLY VIEWED', 'king'); ?></strong><i class="icons icon-angle-down"></i></h5>
						<a href="#" class="clear-recent-products"><i class="icons icon-cancel-circled"></i> <?php _e('Clear All', 'king'); ?></a>
					</div>
				</div>
				<?php while ( $r->have_posts()):
					$r->the_post(); 
					global $product;
				?>
					<div class="col-lg-2 col-md-3 col-sm-4">
						<!-- Shop Product -->
						<div class="recently-viewed-product">
							<div class="featured-image">
								<a href="#" class="remove-product-button" data-id="<?php echo esc_attr($product->id); ?>"><i class="icons icon-cancel-circled"></i></a>
								<a href="<?php echo esc_url( get_permalink( $product->id ) ); ?>" title="<?php echo esc_attr( $product->get_title() ); ?>"><?php echo $product->get_image(); ?></a>
								<div class="product-buttons">
									<a href="#" class="button"><?php _e('Save for later', 'king'); ?></a>
								</div>
							</div>
							<div class="product-info">
								<span><?php echo $product->get_title(); ?></span>
								<?php echo $product->get_price_html(); ?>
							</div>
						</div>
						<!-- /Shop Product -->
					</div>
				<?php endwhile; ?>
			</div>
		</section>
		<!-- /Recently Viewed -->
	
	<?php endif;
	wp_reset_postdata();
}

function king_post_icon() {
	$xs_post_format = get_post_format();
	switch ($xs_post_format) {
		case 'video':
			echo '<span class="video-icon"></span>';
			break;

		case 'image':
			echo '<span class="photo-icon"></span>';
			break;

		case 'audio':
			echo '<span class="audio-icon"></span>';
			break;

		case 'link':
			echo '<span class="link-icon"></span>';
			break;

		default:
			echo '<span class="document-icon"></span>';
			break;

	}
}

function king_footer_page() {
	
	
	if ((king_get_opt('show_selected_page_content_above_footer') != 'yes') || (function_exists('is_shop') && is_shop()) ) {
		return;
	}
	
	$footer_page = king_get_opt('footer_page');
	
	if (!intval($footer_page)) {
		return;
	}
	
	$args = array(
		'posts_per_page' => 1,
		'page_id' => $footer_page,
		'post_type' => 'page',
		'post_status' => array('publish', 'private')
	);
	$query = new WP_Query( $args );
	
	if ($query -> have_posts()) : ?>
		<div class="clearfix"></div>
		<div class="footer-page">
			<div class="container">
				<div class="row">

					<?php
					while ($query -> have_posts()) : 
						$query -> the_post();

						switch (get_post_meta(get_the_ID(),'content_margin',true)) {
							case 'no_margin':
								$padding_class = 'no-padding';
								break;

							case 'only_bottom_margin':
								$padding_class = 'no-top-padding small-padding';
								break;

							case 'only_top_margin':
								$padding_class = 'no-bottom-padding small-padding';
								break;

							default:
								$padding_class = 'small-padding';
								break;
						} ?>
						<section class="<?php echo sanitize_html_classes($padding_class); ?>"> 
							<?php the_content(); ?>
						</section>
					<?php endwhile; ?>

				</div>
			</div>
		</div>
		<?php 
		wp_reset_postdata();
	endif;
}
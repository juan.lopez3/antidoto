 <?php
/**
 * Header style 16 template
 *
 * @package king
 * @since king 1.0
 */
?>
<!-- Header -->
<header id="header" class="style-king <?php echo sanitize_html_classes(king_get_opt('sticky_menu_color')); ?> <?php king_the_logo('', true); ?> 
<?php echo sanitize_html_classes(king_get_main_menu_color() == 'header-light' ? 'style-light' : 'style-dark'); ?> <?php echo sanitize_html_classes(king_get_opt('main_menu_header_16') == 'solid' ? 'style-solid' : 'style-gradient'); ?>">

	<?php king_show_king_preheader(); ?>
	
	<!-- Main Header -->
	<div id="main-header">
		<div class="container">
			<div class="row">

				<!-- Logo -->
				<div class="col-lg-3 col-md-3 col-sm-3 logo">
					<?php king_the_logo('light'); ?>
					<div id="main-nav-button">
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>

				<div class="col-sm-9 align-right">

					<div class="navigation-toggle-wrapper">
						<!-- Main Navigation -->
						<?php 
						if (has_nav_menu('primary')):
							wp_nav_menu(array(
								'theme_location'	=> 'primary',
								'container'			=> false,
								'menu_id'			=> 'main-nav',
								'menu_class'		=> 'menu',
								'depth'				=> 3,
								'walker'			=> new king_walker_nav_menu
							));
						endif; ?>
						<!-- /Main Navigation -->

						<!-- Navigation Toggle Button -->
						<div id="navigation-toggle-button">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<!-- /Navigation Toggle Button -->
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- /Main Header -->

</header>
<!-- /Header -->

<?php // get_template_part('inc/top');?>
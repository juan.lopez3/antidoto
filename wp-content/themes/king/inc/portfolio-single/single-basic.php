<?php
/**
 * The template for displaying portfolio single basic content
 *
 * @package king
 * @since king 1.0
 */

wp_enqueue_script ( 'king-prettyphoto-js' );

if (have_posts()) : while (have_posts()): the_post(); ?>
<div class="container">
    <section class="row big-padding project-single-details">
		<div class="col-lg-8 col-md-7 project-single-preview">
			<?php
			if (get_post_format() == 'video'):
				
				wp_enqueue_script ( 'king-fitvids' );
				
				$video_url = get_post_meta($post->ID, 'video_url', true);
				if (!empty($video_url)):
					$embadded_video = king_get_embaded_video($video_url);
				elseif (empty($video_url)):
					$embadded_video = get_post_meta($post->ID, 'embedded_video', true);
				endif;
				echo '<div class="format-video">' . wp_kses($embadded_video,king_add_iframe_to_allowed_tags()). '</div>';

			elseif (get_post_format() == 'gallery'):
				$gallery = get_post_meta($post->ID, 'gallery_images', true);
				$gallery_html = '';
				if (is_array($gallery)):
					foreach ($gallery as $image):
						$gallery_html .= '<li>' . king_get_image($image['image'], '', $image['title'], 'king-portfolio-single-style2') . '</li>';
					endforeach;
				endif;
				
				wp_enqueue_script ( 'king-jquery-flexslider' );
				echo ' <div class="flexslider portfolio-flexslider"><ul class="slides">' . $gallery_html . '</ul> </div>';
			else:
				the_post_thumbnail('king-portfolio-single-style2', array('class' => 'img-responsive', 'alt' => get_the_title()));
			endif;?>
			<div class="project-tag">
				<span><?php _e('Taged With:', 'king'); ?></span>
				<ul class="tag-list">
					<li><?php echo get_the_term_list($post->ID,'portfolio-tag', '', ''); ?></li>
				</ul>
			</div>

			<?php
			 $content = get_the_content(); 
			 if(strpos($content, '[vc_') === false ): ?>
			 <div class="content">
			 	<?php the_content(); ?>
			 </div>
			 <?php endif; 
			if (comments_open()):
				comments_template('', true);
			endif;
			?>
		</div>
		<div class="col-lg-3 col-lg-offset-1 col-md-5 project-single-sidebar">
			<div class="project-description">
				<?php
					$xs_author              = get_post_meta(get_the_ID(), 'work_author', true);
					$xs_author_url          = get_post_meta(get_the_ID(), 'work_author_url', true);
					$xs_project_description = get_post_meta(get_the_ID(), 'project_description', true);
				?>
				<?php king_share_buttons('social-media'); ?>
				<?php if ($xs_author != ''): ?>
					<h6 class="bold"><?php _e('Work by', 'king'); ?> <a href="<?php echo esc_url($xs_author_url); ?>"><?php echo esc_html($xs_author); ?></a></h6>
				<?php endif; ?>
				<?php if(!empty($xs_project_description)): ?>
					<p><?php echo esc_html($xs_project_description); ?></p>
				<?php endif; ?>
				<?php echo get_the_term_list( $post->ID, 'portfolio-tag', '<ul class="list"><li>', '</li><li>', '</li></ul>' ); ?>
				<?php 
					if(class_exists('WPBakeryShortCode_VC_Wp_Tagcloud')):
						echo do_shortcode('[vc_wp_tagcloud taxonomy="post_tag" title="Tags"]');
					endif;
				?>
			</div>
		</div>
    </section>

    <?php (strpos($content, '[vc_') !== false ) ? the_content():''; ?>
</div>
<?php endwhile; endif; ?>


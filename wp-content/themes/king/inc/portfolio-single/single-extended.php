<?php 
/**
 * The template for displaying portfolio single extended content
 *
 * @package king
 * @since king 1.0
 */

if (have_posts()): while (have_posts()): the_post(); ?>    
<div class="container">
	<section class="row normal-padding no-bottom-padding">        
		<div class="col-lg-12 col-md-12 col-sm-12">            
			<?php the_content(); ?>        
		</div>    
	</section>
</div>
<?php endwhile; endif; ?>
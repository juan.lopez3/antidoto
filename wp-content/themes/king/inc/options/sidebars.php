<?php
$options = array(
	array(
		'label' => 'Sidebars',
		'id' => 'user_sidebars',
		'type' => 'list-item',
		'desc' => __('List of user defined sidebars. Please use "save changes" button after adding or editing sidebars.', 'framework'),
		'settings' => array(
			array(
				'label' => __('Description', 'framework'),
				'id' => 'user_sidebar_description',
				'type' => 'text',
				'desc' => '',
				'std' => '',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			)
		),
		'std' => '',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'section' => 'sidebars'
	),
);


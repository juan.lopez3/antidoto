<?php

$options = array(
	array(
		'id' => 'preloader',
		'label' => __('Preloader.', 'framework'),
		'desc' => __('If enabled Preloader preloader will be displayed.', 'framework'),
		'std' => '',
		'type' => 'Radio',
		'section' => 'general_settings',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => 'switcher-on',
		'choices' => array(
			array(
				'value' => 'enabled',
				'label' => __('Enabled', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'disabled',
				'label' => __('Disabled', 'framework'),
				'src' => ''
			),
		)
	),
	array(
		'id' => 'preloader_text',
		'label' => __('Preloader Text', 'framework'),
		'desc' => __('Text displays in preloader.', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'general_settings',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'condition' => '',
		'operator' => ''
	),
	array(
		'id' => 'body_class',
		'label' => __('Layout', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'general_settings',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'w1170',
				'label' => __('Wide 1170px', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'b1170',
				'label' => __('Boxed 1170px', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'boxed-layout2',
				'label' => __('Big Boxed', 'framework'),
				'src' => ''
			),
			array(
                'value' => 'page-border',
                'label' => __('Page Border', 'framework'),
                'src' => ''
            ),
		)
	),
	array(
		'id' => 'add_google_maps_api',
		'label' => __('Add Google Maps API.', 'framework'),
		'desc' => __('If enabled Google API scipt is included. Disable if it\'s already included.', 'framework'),
		'std' => '',
		'type' => 'Radio',
		'section' => 'general_settings',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => 'switcher-on',
		'choices' => array(
			array(
				'value' => 'enabled',
				'label' => __('Enabled', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'disabled',
				'label' => __('Disabled', 'framework'),
				'src' => ''
			),
		)
	),
	array(
		'id' => 'retina_support',
		'label' => __('Retina support', 'framework'),
		'desc' => __('If enabled all images should be uploaded 2x larger. Requires more server resources if enabled.', 'framework'),
		'std' => '',
		'type' => 'Radio',
		'section' => 'general_settings',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => 'switcher-off',
		'choices' => array(
			array(
				'value' => 'disabled',
				'label' => __('Disabled', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'enabled',
				'label' => __('Enabled', 'framework'),
				'src' => ''
			)
		)
	),
	array(
		'id' => 'enable_responsiveness',
		'label' => __('Enable responsiveness', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Radio',
		'section' => 'general_settings',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => 'switcher-on',
		'choices' => array(
			array(
				'value' => 'enabled',
				'label' => __('Enabled', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'disabled',
				'label' => __('Disabled', 'framework'),
				'src' => ''
			),
		)
	),
	array(
		'id' => 'control_panel',
		'label' => __('Show control panel', 'framework'),
		'desc' => __('Shows the Control Panel on your homepage if enabled.', 'framework'),
		'std' => '',
		'type' => 'select',
		'section' => 'general_settings',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'disabled',
				'label' => __('Disabled', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'enabled_admin',
				'label' => __('Enabled for administrator', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'enabled_all',
				'label' => __('Enabled for all', 'framework'),
				'src' => ''
			)
		)
	),
	array(
		'id' => 'background_color',
		'label' => __('Background color', 'framework'),
		'desc' => __('Enabled only when boxed layout is selected', 'framework'),
		'std' => '',
		'type' => 'colorpicker',
		'section' => 'general_settings',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'background_pattern',
		'label' => __('Background pattern', 'framework'),
		'desc' => __('Enabled only when boxed layout is selected', 'framework'),
		'std' => '',
		'type' => 'Select',
		'section' => 'general_settings',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => king_get_background_patterns()
	),
	array(
		'id' => 'background_image',
		'label' => __('Background image', 'framework'),
		'desc' => __('Choose "Image" option on "Background pattern" list and boxed layout to enable background', 'framework'),
		'std' => '',
		'type' => 'Upload',
		'section' => 'general_settings',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'background_repeat',
		'label' => __('Background repeat', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'general_settings',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'repeat',
				'label' => __('Repeat horizontally & vertically', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'repeat-x',
				'label' => __('Repeat horizontally', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'repeat-y',
				'label' => __('Repeat vertically', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'no-repeat',
				'label' => __('No repeat', 'framework'),
				'src' => ''
			)
		)
	),
	array(
		'id' => 'background_position',
		'label' => __('Background position', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'general_settings',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'left',
				'label' => __('Left', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'center',
				'label' => __('Center', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'right',
				'label' => __('Right', 'framework'),
				'src' => ''
			)
		)
	),
	array(
		'id' => 'background_attachment',
		'label' => __('Background attachment', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'general_settings',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'scroll',
				'label' => __('Scroll', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'fixed',
				'label' => __('Fixed', 'framework'),
				'src' => ''
			)
		)
	),
	array(
		'id' => 'background_size',
		'label' => __('Background size', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'general_settings',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'original',
				'label' => __('Original', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'browser',
				'label' => __('Fits to browser size', 'framework'),
				'src' => ''
			)
		)
	),
);


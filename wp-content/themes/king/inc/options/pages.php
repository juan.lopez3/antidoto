<?php
$options = array(
	//pages
	array(
		'id' => 'show_related_posts_on_blog_single',
		'label' => __('Blog single related posts', 'framework'),
		'desc' => __('Show related posts on a single blog post', 'framework'),
		'std' => '',
		'type' => 'Radio',
		'section' => 'pages',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => 'switcher-on',
		'choices' => array(
			array(
				'value' => 'yes',
				'label' => __('Yes', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'no',
				'label' => __('No', 'framework'),
				'src' => ''
			)
		)
	),
	array(
		'id' => 'blog_style',
		'label' => __('Blog style', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'pages',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'king',
				'label' => __('King', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'list',
				'label' => __('List', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'alternate',
				'label' => __('Alternate', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'grid',
				'label' => __('Grid', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'timeline',
				'label' => __('Timeline', 'framework'),
				'src' => ''
			),
		)
	),


	array(
		'id'          => 'portfolio_page',
		'label'       => __('Portfolio page', 'framework'),
		'desc'        => '',
		'std'         => '',
		'type'        => 'page-select',
		'section'     => 'pages',
		'rows'        => '',
		'post_type'   => '',
		'taxonomy'    => '',
		'class'       => ''
	),
	array(
		'id' => 'portfolio_single_style',
		'label' => __('Portfolio single style', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'pages',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'basic',
				'label' => __('Basic', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'extended',
				'label' => __('Extended', 'framework'),
				'src' => ''
			)
		)
	),
	array(
		'id' => 'show_related_projects_on_portfolio_single',
		'label' => __('Related projects', 'framework'),
		'desc' => __('Show related projects on a single portfolio post', 'framework'),
		'std' => '',
		'type' => 'Radio',
		'section' => 'pages',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => 'switcher-on',
		'choices' => array(
			array(
				'value' => 'yes',
				'label' => __('Yes', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'no',
				'label' => __('No', 'framework'),
				'src' => ''
			)
		)
	),
	array(
		'id'          => 'portfolio_page_related_projects_header',
		'label'       => __('Portfolio page - related projects header', 'framework'),
		'desc'        => '',
		'std'         => '',
		'type'        => 'text',
		'section'     => 'pages',
		'rows'        => '',
		'post_type'   => '',
		'taxonomy'    => '',
		'class'       => ''
	),
	array(
		'id'          => '404_content',
		'label'       => __('404 content', 'framework'),
		'desc'        => '',
		'std'         => '',
		'type'        => 'textarea',
		'section'     => 'pages',
		'rows'        => '',
		'post_type'   => '',
		'taxonomy'    => '',
		'class'       => ''
	),
);


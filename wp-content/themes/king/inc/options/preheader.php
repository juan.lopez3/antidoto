<?php
$options = array(
	array(
		'id' => 'disabled_preheader',
		'label' => '.',
		'desc' => __('<b>Preheader is available for header styles 13 and 16 only.</b>', 'framework'),
		'std' => '',
		'type' => 'textblock',
		'section' => 'preheader',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'condition' => 'main_menu_style:not(style13),main_menu_style:not(style16),shop_menu_style:not(style13),shop_menu_style:not(style16)',
		'operator' => 'and'
	),
	array(
		'id' => 'show_preheader',
		'label' => __('Show preheader', 'framework'),
		'std' => '',
		'type' => 'Radio',
		'section' => 'preheader',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => 'switcher-off',
		'choices' => array(
			array(
				'value' => 'no',
				'label' => __('No', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'yes',
				'label' => __('Yes', 'framework'),
				'src' => ''
			)
		),
		'condition' => 'main_menu_style:is(style13),main_menu_style:is(style16),shop_menu_style:is(style13),shop_menu_style:is(style16)',
		'operator' => 'or'
	),
	array(
		'id' => 'preheader_style',
		'label' => __('Preheader style', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'preheader',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
                'value' => 'page-preheader',
                'label' => __('Style 1', 'framework'),
                'src' => ''
            ),
            array(
                'value' => 'shop-preheader',
                'label' => __('Style 2 ( Shop )', 'framework'),
                'src' => ''
            ),
		),
	),
	array(
		'id' => 'preheader_layout',
		'label' => __('Preheader layout', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'preheader',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'with-container',
				'label' => __('With Container', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'full-width-without-container',
				'label' => __('Full Width', 'framework'),
				'src' => ''
			)
		),
		'condition' => 'preheader_style:is(page-preheader)',
		'operator' => 'or'
	),
	array(
		'id' => 'preheader',
		'label' => __('Preheader items', 'framework'),
		'type' => 'list-item',
		'desc' => __('List of user defined preheader items. Please use "save changes" button after you add or edit items.', 'framework'),
		'settings' => array(
			array(
				'label' => __('Type', 'framework'),
				'id' => 'type',
				'type' => 'select',
				'desc' => '',
				'std' => '',
				'type' => 'Select',
				'choices' => king_get_preheader_types()
			),
			array(
				'id' => 'text',
				'label' => __('Text (content for the text type)', 'framework'),
				'desc' => '',
				'std' => '',
				'type' => 'text',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => ''
			)

		),
		'std' => '',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'section' => 'preheader',
		'condition' => 'preheader_style:is(page-preheader)',
		'operator' => 'or'
	),
);


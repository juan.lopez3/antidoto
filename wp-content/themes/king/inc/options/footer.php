<?php
$options = array(
	array(
		'id' => 'show_footer',
		'label' => __('Show Footer', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'footer',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'yes',
				'label' => __('Yes', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'no',
				'label' => __('No', 'framework'),
				'src' => ''
			),

		)
	),
	array(
		'id' => 'show_selected_page_content_above_footer',
		'label' => __('Show selected page content above the footer', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Radio',
		'section' => 'footer',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => 'switcher-off',
		'choices' => array(
			array(
				'value' => 'no',
				'label' => __('No', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'yes',
				'label' => __('Yes', 'framework'),
				'src' => ''
			),
		)
	),

	array(
		'id'          => 'footer_page',
		'label'       => __('Footer page content', 'framework'),
		'desc'        => __('Selected page won\'t be available for direct access.', 'framework'),
		'std'         => '',
		'type'        => 'page-select',
		'section'     => 'footer',
		'rows'        => '',
		'post_type'   => '',
		'taxonomy'    => '',
		'class'       => ''
	),
	
	array(
		'id' => 'show_shop_footer',
		'label' => __('Show Shop Footer', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'footer',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'yes',
				'label' => __('Yes', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'no',
				'label' => __('No', 'framework'),
				'src' => ''
			),

		)
	),
	array(
		'id' => 'footer_text',
		'label' => __('Footer text', 'framework'),
		'desc' => __('You can add copyright text here.', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'footer',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'footer_style',
		'label' => __('Footer style', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'footer',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'default',
				'label' => __('Default', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'normal',
				'label' => __('Normal', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'alternative',
				'label' => __('Alternative with contact form', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'king',
				'label' => __('King', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'king_alt',
				'label' => __('King Alternative', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'king_alt_2',
				'label' => __('King Alternative 2', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'king_light',
				'label' => __('King Light', 'framework'),
				'src' => ''
			)
		)
	),

	array(
		'id' => 'shop_footer_style',
		'label' => __('Shop Footer style', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'footer',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'default',
				'label' => __('Default', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'normal',
				'label' => __('Normal', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'alternative',
				'label' => __('Alternative with contact form', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'king',
				'label' => __('King', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'king_alt',
				'label' => __('King Alternative', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'king_alt_2',
				'label' => __('King Alternative 2', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'king_light',
				'label' => __('King Light', 'framework'),
				'src' => ''
			)
		)
	),

	array(
		'id' => 'footer_text_left',
		'label' => __('Footer text left', 'framework'),
		'desc' => __('Bottom footer left text <strong>Only for footer type king alternative 2</strong>.', 'framework'),
		'std' => '',
		'type' => 'textarea',
		'section' => 'footer',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),

	array(
		'id' => 'footer_text_center',
		'label' => __('Footer text center', 'framework'),
		'desc' => __('Bottom footer text center <strong>Only for footer type king alternative 2</strong>.', 'framework'),
		'std' => '',
		'type' => 'textarea',
		'section' => 'footer',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),

	array(
		'id' => 'hide_footer_widgets',
		'label' => __('Hide footer widgets', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Radio',
		'section' => 'footer',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => 'switcher-off',
		'choices' => array(
			array(
				'value' => 'no',
				'label' => __('No', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'yes',
				'label' => __('Yes', 'framework'),
				'src' => ''
			)
		)
	),

	array(
		'id' => 'sticky_footer',
		'label' => __('Sticky footer', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Radio',
		'section' => 'footer',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => 'switcher-off',
		'choices' => array(
			array(
				'value' => 'no',
				'label' => __('No', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'yes',
				'label' => __('Yes', 'framework'),
				'src' => ''
			)
		)
	),
);


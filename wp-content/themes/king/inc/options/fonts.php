<?php
$options = array(
	//fonts
	array(
		'id' => 'character_sets',
		'label' => __('Additional character sets', 'framework'),
		'desc' => __('Choose the character sets you want to download from Google Fonts','framework'),
		'std' => '',
		'type' => 'checkbox',
		'section' => 'fonts',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'cyrillic',
				'label' => __('Cyrillic', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'cyrillic-ext',
				'label' => __('Cyrillic Extended', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'greek-ext',
				'label' => __('Greek Extended', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'latin-ext',
				'label' => __('Latin Extended', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'vietnamese',
				'label' => __('Vietnamese', 'framework'),
				'src' => ''
			)
		)
	),
	array(
		'id' => 'title_font',
		'label' => __('Title font', 'framework'),
		'desc' => __('Font style for page title','framework'),
		'std' => '',
		'type' => 'select',
		'section' => 'fonts',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => king_get_font_choices()
	),
	array(
		'id' => 'title_font_size',
		'label' => __('Title font size', 'framework'),
		'desc' => __('The size of the page title in pixels','framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'fonts',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => ''
	),
	array(
		'id' => 'content_font',
		'label' => __('Content font', 'framework'),
		'desc' => __('Font style for content','framework'),
		'std' => '',
		'type' => 'select',
		'section' => 'fonts',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => king_get_font_choices()
	),
	array(
		'id' => 'content_font_size',
		'label' => __('Content font size', 'framework'),
		'desc' => __('The size of the page content in pixels','framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'fonts',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => ''
	),
	array(
		'id' => 'menu_font',
		'label' => __('Menu font', 'framework'),
		'desc' => __('Font style for menu items', 'framework'),
		'std' => '',
		'type' => 'select',
		'section' => 'fonts',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => king_get_font_choices()
	),
	array(
		'id' => 'menu_font_size',
		'label' => __('Menu font size', 'framework'),
		'desc' => __('The size of the menu elements in pixels','framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'fonts',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => ''
	),
	array(
		'id' => 'headers_font',
		'label' => __('Header font', 'framework'),
		'desc' => __('Font style for all headers (H1, H2 etc.)','framework'),
		'std' => '',
		'type' => 'select',
		'section' => 'fonts',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => king_get_font_choices()
	),
	array(
		'id' => 'h1_size',
		'label' => __('H1 font size', 'framework'),
		'desc' => __('The size of H1 elements in pixels','framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'fonts',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => ''
	),
	array(
		'id' => 'h2_size',
		'label' => __('H2 font size', 'framework'),
		'desc' => __('The size of H2 elements in pixels','framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'fonts',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => ''
	),
	array(
		'id' => 'h3_size',
		'label' => __('H3 font size', 'framework'),
		'desc' => __('The size of H3 elements in pixels','framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'fonts',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => ''
	),
	array(
		'id' => 'h4_size',
		'label' => __('H4 font size', 'framework'),
		'desc' => __('The size of H4 elements in pixels','framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'fonts',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => ''
	),
	array(
		'id' => 'h5_size',
		'label' => __('H5 font size', 'framework'),
		'desc' => __('The size of H5 elements in pixels','framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'fonts',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => ''
	),
	array(
		'id' => 'h6_size',
		'label' => __('H6 font size', 'framework'),
		'desc' => __('The size of H6 elements in pixels','framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'fonts',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => ''
	),
);


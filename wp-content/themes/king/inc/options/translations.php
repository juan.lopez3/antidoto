<?php
$options = array(
	array(
		'id' => 'enable_translations',
		'label' => __('Enable translations', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Radio',
		'section' => 'translations',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => 'switcher-off',
		'choices' => array(
			array(
				'value' => 'no',
				'label' => __('No', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'yes',
				'label' => __('Yes', 'framework'),
				'src' => ''
			)
		)
	)
);

//get phrases using Poedit, export them to html and copy only phrases (remove all tabs etc. first)
	$traslate_phrases =
'Contact Form
Our Work
All
Comments
Comment navigation
&larr; Older Comments
Newer Comments &rarr;
Comments are closed.
Leave A Reply Form
Name*
E-mail*
Message
Loading posts
Load More
Sort Portfolio
Loading...
By
404
Oops, This page couldn\'t be found!
Go back home
Search Our Website
Can\'t find what you need? Take a moment and do a search below!
Send Message
Clear
is required
Email is invalid
Message Sent
An Error occured.
Please fill all required fields.
Please check your email.
Email sent. Thank you for contacting us
Server error. Pease try again later.
Now:
Slider
Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.
Pages:
No Tweets to show
Page %s
Your browser does not support the audio element.
No comments
1 comment
% comments
Work by
Search here..
Menu
Posts Tagged \"%s\"
Posts by %s
404 Page Not Found
Shop
Home
Archive by Category \"%s\"
Search Results for \"%s\" Query
Login
ITEMS(S)
Cart is empty
View Cart
Checkout
Language
RECENTLY VIEWED
Clear All
Save for later
Your comment is awaiting moderation.
&laquo; Previous
Next &raquo;
About %s
Displays the most recent portfolio items.
Recent Works
Title:
Number of posts to show:
Shows choosen WordPress pages
Side Navi
Sort by:
Page title
Page order
Page ID
Include:
Page IDs, separated by commas.
Displays the most latest posts.
Latest Posts
Social Media Icons
Facebook:
Twitter:
Skype:
Google+:
Vimeo:
Linkdin:
Instagram:
Your site&#8217;s most recent comments.
Alternative Recent Comments
Recent Comments
on
Title with button widget
Line 1:
Line 2:
Button text:
URL:
Popular
Recent
BY
Contact Info
Address:
Phone:
Fax:
E-mail:
Email:
Show social links
Twitter + RSS
Followers
Subscribe
to RSS Feed
Read more
Send message
Related Projects
Learn more
There are no reviews yet.
Add a review
Be the first to review
Leave a Reply to %s
Name
Email
Submit
Your Rating
Rate&hellip;
Perfect
Good
Average
Not that bad
Very Poor
Your Review
Only logged in customers who have purchased this product may leave a review.
Details
Sale!
SKU:
N/A
Zoom
Video
You may also like&hellip;
Rated %d out of 5
out of 5
Your comment is awaiting approval
verified owner
Related Products
Clear selection
This product is currently out of stock and unavailable.
UNFORTUNATELY, YOUR SHOPPING BAG IS EMPTY
Back to store
Item
Description
Product Code
Unit Price
Quantity
Subtotal
Available on backorder
Remove this item
Coupon
Coupon code
Apply Coupon
Update Cart
Continue Shopping
Cart Subtotal
Order Total
(taxes estimated for %s)
Note: Shipping and taxes are estimated%s and will be updated during checkout based on your billing and shipping information.
Shipping #%d
Shipping and Handling
Please use the shipping calculator to see available shipping methods.
Please continue to the checkout and enter your full address to see if there are any available shipping methods.
Please fill in your details to see available shipping methods.
There doesn&lsquo;t seem to be any available shipping methods. Please double check your address, or contact us if you need any help.
Shipping
Estimate Shipping
Select a country&hellip;
State / county
Select a state&hellip;
City
Postcode / Zip
Update Totals
Showing all %d results
Sort by
Default sorting
Sort by popularity
Sort by average rating
Sort by newness
Sort by price: low to high
Sort by price: high to low
Previous
View Page %d of %d
Next
Products tagged &ldquo;
Search results for &ldquo;
Back to %s
Error 404
Posts tagged &ldquo;
Author:
Page';

	$to_translate = explode("\n",$traslate_phrases);

	if (is_array($to_translate)) {
		foreach ($to_translate as $item) {
			$item = trim($item);
			$options[] = array(
				'id' => 'translator_'.  sanitize_title($item),
				'label' => $item,
				'desc' => '',
				'std' => '',
				'type' => 'text',
				'section' => 'translations',
				'rows' => '',
				'post_type' => '',
				'taxonomy' => '',
				'class' => '',
				'default' => $item
			);
		}
	}

<?php

if (king_check_if_wordpress_importer_activated()) {
	$import_message = '<a id="ts-import-sample-data" href="'. admin_url('themes.php').'?page=ot-theme-options&import_sample_data=1&import=1" class="button-primary" data-warning="'.esc_attr(__(' WARNING: clicking this button will install new posts and replace your current data including theme options, sliders and widgets.', 'framework')).'">'.__('Import sample data', 'framework').'</a>';
} else {
	$import_message = __('Wordpress Importer plugin must be installed and activated to import sample data.', 'king').' <a href="'.admin_url('themes.php?page=install-required-plugins').'">'.__('Install and Activate Wordpress Importer', 'king').'</a>';
}

$options = array(
	array(
		'id' => 'import_demo',
		'label' => __('Import demo content', 'framework'),
		'desc' => $import_message,
		'std' => '',
		'type' => 'textblock-titled',
		'section' => 'import',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	)
);

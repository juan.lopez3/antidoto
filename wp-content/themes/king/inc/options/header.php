<?php
$options = array(
	
	array(
		'id' => 'main_menu_style',
		'label' => __('Main header style', 'framework'),
		'desc' => __('Default header style', 'framework'),
		'std' => '',
		'type' => 'Select',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => king_get_header_styles()
	),
	array(
		'id' => 'main_menu_color',
		'label' => __('Main header color style', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'header-light',
				'label' => __('Light', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'header-dark',
				'label' => __('Dark', 'framework'),
				'src' => ''
			)
		),
		'condition' => 'main_menu_style:is(style13),main_menu_style:is(style14),main_menu_style:is(style16),main_menu_style:is(style17)',
		'operator' => 'OR'
	),
	array(
		'id' => 'sticky_menu_color',
		'label' => __('Sticky header color style', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'sticky-header-light',
				'label' => __('Light', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'sticky-header-dark',
				'label' => __('Dark', 'framework'),
				'src' => ''
			)
		),
		'condition' => 'main_menu_style:is(style13),main_menu_style:is(style14),main_menu_style:is(style16),main_menu_style:is(style17)',
		'operator' => 'OR'
	),
	array(
		'id' => 'main_menu_header_16',
		'label' => __('Header 4 background style', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'solid',
				'label' => __('Solid', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'gradient',
				'label' => __('Gradient', 'framework'),
				'src' => ''
			)
		),
		'condition' => 'main_menu_style:is(style16)',
		'operator' => 'AND'
	),
	array(
		'id' => 'shop_menu_style',
		'label' => __('Shop header style', 'framework'),
		'desc' => __('Default header style for all shop pages', 'framework'),
		'std' => '',
		'type' => 'Select',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => king_get_header_styles()
	),
	array(
		'id' => 'shop_menu_color',
		'label' => __('Shop header color style', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'header-light',
				'label' => __('Light', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'header-dark',
				'label' => __('Dark', 'framework'),
				'src' => ''
			)
		),
		'condition' => 'shop_menu_style:is(style13),shop_menu_style:is(style14),shop_menu_style:is(style16)',
		'operator' => 'or'
	),
	
	array(
		'id' => 'logo_url',
		'label' => __('Custom logo', 'framework'),
		'desc' => __('Enter full URL of your logo image or choose upload button', 'framework'),
		'std' => '',
		'type' => 'upload',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'sticky_logo_url',
		'label' => __('Custom Sticky logo', 'framework'),
		'desc' => __('Enter full URL of your logo image or choose upload button', 'framework'),
		'std' => '',
		'type' => 'upload',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'logo_light_url',
		'label' => __('Custom light logo', 'framework'),
		'desc' => __('Enter full URL of your logo image or choose upload button. Good for dark header background.', 'framework'),
		'std' => '',
		'type' => 'upload',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'logo_top_margin',
		'label' => __('Logo top margin', 'framework'),
		'desc' => __('Enter number to set the top space of the logo', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'logo_left_margin',
		'label' => __('Logo left margin', 'framework'),
		'desc' => __('Enter number to set the left space of the logo', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'logo_bottom_margin',
		'label' => __('Logo bottom margin', 'framework'),
		'desc' => __('Enter number to set the bottom space of the logo', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'favicon',
		'label' => __('Favicon', 'framework'),
		'desc' => __('Enter Full URL of your favicon image or choose upload button', 'framework'),
		'std' => '',
		'type' => 'upload',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'header_text',
		'label' => __('Header text', 'framework'),
		'desc' => __('Text displays in the header, html allowed.', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'condition' => 'main_menu_style:is(style1),main_menu_style:is(style2),main_menu_style:is(style4),shop_menu_style:is(style1),shop_menu_style:is(style2),shop_menu_style:is(style4)',
		'operator' => 'or'
	),
	
	array(
		'id' => 'show_breadcrumbs',
		'label' => __('Show breadcrumbs', 'framework'),
		'desc' => __('Show or hide breadcrumbs', 'framework'),
		'std' => '',
		'type' => 'Radio',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => 'switcher-on',
		'choices' => array(
			array(
				'value' => 'yes',
				'label' => __('Yes', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'no',
				'label' => __('No', 'framework'),
				'src' => ''
			)
		)
	),
	array(
		'id' => 'show_search_nav',
		'label' => __('Show search icon in navigation', 'framework'),
		'desc' => __('Show or hide search form right to the main navigation', 'framework'),
		'std' => '',
		'type' => 'Radio',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => 'switcher-on',
		'choices' => array(
			array(
				'value' => 'yes',
				'label' => __('Yes', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'no',
				'label' => __('No', 'framework'),
				'src' => ''
			)
		)
	),
	
	array(
		'id' => 'switch_menu_to_mobile',
		'label' => __('Switch menu to mobile on width (px)', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'text',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => ''
	),
	array(
		'id' => 'megamenu_background',
		'label' => __('Mega Menu background', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'upload',
		'section' => 'header',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => ''
	),
);

<?php
$options = array(
	array(
		'id' => 'default_titlebar_style',
		'label' => __('Default titlebar style', 'framework'),
		'desc' => '',
		'std' => '',
		'section' => 'titlebar',
		'type' => 'Select',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'style1',
				'label' => __('Style 1', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'style2',
				'label' => __('Style 2', 'framework'),
				'desc' => ''
			),
			array(
				'value' => 'style3',
				'label' => __('Style 3', 'framework'),
				'src' => ''
			)
		)
	),
	array(
		'id' => 'default_title_background',
		'label' => __('Default title background', 'framework'),
		'desc' => __('Background image URL', 'framework'),
		'std' => '',
		'type' => 'Upload',
		'section' => 'titlebar',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'default_shop_titlebar_style',
		'label' => __('Default shop titlebar style', 'framework'),
		'desc' => __('Titlebar style for WooCommerce pages', 'framework'),
		'std' => '',
		'section' => 'titlebar',
		'type' => 'Select',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'style1',
				'label' => __('Style 1', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'style2',
				'label' => __('Style 2', 'framework'),
				'desc' => ''
			),
			array(
				'value' => 'style3',
				'label' => __('Style 3', 'framework'),
				'src' => ''
			)
		)
	),
	array(
		'id' => 'default_shop_title_background',
		'label' => __('Default shop title background', 'framework'),
		'desc' => __('Background image URL for WooCommerce shop pages', 'framework'),
		'std' => '',
		'type' => 'Upload',
		'section' => 'titlebar',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'default_title_background_position',
		'label' => __('Title background position', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'titlebar',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'position-left-top',
				'label' => __('left top', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'position-left-center',
				'label' => __('left center', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'position-left-bottom',
				'label' => __('left bottom', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'position-right-top',
				'label' => __('right top', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'position-right-center',
				'label' => __('right center', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'position-right-bottom',
				'label' => __('right bottom', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'position-center-top',
				'label' => __('center top', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'position-center-center',
				'label' => __('center center', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'position-center-bottom',
				'label' => __('center bottom', 'framework'),
				'src' => ''
			)
		)
	),
	array(
		'id' => 'default_title_background_size',
		'label' => __('Title background size', 'framework'),
		'desc' => '',
		'std' => '',
		'type' => 'Select',
		'section' => 'titlebar',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'size-original',
				'label' => __('Original', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'size-cover',
				'label' => __('Scale to the container size', 'framework'),
				'src' => ''
			)
		)
	),
);



<?php
$options = array(
	
	array(
		'id' => 'contact_form_email',
		'label' => __('Contact form email', 'framework'),
		'desc' => __('Email to receive messages from contact forms', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'social',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'contact_form_subject',
		'label' => __('Contact form notification subject', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'social',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	
	array(
		'id' => 'active_social_items',
		'label' => __('Actived items', 'framework'),
		'desc' => __('Items available on your website', 'framework'),
		'std' => '',
		'type' => 'checkbox',
		'section' => 'social',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => '',
		'choices' => array(
			array(
				'value' => 'twitter',
				'label' => __('Twitter', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'facebook',
				'label' => __('Facebook', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'skype',
				'label' => __('Skype', 'framework'),
				'src' => ''
			),

			array(
				'value' => 'google',
				'label' => __('Google+', 'framework'),
				'src' => ''
			),

			array(
				'value' => 'vimeo',
				'label' => __('vimeo', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'linkedin',
				'label' => __('LinkedIn', 'framework'),
				'src' => ''
			),
			array(
				'value' => 'instagram',
				'label' => __('instagram', 'framework'),
				'src' => ''
			),

		),
	),
	array(
		'id' => 'facebook_url',
		'label' => __('Facebook URL', 'framework'),
		'desc' => __('URL to your Facebook account', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'social',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'twitter_url',
		'label' => __('Twitter URL', 'framework'),
		'desc' => __('URL to your Twitter account', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'social',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'skype_username',
		'label' => __('Skype username', 'framework'),
		'desc' => __('Your Skype username', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'social',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'google_plus_url',
		'label' => __('Google+ URL', 'framework'),
		'desc' => __('URL to your Google+ account', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'social',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'vimeo_url',
		'label' => __('Vimeo URL', 'framework'),
		'desc' => __('URL to your Vimeo account', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'social',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'linkedin_url',
		'label' => __('Linkedin URL', 'framework'),
		'desc' => __('URL to your Linkedin account', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'social',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'instagram_url',
		'label' => __('Instagram URL', 'framework'),
		'desc' => __('URL to your Instagram account', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'social',
		'rows' => '',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
);

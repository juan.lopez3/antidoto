<?php
$options = array(
	//integration
	array(
		'id' => 'google_analytics_id',
		'label' => __('Google Analytics ID', 'framework'),
		'desc' => __('Your Google Analytics ID eg. UA-1xxxxx8-1', 'framework'),
		'std' => '',
		'type' => 'text',
		'section' => 'integration',
		'rows' => '10',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'scripts_header',
		'label' => __('Header scripts', 'framework'),
		'desc' => __('Scripts will be added to the header. Don\'t forget to add &lsaquo;script&rsaquo;;...&lsaquo;/script&rsaquo; tags.', 'framework'),
		'std' => '',
		'type' => 'textarea-simple',
		'section' => 'integration',
		'rows' => '10',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'scripts_footer',
		'label' => __('Footer scripts', 'framework'),
		'desc' => __('Scripts will be added to the footer. You can use this for Google Analytics etc. Don\'t forget to add &lsaquo;script&rsaquo;...&lsaquo;/script&rsaquo; tags.', 'framework'),
		'std' => '',
		'type' => 'textarea-simple',
		'section' => 'integration',
		'rows' => '10',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
	array(
		'id' => 'custom_css',
		'label' => __('Custom CSS', 'framework'),
		'desc' => __('Please add css classes only', 'framework'),
		'std' => '',
		'type' => 'textarea-simple',
		'section' => 'integration',
		'rows' => '10',
		'post_type' => '',
		'taxonomy' => '',
		'class' => ''
	),
);


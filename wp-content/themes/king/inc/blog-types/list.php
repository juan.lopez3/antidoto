<?php
/**
 * The template for displaying list blog post content
 *
 * @package king
 * @since king 1.0
 */

wp_enqueue_script ( 'king-prettyphoto-js' );

$xs_post_format = get_post_format();
?>
<!-- Post Item -->
<article <?php post_class('blog-post blog-post-classic col-lg-12 col-md-12 col-sm-12');?>>
	<div class="blog-post-list">

	    <div class="blog-post-meta">

	        <?php get_template_part('inc/post-date');?>

	    </div>

	    <div class="blog-post-content">

	    	<header>
		        <!-- Post Image -->
		        <div class="post-thumbnail">
		            <?php
					switch ($xs_post_format) {
						case 'video':
							
							$url = get_post_meta($post->ID, 'video_url', true);
							if (!empty($url)) {
								$embadded_video = king_get_embaded_video($url);
							} else if (empty($url)) {
								$embadded_video = get_post_meta($post->ID, 'embedded_video', true);
							}

							if (isset($embadded_video) && !empty($embadded_video)): 
								wp_enqueue_script ( 'king-fitvids' ); ?>
								<div class="post-video format-video">
									<?php echo wp_kses($embadded_video,king_add_iframe_to_allowed_tags()); ?>
								</div>
							<?php elseif (has_post_thumbnail()): ?>
								<?php the_post_thumbnail('king-default', array('class' => 'img-responsive')); ?>
								<div class="post-hover">
									<a class="link-icon" href="<?php echo esc_url(get_permalink()); ?>"></a>
									<a class="search-icon" href="<?php echo esc_url(king_img_url()); ?>" rel="prettyPhoto"></a>
								</div>
							<?php endif; ?>

							<?php
							break;
						
						case 'audio':
							?>
								<?php $audio_url = get_post_meta(get_the_ID(), 'audio_url', true);
								if ($audio_url != ''): ?>
									<div class="audio-player-box">
										<audio>
											<source class="sc-audio-player" type="audio/mpeg" src="<?php echo esc_url($audio_url); ?>">
											<?php _e('Your browser does not support the audio element.','king'); ?>
										</audio>
									</div>
								<?php endif; ?>
							<?php
							break;
							
						case 'link':
							?>
								<?php $url = get_post_meta(get_the_ID(),'link',true);
								if (!empty($url)): ?>
									<blockquote class="post-blockquote style-link">
										<div class="link-contents">
											<span class="icon-container">
												<i class="fa fa-link"></i>
											</span>
											<p><?php echo esc_html(get_post_meta(get_the_ID(),'link_description',true)); ?></p>
											<a class="link" href="<?php echo esc_url($url); ?>" target="_blank"><?php echo esc_url($url); ?></a>
										</div>
									</blockquote>
									<?php if (has_post_thumbnail()): ?>
										<!-- Post Image -->
										<div class="post-thumbnail" style="background-image: url(<?php echo esc_url(king_get_image_by_id(get_post_thumbnail_id(get_the_ID()),'king-default')); ?>)">
											<?php the_post_thumbnail('king-default', array('class' => 'img-responsive')); ?>
										</div>
										<!-- /Post Image -->
									<?php endif; ?>
								<?php endif; ?>
							<?php
							break;
							
						case 'quote':
							?>
							<blockquote class="post-blockquote">
								<p><?php echo get_post_meta(get_the_ID(),'quote_text',true);?></p>
								<p>- <?php _e('By','king'); ?> <?php echo get_post_meta(get_the_ID(),'author_text',true);?></p>
							</blockquote>
							<?php

							break;
						
						default:
							the_post_thumbnail('king-default', array('class' => 'img-responsive'));
					}			
					?>

		        </div>
		        <!-- /Post Image -->
	        </header>

	        <!-- Post Content -->
	        <div class="post-content">
	            <ul class="post-meta">
	            	<li>
	            		<span><?php echo get_the_date('d M');?></span>
	            		<?php echo get_the_date('Y');?>
	            	</li>
	        		<?php $categories = get_the_category_list(', ');
	        		if (!empty($categories)): ?>
	        			<li><?php _e('In', 'king');?> <?php echo king_get_shortened_string_by_letters(strip_tags($categories), 15); ?></li>
	        		<?php endif; ?>
	        		<li><?php _e('By', 'king');?> <?php the_author();?></li>
	            </ul>
	            <h4><a href='<?php echo esc_url(get_permalink());?>' title=""><?php the_title(); ?></a></h4>
	            <?php
				$excerpt_length = get_post_meta(CURRENT_ID, 'excerpt_length', true);
				if (empty($excerpt_length)):
					$excerpt_length = 'regular';
				endif;
				echo king_get_the_excerpt_theme($excerpt_length); ?>
	            <a class="read-more big" title="" href="<?php echo esc_url(get_permalink());?>"><?php _e('Learn more', 'king'); ?></a>
	        </div>
	        <!-- /Post Content -->

	    </div>

	</div>
</article>
<!-- /Post Item -->



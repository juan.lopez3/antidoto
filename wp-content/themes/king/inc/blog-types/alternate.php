<?php
/**
 * The template for displaying altenate blog post content
 *
 * @package king
 * @since king 1.0
 */

wp_enqueue_script ( 'king-prettyphoto-js' );

global $alternate_count, $post;
$xs_post_format = get_post_format();
if ($alternate_count == 0): $alternate_count = 1;endif;?>

<?php 

/* Full Width Alternate */			
$post_alternate_full_width = false;

if($xs_post_format != 'video' 
   && $xs_post_format != 'audio' 
   && $xs_post_format != 'quote' 
   && !has_post_thumbnail()){
	
	$post_alternate_full_width = true;
	
}

?>


<?php if ($alternate_count == 1): $alternate_count = 2; ?>
    <article <?php post_class('blog-post alternate-style');?>>
		
		<div class="blog-post-alternate">
		
			<!-- Post Image -->
			<div class="blog-post-meta">
				<?php get_template_part('inc/post-date'); ?>
			</div>
			
			<div class="blog-post-content">
				
				
				<?php if(!$post_alternate_full_width): ?>
				<header class="col-sm-6 col-lg-push-6 col-md-push-6 col-sm-push-6">
					<div class="post-thumbnail">


						<?php
						switch ($xs_post_format) {

							case 'video':
								?>

								<?php
								$url = get_post_meta($post->ID, 'video_url', true);
								if (!empty($url)) {
									$embadded_video = king_get_embaded_video($url);
								} else if (empty($url)) {
									$embadded_video = get_post_meta($post->ID, 'embedded_video', true);
								}

								if (isset($embadded_video) && !empty($embadded_video)): 
									wp_enqueue_script ( 'king-fitvids' ); ?>
									<div class="post-video format-video">
										<?php echo wp_kses($embadded_video,king_add_iframe_to_allowed_tags()); ?>

									</div>
								<?php elseif (has_post_thumbnail()):
									the_post_thumbnail('king-featured_projects_5', array('class' => 'img-responsive', 'alt' => get_the_title()));
									?>
									<div class="post-hover">
										<a class="link-icon" href="<?php echo esc_url(get_permalink()); ?>"></a>
										<a class="search-icon" href="<?php echo esc_url(king_img_url()); ?>" rel="prettyPhoto"></a>
									</div>
								<?php endif; ?>

								<?php
								break;
							case 'audio':
								?>
								
									<?php $audio_url = get_post_meta(get_the_ID(), 'audio_url', true);
									if ($audio_url != ''):?>
										<div class="audio-player-box">
										<audio>
											<source type="audio/mpeg" src="<?php echo esc_url($audio_url); ?>"></source>
											<?php _e('Your browser does not support the audio element.', 'king'); ?>
										</audio>
										</div>
									<?php endif; ?>
								 <?php
								?>
								<?php if ( has_post_thumbnail()) : ?>
								<div class="post-hover">

									<a class="link-icon" href="<?php echo esc_url(get_permalink()); ?>"></a>
									<a class="search-icon" href="<?php echo esc_url(king_img_url()); ?>" rel="prettyPhoto"></a>

								</div>
							<?php endif; ?>

								<?php
								break;
							case 'link':
								?>
								<div class="link-container">
									<?php $url = get_post_meta(get_the_ID(),'link',true);
									if (!empty($url)): ?>
										<blockquote class="post-blockquote style-link">
											<div class="link-contents">
												<span class="icon-container">
													<i class="fa fa-link"></i>
												</span>
												<p><?php echo esc_html(get_post_meta(get_the_ID(),'link_description',true)); ?></p>
												<a class="link" href="<?php echo esc_url($url); ?>" target="_blank"><?php echo esc_url($url); ?></a>
											</div>
										</blockquote>
										<?php if (has_post_thumbnail()): ?>
											<!-- Post Image -->
											<div class="post-thumbnail" style="background-image: url(<?php echo esc_url(king_get_image_by_id(get_post_thumbnail_id(get_the_ID()),'king-default'))?>)">
												<?php  the_post_thumbnail('king-default', array('class' => 'img-responsive')); ?>
											</div>
											<!-- /Post Image -->
										<?php endif; ?>
									<?php endif; ?>
								</div>
								<?php break;
							case 'quote':
								?>
								<blockquote class="post-blockquote">

									<p><?php echo get_post_meta(get_the_ID(), 'quote_text', true); ?></p>
									<p>- <?php _e('By','king'); ?> <?php echo get_post_meta(get_the_ID(),'author_text',true);?></p>

								</blockquote>
								<?php

								break;
							default:
								the_post_thumbnail('king-featured_projects_5', array('class' => 'img-responsive', 'alt' => get_the_title()));
								?>
									<?php if ( has_post_thumbnail()) : ?>
										<div class="post-hover">
											<a class="link-icon" href="<?php echo esc_url(get_permalink()); ?>"></a>
											<a class="search-icon" href="<?php echo esc_url(king_img_url()); ?>" rel="prettyPhoto"></a>
										</div>
									<?php endif; ?>
								<?php
						}


						?>
					</div>
					<!-- /Post Image -->
				</header>
				
				<?php endif; ?>


				<!-- Post Content -->
				<div class="post-content <?php if(!$post_alternate_full_width){ ?>col-sm-6 col-lg-pull-6 col-md-pull-6 col-sm-pull-6<?php } else{ ?>col-md-12 full-width-alternate<?php } ?>">

				    <ul class="post-meta">
						<li><?php echo king_post_categories(get_the_ID()); ?></li>
						<li><?php comments_number( __('No comments', 'king'), __('1 comment', 'king'), __('% comments', 'king')) ;?></li>
					</ul>

					<h4 class="post-title"><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></h4>

					<?php
					$excerpt_length = get_post_meta(CURRENT_ID, 'excerpt_length', true);
					if (empty($excerpt_length)):
						$excerpt_length = 'regular';
					endif;
					echo king_get_the_excerpt_theme($excerpt_length); ?>
					<a class="read-more big" href="<?php echo esc_url(get_permalink()); ?>"><?php _e('Learn more', 'king'); ?></a>

				</div>
				<!-- /Post Content -->
			
			</div>
			
		</div>
		
    </article>
<?php else: $alternate_count = 1; ?>

    <article <?php post_class('blog-post alternate-style');?>>
		
		<div class="blog-post-alternate">
		
			<!-- Post Image -->
			<div class="blog-post-meta">
				<?php get_template_part('inc/post-date'); ?>

			</div>
			
			<div class="blog-post-content">
				
				
				<?php if(!$post_alternate_full_width): ?>
				<header class="col-sm-6">
					<div class="post-thumbnail">


						<?php
						switch ($xs_post_format) {

							case 'video':
								?>

								<?php
								$url = get_post_meta($post->ID, 'video_url', true);
								if (!empty($url)) {
									$embadded_video = king_get_embaded_video($url);
								} else if (empty($url)) {
									$embadded_video = get_post_meta($post->ID, 'embedded_video', true);
								}

								if (isset($embadded_video) && !empty($embadded_video)): 
									wp_enqueue_script ( 'king-fitvids' ); ?>
									<div class="post-video format-video">
										<?php echo wp_kses($embadded_video,king_add_iframe_to_allowed_tags()); ?>

									</div>
								<?php elseif (has_post_thumbnail()):
									the_post_thumbnail('king-featured_projects_5', array('class' => 'img-responsive', 'alt' => get_the_title()));
									?>
									<div class="post-hover">
										<a class="link-icon" href="<?php echo esc_url(get_permalink()); ?>"></a>
										<a class="search-icon" href="<?php echo esc_url(king_img_url()); ?>" rel="prettyPhoto"></a>
									</div>
									
								<?php endif; ?>

								<?php
								break;
							case 'audio':
								?>
								
									<?php $audio_url = get_post_meta(get_the_ID(), 'audio_url', true);
									if ($audio_url != ''):?>
										<div class="audio-player-box">
										<audio>
											<source type="audio/mpeg" src="<?php echo esc_url($audio_url); ?>"></source>
											<?php _e('Your browser does not support the audio element.', 'king'); ?>
										</audio>
										</div>


									<?php endif; ?>
								 <?php
								?>
								<?php if ( has_post_thumbnail()) : ?>
								<div class="post-hover">

									<a class="link-icon" href="<?php echo esc_url(get_permalink()); ?>"></a>
									<a class="search-icon" href="<?php echo esc_url(king_img_url()); ?>" rel="prettyPhoto"></a>

								</div>
							<?php endif; ?>
								<?php
								break;
							case 'link':
								?>
								<div class="link-container">
									<?php $url = get_post_meta(get_the_ID(),'link',true);
									if (!empty($url)): ?>
										<blockquote class="post-blockquote style-link">
											<div class="link-contents">
												<span class="icon-container">
													<i class="fa fa-link"></i>
												</span>
												<p><?php echo esc_html(get_post_meta(get_the_ID(),'link_description',true)); ?></p>
												<a class="link" href="<?php echo esc_url($url); ?>" target="_blank"><?php echo esc_url($url); ?></a>
											</div>
										</blockquote>
										<?php if (has_post_thumbnail()): ?>
											<!-- Post Image -->
											<div class="post-thumbnail" style="background-image: url(<?php echo esc_url(king_get_image_by_id(get_post_thumbnail_id(get_the_ID()),'king-default')); ?>)">
												<?php the_post_thumbnail('king-default', array('class' => 'img-responsive')); ?>
											</div>
											<!-- /Post Image -->
										<?php endif; ?>
									<?php endif; ?>
								</div>
								<?php break;
							case 'quote':
								?>
								<blockquote class="post-blockquote">

									<p><?php echo get_post_meta(get_the_ID(), 'quote_text', true); ?></p>
									<p>- <?php _e('By','king'); ?> <?php echo get_post_meta(get_the_ID(),'author_text',true);?></p>

								</blockquote>
								<?php

								break;
							default:
								the_post_thumbnail('king-featured_projects_5', array('class' => 'img-responsive', 'alt' => get_the_title()));
								?>
								<?php if ( has_post_thumbnail()) : ?>
									<div class="post-hover">
										<a class="link-icon" href="<?php echo esc_url(get_permalink()); ?>"></a>
										<a class="search-icon" href="<?php echo esc_url(king_img_url()); ?>" rel="prettyPhoto"></a>
									</div>
								<?php endif; ?>
							<?php
						}
						?>
					</div>
					<!-- /Post Image -->
				</header>
				<?php endif; ?>

				<!-- Post Content -->
				<div class="post-content <?php if(!$post_alternate_full_width){ ?>col-lg-6 col-md-6 col-sm-6<?php } else{ ?>col-md-12 full-width-alternate<?php } ?>">

					<ul class="post-meta">
						<li><?php echo king_post_categories(get_the_ID()); ?></li>
						<li><?php comments_number( __('No comments', 'king'), __('1 comment', 'king'), __('% comments', 'king')) ;?></li>
					</ul>

					<h4 class="post-title"><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></h4>

					<?php
					$excerpt_length = get_post_meta(CURRENT_ID, 'excerpt_length', true);
					if (empty($excerpt_length)):
						$excerpt_length = 'regular';
					endif;
					echo king_get_the_excerpt_theme($excerpt_length); ?>
					<a class="read-more big" href="<?php echo esc_url(get_permalink()); ?>"><?php _e('Learn more', 'king'); ?></a>

				</div>
				<!-- /Post Content -->
			
			</div>
			
		</div>
		
    </article>
<?php endif; ?>
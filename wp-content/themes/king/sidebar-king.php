<?php
/**
 * The Sidebar containing the left widget areas.
 *
 * @package king
 * @since king 1.0
 */
?>
<!-- Sidebar -->
<div class="col-md-3 col-md-offset-1">
	<div class="king-blog-sidebar">
		<div class="sidebar">
			<?php if (is_active_sidebar( king_get_single_post_sidebar_id('right_sidebar') )): ?>
				<?php dynamic_sidebar( king_get_single_post_sidebar_id('right_sidebar') ); ?>
			<?php endif; ?>
		</div>
	</div>
</div>
<!-- End Sidebar -->
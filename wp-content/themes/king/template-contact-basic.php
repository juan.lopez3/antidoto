<?php
/**
 * Template Name: Contact Basic
 * 
 * @package king
 * @since king 1.0
 */

get_header();

if (have_posts()): while (have_posts()): the_post(); ?>
<div class="king-page">
	<div class="container">
		<!-- Google Map -->
		<section class="full-width google-map-ts">
			<?php echo do_shortcode(get_post_meta(get_the_ID(), 'contact_map', true)); ?>
		</section>
		<!-- /Google Map -->
		<div class="row">
			<div class="col-md-12 small-padding"><?php the_content(); ?></div>
		</div>
	</div>
</div>
<?php endwhile; 
endif;

get_footer(); ?>
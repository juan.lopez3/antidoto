<?php
/**
 * The default template for displaying single post content
 *
 * @package king
 * @since king 1.0
 */

$classes = array(
	'post',
	(get_post_format() ? 'format-' . get_post_format() : ''),
	'blog-post-king'
);
?>

<article class="blog-post">
	<div <?php post_class($classes); ?>>
		<div class="blog-post-content">

			<header>
				<!-- Post Image -->
				<div class="post-thumbnail">
					<?php
					$thumb = '';
					$add_thumb = false;
					switch (get_post_format()) {
						case 'gallery':
							wp_enqueue_script ( 'king-jquery-flexslider' );
							$gallery = get_post_meta($post->ID, 'gallery_images',true);
							if (is_array($gallery) && count($gallery) > 0): ?>	
								<!-- Post Gallery -->	
								<div class="flexslider king-post-gallery">
									<ul class="slides">
										<?php foreach ($gallery as $image): ?>
											<li>
												<?php echo king_get_image($image['image'],'',$image['title'],'king-alternative'); ?>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
								<!-- End Post Gallery -->
							<?php else:
								$add_thumb = true;
							endif; 

							break;
						case 'video':

							$url = get_post_meta($post->ID, 'video_url', true);
							if (!empty($url)) {
								$embadded_video = king_get_embaded_video($url);
							} else if (empty($url)) {
								$embadded_video = get_post_meta($post->ID, 'embedded_video', true);
							}

							if (isset($embadded_video) && !empty($embadded_video)): 
								wp_enqueue_script ( 'king-fitvids' ); ?>
								<div class="post-videp format-video">
									<?php echo wp_kses($embadded_video,king_add_iframe_to_allowed_tags()); ?>
								</div>
							<?php else: 
								$add_thumb = true;
							endif;
							break;
						case 'audio':
							$audio_url = get_post_meta(get_the_ID(), 'audio_url', true);
							if ($audio_url != ''): ?>
								<div class="audio-player-box">
									<audio>
										<source class="sc-audio-player" type="audio/mpeg" src="<?php echo esc_attr($audio_url); ?>">
										<?php _e('Your browser does not support the audio element.','king'); ?>
									</audio>
								</div>
							<?php endif; ?>
						<?php break;

						case 'link': 
							$url = get_post_meta(get_the_ID(),'link',true);
							if (!empty($url)):?>
								<div class="link-container">
									<blockquote class="post-blockquote style-link">
										<div class="link-contents">
											<span class="icon-container">
												<i class="fa fa-link"></i>
											</span>
											<p><?php echo esc_html(get_post_meta(get_the_ID(),'link_description',true)); ?></p>
											<a class="link" href="<?php echo esc_url($url); ?>" target="_blank"><?php echo esc_url($url); ?></a>
										</div>
									</blockquote>
									<!-- Post Image -->
									<?php if (has_post_thumbnail()): ?>
										<!-- Post Image -->
										<div class="post-thumbnail" style="background-image: url(<?php echo esc_url(king_get_image_by_id(get_post_thumbnail_id(get_the_ID()),'king-default')); ?>)">
											<?php  the_post_thumbnail('king-default', array('class' => 'img-responsive')); ?>
										</div>
										<!-- /Post Image -->
									<?php endif; ?>
									<!-- /Post Image -->
								</div>
							<?php else:
								$add_thumb = true;
							endif; ?>
					<?php break;

						case 'quote': ?>
							<blockquote class="post-blockquote">
								<p><?php echo get_post_meta(get_the_ID(),'quote_text',true);?></p>
								<p>-<?php _e('By', 'king'); ?><?php echo get_post_meta(get_the_ID(),'author_text',true);?></p>
							</blockquote>
					<?php break;
						default:
							$add_thumb = true;
					}

					if ($add_thumb):
						the_post_thumbnail('king-blog', array('class' => 'img-responsive', 'alt' => get_the_title()));
					endif;
					?>
				</div>
				<!-- /Post Image -->
				<h4 class="post-title"><a href="<?php echo esc_url(get_permalink());?>" title="<?php echo esc_attr(get_the_title());?>"><?php the_title(); ?></a></h4>
				<div class="blog-post-meta">
					<span><?php the_date(get_option('date_format'));?>, <?php _e('in', 'king'); ?> <?php echo get_the_category_list(' '); ?>, <?php _e('by', 'king');?> <?php the_author();?>, <a href="<?php echo esc_url(get_comments_link()); ?>"><?php comments_number( __('No comments', 'king'), __('1 comment', 'king'), __('% comments', 'king')) ;?></a></span>
				</div>
			</header>

			<!-- Post Content -->
			<div class="post-content">
				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'king' ), 'after' => '</div>' ) ); ?>
			</div>
			<!-- /Post Content -->

			<footer>
				<div class="socials-container">
					<?php king_share_buttons_king(array('facebook','twitter','google_plus')); ?>
				</div>
			</footer>

		</div><!-- End .blog-post-content -->
	</div> <!-- End .blog-post-king -->
</article> <!-- End .blog-post -->
		
<?php if (ot_get_option('show_author_on_blog_single') != 'no'): ?>
	<!-- King Author -->
	<div class="king-post-author">
		<div class="author-avatar">
			<?php echo get_avatar( get_the_author_meta( 'ID' ), 115 ); ?>
		</div>
		<div class="author-info">
			<span class="name"><?php the_author();?></span>
			<span class="position"><?php echo esc_html(get_the_author_meta( 'position' )); ?></span>
			<p><?php echo get_the_author_meta( 'description' ); ?></p>
		</div>
	</div><!-- /King Author -->
<?php endif; ?>
		
		


<?php if (ot_get_option('show_related_posts_on_blog_single')): 
	
	$post_categories = wp_get_post_categories( get_the_ID() );
	
	if (!is_wp_error($post_categories) && is_array($post_categories) && count($post_categories) > 0):

		$args = array(
			'numberposts' => '',
			'posts_per_page' => 2,
			'offset' => 0,
			'category__in' => $post_categories,
			'orderby' => 'date',
			'order' => 'DESC',
			'include' => '',
			'exclude' => '',
			'meta_key' => '',
			'meta_value' => '',
			'post_type' => 'post',
			'post_mime_type' => '',
			'post_parent' => '',
			'paged' => $paged,
			'post_status' => 'publish'
		);
		$query = new WP_Query( $args );

		if ($query -> have_posts()): ?>
			<!-- Related Posts -->
			<div class="king-related-posts">
				<h3 class="section-heading"><?php _e('Related Posts', 'king'); ?></h3>

				<div class="row">
					
					<?php while ($query -> have_posts()): $query -> the_post(); ?>
						<div class="col-sm-6">
							<div class="blog-post blog-post-bg">
								<article class="blog-post-king">

									<div class="blog-post-content">
										<header>
											<h4 class="post-title post-title-2"><a href="<?php echo esc_url(get_permalink());?>" title="<?php echo esc_attr(get_the_title());?>"><?php the_title(); ?></a></h4>
											<div class="blog-post-meta">
												<span><?php the_date(get_option('date_format'));?>, <?php _e('in', 'king'); ?> <?php echo get_the_category_list(' '); ?>, <?php _e('by', 'king');?> <span class="post-author"><?php the_author();?></span></span>
											</div>
											
											<div class="post-thumbnail">
												<?php the_post_thumbnail('king-blog', array('class' => 'img-responsive', 'alt' => get_the_title())); ?>
											</div>
										</header>

										<!-- Post Content -->
										<div class="post-content">
											<p><?php
												echo king_get_the_excerpt_theme(15); ?>
											</p>
										</div>
										<!-- /Post Content -->

										<footer>
											<div class="socials-container">
												<?php king_share_buttons_king(array('facebook','twitter','google_plus')); ?>
											</div>
											<div class="comments-counter">
												<a href="<?php echo esc_url(get_comments_link()); ?>"><?php comments_number( __('0', 'king'), __('1', 'king'), __('%', 'king')) ;?></a>
											</div>
										</footer>

									</div>

								</article>
							</div>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>
					
				</div>
			</div> 
			<!-- End Related Posts -->
		<?php endif; ?>
	<?php endif; ?>
<?php endif; ?>

<?php $tags = wp_get_post_tags($post->ID); ?>

<?php if (!is_wp_error($tags) && is_array($tags) && count($tags) > 0): ?>
		<!-- Post Tags -->
		<div class="king-post-tags">
			<span class="title"><?php _e('Taged with:', 'king'); ?></span>
			<?php foreach ($tags as $tag): ?>
				<a href="<?php echo esc_url(get_tag_link($tag -> term_id)); ?>" class="tag"><?php echo esc_html($tag -> name);?></a>
			<?php endforeach; ?>
		</div>
		<!-- End Post Tags -->
<?php endif; ?>

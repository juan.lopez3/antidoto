<?php
/*
 * Template Name: Portfolio 1 Column With Sidebar
 */
get_header();

wp_enqueue_script ( 'king-prettyphoto-js' );
wp_enqueue_script ( 'king-mixitup' );

global $template_portfolio_columns, $template_portfolio_columns_size, $query_string;

if (get_query_var('paged')) {
    $paged = get_query_var('paged');
} elseif (get_query_var('page')) { // applies when this page template is used as a static homepage in WP3+
    $paged = get_query_var('page');
} else {
    $paged = 1;
}
$portfolio_categories = get_post_meta(get_the_ID(), 'portfolio_categories', true);
?>
<div class="king-page">
	<!-- container -->
	<div class="container">
		<section id="projects-container" class="portfolio-1column normal-padding">
			<div class="row">
				<div class="col-md-9 col-sm-8">
					<div class="sorting-tags light">
						<div class="filter" data-filter="all"><?php _e('All', 'king'); ?></div>
						<?php $folio_cats = get_terms('portfolio-categories');

						foreach ($folio_cats as $folio_cat):
							if (is_array($portfolio_categories) && !in_array($folio_cat -> term_id, $portfolio_categories)):
								continue;
							endif;
							?>
							<div class="filter" data-filter=".category-<?php echo esc_attr($folio_cat->slug); ?>"><?php echo esc_html($folio_cat->name); ?></div>
						<?php endforeach; ?>

					</div>


					<?php
					$args = array(
						'numberposts' => '',
						'posts_per_page' => $posts_per_page,
						'offset' => 0,
						'meta_query' => array(array('key' => '_thumbnail_id')), //get posts with thumbnails only
						'orderby' => 'date',
						'order' => 'DESC',
						'include' => '',
						'exclude' => '',
						'meta_key' => '',
						'meta_value' => '',
						'post_type' => 'portfolio',
						'post_mime_type' => '',
						'post_parent' => '',
						'paged' => $paged,
						'post_status' => 'publish'
					);

					if (is_array($portfolio_categories)):
						$args['tax_query'] = array(
							'relation' => 'AND',
							array(
								'taxonomy' => 'portfolio-categories',
								'terms' => $portfolio_categories,
								'operator' => 'IN'
							)
						);
					endif;

					query_posts($args);
					?>
					<?php if (have_posts()) : ?>
						<?php while (have_posts()):
							the_post();
							$item_cats = wp_get_post_terms(get_the_ID(), 'portfolio-categories');
							$cat = array();
							$cat1 = array();
							foreach ($item_cats as $item_cat) {
								if (is_array($portfolio_categories) && !in_array($item_cat -> term_id, $portfolio_categories)):
									continue;
								endif;
								$cat[$item_cat->slug] = 'category-' . $item_cat->slug;
								$cat1[$item_cat->slug] = $item_cat->name;
							}
							?>
							<div class="project-item mix <?php echo sanitize_html_classes(implode(' ', $cat)); ?>">
								<div class="row">
									<div class="col-md-12">
										<div class="project style-king">
											<div class="project-image wow animated fadeInLeft">
												<?php the_post_thumbnail('king-alternative', array('class' => 'img-responsive', 'alt' => get_the_title())); ?>
												<div class="project-hover">
													<div>
														<div>
															<span class="category"><?php echo king_get_shortened_string_by_letters($cat1_html, 60); ?></span>
															<span class="separator"></span>
															<h4 class="project-title"><?php echo king_get_shortened_string_by_letters(get_the_title(),25); ?></h4>
															<p><?php echo strip_tags(king_get_the_excerpt_theme(10)); ?></p>
															<a class="project-button" href="<?php echo esc_url(king_img_url()); ?>" rel="prettyPhoto"><?php _e('zoom', 'king'); ?></a>
															<a class="project-button" href="<?php echo esc_url(get_permalink()); ?>"><?php _e('view project', 'king');?></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endwhile;
						wp_reset_query(); ?>
					<?php endif; ?>
				</div>
				<?php get_sidebar(); ?>
			</div>


		</section>
	</div>
	<!-- /container -->
</div>
<!-- /king-page -->
<?php get_footer(); ?>
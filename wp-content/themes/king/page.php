<?php
/**
 * The Template for displaying all single posts.
 *
 * @package king
 * @since king 1.0
 */

//wp_head();
get_header();

switch (get_post_meta(get_the_ID(),'content_margin',true)) {
	case 'no_margin':
		$padding_class = 'no-padding';
		break;
	
	case 'only_bottom_margin':
		$padding_class = 'no-top-padding small-padding';
		break;
	
	case 'only_top_margin':
		$padding_class = 'no-bottom-padding small-padding';
		break;
	
	default:
		$padding_class = 'small-padding';
		break;
}

$class = king_check_if_any_sidebar(
	'col-md-12 '.$padding_class,
	'col-md-8 '.$padding_class,
	'');

if (king_get_single_post_sidebar_position() == 'left') {
	$class .= ' col-md-push-4';
}
?>


            <!--<--------------------seccion img. de inicio--------->
            
            <div class="img_inicio">
                
                <img src="<?php the_field('imagen_inicio')?>">
                
            </div>
            
            <!--------------------------seccion que es antídoto-------------<-->
            
            <div class="img_que_es">
                
                <img src="<?php // the_field('que_es_antidoto')?>">
                
            </div>
       
            <!--<----------seccion programación--------------->
            
            <div>
                <div class="titulo_prog">
                
                    <h1>PROGRAMACIÓN</h1>
                </div>
                <div class="cont_slaider_prog">
                    
                    <!--consulta programación-->
                        <?php $arg_prog = array(
                            'post_type'=>'programacion',
                            'post_status' => 'publish',
                            'meta_key'=>'dia_programacion',
                            'orderby'=>'meta_value_num',
                            'order'     => 'ASC',
                            'meta_query' => array(
                                'relation' => 'AND',
                                array('key'=>'mes_programacion',
                                    'value'=>  mes_castellano(date('m')),
                                    'compare'=>'='
                                    ),
                                array('key'=>'anio',
                                    'value'=> date('Y'),
                                    'compare'=>'='
                                    )
                            )
                        );
                                
                                $query_prog = new WP_Query($arg_prog);
                                
//                                contador para identificar los div de los eventos
                                $cont = 0;
                                ?>
                    
                    <section class="<?php echo sanitize_html_classes($class); ?>">
				<?php if (have_posts()) : ?>
					<?php /* Start the Loop */ ?>
					<?php while (have_posts()) : the_post(); ?>
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'king' ), 'after' => '</div>' ) ); ?>
					<?php endwhile; ?>
					<?php if (get_post_meta($post->ID, 'show_comment_form', true) != 'no'):
						if (comments_open() || '0' != get_comments_number() ): ?>
							<div class="row">
								<div class="col-sm-12"><?php comments_template('', true); ?></div>
							</div>
						<?php endif;
					endif;
					?>
				<?php else : //No posts were found ?>
					<?php get_template_part('no-results'); ?>
				<?php endif; ?>
			</section>
			<?php king_get_single_post_sidebar('left'); ?>
			<?php king_get_single_post_sidebar('right'); ?>
                    
                    <?php if($query_prog->have_posts()): while ($query_prog->have_posts()): $query_prog->the_post()?>
                    
                    <div class="cont_prog" id="<?php echo 'evento'.$cont?>">
                        <div class="dia_eve">
                            <h1> <?php the_field('dia_programacion')?> </h1>        
                        </div>
                        <div class="mes_eve">
                            <h2> <?php the_field('mes_programacion')?> </h2>
                        </div>
                        <div class="hora_eve">
                            <?php the_field('hora_evento')?> : <?php the_field('minutos_evento')?> <?php the_field('horario_evento')?>
                        </div>
                        <div>
                            <?php the_title() ?>
                        </div>
                        <div class="costo_eve">
                            <?php 
                            
                            if(get_field('costo_evento')==0){
                                
                                echo 'GRATIS';
                            }else{
                                echo '$'.get_field('costo_evento');
                            }
                            
                            ?>
                            
                        </div>
                    </div>
                   
                    <?php $cont = $cont+1?>
                    <?php endwhile;   endif;?>
                     <div class="flecha_izq" ">
                         <a id="flecha_izq"  href="#" > izq </a>
                    </div >
                    <div class="flecha_der" >
                        <a id="flecha_der" href="#"> der </a>
                    </div>
                    <label id="val_i"> 0</label>
                    <label id="limite"> 1</label>
                </div>
            </div>
            
            <!---------------------------Sección de catálogo---------------->
            <div class="container catalogo">
                <img src="<?php echo get_template_directory_uri().'/img_antidoto/fondo-parque.png'?>">
                <div class="container descargas">
                    <h1> CATÁLOGO DE LA OBRA</h1>
                    <?php if(have_posts()): while (have_posts()): the_post()?>
                    
                    <div class="link_ver"> 
                        <a href="<?php echo wp_get_attachment_url(get_field('documento_descarga'));?>" target="black" >VER</a>
                    </div>
                    <div class="link_descarga">
                        <a href="<?php echo wp_get_attachment_url(get_field('documento_descarga'));?>"  download="Catálogo_antídoto" >DESCARGAR</a>
                    </div>
                    
                    <?php endwhile; endif?>      
                </div>
            </div>
            
           
           
    
           <div>
               
               
           
        </div>
        
                               
        <?php // endwhile; endif; ?>
        
	<!-- /container -->
</div>


<!-- /king-page -->
<?php get_footer();

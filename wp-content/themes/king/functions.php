<?php
/**
 * King functions and definitions
 *
 * @package king
 * @since king 1.0
 */
/**
 * Optional: set 'ot_show_pages' filter to false.
 * This will hide the settings & documentation pages.
 */
add_filter ( 'ot_show_pages', '__return_false' );

/**
 * Optional: set 'ot_show_new_layout' filter to false.
 * This will hide the "New Layout" section on the Theme Options page.
 */
add_filter ( 'ot_show_new_layout', '__return_false' );

/**
 * Required: set 'ot_theme_mode' filter to true.
 */
add_filter ( 'ot_theme_mode', '__return_true' );

/**
 * Setting constant to inform the plugin that them is activated
 */
define ('KING_THEME_ACTIVATED' , true);

/**
 * Required: include OptionTree.
 */
require_once ( get_template_directory() . '/option-tree/ot-loader.php');

/**
 * Theme Options
 */
require_once ( get_template_directory() . '/inc/theme-options.php');
/**
 * Meta boxes + page builder
 */

require_once ( get_template_directory() . '/inc/meta-boxes.php');

/**
 * Framework functions
 */
require_once ( get_template_directory() . '/framework/framework.php');

/**
 * Widgets initalization
 */
require_once (get_template_directory() . '/inc/widgets.php');

/**
 * Shortcodes initalization
 */
require_once (get_template_directory() . '/inc/shortcodes.php');

/**
 * Third Party Plugins activation
 */
require_once (get_template_directory() . '/framework/plugins-activation.php');

/**
 * Comment Walker Class
 */
require_once (get_template_directory() . '/inc/Comment_Walker.class.php');

/**
 * Framework menus
 */
require_once (get_template_directory() . '/framework/custom-menus.php');

/**
 * Framework menus
 */
require_once (get_template_directory() . '/framework/class/king_extra_taxonomy_fields.class.php');

/**
 * Sample data importer
 */
require_once (get_template_directory() . '/framework/importer/importer.php');

/**
 * Exerpt length
 * Usage king_the_excerpt_theme('short');
 */
define ( 'TINY_EXCERPT', 12 );
define ( 'SHORT_EXCERPT', 22 );
/**
 */
define ( 'REGULAR_EXCERPT', 40 );
define ( 'LONG_EXCERPT', 55 );

/**
 * WooCommerce Extra Feature
 * --------------------------
 *
 * Change number of related products on product page
 * Set your own value for 'posts_per_page'
 */
function king_woo_related_products_limit() {
    global $product;

    $args = array (
        'post_type' => 'product',
        'no_found_rows' => 1,
        'posts_per_page' => 4,
        'ignore_sticky_posts' => 1,
        // 'orderby' => $orderby,
        // 'post__in' => $related,
        'post__not_in' => array (
            $product->id
        )
    );
    return $args;
}

add_filter ( 'woocommerce_related_products_args', 'king_woo_related_products_limit' );

/**
 * Enable Retina Support
 */
function king_theme_init() {
    if (! is_admin () && ot_get_option ( 'retina_support' ) == 'enabled') {
        define ( 'RETINA_SUPPORT', true );
    }
}

add_action ( 'init', 'king_theme_init' );

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since king 1.0
 */
if (! isset ( $content_width )) {
    $content_width = 848; /* pixels */
}

if (! function_exists ( 'king_theme_setup' )) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * @since king 1.0
     */
    function king_theme_setup() {
        /**
         * Custom template tags for this theme.
         */
        require (get_template_directory () . '/inc/template-tags.php');

        /**
         * Custom functions that act independently of the theme templates
         */
        require (get_template_directory () . '/inc/tweaks.php');

        /**
         * Make theme available for translation
         */
        load_theme_textdomain ( 'king', get_template_directory () . '/languages' );
        load_theme_textdomain ( 'framework', get_template_directory () . '/languages' );

		/**
		* Custom image sizes
		*/
	   add_theme_support( 'post-thumbnails' );
	   if (function_exists('king_add_image_sizes')) {
		   king_add_image_sizes();
	   }
		
        /**
         * Post Format
         */
        add_theme_support ( 'post-formats', array (
            'aside',
            'gallery',
            'image',
            'audio',
            'video',
            'quote',
            'status',
            'link',
        ) );

        /**
         * Add default posts and comments RSS feed links to head
         */
        add_theme_support ( 'automatic-feed-links' );
		
		/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	   add_theme_support( 'title-tag' );
		
        /**
         * This theme uses wp_nav_menu() in one location.
         */
        register_nav_menus ( array (
            'primary' => __ ( 'Primary Menu', 'king' ),
            'primary-left' => __ ( 'Primary Left Menu', 'king' ),
            'primary-right' => __ ( 'Primary Right Menu', 'king' ),
            'footer' => __ ( 'Footer Menu', 'king' ),
            'shop-header'=>__('Shop Header','king')
        ) );

        add_editor_style ( 'css/main.css' );
		
		/**
		* Woocommerce support
		*/
	   add_theme_support ( 'woocommerce' );
    }

endif; // theme_setup
add_action ( 'after_setup_theme', 'king_theme_setup' );

if (!function_exists('king_add_image_sizes')) {
	/**
	 * Add image sizes
	 */
	function king_add_image_sizes() {
		
		add_image_size ( 'king-pinterest', 735, 735, true );

		// standard content
		add_image_size ( 'king-default', 800, 320, true );
		add_image_size ( 'king-alternate', 400, 320, true );
		add_image_size ( 'king-alternative', 800, 530, true );
		add_image_size ( 'king-blog', 920, 529, true );
		add_image_size ( 'king-square', 285, 285, true );
	
		add_image_size ( 'king-featured_projects_5', 570, 432, true );
		add_image_size ( 'king-portfolio-shortcode', 617, 327, true );
		add_image_size ( 'king-portfolio-shortcode-h', 617, 654, true );
		add_image_size ( 'king-portfolio-3-shortcode', 571, 568, true );
		add_image_size ( 'king-portfolio-3-shortcode-v', 571, 299, true );

		add_image_size ( 'king-latest-works', 387, 252, true );
		add_image_size ( 'king-team-member-single', 360, 384, true );
		
		add_image_size ( 'king-blog-sh-v', 580,	650, true );
		add_image_size ( 'king-blog-sh-h', 580,	210, true  );
		add_image_size ( 'king-blog-sh-v2', 580, 860, true  );

		add_image_size ( 'king-latest-news', 360, 321, true);
		add_image_size ( 'king-latest-news-2', 395, 382, true);
		add_image_size ( 'king-latest-works', 395, 382, true);
		add_image_size ( 'king-portfolio-single-style2', 870, 475, true );
		add_image_size ( 'king-blog-3-half-height', 792, 397, true );
		add_image_size ( 'king-blog-3-half-height-small', 395, 397, true );
		add_image_size ( 'king-recent-works-slider-widget', 264, 172, true );
	}
}

/**
 * Removing parent category dropdown from menu custom post type category
 * @return type
 */
function king_remove_menu_parent_category()
{
	// don't run in the Tags screen
    if ( 'menu-categories' != $_GET['taxonomy'] )
        return;
	?>
        <script type="text/javascript">
            jQuery(document).ready(function($)
            {     
                $('label[for=parent]').<?php echo (isset( $_GET['action'] ) ? 'parent().parent()' : 'parent()'); ?>.remove();       
            });
        </script>
    <?php
}

/**
 * Modify title placeholder for custom post types
 *
 * @since king 1.0
 */
add_filter ( 'enter_title_here', 'king_custom_enter_title' );
function king_custom_enter_title($input) {
    global $post_type;

    if (is_admin () && 'team' == $post_type) {
        return __ ( 'Enter team member name here', 'king' );
    }
    return $input;
}

/**
 * Enqueue scripts and styles
 *
 * @since king 1.0
 */
function king_theme_scripts() {
    
	wp_enqueue_style ('king-prettyphoto-css', get_template_directory_uri () . '/js/prettyphoto/css/prettyPhoto.css', array (), null, 'all' );
	wp_enqueue_style ('king-twitter-bootstrap', get_template_directory_uri () . '/css/bootstrap.min.css', array (), null, 'all' );
    wp_enqueue_style ('king-fontello', get_template_directory_uri () . '/css/fontello.css', array (), null, 'all' );
    wp_enqueue_style ('king-animation', get_template_directory_uri () . '/css/animate.css', array (), null, 'all' );
    wp_enqueue_style ('king-flexSlider', get_template_directory_uri () . '/css/flexslider.css', array (), null, 'all' );
    wp_enqueue_style ('king-nouislider', get_template_directory_uri () . '/css/jquery.nouislider.css', array (), null, 'all' );
    wp_enqueue_style ('king-jquery-ui', get_template_directory_uri () . '/css/jquery-ui.min.css', array (), null, 'all' );
	wp_enqueue_style ('king-style', get_template_directory_uri () . '/css/style.css', array (), null, 'all' );	
	wp_enqueue_style ('king-owl-carousel', get_template_directory_uri () . '/css/owl.carousel.css', array (), null, 'all' );
	wp_enqueue_style ('king-owl-transitions', get_template_directory_uri () . '/css/owl.transitions.css', array (), null, 'all' );
    wp_enqueue_style ('king-google-fonts', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800', array (), null, 'all' );
    wp_enqueue_style ('king-custom-style', get_stylesheet_uri () );
	wp_enqueue_style ('king-responsive', get_template_directory_uri () . '/css/responsive.css', array (), null, 'all' );

    wp_enqueue_script ('king-modernizr', get_template_directory_uri () . '/js/modernizr.js', array ('jquery'), null, true );
	wp_enqueue_script ('king-imagesLoaded', get_template_directory_uri () . '/js/imagesloaded.pkgd.min.js', array ('jquery'), null, true );
	wp_enqueue_script ('king-bootstrap-js', get_template_directory_uri () . '/js/bootstrap.min.js', array ('jquery'), null, true );
	wp_enqueue_script ('king-jquery-ui', get_template_directory_uri () . '/js/jquery-ui.min.js', array ('jquery'), null, true );
    wp_enqueue_script ('king-jquery-easing', get_template_directory_uri () . '/js/jquery.easing.1.3.js', array ('jquery'), null, true );
    wp_enqueue_script ('king-jquery-mousewheel', get_template_directory_uri () . '/js/jquery.mousewheel.min.js', array ('jquery'), null, true );
    wp_enqueue_script ('king-wow', get_template_directory_uri () . '/js/wow.min.js', array ('jquery'), null, true );
    wp_enqueue_script('king-niceScroll', get_template_directory_uri().'/js/jquery.nicescroll.min.js',array('jquery'),null,true);
	wp_enqueue_script('king-scrollMe', get_template_directory_uri().'/js/jquery.scrollme.min.js',array('jquery'),null,true);
	wp_enqueue_script('king-fullpage', get_template_directory_uri().'/js/jquery.fullPage.min.js',array('jquery'),null,true);
	wp_enqueue_script('king-slimscroll', get_template_directory_uri().'/js/jquery.slimscroll.min.js',array('jquery'),null,true);
	wp_enqueue_script('king-dlMenu', get_template_directory_uri().'/js/jquery.dlmenu.js',array('jquery'),null,true);
    wp_enqueue_script('king-vivus', get_template_directory_uri().'/js/vivus.min.js',array('jquery'),null,true);
	wp_enqueue_script('king-stellar', get_template_directory_uri().'/js/jquery.stellar.min.js',array('jquery'),null,true);
	
	wp_register_script ('king-prettyphoto-js', get_template_directory_uri () . '/js/prettyphoto/js/jquery.prettyPhoto.js', array ('jquery'), null, true );
    wp_register_script ('king-jquery-flexslider', get_template_directory_uri () . '/js/jquery.flexslider-min.js', array ('jquery'), null, true );
    wp_register_script ('king-jquery-knob', get_template_directory_uri () . '/js/jquery.knob.js', array ('jquery'), null, true );
    wp_register_script ('king-mixitup', get_template_directory_uri () . '/js/jquery.mixitup.min.js', array ('jquery'), null, true );
    wp_register_script ('king-jquery-masonry2', get_template_directory_uri () . '/js/masonry.pkgd.min.js', array ('jquery'), null, true );
	wp_register_script ('king-fitvids', get_template_directory_uri () . '/js/jquery.fitvids.js', array ('jquery'), null, true );
    wp_register_script ('king-nouislider', get_template_directory_uri () . '/js/jquery.nouislider.min.js', array ('jquery'), null, true );
    wp_register_script ('king-isotope', get_template_directory_uri () . '/js/isotope.pkgd.min.js', array ('jquery'), null, true );
	wp_register_script ('king-retina', get_template_directory_uri().'/js/retina.js',null,null,true);
	wp_register_script ('king-owl-carousel', get_template_directory_uri().'/js/owl.carousel.min.js',array('jquery'),null,true);
    wp_register_script ('king-instafeed', get_template_directory_uri().'/js/instafeed.min.js',array('jquery'),null,true);
	wp_register_script ('king-google-map-api', 'http://maps.googleapis.com/maps/api/js?sensor=false',array(),null,true);
	wp_register_script ('king-cd-google-map', get_template_directory_uri().'/js/cd-google-map.js',array('king-google-map-api'),null,true);
	wp_register_script ('king-main', get_template_directory_uri () . '/js/script.js', array ('jquery'), null, true );
	
	// Localize the script with new data
	$translation_array = array(
		'loading_posts' => __( 'Loading Posts...' , 'king' ),
		'load_more' => __( 'Load more' , 'king' ),
	);
	wp_localize_script( 'king-main', 'king_lang', $translation_array );
	
	wp_enqueue_script ( 'king-main' );
	
    if (defined ( 'RETINA_SUPPORT' ) && RETINA_SUPPORT === true) {
        wp_enqueue_script ( 'king-retina' );
    }

    if (is_singular () && comments_open () && get_option ( 'thread_comments' )) {
        wp_enqueue_script ( 'comment-reply' );
    }
}

add_action ( 'wp_enqueue_scripts', 'king_theme_scripts' );

$ie9tags = create_function ( '', 'echo \'<!--[if lt IE 9]><script>document.createElement("header"); document.createElement("nav"); document.createElement("section"); document.createElement("article"); document.createElement("aside"); document.createElement("footer"); document.createElement("hgroup");</script><![endif]-->\';' );
add_action ( 'wp_head', $ie9tags );

$html5shim = create_function ( '', 'echo \'<!--[if lt IE 9]><script src="\'.get_template_directory_uri().\'/js/html5.js"></script><![endif]-->\';' );
add_action ( 'wp_head', $html5shim );

$icomoon = create_function ( '', 'echo \'<!--[if lt IE 7]><script src="\'.get_template_directory_uri().\'/js/icomoon.js"></script><![endif]-->\';' );
add_action ( 'wp_head', $icomoon );

$ie_style = create_function ( '', 'echo \'<!--[if lt IE 9]><link href="\'.get_template_directory_uri().\'/css/ie.css" rel="stylesheet"><![endif]-->\';' );
add_action ( 'wp_head', $ie_style );

$ie_scripts = create_function ( '', 'echo \'
<!--[if lt IE 9]>
<script src="\'.get_template_directory_uri().\'/js/jquery.placeholder.js"></script>
<script src="\'.get_template_directory_uri().\'/js/script_ie.js"></script>
<![endif]-->\';' );
add_action ( 'wp_head', $ie_scripts );

$no_fouc = create_function ( '', 'echo \'
<!-- Preventing FOUC -->
<style>
.no-fouc{ display:none; }
</style>

<script>

// Prevent FOUC(flash of unstyled content)
jQuery("html").addClass("no-fouc");

jQuery(document).ready(function($) {

	$("html").show();

	setFullWidthFirst();

	// Set Full Width on Resize & Load
	$(window).bind("load", function(){

		setFullWidthFirst();
        setTimeout(function(){
            setFullWidthFirst();
        }, 500);

	});

	// Set Full Width Function
	function setFullWidthFirst(){
		
		if(!$("body").hasClass("b960") && !$("body").hasClass("b1170")){
			$(".full-width").each(function(){
				
				var element = $(this);

				// Reset Styles
				element.css("margin-left", "");
                element.css("padding-left", "0!important");
				element.css("width", "");	

				var element_x = element.offset().left;

				// Set New Styles
				element.css("margin-left", -element_x+"px");
				element.css("width", $(window).width()+"px");	
                element.css("padding-left", "");

			});
		}

	}

}); 


var mobilenav_screen_size = \'.(ot_get_option(\'switch_menu_to_mobile\') ? ot_get_option(\'switch_menu_to_mobile\') : 991 ).\';

</script>\';
');
add_action ( 'wp_head', $no_fouc );


function king_add_mobilenav_css() {
	
	$ie_style = create_function ( '', 'echo \'<link rel="stylesheet" href="\'.get_template_directory_uri().\'/css/mobilenav.css" media="screen and (max-width: \'.(ot_get_option(\'switch_menu_to_mobile\') ? ot_get_option(\'switch_menu_to_mobile\') : 991 ).\'px)">\';' );
	add_action ( 'wp_head', $ie_style );
}
add_action ('init', 'king_add_mobilenav_css');


function king_ajaxurl() {
    ?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    </script>
<?php
}

add_action ( 'wp_head', 'king_ajaxurl' );

/**
 * Google analytics ourput
 */
function king_google_analytics_output() {
    if (ot_get_option ( 'google_analytics_id' ) != "") {
        ?>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', '<?php echo ot_get_option('google_analytics_id'); ?>']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
    <?php
    }
}

add_action ( 'wp_footer', 'king_google_analytics_output', 1200 );

/**
 * Register theme widgets
 *
 * @since king 1.0
 */
function king_theme_widgets_init() {
    register_sidebar ( array (
        'name' => __ ( 'Main Sidebar', 'king' ),
        'id' => 'main',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ) );
	
	register_sidebar ( array (
        'name' => __ ( 'Header Sidebar', 'king' ),
        'id' => 'header',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="menu-title">',
        'after_title' => '</h4>'
    ) );

    for($i = 1; $i <= 4; $i ++) {
        register_sidebar ( array (
            'name' => __ ( 'Footer Area', 'king' ) . ' ' . $i,
            'id' => 'footer-area-' . $i,
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4>',
            'after_title' => '</h4>'
        ) );
    }
	
	for($i = 1; $i <= 2; $i ++) {
        register_sidebar ( array (
            'name' => __ ( 'Footer 2 Area', 'king' ) . ' ' . $i,
            'id' => 'footer-2-area-' . $i,
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h4>',
            'after_title' => '</h4>'
        ) );
    }

	register_sidebar ( array (
        'name' => __ ( 'Shop', 'king' ),
        'id' => 'shop',
        'before_widget' => '<div id="%1$s" class="dupa shop-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="header"><h4>',
        'after_title' => '</h4><div class="arrow"></div></div>'
    ) );
	
    register_sidebar ( array (
        'name' => __ ( 'Shop Footer Area', 'king' ) . ' 1',
        'id' => 'shop-footer-area-1',
        'before_widget' => '<div id="%1$s" class="shop-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ) );
    register_sidebar ( array (
        'name' => __ ( 'Shop Footer Area', 'king' ) . ' 2',
        'id' => 'shop-footer-area-2',
        'before_widget' => '<div id="%1$s" class="shop-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ) );
    register_sidebar ( array (
        'name' => __ ( 'Shop Footer Area', 'king' ) . ' 3',
        'id' => 'shop-footer-area-3',
        'before_widget' => '<div id="%1$s" class="shop-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ) );
	
    $user_sidebars = ot_get_option ( 'user_sidebars' );

    if (is_array ( $user_sidebars )) {
        foreach ( $user_sidebars as $sidebar ) {
            register_sidebar ( array (
                'name' => $sidebar ['title'],
                'id' => sanitize_title ( $sidebar ['title'] ),
                'before_widget' => '<div id="%1$s" class="%2$s">',
                'after_widget' => '</div>',
                'before_title' => '<h5>',
                'after_title' => '</h5>'
            ) );
        }
    }
}

add_action ( 'widgets_init', 'king_theme_widgets_init' );

/**
 * Add classes to body
 *
 * @param array $classes
 * @return array
 * @since framework 1.0
 */
// Add specific CSS class by filter
add_filter ( 'body_class', 'king_get_body_main_class' );
function king_get_body_main_class($classes) {

    // add body class and main menu style selected from control panel
    $added_body_class = false;
    $added_main_menu_style = false;
    if (king_check_if_control_panel())
	{
		if (king_check_if_use_control_panel_cookies() && !empty($_COOKIE['theme_body_class']))
		{
			$added_body_class = true;
			$classes[] = $_COOKIE['theme_body_class'];
		}
		else if (isset($_GET['switch_layout']) && !empty($_GET['switch_layout']))
		{
			$added_body_class = true;
			$classes[] = $_GET['switch_layout'];
		}

		if (king_check_if_use_control_panel_cookies() &&  !empty($_COOKIE['theme_main_menu_style']))
		{
			$main_menu_style = $_COOKIE['theme_main_menu_style'];
		}
		elseif (isset($_GET['switch_main_menu_style']) && !empty($_GET['switch_main_menu_style']))
		{
			$main_menu_style = $_GET['switch_main_menu_style'];
		}

		if (!empty($main_menu_style))
		{
			$added_main_menu_style = true;
			$classes[] = king_get_body_header_class($main_menu_style);
		}
	}
    // add body_class set in theme options only if not added from control panel
    if ($added_body_class == false) {
		
		if (is_singular(array('post', 'page')) && $page_body_class = get_post_meta(get_the_ID(), 'body_class', true) ) {
			$class = $page_body_class;
		}
		else {
			$class = ot_get_option('body_class');
		}
		
        if (empty ( $class )) {
            $class = 'w1170';
        }
        $classes[] = $class;
    }
	
	//add body_class set in theme options only if not added from control panel
	if ($added_main_menu_style == false)
	{
		$style = king_get_main_menu_style();
		if (!empty($style))
		{
			$classes[] = king_get_body_header_class($style);
		}
	}
	//add class if preheader is enabled
	$header = king_get_main_menu_style();
	if (in_array($header, array('style13', 'style16') ) && king_get_opt('show_preheader') == 'yes') {
		$classes[] = 'preheader-on';
	}
		
	$body_margin = get_post_meta(get_the_ID(), 'body_margin', true);
	if ($body_margin == 'transparent_header') {
		$classes[] = 'transparent-header';
	}

	$preloader = ot_get_option('preloader');
	if (!empty($preloader) && $preloader != 'disabled') {
		$classes[] = 'page_transitions_enabled';
	}
	
	if (ot_get_option('sticky_footer') == 'yes') {
		$classes[] = 'sticky-footer-on';
	}
	
	if (ot_get_option('enable_responsiveness') == 'disabled') {
		$classes[] = 'responsiveness-off';
	}

    $onepage_scroll = get_post_meta(get_the_ID(), 'onepage_scroll', true);
    if($onepage_scroll == 'yes') {
        $classes[] = 'onepage-scroll';
    }
	
	if (is_singular()) {
		$single_post_sidebar_position = king_get_single_post_sidebar_position();
		if ($single_post_sidebar_position != 'no') {
			$classes[] = 'sidebar-'.$single_post_sidebar_position;
		}
	} else {
		$classes[] = 'sidebar-right';
	}
	
    return $classes;
}

/**
 * Get body class
 * @param type $style
 * @return string
 */
function king_get_body_header_class($style) {
	
	switch ($style) {
		
		case 'style13':
			$class = 'headerstyle-king';
			break;
			
		case 'style14':
			$class = 'headerstyle-king';
			break;
		
		case 'style15':
			$class = 'headerstyle-king';
			break;
		
		case 'style16':
        case 'style17':
			$class = 'headerstyle-king';
			break;
		
		case 'style1':
		default:
			$class = 'headerstyle1';
	}
	return $class;
}

/**
 * Get a list of supported header styles
 *
 * @return array
 */
function king_get_header_styles($empty_option = false) {
    if ($empty_option === true) {
        $styles [] = array (
            'value' => 'default',
            'label' => __ ( 'Default', 'framework' ),
            'src' => ''
        );
    }
	$styles [] = array (
        'value' => 'style13',
        'label' => __ ( 'Style 1', 'framework' ), // style 13
        'src' => ''
    );
	$styles [] = array (
        'value' => 'style14',
        'label' => __ ( 'Style 2', 'framework' ), // style 14
        'src' => ''
    );
	$styles [] = array (
        'value' => 'style15',
        'label' => __ ( 'Style 3', 'framework' ), // style 15
        'src' => ''
    );
	$styles [] = array (
        'value' => 'style16',
        'label' => __ ( 'Style 4 ( Default )', 'framework' ), // style 16
        'src' => ''
    );
    $styles [] = array (
        'value' => 'style17',
        'label' => __ ( 'Style 5', 'framework' ), // style 16
        'src' => ''
    );
    return $styles;
}

/**
 * Get control panel colors
 *
 * @return array
 */
function king_get_control_panel_colors() {
    return array (
        '#efbe4a',
        '#f97854',
        '#e66262',
        '#d273ce',
        '#775bf8',
        '#6297e6',
        '#71c7dd',
        '#3edbd9',
        '#5fd586',
        '#abc035',
        '#bcbcbc',
        '#222a2c'
    );
}
function king_get_control_panel_backgrounds() {
    return array (
        'img1.gif',
        'img2.png',
        'img3.jpg',
        'img4.jpg',
        'img5.jpg',
        'img6.jpg',
        'img7.png',
        'img8.jpg',
        'img9.png',
        'img10.jpg',
        'img11.jpg',
        'img12.jpg',
        'img13.jpg',
    );
}

function king_get_control_panel_demo_backgrounds() {
    return array(
        array('thumb.jpg', 'bthumb4.jpg', 'http://arenaofthemes.wpengine.com/index-alt-3/'),
        array('thumb1.jpg', 'bthumb1.jpg', 'http://arenaofthemes.wpengine.com/'),
        array('thumb2.jpg','bthumb5.jpg', 'http://arenaofthemes.wpengine.com/scandi/'),
        array('thumb3.jpg', 'bthumb2.jpg', 'http://arenaofthemes.wpengine.com/index-alt/'),
        array('thumb5.jpg', 'bthumb3.jpg', 'http://arenaofthemes.wpengine.com/index-alt-2/'),
        array('thumb4.jpg', 'bthumb6.jpg', 'http://arenaofthemes.wpengine.com/shop/'),
    );
}

/**
 * Gettext filter, used in functions: __,_e etc.
 *
 * @global array $ns_options_translations
 * @param string $content
 * @param string $text
 * @param string $domain
 * @return string
 */
if (! is_admin ()) {
    function king_translation($content, $text, $domain) {
        if (in_array ( $domain, array (
            'king'
        ) )) {
            return ot_get_option ( 'translator_' . sanitize_title ( $content ), $content );
        }
        return $content;
    }
    function king_check_if_translations_enabled() {
        if (ot_get_option ( 'enable_translations' ) == 'yes') {
            add_filter ( 'gettext', 'king_translation', 20, 3 );
        }
    }

    add_action ( 'init', 'king_check_if_translations_enabled' );
}

/**
 * Woocommerce support - hiding title on product's list
 *
 * @param type $content
 * @return boolean
 */
function king_hide_woocommerce_page_title($content) {
    return false;
}

add_filter ( 'woocommerce_show_page_title', 'king_hide_woocommerce_page_title' );

/**
 * Get animation class for animated shortcodes
 *
 * @param type $animation
 * @return string
 */
function king_get_animation_class($animation, $add_class_attr = false) {
    if (! empty ( $animation )) {
        $class = ' wow ' . $animation;

        if ($add_class_attr === true) {
            return 'class="' . sanitize_html_classes($class) . '"';
        }
        return $class;
    }
    return '';
}

function king_change_excerpt_more($more) {
    return '';
}
add_filter ( 'excerpt_more', 'king_change_excerpt_more' );

/**
 * Return the concatinated category list of a post
 *
 * @param null $post_id
 * @return string
 */
function king_post_categories($post_id = null) {
    if (! $post_id) {
        global $post;
        $post_id = $post->ID;
    }

    $post_categories = wp_get_post_categories ( $post_id );
    $cats = array ();

    foreach ( $post_categories as $c ) {
        $cat = get_category ( $c );
        $cats [] = '<a href="' . esc_url(get_term_link ( $c, 'category' )) . '">' . $cat->name . '</a>';
    }

    return implode ( ',', $cats );
}
function king_img_url($size = 'full') {
    global $post;
    $image = wp_get_attachment_image_src ( get_post_thumbnail_id ( $post->ID ), $size );
    return $image [0];
}

/**
 * Process the contact form (template)
 */
if (isset ( $_POST ['contact-form-value'] )) {
    function king_process_contact_form() {
        $data = array ();
        $err = array ();
        $headers = null;
        if (! wp_verify_nonce ( $_POST ['contact_nonce'], 'contact_form_submission' )) {
            $data ['status'] = 0;
            $err = array ();
            $err [] = __("Invalid Form Submission",'king');
            $data ['message'] = implode ( '<br>', $err );
            exit ( 0 );
        }

        if ($_POST ['contact-form-value'] == 1) {
        	$name = $field['Name'] = filter_var($_POST['name'], FILTER_SANITIZE_STRING); 
            $email = $field['Email'] = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
            $field['Message'] = filter_var($_POST['msg'], FILTER_SANITIZE_STRING);

            foreach ( $field as $k => $v ) {
                if (trim ( $v ) == '') {
                    $err [] = $k . ' ' . __ ( 'is required', 'king' );
                }
            }

            if (! $field ['Email'] ) {
                $err [] = __ ( 'Email is invalid', 'king' );
            }

            if (empty ( $err )) {
                // send the mail

                $to = ot_get_option ( 'contact_form_email' );
                $headers = "From: $name <$email>" . "\r\n";

                add_filter ( 'wp_mail_content_type', 'king_set_html_content_type' );
                function king_set_html_content_type() {
                    return 'text/html';
                }
				
				$subject = ot_get_option ( 'contact_form_subject' );
				if (empty($subject)) {
					$subject = __('Query', 'king');
				}
				
				$message_content =  __('From:', 'king').' '.$name."<br>";
				$message_content .= __('Email:', 'king').' <a href="mailto'.esc_attr($email).'">'.$email.'</a>'."<br>";
				$message_content .= __('Message:', 'king').' '.$field['Message']."\n";
				
                if (wp_mail ( $to, $subject, $message_content, $headers )) {
                    $data ['status'] = 1;
                    $data ['message'] = __( 'Message Sent', 'king' );
                } else {
                    $data ['status'] = 0;
                    $data ['message'] = __( 'An Error occured.', 'king' );
                }

                // Reset content-type to avoid conflicts -- http://core.trac.wordpress.org/ticket/23578
                remove_filter ( 'wp_mail_content_type', 'set_html_content_type' );
            } else {
                $data ['status'] = 0;
                $data ['message'] = implode ( '<br>', $err );
            }
            echo json_encode ( $data );
            exit ( 0 );
        }
    }
    add_action ( 'init', 'king_process_contact_form' );
}

/**
 * Process the contact form (footer-2)
 */
function king_process_footer_contact_form() {
	
	global $king_cf_error, $king_cf_message, $king_cf_form_name, $king_cf_form_email, $king_cf_form_message;
	
	if (!isset($_POST['cf_wpnonce'])) {
		return;
	}
	
	
	$king_cf_form_name = '';
	$king_cf_form_email = '';
	$king_cf_form_message = '';
	
	$king_cf_error = false; 
	$king_cf_message = '';
	
	//checking if email is valid
	$email = ot_get_option('contact_form_email');
	if ( !is_email( $email ) )
	{
		$king_cf_error = true;
	}
	
	if (wp_verify_nonce($_POST['cf_wpnonce'],'footer-2-cf_mar'))
	{
		$king_cf_form_name = sanitize_text_field($_POST['form_name']);
		$king_cf_form_email = sanitize_text_field($_POST['form_email']);
		$king_cf_form_message = filter_var($_POST['form_message'], FILTER_SANITIZE_STRING);
		
		$king_cf_error = false;
		if (empty($king_cf_form_name) || empty($king_cf_form_email) || empty($king_cf_form_message)) {
			$king_cf_message .= '<p>' . __('Please fill all required fields.','king') . '</p>';
			$king_cf_error = true;
		}
		
		if ( $king_cf_error == false && !is_email( $king_cf_form_email )) {
			$king_cf_message .= '<p>' . __('Please check your email.','king') . '</p>';
			$king_cf_error = true;
		}
		
		if ( $king_cf_error === false ) {
			
			$site_name = is_multisite() ? $current_site->site_name : get_bloginfo('name');
			if (wp_mail($email, $site_name, esc_html($king_cf_form_message),'From: "'. esc_html($king_cf_form_name) .'" <' . esc_html($king_cf_form_email) . '>')) {
				$king_cf_message = '<p>' . __('Email sent. Thank you for contacting us','king') . '</p>';
			} else {
				$king_cf_message = '<p>' .__('Server error. Pease try again later.','king') . '</p>';
				$king_cf_error = true;
			}
		}
	}
	
	return;
}
add_action ( 'init', 'king_process_footer_contact_form' );

/*
 * Replacement for get_adjacent_post() This supports only the custom post types you identify and does not look at categories anymore. This allows you to go from one custom post type to another which was not possible with the default get_adjacent_post(). Orig: wp-includes/link-template.php @param string $direction: Can be either 'prev' or 'next' @param multi $post_types: Can be a string or an array of strings
 */
function king_mod_get_adjacent_post($direction = 'prev', $post_types = 'post') {
    global $post, $wpdb;

    if (empty ( $post ))
        return NULL;
    if (! $post_types)
        return NULL;

    if (is_array ( $post_types )) {
        $txt = '';
        for($i = 0; $i <= count ( $post_types ) - 1; $i ++) {
            $txt .= "'" . $post_types [$i] . "'";
            if ($i != count ( $post_types ) - 1)
                $txt .= ', ';
        }
        $post_types = $txt;
    }

    $current_post_date = $post->post_date;

    $join = '';
    $in_same_cat = FALSE;
    $excluded_categories = '';
    $adjacent = $direction == 'prev' ? 'previous' : 'next';
    $op = $direction == 'prev' ? '<' : '>';
    $order = $direction == 'prev' ? 'DESC' : 'ASC';

    $join = apply_filters ( "get_{$adjacent}_post_join", $join, $in_same_cat, $excluded_categories );
    $where = apply_filters ( "get_{$adjacent}_post_where", $wpdb->prepare ( "WHERE p.post_date $op %s AND p.post_type IN({$post_types}) AND p.post_status = 'publish'", $current_post_date ), $in_same_cat, $excluded_categories );
    $sort = apply_filters ( "get_{$adjacent}_post_sort", "ORDER BY p.post_date $order LIMIT 1" );

    $query = "SELECT p.* FROM $wpdb->posts AS p $join $where $sort";
    $query_key = 'adjacent_post_' . md5 ( $query );
    $result = wp_cache_get ( $query_key, 'counts' );
    if (false !== $result)
        return $result;

    $result = $wpdb->get_row ( "SELECT p.* FROM $wpdb->posts AS p $join $where $sort" );
    if (null === $result)
        $result = '';

    wp_cache_set ( $query_key, $result, 'counts' );
    return $result;
}

/**
 * Generate Select values for transparency
 *
 * @return array
 */
function king_transparentArray() {
    for($i = 0; $i < 101; $i ++) {
        $d [] = $i . '%';
    }
    return $d;
}
/* Woocommerce */


/**
 * Remove Woocommerce Default styles
 * @param $enqueue_styles
 * @return mixed
 */
function king_dequeue_styles( $enqueue_styles ) {
    unset( $enqueue_styles['woocommerce-general'] ); // Remove the gloss
    unset( $enqueue_styles['woocommerce-layout'] ); // Remove the layout
    unset( $enqueue_styles['woocommerce-smallscreen'] ); // Remove the smallscreen optimisation
    return $enqueue_styles;
}
add_filter( 'woocommerce_enqueue_styles', 'king_dequeue_styles' );

//remove_filter('woocommerce_before_single_product_summary','woocommerce_show_product_sale_flash',10);

remove_filter('woocommerce_single_product_summary','woocommerce_template_single_sharing',50);
function king_woocommerce_share_btns(){
    echo  '<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style ">
<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
<a class="addthis_button_tweet"></a>
<a class="addthis_button_pinterest_pinit" pi:pinit:layout="horizontal"></a>
<a class="addthis_counter addthis_pill_style"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4fa0dd020995c117"></script>
<!-- AddThis Button END -->';
}
add_action('woocommerce_share','king_woocommerce_share_btns',5);

/**
 * Define image sizes on theme activation
 */
function king_woocommerce_image_dimensions() {
  	$catalog = array(
		'width' 	=> '263',	// px
		'height'	=> '341',	// px
		'crop'		=> 1 		// true
	);
 
	$single = array(
		'width' 	=> '370',	// px
		'height'	=> '478',	// px
		'crop'		=> 1 		// true
	);
 
	$thumbnail = array(
		'width' 	=> '166',	// px
		'height'	=> '214',	// px
		'crop'		=> 1 		// true
	);
 
	// Image sizes
	update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
	update_option( 'shop_single_image_size', $single ); 		// Single product image
	update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
}

add_action ( 'after_switch_theme', 'king_woocommerce_image_dimensions' );


function king_woocommerce_get_price_html($price) {
	
	return str_replace( '<ins>', '<ins class="price blue">'.__('Now:', 'king').' ', $price );
}

add_filter('woocommerce_get_price_html', 'king_woocommerce_get_price_html');

//Remove WooCoomerce default hooks
remove_action('woocommerce_single_product_summary','woocommerce_template_single_rating',10);
remove_action('woocommerce_single_product_summary','woocommerce_template_single_sharing',50);
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);


/**
 * Slider option for woocommerce shop category
 */

add_action('product_cat_edit_form_fields','king_product_cat_edit_form_fields');
add_action('product_cat_add_form_fields','king_product_cat_edit_form_fields');
add_action('edited_product_cat', 'king_product_cat_save_form_fields', 10, 2);
add_action('created_product_cat', 'king_product_cat_save_form_fields', 10, 2);

function king_product_cat_save_form_fields($term_id) {
    $meta_name = 'slider';
    if ( isset( $_POST[$meta_name] ) ) {
        $meta_value = $_POST[$meta_name];
        // This is an associative array with keys and values:
        // $term_metas = Array($meta_name => $meta_value, ...)
        $term_metas = get_option("taxonomy_{$term_id}_metas");
        if (!is_array($term_metas)) {
            $term_metas = Array();
        }
        // Save the meta value
        $term_metas[$meta_name] = $meta_value;
        update_option( "taxonomy_{$term_id}_metas", $term_metas );
    }
}

function king_product_cat_edit_form_fields ($term_obj) {
    // Read in the order from the options db
    $term_id = $term_obj->term_id;
    $term_metas = get_option("taxonomy_{$term_id}_metas");
    if ( isset($term_metas['slider']) ) {
        $slider = $term_metas['slider'];
    } else {
        $slider = '0';
    }
	
	$options = king_get_slider_items_for_theme_options();
?>
    <tr class="form-field">
		<th valign="top" scope="row">
			<label for="slider"><?php _e('Slider', ''); ?></label>
		</th>
		<td>
			<?php
			if (is_array($options) && count($options) > 0) { ?>
				<select name="slider" id="slider">
					<?php foreach ($options as $option) { ?>
						<option value="<?php echo esc_attr($option['value']); ?>" <?php echo ($option['value'] == $slider ? 'selected' : ''); ?>><?php echo esc_html($option['label']); ?></option>
					<?php } ?>
				</select>
			<?php } ?>
		</td>
	</tr>
<?php 
}

/**
 * Empty the cart
 * @global object $woocommerce
 */
function king_woocommerce_clear_cart_url() {
  global $woocommerce;
	
	if (is_object($woocommerce) && isset( $_GET['empty-cart'] ) ) {
		$woocommerce->cart->empty_cart();
		wp_redirect( get_permalink( woocommerce_get_page_id( 'shop' ) ) );
	}
}
add_action( 'init', 'king_woocommerce_clear_cart_url');

//
// Ajax WooCommerce - Add Card - Add Contents
// ------------------------------------------------------------------------------
if ( ! function_exists( 'king_woocommerce_top_bar_ajax') ) {
  function king_woocommerce_top_bar_ajax( $fragments ) {
    global $woocommerce;

    ob_start();
    woocommerce_mini_cart();
    $mini_cart = ob_get_clean();

    $fragments['.shopping-cart-dropdown'] = '<div class="shopping-cart-dropdown">'.$mini_cart.'</div>';
    $fragments['.cart-count']             = '<span class="cart-count">'. sprintf('%d items(s)', $woocommerce->cart->cart_contents_count).'</span>';
    $fragments['.cart-total']             = '<span class="cart-total">'.$woocommerce->cart->get_cart_total().'</span>';  
    //$fragments['.dropdown-item-count']  = '<div class="dropdown-item-count"><h4>'. sprintf(_n('%d item <span>in cart</span>', '%d items <span>in cart</span>', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count).'</h4></div>';

    return $fragments;
  }
  add_filter('add_to_cart_fragments', 'king_woocommerce_top_bar_ajax');
}



/**
 * Force Visual Composer to initialize as "built into the theme". This will hide certain tabs under the Settings->Visual Composer page
 */
if(function_exists('vc_set_as_theme')) {
	vc_set_as_theme();
}

$settings = array(
	'menu-categories' => array('order', 'image')
);
$tf = new king_extra_taxonomy_fields($settings);

/**
 * Get post_id for the current language
 * @param type $post_id id from default language
 * @param type $type
 * @return type
 */
function king_get_current_language_post_id($post_id, $type = 'page') {
	if (!function_exists('icl_get_languages') || empty($post_id) || !defined('ICL_LANGUAGE_CODE')) {
		return $post_id;
	}
	
	$lang_post_id = icl_object_id($post_id, $type, true, ICL_LANGUAGE_CODE);
	if (!empty($lang_post_id)) {
		return $lang_post_id;
	}
	
	return $post_id;
}

/**
 * User profile contact methods
 * @param type $contactmethods
 * @return type
 */
function king_new_contactmethods( $contactmethods ) {
	
	$contactmethods['twitter'] = 'Twitter';
	$contactmethods['facebook'] = 'Facebook';
	$contactmethods['pinterest'] = 'Pinterest';
	$contactmethods['google-plus'] = 'Google+';
	$contactmethods['position'] = __('Position', 'king');

	return $contactmethods;
}
add_filter('user_contactmethods','king_new_contactmethods',10,1);

/**
 * Added svg support on media uploader
 * @param type $mimes
 * @return type
 */
function king_mime_types( $mimes ) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'king_mime_types');

/**
 * Add footer page custom vc css
 * @return type
 */
function king_add_footer_page_custom_vc_css() {
	
	if (king_get_opt('show_selected_page_content_above_footer') != 'yes') {
		return;
	}
	
	$footer_page = king_get_opt('footer_page');
	
	if (!intval($footer_page)) {
		return;
	}
	
	
	$shortcodes_custom_css = get_post_meta( $footer_page, '_wpb_shortcodes_custom_css', true );
	if ( ! empty( $shortcodes_custom_css ) ) {
		echo '<style type="text/css" data-type="vc_shortcodes-custom-css">';
		echo wp_kses_post($shortcodes_custom_css);
		echo '</style>';
	}
}

add_action('wp_head','king_add_footer_page_custom_vc_css');

/**
 * Force 404 error on page
 * @global type $post
 * @global type $wp_query
 */
function king_force_404_for_footer_page() {
  global $post;
  if ( is_singular( ) && $post->ID == king_get_opt('footer_page') ) {
    global $wp_query;
    $wp_query->set_404();
    status_header(404);
  }
}
add_action( 'wp', 'king_force_404_for_footer_page' );

/**
* @return null
* @param none
* Add inline styles
**/
if( !function_exists('king_inline_styles')) {

  function king_enqueue_inline_styles() {

    global $king_inline_styles;

    if ( ! empty( $king_inline_styles ) ) {
      echo '<style id="custom-shortcode-css" type="text/css">'. king_css_compress( join( '', $king_inline_styles ) ) .'</style>';
    }
  }
}
add_action( 'wp_footer', 'king_enqueue_inline_styles' );

if ( version_compare( $GLOBALS['wp_version'], '4.1', '<' ) ) :
	/**
	 * Filters wp_title to print a neat <title> tag based on what is being viewed.
	 *
	 * @param string $title Default title text for current view.
	 * @param string $sep Optional separator.
	 * @return string The filtered title.
	 */
	function king_wp_title( $title, $sep ) {
		if ( is_feed() ) {
			return $title;
		}

		global $page, $paged;

		// Add the blog name
		$title .= get_bloginfo( 'name', 'display' );

		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title .= " $sep $site_description";
		}

		// Add a page number if necessary:
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title .= " $sep " . sprintf( __( 'Page %s', 'king' ), max( $paged, $page ) );
		}

		return $title;
	}
	add_filter( 'wp_title', 'king_wp_title', 10, 2 );

	/**
	 * Title shim for sites older than WordPress 4.1.
	 *
	 * @link https://make.wordpress.org/core/2014/10/29/title-tags-in-4-1/
	 * @todo Remove this function when WordPress 4.3 is released.
	 */
	function king_render_title() {
		?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php
	}
	add_action( 'wp_head', 'king_render_title' );
endif;

add_action('init', 'reg_post_personalizado');


   function reg_post_personalizado() {

   //        registro de post personalizado catalogo obras
   register_post_type('catalogo_obras', array(
      'labels' => array('name' => 'catalogo_obras',
                        'singular_name' => 'catalogo_obras'
                       ),
      'description' => 'catalogo de la obras',
      'public' => true,
      'menu_position' => 5,
      'supports' => array('title')
   ));
   
   
   register_post_type('programacion', array(
      'labels' => array('name' => 'programacion',
                        'singular_name' => 'programacion'
                       ),
      'description' => 'programacion',
      'public' => true,
       'show_ui'       => true,
      'menu_position' => 5,
      'supports' => array('title')
   ));

  
}

function  reg_javaScript(){
    
    if(!is_admin()){
        
          wp_deregister_script('jquery');
          wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js', false, '2.0.3', true);
          wp_enqueue_script('jquery');
        
          wp_register_script('inicio_antidoto', get_template_directory_uri().'/js_antidoto/inicio_antidoto.js', array('jquery'), true);

          wp_enqueue_script('inicio_antidoto');
        
    }
}

add_action("init", "reg_javaScript");

 //funcion para dar el mes en castellano
//cuando se ingresa el número del mes proveniente de la función
//getdate
//@return mes en castellano
//@param integer  $mes en formato numérico
//        

    function mes_castellano($num_mes){

            $mes = array('enero','febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
            $inx = $num_mes-1;
            return $mes[$inx];
    }
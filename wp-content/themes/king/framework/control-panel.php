<?php
/**
 * Control panel
 *
 * Shows the Control Panel on your homepage if enabled.
 *
 * @package framework
 * @since framework 1.0
 */

if (king_check_if_use_control_panel_cookies() && isset($_COOKIE['theme_body_class']))
{
	$body_class = $_COOKIE['theme_body_class'];
}
elseif (isset($_GET['switch_layout']) && !empty($_GET['switch_layout']))
{
	$body_class = $_GET['switch_layout'];
}
else
{
	$body_class = ot_get_option('body_class');
}
$main_menu_style = king_get_main_menu_style();
$background_patterns = king_get_background_patterns(true);

?>
<div id="control-panel">
	<div class="panel-content">
		<div class="btn-wrapper">
	 		<a href="#" class="btn btn-bordered btn-black"><?php _e('Buy Theme!', 'framework'); ?></a>
		</div>
		<h3><?php _e('Layout','framework'); ?></h3>
		<div class="layout-buttons clearfix">
			<a class="<?php echo ($body_class == 'w1170' ? 'active' : ''); ?>" href="<?php echo add_query_arg( array('switch_layout' => 'w1170') ); ?>">Wide</a>
			<?php if(get_the_ID() != 6145 ): ?>
				<a class="<?php echo ($body_class == 'boxed-layout2' ? 'active' : ''); ?>" href="<?php echo add_query_arg( array('switch_layout' => 'boxed-layout2') ); ?>">Framed</a>
				<a class="<?php echo ($body_class == 'b1170' ? 'active' : ''); ?>" href="<?php echo add_query_arg( array('switch_layout' => 'b1170') ); ?>">Boxed</a>
			<?php endif;
			?>
		</div>

		<?php $colors = king_get_control_panel_colors(); ?>
		<?php if (is_array($colors) && count($colors) > 0): ?>
			<div id="panel-main-color-container">
				<h3><?php _e('Choose main color','framework'); ?></h3>
				<ul id="panel-main-color">
					<?php foreach ($colors as $color): ?>
						<li><div style="background-color: <?php echo esc_attr($color); ?>"></div></li>
					<?php endforeach; ?>
				</ul>
				<div class="clear"></div>
			</div>
		<?php endif; ?>

		<?php $background_images = king_get_control_panel_backgrounds(); ?>
		<?php if (is_array($background_images) && count($background_images) > 0): ?>
			<div id="background-image-container">
				<h3><?php _e('Boxed Backgrounds','framework'); ?></h3>
				<?php if (!in_array($body_class,array('b1170','boxed-layout2'))): ?>
					<?php _e('Boxed layout only','framework'); ?>
				<?php else: ?>
					<ul id="background-image">
						<?php foreach ($background_images as $background): ?>
							<li><div style="background-image: url(<?php echo esc_url(get_template_directory_uri().'/img/body-img/thumbs/'.$background); ?>);" data-img="<?php echo esc_attr($background); ?>"></div></li>
						<?php endforeach; ?>
					</ul>
					<div class="clear"></div>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<div id="reset-to-defaults">
			<a href="<?php echo add_query_arg( 'control_panel=defaults', '', home_url( $wp->request )); ?>"><?php _e('Reset to defaults','framework');?></a>
		</div>
		<div class="clear"></div>
		<?php $demo_images = king_get_control_panel_demo_backgrounds(); ?>
		<?php if (is_array($demo_images) && count($demo_images) > 0): ?>
			<div id="demo-content-layout">
				<h3><?php _e('Demo Layouts','framework'); ?></h3>
				<ul id="demo-layouts">
				<?php 
					for($row = 0; $row < count($demo_images); $row++): ?>
					<li>
						<div class="thumbnail-big">
							<img src="<?php echo esc_url(get_template_directory_uri().'/img/demo-img/'.$demo_images[$row][1]); ?>" alt="" />
						</div>
						<a href="<?php echo esc_url($demo_images[$row][2]); ?>"><img src="<?php echo esc_url(get_template_directory_uri().'/img/demo-img/'.$demo_images[$row][0]); ?>" alt="thumb" /></a>
					</li>
				<?php endfor; ?>
				</ul>
			</div>
		<?php endif; ?>
	</div>
	<div class="panel-switcher"></div>
</div>
<script>
	jQuery(document).ready(function($) {

		var changeRight = 280;

		$('#control-panel .panel-switcher').click(function() {
			$('#control-panel').animate({
				right: '+=' + changeRight
				}, 500, function() {
					changeRight = changeRight * -1;
				}
			);
		});


		$('#panel-main-color li div').click(function() {

			var current_color_container = this;
			$(current_color_container).html('<i class="icons icon-spinner fa-pulse"></i>');
			$color = $(this).css('background-color');
			
			jQuery.ajax({
				type: 'GET',
				url: '<?php echo admin_url('admin-ajax.php'); ?>',
				data: {
					action: 'the_theme_dynamic_styles',
					main_color: rgb2hex($color)
				},
				success: function(data, textStatus, XMLHttpRequest){
					
					$('.sc-call-to-action').each( function() {
						$(this).attr('data-color', $color);
						$(this).css('background-color', $color);
					});

					$('.circular-progressbar').each(function() {
						$(this).attr('data-fgcolor', $color);
					});

					$('.service').each(function() {
						var svgContext = $(this).find('object')[0].contentDocument,
	                        strokeColor = svgContext.createElementNS('http://www.w3.org/2000/svg', 'style');
	                    strokeColor.textContent = 'svg, svg path { stroke: ' + $color + '; }';
	                    svgContext.getElementById("Layer_1").appendChild(strokeColor);
					});

					$('.services-list.style1 h3').css('color', $color);
		
					$(current_color_container).html('');
					jQuery("#dynamic-styles").html('');
					jQuery("#dynamic-styles").html(data);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown){
					//alert(errorThrown);
				}
			});
		});

		$('#background-pattern li div').click(function() {

			background_image = '<?php echo get_template_directory_uri(); ?>/img/body-bg/' + $(this).attr('data-bg');
			$('body').css({background : 'url('+background_image+')'});
			cookie_val = 'body-bg/' + $(this).attr('data-bg');


			var cookieStr = escape('theme_background') +"=";
			if (typeof cookie_val != "undefined") {
				cookieStr += escape(cookie_val);
			}

			document.cookie = cookieStr;
		});

		$('#background-image li div').click(function() {

			background_image = '<?php echo get_template_directory_uri(); ?>/img/body-img/' + $(this).attr('data-img');
			$('body').css({
				background : 'url('+background_image+') no-repeat fixed center'
			});
			cookie_val = 'body-img/' + $(this).attr('data-img');

			var cookieStr = escape('theme_background') +"=";
			if (typeof cookie_val != "undefined") {
				cookieStr += escape(cookie_val);
			}

			document.cookie = cookieStr;
		});

		//set default background when boxed is choosed
		$('#choice-b1170').click(function() {

			cookie_val = 'body-img/img8.jpg';
			document.cookie = escape('theme_background') + "=" + escape(cookie_val);
		});

		//clear background when boxed is choosed
		$('#choice-w1170').click(function() {

			cookie_val = '';
			document.cookie = escape('theme_background') + "=" + escape(cookie_val);
		});
		
		$('#control-panel button').click(function(){
		   window.location.href=$(this).find('option:selected').val();
		});

	});

	var hexDigits = new Array
        ("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");

	//Function to convert hex format to a rgb color
	function rgb2hex(rgb) {
		rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
		return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
	}

	function hex(x) {
		return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
	}

 </script>
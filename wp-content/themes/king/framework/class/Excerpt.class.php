<?php
/**
 * Exerpt length
 * 
 * @package framework
 * @since framework 1.0
 */

class king_Excerpt {

  // Default length (by WordPress)
  public static $length = 55;

  public static $types = array(
      'tiny' => TINY_EXCERPT,
      'short' => SHORT_EXCERPT,
      'regular' => REGULAR_EXCERPT,
      'long' => LONG_EXCERPT
    );

  /**
   * Sets the length for the excerpt,
   *
   * @param string $new_length 
   * @return void
   */
  public static function length($new_length = 55) {
    king_Excerpt::$length = $new_length;

    add_filter('excerpt_length', 'king_Excerpt::new_length');

    king_Excerpt::output();
  }
  
  /**
   * Sets the length for the excerpt 
   *
   * @param string $new_length 
   * @return void
   */
  public static function get_by_length($new_length = 55) {
    king_Excerpt::$length = $new_length;

    add_filter('excerpt_length', 'king_Excerpt::new_length');

    return king_Excerpt::get();
  }

  // Tells WP the new length
  public static function new_length() {
    if( isset(king_Excerpt::$types[king_Excerpt::$length]) )
      return king_Excerpt::$types[king_Excerpt::$length];
    else
      return king_Excerpt::$length;
  }

  // Echoes out the excerpt
  public static function output() {
    the_excerpt();
  }
  
  // Echoes out the excerpt
  public static function get() {
    return get_the_excerpt();
  }
}
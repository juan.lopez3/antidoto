<?php
/**
 * Framework functions, improvements and additions to OptionTree
 *
 * @package framework
 * @since framework 1.0
 */

add_filter('widget_text', 'do_shortcode');

if (is_admin())
{
	function king_admin_head() {
		?>
		<script type="text/javascript">
			var popupurl = '<?php echo get_template_directory_uri() . '/framework/popup.php'; ?>';
			var themeurl = '<?php echo get_template_directory_uri(); ?>';
		</script>
	<?php
	}

	add_action('admin_head', 'king_admin_head');

	/**
	* Initalize theme options scripts
	*/
	function king_load_custom_wp_admin_style() {
		wp_enqueue_style('king-custom_wp_admin_css', get_template_directory_uri() . '/framework/css/admin.css', false);
		wp_enqueue_style('king-jquery_select2_css', get_template_directory_uri() . '/framework/css/select2.css', false);
		wp_enqueue_style('king-custom_option_tree_css', get_template_directory_uri() . '/framework/css/option-tree.css', array('ot-admin-css'));
		wp_enqueue_style('king-fontello', get_template_directory_uri() . '/css/fontello.css', array(), null, 'all' );
		wp_enqueue_style('king-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), null, 'all' );
		
		wp_enqueue_script( 'king-framework', get_template_directory_uri().'/framework/js/framework.js',array('jquery'),null);
		wp_enqueue_script( 'king-jquery_select2', get_template_directory_uri().'/framework/js/select2.min.js',array('jquery'),null);
	}

	add_action('admin_enqueue_scripts', 'king_load_custom_wp_admin_style');

	/**
	 * Register tinymce plugins
	 * @param array $plugin_array
	 * @return array
	 */
	function king__register_tinymce_javascript($plugin_array) {
	   $plugin_array['code'] = get_template_directory_uri().'/framework/js/tinymce/plugins/code/plugin.min.js';
	   return $plugin_array;
	}
	
	add_filter('mce_external_plugins', 'king__register_tinymce_javascript');

	/**
	* Tinymce extensions
	*/
	require_once( get_template_directory().'/framework/tinymce.php' );
}

/**
 * Framework scripts
 */
function king_framework_scripts() {
	if (ot_get_option('control_panel') == 'enabled_admin' && current_user_can('manage_options') || ot_get_option('control_panel') == 'enabled_all') {
		wp_register_style( 'king-control-panel', get_template_directory_uri() . '/framework/css/control-panel.css', array(), null, 'all' );
		wp_enqueue_style( 'king-control-panel' );
	}
}
add_action( 'wp_enqueue_scripts', 'king_framework_scripts' );

/**
 * Custom popup items
 * @return array
 */
function king_get_custom_popup_items() {

	$custom_popup_items = array(
		array(
			'shortcode' => 'custom_icon',
			'name' => __('Icon','framework'),
			'description' => '',
			'usage' => '',
			'code' => '{icon}',
			'fields' => array(
				'icon' => array(
					'type' => 'select',
					'label' => '',
					'values' => king_getFontAwesomeArray(true),
					'default' => '',
					'desc' => '',
					'class' => 'icons-dropdown'
				)
			)
		)
	);

	return $custom_popup_items;
}

/**
 * Get option value, if is_singular option value is taken from post meta
 * @param type $option
 * @param type $skip_local_if
 * @return type
 */
function king_get_opt($option, $skip_local_if = 'default') {
	
	$value = ot_get_option($option);

	if (is_singular()) {
		$local_value = get_post_meta(get_the_ID(),$option, true);
		if (!empty($local_value) && $local_value != $skip_local_if) {
			$value = $local_value;
		}
	}
	
	return $value;
}

/**
 * Get main menu style
 * @return type
 */
function king_get_main_menu_style()
{
	$control_panel = ot_get_option('control_panel');
	if (king_check_if_use_control_panel_cookies() && isset($_COOKIE['theme_main_menu_style']) && !empty($_COOKIE['theme_main_menu_style']) && ($control_panel == 'enabled_admin' && current_user_can('manage_options') || $control_panel == 'enabled_all')) {
		return $_COOKIE['theme_main_menu_style'];
	}
	else if (isset($_GET['switch_main_menu_style']) && !empty($_GET['switch_main_menu_style'])) {
		return $_GET['switch_main_menu_style'];
	}

	//get main menu style for specified page
	$main_menu_style = '';
	
	if (is_page()) {
		$main_menu_style = get_post_meta(get_the_ID(), 'main_menu_style', true);
	}

	if (!empty($main_menu_style) && $main_menu_style != 'default') {
		return $main_menu_style;
	} else {
		
		if (function_exists('is_woocommerce') && is_woocommerce()) {
			return ot_get_option('shop_menu_style');
		} else {
			return ot_get_option('main_menu_style');
		} 
	}
}

/**
 * Get main footer style
 * @return type
 */
function king_get_main_footer_style()
{
	//get main menu style for specified page
	$main_footer_style = '';
	
	if (is_page()) {
		$main_footer_style = get_post_meta(get_the_ID(), 'footer_style', true);
	}

	if (!empty($main_footer_style) && $main_footer_style != 'default') {
		return $main_footer_style;
	} else {
		
		if (function_exists('is_woocommerce') && is_woocommerce()) {
			return ot_get_option('shop_footer_style');
		} else {
			return ot_get_option('footer_style');
		} 
	}
}

/**
 * Get main menu style
 * @return type
 */
function king_get_main_menu_color()
{
	if (!is_page() && function_exists('is_woocommerce') && is_woocommerce()) {
		return king_get_opt('shop_menu_color');
	} else {
		return king_get_opt('main_menu_color');
	} 
}

/**
 * Switch styles when
 *
 * @since framework 1.0
 */
function king_switch_styles()
{
	if (isset($_GET['control_panel']) && $_GET['control_panel'] == 'defaults')
	{
		setcookie('theme_body_class', '');
		setcookie('theme_main_menu_style', '');
		setcookie('theme_background', '');
		setcookie('theme_main_color','',null,'/');
		unset($_COOKIE['theme_body_class']);
		unset($_COOKIE['theme_main_menu_style']);
		unset($_COOKIE['theme_background']);
		unset($_COOKIE['theme_main_color']);
	}
	if (isset($_GET['switch_layout']) && in_array($_GET['switch_layout'],array('w1170','b1170','w960','b960','boxed-layout2')))
	{
		if (king_check_if_control_panel())
		{
			setcookie('theme_body_class', $_GET['switch_layout']);
			$_COOKIE['theme_body_class'] = $_GET['switch_layout'];
		}
	}

	if (isset($_GET['switch_main_menu_style']) && !empty($_GET['switch_main_menu_style']))
	{
		if (king_check_if_control_panel())
		{
			setcookie('theme_main_menu_style', $_GET['switch_main_menu_style']);
			$_COOKIE['theme_main_menu_style'] = $_GET['switch_main_menu_style'];
		}
	}

}
add_action( 'init', 'king_switch_styles' );

/**
 * Define current id (from get) for further use
 *
 * @since framework 1.0
 */
function king_define_current_id()
{
	define('CURRENT_ID',is_singular() ? get_the_ID() : null);
}
add_action( 'wp_head', 'king_define_current_id' );

/**
 * Get current id (set in wp_head)
 *
 * @since framework 1.0
 */
function king_get_current_id()
{
	return CURRENT_ID;
}

/**
 * Get position of the sidebar defined for single post or page
 *
 * @return string sidebar position, possible values: left, right, both, no
 *
 * @since framework 1.0
 */
function king_get_single_post_sidebar_position()
{
	return get_post_meta(king_get_current_id(), 'sidebar_position_single',true);
}

/**
 * Get one of the registered sidebar, depends on Theme options or single post settings
 * Used when sidebar is defined in OptionTree and attached to single post or page using metaboxes
 * Function gets left_sidebar or right_sidebar (these are names of metaboxes not sidebars ids!)
 *
 * @param string sidebar position left or right
 * @param string default default sidebar is not set
 * @return string/bool return string sidebar id or false if sidebar doesn't exists
 *
 * @since framework 1.0
 */
function king_get_single_post_sidebar_id($position, $default = 'main')
{
	$sidebar = get_post_meta(king_get_current_id(), $position,true);
	if (empty($sidebar))
	{
		$sidebar = $default;
	}
	return $sidebar;
}

/**
 * Show sigle post sidebar, depends on position set on the post/page
 *
 * @since framework 1.0
 */
function king_get_single_post_sidebar($position)
{
	if (!empty($position))
	{
		$single_post_sidebar_position = king_get_single_post_sidebar_position();

		switch ($single_post_sidebar_position)
		{
			case 'left':
				if ( in_array( $position,array( 'left' ) ) )
				{
					get_sidebar($position);
				}
				break;

			case 'left2':
				if ( in_array( $position,array( 'left', 'left2' ) ) )
				{
					get_sidebar($position);
				}
				break;

			case 'right2':
				if ( in_array( $position,array( 'right', 'right2' ) ) )
				{
					get_sidebar($position);
				}
				break;

			case 'both':
				if ( in_array( $position,array( 'left','right' ) ) )
				{
					get_sidebar($position);
				}
				break;

			case 'no':
				break;

			case 'right':
			default:
				if ( in_array( $position,array( 'right' ) ) )
				{
					get_sidebar($position);
				}
				break;
		}
	}

	return '';
}

/**
 * Check if two sidebars are visible
 *
 * @since framework 1.0
 */
function king_check_if_two_sidebars( )
{
	$single_post_sidebar_position = king_get_single_post_sidebar_position();
	if (in_array($single_post_sidebar_position,array('left2','right2','both')))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/**
 * Check if any sidebar is on
 *
 * @since framework 1.0
 */
function king_check_if_sidebar( )
{
	$single_post_sidebar_position = king_get_single_post_sidebar_position();
	if ($single_post_sidebar_position == 'no')
	{
		return false;
	}
	else
	{
		return true;
	}
}

/**
 * Check if any sidebar is on
 *
 * @since framework 1.0
 */
function king_check_if_any_sidebar($ifNoSidebars,$ifOneSidebar, $ifTwoSidebars )
{
	if (king_check_if_two_sidebars( ))
	{
		return $ifTwoSidebars;
	}
	if (king_check_if_sidebar())
	{
		return $ifOneSidebar;
	}
	return $ifNoSidebars;
}

/**
 * Replace excerpt "more"
 *
 * @since framework 1.0
 */
function king_new_excerpt_more( $excerpt )
{
	return str_replace( '[...]', '...', $excerpt );
}
add_filter( 'wp_trim_excerpt', 'king_new_excerpt_more' );

/**
 * Echo excerpt
 *
 * @param string/int integer for custom lenngth for tiny,short/regular/long for defined excerpt
 * @since framework 1.0
 */
require_once 'class/Excerpt.class.php';
function king_the_excerpt_theme($length = 55)
{
	king_Excerpt::length($length);
}

/**
 * get excerpt
 *
 * @param string/int integer for custom lenngth for tiny,short/regular/long for defined excerpt
 * @since framework 1.0
 */
function king_get_the_excerpt_theme($length = 55)
{
	return king_Excerpt::get_by_length($length);
}

/**
 * Get shortened string to words limit
 *
 * @param $text string
 * @param $word_limit
 * @return string cut to x words
 * @since framework 1.0
 */
function king_get_shortened_string($text,$word_limit)
{
	$words = explode(' ', $text, ($word_limit + 1));
	if(count($words) > $word_limit)
	{
		array_pop($words);
		return implode(' ', $words)."...";
	}
	else
	{
		return implode(' ', $words);
	}
}

/**
 * Get shortened string to words limit
 *
 * @param $text string
 * @param $word_limit
 * @return string cut to x words
 * @since framework 1.0
 */
function king_get_shortened_string_by_letters($text,$letters_limit, $at_space = true, $add = '...')
{
	if(strlen($text) > $letters_limit)
	{
		if ($at_space) {
			$pos = strpos($text, ' ', $letters_limit);

			if (!$pos) {
				return $text;
			}
			return substr($text, 0, $pos).$add;
		}
		return substr($text, 0, $letters_limit).$add;
	}
	else
	{
		return $text;
	}
}

/**
 * Get class for specified sidebar
 *
 * @param string $url
 * @param bool $return_url 
 * @return string
 * @since framework 1.0
 */
function king_get_embaded_video($url, $return_url = false) {

	if (strstr($url,'vimeo'))
	{
		if (preg_match('/(\d+)/', $url, $matches))
		{
			if ($return_url) {
				return 'http://player.vimeo.com/video/'.$matches[0].'?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff';
			}
			return '<iframe src="http://player.vimeo.com/video/'.$matches[0].'?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff"></iframe>';
		}
	}
	else if (strstr($url,'youtu'))
	{
		$pattern = "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#";
		if (preg_match($pattern, $url, $matches))
		{
			if ($return_url) {
				return 'http://www.youtube.com/embed/'.$matches[0];
			}
			return '<iframe src="http://www.youtube.com/embed/'.$matches[0].'"></iframe>';
		}
	}
	return '';
}

/**
 * Get image tag
 * @param string $image_src
 * @param string $class
 * @param string $alt
 * @param string $size
 * @return string
 */
function king_get_image($image_src, $class = '', $alt = '', $size = 'full') {

	if (empty($image_src)) {
		return;
	}
	$attachment_id = king_get_attachment_id_from_src($image_src);

	$attr = array(
		'class'	=> $class,
		'alt'   => $alt,
	);

	if ($attachment_id) {
		return wp_get_attachment_image( $attachment_id, $size, false, $attr);
	} else {
		return '<img src="'.$image_src.'" class="'.$attr['class'].'" alt="'.$alt.'" />';
	}
}

/**
 * Get attachment id from src
 * @global object $wpdb
 * @param string $image_src
 * @return int
 */
function king_get_attachment_id_from_src ($image_src) {

	global $wpdb;
	$query = $wpdb -> prepare("SELECT ID FROM {$wpdb->posts} WHERE guid= %s ",$image_src);
	$id = $wpdb->get_var($query);
	return $id;
}

/**
 * Get prev slider text
 * @return string
 */
function king_get_prev_slider_text()
{
	if (defined('SLIDER_PREV_TEXT')) {
		return SLIDER_PREV_TEXT;
	}
	return __('Previous', 'framework');
}

/**
 * Get next slider text
 * @return string
 */
function king_get_next_slider_text()
{
	if (defined('SLIDER_NEXT_TEXT')) {
		return SLIDER_NEXT_TEXT;
	}
	return __('Next', 'framework');
}

/**
 * Save like for a post ajax request
 */
function king_save_post_like_func()
{
	if (!isset($_GET['post_id']) || empty($_GET['post_id']))
	{
		die(); //die on wrong get parameter
	}
	$post_id = $_GET['post_id'];
	$likes = get_post_meta($post_id,'theme_like',true);

	//don't add new like if cookie exists
	if (isset($_COOKIE['saved_post_like_'.$post_id]) && $_COOKIE['saved_post_like_'.$post_id] == 1)
	{
		echo intval($likes);
		die();
	}
	$likes++;
	setcookie('saved_post_like_'.$post_id,1,time()+60*60*24*30,'/');
	update_post_meta($post_id, 'theme_like',$likes);
	echo intval($likes);
	die();
}
add_action( 'wp_ajax_nopriv_save_post_like', 'king_save_post_like_func' );
add_action( 'wp_ajax_save_post_like', 'king_save_post_like_func' );

/**
 * Get post likes
 * @param type $post_id
 * @return type
 */
function king_get_theme_likes($post_id = 0)
{
	global $post;

	if (empty($post_id))
	{
		$post_id = $post -> ID;
	}
	return intval(get_post_meta($post_id,'theme_like',true));
}

/**
 * Check if control panel is active
 * @return boolean
 */
function king_check_if_control_panel()
{
	$control_panel = ot_get_option('control_panel');

	if ($control_panel == 'enabled_admin' && current_user_can('manage_options') || $control_panel == 'enabled_all')
	{
		return true;
	}
	return false;
}

/**
 * Check if control panel is active
 * @return boolean
 */
function king_check_if_use_control_panel_cookies()
{
	return false;
}

/**
 * Change color
 * @param type $color
 * @param type $percentage_change
 * @return type
 */
function king_change_color($color, $percentage_change, $return_rgba = false, $transparency = '')
{
    $color = substr( $color, 1 );
    $rgb = '';
    $percentage_change = $percentage_change/100*255;

	//make it darker
    if  ($percentage_change < 0 )
    {

        $per =  abs($percentage_change);
        for ($x=0;$x<3;$x++)
        {
            $c = hexdec(substr($color,(2*$x),2)) - $percentage_change;
            $c = ($c < 0) ? 0 : dechex($c);
            $rgb .= (strlen($c) < 2) ? '0'.$c : $c;
        }
    }
	//make it ligher
    else
    {
        for ($x=0;$x<3;$x++)
        {
            $c = hexdec(substr($color,(2*$x),2)) + $percentage_change;
            $c = ($c > 255) ? 'ff' : dechex($c);
			$rgb .= (strlen($c) < 2) ? '0'.$c : $c;
        }
    }
	if ($return_rgba)
	{
		return king_hex_to_rgb($rgb,$transparency);
	}
	else
	{
		return '#'.$rgb;
	}
}

/**
 * Convert color to rgb
 * @param type $color
 * @param type $percentage_change
 * @return type
 */
function king_hex_to_rgb($hex,$transparency = '') {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   return 'rgba('.implode(",", $rgb).(!empty($transparency) ? ', '.$transparency : '').')';
}

/**
 * Get a list of available sliders for theme options
 *
 * @package framework
 * @since framework 1.0
 */
function king_get_layer_slider_items_for_theme_options()
{
	global $wpdb;

	if($wpdb->get_var("SHOW TABLES LIKE '".$wpdb->prefix . "layerslider'") != $wpdb->prefix . "layerslider") {
		return false;
	}

    // Get sliders
    $sliders = $wpdb->get_results( "
		SELECT
			*
		FROM
			".$wpdb->prefix . "layerslider
        WHERE
			flag_hidden = '0' AND
			flag_deleted = '0'
        ORDER BY
			date_c ASC
		LIMIT
			100");

    // Iterate over the sliders
	$slider_items = array();

    foreach($sliders as $key => $item) {

		$slider_items[] = array(
			'value'       => 'LayerSlider-'.$item -> id,
			'label'       => 'LayerSlider - '.$item -> name,
			'src'         => ''
		);
	}
	return $slider_items;
}

/**
 * Get font array
 * @param bool $bNoIconChoice
 * @param array $add_icons
 * @param bool $return_theme_options
 * @param bool $sort
 * @return array
 */
function king_getFontAwesomeArray($bNoIconChoice = false,$add_icons = null, $return_theme_options = false, $sort = false)
{
	$icons = array();

	if (is_array($add_icons))
	{
		foreach($add_icons as $key => $icon){
			$icons[$key] = $icon;
		}
	}
	
	$icons = array_merge_recursive($icons, king_get_fontello_icons());

	if ($sort == true) {
		asort($icons);
	}
		
	if (count($icons) > 0)
	{
		$icons_return = array();
		if ($bNoIconChoice === true)
		{
			$icons_return['no'] = __('no icon','framework');
		}
		$icons_return = array_merge($icons_return,$icons);

		if ($return_theme_options) {
			$icons_theme_options = array();
			foreach ($icons_return as $key => $icon) {
				$icons_theme_options[] = array(
					'value'       => $key,
					'label'       => $icon,
					'src'         => ''
				);
			}
			return $icons_theme_options;
		}
		
		return $icons_return;
	}

	return array('empty' => __('empty','framework'));
}

/**
 * Process and send mail from shortcode "form"
 * @global bool $shortcode_form_email_sent
 * @return void
 */
function king_process_form_shortcode() {
	if ($_SERVER['REQUEST_METHOD'] != 'POST' || !isset($_POST['_wpnonce']) || !wp_verify_nonce($_POST['_wpnonce'],'shortcode-form_')) {
		return;
	}
	$data = array();
	foreach ($_POST as $key => $val) {
		if (!strstr($key,'shortcode_form_')) {
			continue;
		}
		$data[] = str_replace('shortcode_form_','',$key).': '.filter_var(stripslashes($val), FILTER_SANITIZE_STRING);
	}

	//send email
	if (count($data) > 0) {
		global $shortcode_form_email_sent;
		wp_mail( get_option( 'admin_email' ), __('Form', 'framework').': '.get_the_title(get_the_ID()), implode($data,"\n"));
		$shortcode_form_email_sent = true;
	}
}
add_action('get_header','king_process_form_shortcode');

/**
 * Get user defined menus
 * @return array
 */
function king_get_user_defined_menus() {
	$menus = get_terms( 'nav_menu', array( 'hide_empty' => false ) );
	
	$menuArray = array();
	foreach ( $menus as $menu ) {
		$menuArray[$menu->term_id] = $menu -> name;
	}
	return $menuArray;
}

/**
 * Get image source by attachement_id
 * @param int $image
 * @param string $full
 * @return string
 */
function king_get_image_by_id($image, $size = 'full') {
	$image_id = intval($image);	
	if (!empty($image_id)) {
		$image = wp_get_attachment_image_src( $image_id, $size);
		if (is_array($image) && isset($image[0])) {
			return $image[0];
		}
	}
	
	return null;
}

/**
 * Turn off comments for pages by default
 * @param type $data
 * @return int
 */
function king_default_comments_off( $data ) {
    if( $data['post_type'] == 'page' && $data['post_status'] == 'auto-draft' ) {
        $data['comment_status'] = 0;
        $data['ping_status'] = 0;
    }

    return $data;
}
add_filter( 'wp_insert_post_data', 'king_default_comments_off' );

/**
 * Get categories sorted by king_taxonomy_order$term_id
 * @param type $terms
 */
function king_get_sorted_terms($terms) {
	
	if (!is_array($terms)) {
		return $terms;
	}
	$terms_order = array();
	$terms_keys = array();
	
	foreach ($terms as $key => $term) {
		$terms_order[$term -> term_id] = intval(get_option('king_taxonomy_order'.$term -> term_id));
		$terms_keys[$term -> term_id] = $key;
	}
	asort($terms_order);
	$new_terms = array();
	foreach ($terms_order as $term_id => $order) {
		$new_terms[] = $terms[$terms_keys[$term_id]];
	}
	
	return $new_terms;
}

/**
 * Adds paragraph html tag if such tag is not found
 * @param string $content
 * @return string
 */
function king_add_paragraph($content) {
	if (preg_match("#<p[^>]*>(.*?)</p>#i", $content, $matches) == 0) {
		$content = '<p>'.$content.'</p>';
	}
	return $content;
}

if ( ! function_exists( "sanitize_html_classes" ) && function_exists( "sanitize_html_class" ) ) {
	/**
	 * Sanitize many classes at once. Useful when we can't use sanitize_html_class on a string with 
	 * many classes separated by spaces.
	 *
	 * @uses   sanitize_html_class
	 * @param  (mixed: string/array) $class   "blue hedgehog goes shopping" or array("blue", "hedgehog", "goes", "shopping")
	 * @param  (mixed) $fallback Anything you want returned in case of a failure
	 * @return (mixed: string / $fallback )
	 */
	function sanitize_html_classes( $class, $fallback = null ) {

		// Explode it, if it's a string
		if ( is_string( $class ) ) {
			$class = explode(" ", $class);
		} 

		if ( is_array( $class ) && count( $class ) > 0 ) {
			$class = array_map("sanitize_html_class", $class);
			return implode(" ", $class);
		}
		else { 
			return sanitize_html_class( $class, $fallback );
		}
	}
}

/**
 * Adds iframe to a list of allowed tags and returns an array with allowed tags
 * @return array
 */
function king_add_iframe_to_allowed_tags() {
	$my_allowed = wp_kses_allowed_html( 'post' );
	// iframe
	$my_allowed['iframe'] = array(
		'align' => true,
		'width' => true,
		'height' => true,
		'frameborder' => true,
		'name' => true,
		'src' => true,
		'id' => true,
		'class' => true,
		'style' => true,
		'scrolling' => true,
		'marginwidth' => true,
		'marginheight' => true,
	);
	
	return $my_allowed;
}

/**
 * Conntect WP_Filesystem
 * @global type $wp_filesystem
 * @param type $url
 * @param type $method
 * @param type $context
 * @param type $fields
 * @return boolean
 */
function king_connect_fs($url, $method, $context, $fields = null) {
	global $wp_filesystem;

	require_once(ABSPATH . 'wp-admin/includes/file.php');
	
	if(false === ($credentials = request_filesystem_credentials($url, $method, false, $context, $fields))) {
		return false;
	}

	//check if credentials are correct or not.
	if(!WP_Filesystem($credentials)) {
		request_filesystem_credentials($url, $method, true, $context);
		return false;
	}

	return true;
}

/**
 * Read file using wp_filesystem
 * @global type $wp_filesystem
 * @param type $file_dir
 * @param type $file_name
 * @param string $nonce_url
 * @return string|\WP_Error
 */
function king_read_file($file_dir, $file_name, $nonce_url = '') {
	global $wp_filesystem;

	if (empty($nonce_url)) {
		$nonce_url = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];;
	}
	
	$url = wp_nonce_url($nonce_url, "king-filesystem-nonce");

	if (king_connect_fs($url, "", $file_dir)) {
		$dir = $wp_filesystem->find_folder($file_dir);
		$file = trailingslashit($dir) . $file_name;

		if ($wp_filesystem->exists($file)) {
			$text = $wp_filesystem->get_contents($file);
			if (!$text) {
				return "";
			} else {
				return $text;
			}
		} else {
			return new WP_Error("filesystem_error", "File doesn't exist");
		}
	} else {
		return new WP_Error("filesystem_error", "Cannot initialize filesystem");
	}
}

/**
 * Prevents from creating different image sizes than default thumbnail, medium, large
 * Images are created on demand
 * @global array $_wp_additional_image_sizes
 * @param type $out
 * @param int $id
 * @param string $size
 * @return boolean|array
 */
add_filter('image_downsize', 'king_media_downsize', 10, 3);
function king_media_downsize($out, $id, $size) {
	// If image size exists let WP serve it like normally
	$imagedata = wp_get_attachment_metadata($id);
	
	if (!is_string($size)) {
		return false;
	}
	
	if (is_array($imagedata) && isset($imagedata['sizes'][$size])) {
		return false;
	}

	// Check that the requested size exists, or abort
	global $_wp_additional_image_sizes;
	if (!isset($_wp_additional_image_sizes[$size])) {
		return false;
	}

	// Make the new thumb
	if (!$resized = image_make_intermediate_size(
		get_attached_file($id),
		$_wp_additional_image_sizes[$size]['width'],
		$_wp_additional_image_sizes[$size]['height'],
		$_wp_additional_image_sizes[$size]['crop']
	))
		return false;

	// Save image meta, or WP can't see that the thumb exists now
	$imagedata['sizes'][$size] = $resized;
	wp_update_attachment_metadata($id, $imagedata);

	// Return the array for displaying the resized image
	$att_url = wp_get_attachment_url($id);
	return array(dirname($att_url) . '/' . $resized['file'], $resized['width'], $resized['height'], true);
}

/**
 * Prevent resize on upload
 * @param array $sizes
 * @return array
 */
function king_media_prevent_resize_on_upload($sizes) {
	// Removing these defaults might cause problems, so we don't
	return array(
		'thumbnail' => $sizes['thumbnail'],
		'medium' => $sizes['medium'],
		'large' => $sizes['large']
	);
}
add_filter('intermediate_image_sizes_advanced', 'king_media_prevent_resize_on_upload');
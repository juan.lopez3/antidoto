<?php
/*
 * Template Name: Blog - Grid Style
 * 
 * @package king
 * @since king 1.0
 */
get_header();

wp_enqueue_script ( 'king-jquery-masonry2' );

//adhere to paging rules
if ( get_query_var('paged') ) {
    $paged = get_query_var('paged');
} elseif ( get_query_var('page') ) { // applies when this page template is used as a static homepage in WP3+
    $paged = get_query_var('page');
} else {
    $paged = 1;
}

$posts_per_page = get_post_meta(get_the_ID(),'number_of_items',true);
if (!$posts_per_page) {
    $posts_per_page = get_option('posts_per_page');
}

$blog_categories = get_post_meta(get_the_ID(), 'blog_categories', true);

global $query_string;
$args = array(
    'numberposts'     => '',
    'posts_per_page' => $posts_per_page,
    'offset'          => 0,
    'category__in' => is_array($blog_categories) ? $blog_categories : '',
    'orderby'         => 'date',
    'order'           => 'DESC',
    'include'         => '',
    'exclude'         => '',
    'meta_key'        => '',
    'meta_value'      => '',
    'post_type'       => 'post',
    'post_mime_type'  => '',
    'post_parent'     => '',
    'paged'				=> $paged,
    'post_status'     => 'publish'
);
query_posts( $args );

$url = king_get_theme_next_page_url();
?>
<div class="king-blog">
	<div class="container">
		<div class="masonry-container">
			<div class="grid-sizer"></div>
			<?php if (have_posts()) : ?>
				<div id="post-items">
					<div class="row">
						<?php /* Start the Loop */ 
						while (have_posts()) : the_post(); ?>
							<div class="masonry-box col-md-4">
								<?php   get_template_part('inc/king-content/content',get_post_format());?>
							</div>
						<?php endwhile;  ?>
					</div>
				</div>
				<?php wp_reset_query(); ?>
			<?php else : //No posts were found ?>
				<?php get_template_part('no-results'); ?>
			<?php endif; ?>
		</div>
		<div class="row">
			<div class="align-center load-more">
				<?php 
				if (!empty($url)): ?>
					<a class="button big blue button-load-more" id="load-more" href="<?php echo esc_url($url); ?>" data-loading="<?php _e('Loading posts', 'king'); ?>"><?php _e('Load More', 'king'); ?></a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer();?>
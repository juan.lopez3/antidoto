<?php

/**

 * The Sidebar containing the footer widget areas.

 *

 * @package king

 * @since king 1.0

 */





global $post, $king_cf_error, $king_cf_message, $king_cf_form_name, $king_cf_form_email, $king_cf_form_message;

$hide_footer_widgets = king_get_opt('hide_footer_widgets');
?>

<div class="king-blog-sidebar col-sm-3 min-height-footer">
	<?php if ($hide_footer_widgets != 'yes'): ?>
		<div class="sidebar">
			<?php if (is_active_sidebar( 'footer-2-area-1' )): ?>
				<?php dynamic_sidebar( 'footer-2-area-1' ); ?>
			<?php endif; ?>
		</div>	
	<?php endif; ?>
</div>

<!-- Contact form -->

<?php



$cf_header = '';

$header_style_html = '';

$cf_name_label = '';

$cf_name_icon = '';

$cf_email_label = '';

$cf_email_icon = '';

$cf_message_label = '';

$cf_send_label = '';

if (is_page()) {

	$cf_font_style = get_post_meta($post -> ID, 'footer_cf_font_style', true);

	$cf_font_size = get_post_meta($post -> ID, 'footer_cf_font_size', true);

	$cf_color = get_post_meta($post -> ID, 'footer_cf_color', true);

	$cf_header = get_post_meta($post -> ID, 'footer_cf_header', true);

	$cf_name_label = get_post_meta($post -> ID, 'footer_cf_name_label', true);

	$cf_name_icon = get_post_meta($post -> ID, 'footer_cf_name_icon', true);

	$cf_email_label = get_post_meta($post -> ID, 'footer_cf_email_label', true);

	$cf_email_icon = get_post_meta($post -> ID, 'footer_cf_email_icon', true);

	$cf_message_label = get_post_meta($post -> ID, 'footer_cf_message_label', true);

	$cf_send_label = get_post_meta($post -> ID, 'footer_cf_send_label', true);

	$cf_submit_style = get_post_meta($post -> ID, 'footer_cf_submit_style', true);

	

	$header_style = array();

	if (!empty($cf_font_style) && $cf_font_style != 'default' && $cf_font_style != 'google_web_fonts') {

		$header_style[] = 'font-family: '.str_replace('google_web_font_', '', $cf_font_style);

	}	

	if (!empty($cf_font_size) && intval($cf_font_size) > 0) {

		$header_style[] = 'font-size: '.intval($cf_font_size).'px';

	}

	if (!empty($cf_color)) {

		$header_style[] = 'color: '.$cf_color;

	}
	
}



?>



<div  class="col-lg-6 col-md-6 col-sm-6 footer-contact-form wow animated fadeInUp" data-animation="fadeInUp" data-wow-duration="0.8s">

	<form id="footer-contact-form" _lpchecked="1" method="post" action="<?php echo get_the_permalink(get_the_ID()); ?>#footer-contact-form">

		<?php wp_nonce_field( 'footer-2-cf_mar', 'cf_wpnonce', true, true ); ?>

		<h3 class="cursive-style" <?php echo (count($header_style) > 0 ? 'style="'.implode(';', $header_style).'"' : ''); ?>><?php echo !empty($cf_header) ? $cf_header : __('Contact Form', 'king'); ?></h3>

		<?php if ($king_cf_error || $king_cf_message): ?>

			<div class="<?php echo ($king_cf_error === true ? 'error': 'message')?>"><?php echo wp_kses_post($king_cf_message); ?> </div>

		<?php endif; ?>

		<div class="iconic-input">

			<input type="text" name="form_name" placeholder="<?php echo empty($cf_name_label) ? esc_attr('Name', 'king') : esc_attr($cf_name_label); ?>*" value="<?php echo isset($king_cf_form_name) ? esc_attr($king_cf_form_name) : ''; ?>">

			<i class="icons <?php echo empty($cf_name_icon) || $cf_name_icon == 'default'  ? 'icon-user-1' : esc_attr($cf_name_icon); ?>"></i>

		</div>

		<div class="iconic-input">

			<input type="text" name="form_email" placeholder="<?php echo empty($cf_email_label) ? esc_attr('E-mail', 'king') : esc_attr($cf_email_label); ?>*" value="<?php echo isset($king_cf_form_email) ? esc_attr($king_cf_form_email) : ''; ?>">

			<i class="icons <?php echo empty($cf_email_icon) || $cf_email_icon == 'default' ? 'icon-mail-4' : esc_attr($cf_email_icon); ?>"></i>

		</div>

		<textarea rows="6" name="form_message" placeholder="<?php echo empty($cf_message_label) ? esc_attr('Message', 'king') : esc_attr($cf_message_label); ?>*"><?php echo isset($king_cf_form_message) ? $king_cf_form_message : ''; ?></textarea>

		<input class="unfilled big black <?php echo ($cf_submit_style != 'default' ? $cf_submit_style : ''); ?>" type="submit" value="<?php echo empty($cf_send_label) ? esc_attr('Send Message', 'king') : esc_attr($cf_send_label); ?>">

	</form>

</div>

<!-- /Contact form -->

<div class="king-blog-sidebar col-md-3 min-height-footer">
	<?php if ($hide_footer_widgets != 'yes'): ?>
		<div class="sidebar">
			<?php if (is_active_sidebar( 'footer-2-area-2' )): ?>
				<?php dynamic_sidebar( 'footer-2-area-2' ); ?>
			<?php endif; ?>
		</div>	
	<?php endif; ?>
</div>